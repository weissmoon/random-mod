package weissmoon.random.advancements;

import com.google.gson.JsonObject;
import net.minecraft.advancements.criterion.AbstractCriterionTrigger;
import net.minecraft.advancements.criterion.CriterionInstance;
import net.minecraft.advancements.criterion.EntityPredicate;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.loot.ConditionArrayParser;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 9/10/21.
 */
public class EnvyTrigger extends AbstractCriterionTrigger<EnvyTrigger.Instance>{

    private static final ResourceLocation ID = new ResourceLocation(Reference.MOD_ID, "envy");
    public static final EnvyTrigger INSTANCE = new EnvyTrigger();

    @Override
    protected Instance deserializeTrigger(JsonObject json, EntityPredicate.AndPredicate entityPredicate, ConditionArrayParser conditionsParser){
        return new Instance(entityPredicate);
    }

    @Override
    public ResourceLocation getId(){
        return ID;
    }

    public void trigger(ServerPlayerEntity player){
        this.triggerListeners(player, (isntance -> true));
    }



    static class Instance extends CriterionInstance{

        public Instance(EntityPredicate.AndPredicate playerCondition){
            super(ID, playerCondition);
        }
    }
}
