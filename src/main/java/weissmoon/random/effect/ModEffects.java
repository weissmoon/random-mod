package weissmoon.random.effect;

import net.minecraft.potion.Effect;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 3/22/22.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModEffects{

    public static final Effect RESONANCE = new ResonanceEffect();

    public static void init(){
        MinecraftForge.EVENT_BUS.register(ModEffects.class);
    }

    @SubscribeEvent
    public static void registerEffects(RegistryEvent.Register<Effect> evt) {
        IForgeRegistry<Effect> GameRegistry = evt.getRegistry();

        GameRegistry.register(RESONANCE);
    }
}
