package weissmoon.random.effect;

import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectType;
import net.minecraft.potion.Effects;
import net.minecraft.util.math.AxisAlignedBB;

import java.util.List;

/**
 * Created by Weissmoon on 4/11/22.
 */
public class ResonanceEffect extends Effect{

    protected ResonanceEffect(){
        super(EffectType.NEUTRAL, 0x00c6ff);
        setRegistryName("weissrandom:resonance");
    }

    public boolean isReady(int duration, int amplifier) {
        return true;
    }

    public void performEffect(LivingEntity entityLivingBaseIn, int amplifier) {
        double range = 5 * amplifier;
        AxisAlignedBB box = entityLivingBaseIn.getBoundingBox().expand(range, range, range).expand(-range, -range, -range);
        List<LivingEntity> entities = entityLivingBaseIn.world.getLoadedEntitiesWithinAABB(LivingEntity.class, box);
        entities.forEach(entity -> {
            entity.addPotionEffect(new EffectInstance(Effects.GLOWING, 10));
        });
    }
}
