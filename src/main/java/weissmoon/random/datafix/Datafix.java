package weissmoon.random.datafix;

import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.Event;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * Created by Weissmoon on 12/21/19.
 */
public class Datafix {

    @SubscribeEvent
    public static void onMissingRegistryItems(RegistryEvent.MissingMappings<Item> event){
        event.getMappings();
        System.out.println(event.getName());
        event.setResult(Event.Result.DENY);
    }
}
