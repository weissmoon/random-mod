package weissmoon.random.compat;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import top.theillusivec4.curios.api.CuriosApi;
import top.theillusivec4.curios.api.SlotTypePreset;
import top.theillusivec4.curios.api.type.inventory.ICurioStacksHandler;
import weissmoon.random.item.equipment.ItemQuiverBase;

/**
 * Created by Weissmoon on 12/30/19.
 */
public class CuriosCompat {

    public static void enqueueIMCCurio(InterModEnqueueEvent event){
        InterModComms.sendTo("curios", "register_type", () ->
                SlotTypePreset.RING.getMessageBuilder().build());
        InterModComms.sendTo("curios", "register_type", () ->
            SlotTypePreset.BELT.getMessageBuilder().build());
    }

    public static boolean hasQuiverCurio(PlayerEntity player){
        ImmutableTriple<String, Integer, ItemStack> result =
        CuriosApi.getCuriosHelper().getCuriosHandler(player).map(handler -> {
            for (String id : handler.getCurios().keySet()) {
                ICurioStacksHandler stackHandler = handler.getCurios().get(id);

                if (stackHandler != null)
                    for (int i = 0; i < stackHandler.getSlots(); i++){
                        ItemStack stack = stackHandler.getStacks().getStackInSlot(i);

                        if (!stack.isEmpty() && stack.getItem() instanceof ItemQuiverBase)//filter.test(stack)) {
                            if (ItemQuiverBase.hasArrowStatic(stack, (ItemQuiverBase)stack.getItem()))
                                //return true;
                                return new ImmutableTriple<>(id, i, stack);
                    }
            }
            return new ImmutableTriple<>("", 0, ItemStack.EMPTY);
        }).orElse(new ImmutableTriple<>("", 0, ItemStack.EMPTY));
        return !result.getRight().isEmpty();
    }

    public static ItemStack getFirstQuiver(PlayerEntity player){
        ImmutableTriple<String, Integer, ItemStack> result =
        CuriosApi.getCuriosHelper().getCuriosHandler(player).map(handler -> {
            for(String id : handler.getCurios().keySet()){
                ICurioStacksHandler stackHandler = handler.getCurios().get(id);

                if (stackHandler != null)
                    for (int i = 0; i < stackHandler.getSlots(); i++) {
                        ItemStack stack = stackHandler.getStacks().getStackInSlot(i);

                        if (!stack.isEmpty() && stack.getItem() instanceof ItemQuiverBase)//filter.test(stack)) {
                            if (ItemQuiverBase.hasArrowStatic(stack, (ItemQuiverBase)stack.getItem()))
                                //return stack;
                                return new ImmutableTriple<>(id, i, stack);
                    }
            }
            return new ImmutableTriple<>("", 0, ItemStack.EMPTY);
        }).orElse(new ImmutableTriple<>("", 0, ItemStack.EMPTY));
        return result.getRight();
    }
}
