package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class ShusBreathHUDItem extends AtumBowHUDItem{

    private static final ItemStack slownessArrow = new ItemStack(ModelItems.arrowQuick);

    @Override
    public ItemStack getArrowStack(){
        return slownessArrow;
    }
}
