package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class HorusSoaringHUDItem extends AtumBowHUDItem{

    private static final ItemStack straightArrow = new ItemStack(ModelItems.arrowStrai);

    @Override
    public ItemStack getArrowStack(){
        return straightArrow;
    }
}
