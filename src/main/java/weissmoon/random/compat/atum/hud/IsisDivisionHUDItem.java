package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class IsisDivisionHUDItem extends AtumBowHUDItem{

    private static final ItemStack doubleArrow = new ItemStack(ModelItems.arrowDouble);

    @Override
    public ItemStack getArrowStack(){
        return doubleArrow;
    }
}
