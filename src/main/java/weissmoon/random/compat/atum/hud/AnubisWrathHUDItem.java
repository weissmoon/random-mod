package weissmoon.random.compat.atum.hud;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class AnubisWrathHUDItem implements IHUDItem{

    private final ItemStack soul = new ItemStack(ModelItems.soulOrb);

    @Override
    public boolean isSimple(ItemStack stack){
        return false;
    }

    @Override
    public boolean doRenderHUD(ItemStack stack){
        return true;
    }

    @Override
    public void renderHUD(MatrixStack matrixStack, ItemStack stack){
        int count = NBTHelper.getInt(stack, "souls");
        //TODO level colour
        int colour;
        if(count < 50)
            colour = 16777215;
        else if(count < 150)
            colour = 16733695;
        else if (count < 500)
            colour = 16733525;
        else
            colour = 11141120;
        HUDHelper.renderItemOppositeOffhandHud(matrixStack, soul, String.valueOf(count), colour);
    }
}
