package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.compat.atum.hud.AtumBowHUDItem;
import weissmoon.random.item.model.ModelItems;


/**
 * Created by Weissmoon on 6/2/21.
 */
public class GroundingBowHUDItem extends AtumBowHUDItem{

    private static final ItemStack slownessArrow = new ItemStack(ModelItems.arrowSlow);

    @Override
    public ItemStack getArrowStack(){
        return slownessArrow;
    }
}
