package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class TefnutsRainHUDItem extends AtumBowHUDItem{

    private static final ItemStack rainArrow = new ItemStack(ModelItems.arrowRain);

    @Override
    public ItemStack getArrowStack(){
        return rainArrow;
    }
}
