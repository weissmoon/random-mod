package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class MontusBlastHUDItem extends AtumBowHUDItem{

    private static final ItemStack arrowExplosive = new ItemStack(ModelItems.arrowExplosive);

    @Override
    public ItemStack getArrowStack(){
        return arrowExplosive;
    }
}
