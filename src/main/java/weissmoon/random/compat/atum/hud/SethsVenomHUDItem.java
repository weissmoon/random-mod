package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class SethsVenomHUDItem extends AtumBowHUDItem{

    private static final ItemStack poisonArrow = new ItemStack(ModelItems.arrowPoison);

    @Override
    public ItemStack getArrowStack(){
        return poisonArrow;
    }
}
