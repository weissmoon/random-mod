package weissmoon.random.compat.atum.hud;

import net.minecraft.item.ItemStack;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 6/4/21.
 */
public class RasFuryHUDItem extends AtumBowHUDItem{

    private static final ItemStack arrowFire = new ItemStack(ModelItems.arrowFire);

    @Override
    public ItemStack getArrowStack(){
        return arrowFire;
    }
}
