package weissmoon.random.compat.atum.hud;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.random.Randommod;
import weissmoon.random.compat.BaublesCompat;
import weissmoon.random.compat.CuriosCompat;
import weissmoon.random.item.equipment.ItemQuiverBase;

import static weissmoon.random.item.equipment.ItemQuiverBase.hasArrowStatic;
import static weissmoon.random.item.equipment.ItemQuiverBase.isQuiver;

/**
 * Created by Weissmoon on 6/4/21.
 */
public abstract class AtumBowHUDItem implements IHUDItem{

    @Override
    public boolean isSimple(ItemStack stack){
        return false;
    }

    @Override
    public boolean doRenderHUD(ItemStack stack){
        PlayerEntity player = Minecraft.getInstance().player;
        for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
            ItemStack itemstack = player.inventory.getStackInSlot(i);

            if(itemstack.getItem() instanceof ArrowItem)
                return true;

            if(itemstack.getItem() instanceof ItemQuiverBase)
                if (hasArrowStatic(itemstack, (ItemQuiverBase)itemstack.getItem()))
                    return true;
        }
        if(Randommod.baublesLoaded)
            if (BaublesCompat.hasQuiverBauble(player)) {
                ItemStack quiverBauble = CuriosCompat.getFirstQuiver(player);
                return hasArrowStatic(quiverBauble, (ItemQuiverBase) quiverBauble.getItem());
            }
        if(Randommod.curiosLoaded)
            if (CuriosCompat.hasQuiverCurio(player)) {
                ItemStack quiverCurio = CuriosCompat.getFirstQuiver(player);
                return hasArrowStatic(quiverCurio, (ItemQuiverBase) quiverCurio.getItem());
            }
        return false;
    }

    @Override
    public void renderHUD(MatrixStack matrixStack, ItemStack hudItemStack) {
        PlayerEntity player = Minecraft.getInstance().player;
        int count = 0;

        {
            ItemStack offHand = player.getHeldItem(Hand.OFF_HAND);
            if(isQuiver(player.getHeldItem(Hand.OFF_HAND))){
                if (hasArrowStatic(offHand, (ItemQuiverBase) offHand.getItem())) {
                    ItemStack quiver = ((ItemQuiverBase) offHand.getItem()).getActiveArrow(offHand);
                    count += quiver.getCount();
                }
            }else if(offHand.getItem() instanceof ArrowItem){
                count += offHand.getCount();
            }
            if (Randommod.baublesLoaded) {
                if (BaublesCompat.hasQuiverBauble(player)) {
                    ItemStack quiverBauble = BaublesCompat.getFirstQuiver(player);
                    if (hasArrowStatic(quiverBauble, (ItemQuiverBase) quiverBauble.getItem())) {
                        ItemStack quiverArrow = ((ItemQuiverBase) quiverBauble.getItem()).getActiveArrow(quiverBauble);
                        count += quiverArrow.getCount();
                    }
                }
            }
            if (Randommod.curiosLoaded) {
                if (CuriosCompat.hasQuiverCurio(player)) {
                    ItemStack quiverCurio = CuriosCompat.getFirstQuiver(player);
                    if (hasArrowStatic(quiverCurio, (ItemQuiverBase) quiverCurio.getItem())) {
                        ItemStack quiverArrow = ((ItemQuiverBase) quiverCurio.getItem()).getActiveArrow(quiverCurio);
                        count += quiverArrow.getCount();
                    }
                }
            }
        }
        for (int i = 0; i < player.inventory.getSizeInventory(); ++i) {
            ItemStack itemstack = player.inventory.getStackInSlot(i);

            if(isQuiver(itemstack))
                if (hasArrowStatic(itemstack, (ItemQuiverBase) itemstack.getItem())) {
                    ItemStack quiverArrow = ((ItemQuiverBase)itemstack.getItem()).getActiveArrow(itemstack);
                    count += quiverArrow.getCount();
                }
            if(itemstack.getItem() instanceof ArrowItem)
                count += itemstack.getCount();
        }
        ItemStack arrowStack = getArrowStack();
        HUDHelper.renderItemOppositeOffhandHud(matrixStack, arrowStack, String.valueOf(count), 16777215);
    }

    public abstract ItemStack getArrowStack();
}
