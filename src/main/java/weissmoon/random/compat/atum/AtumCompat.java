package weissmoon.random.compat.atum;

import com.teammetallurgy.atum.api.AtumMats;
import com.teammetallurgy.atum.init.AtumItems;
import net.minecraft.inventory.EquipmentSlotType;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.random.client.hud.BowHUDItem;
import weissmoon.random.compat.atum.hud.*;
import weissmoon.random.item.equipment.armour.ItemDesertArmour;
import weissmoon.random.item.equipment.tools.daggers.ItemDagger;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 6/2/21.
 */
public class AtumCompat{

    public static final ItemDagger limestoneDagger = new ItemDagger(AtumMats.LIMESTONE, -2, "limestone");
    public static final ItemDesertArmour gildedDesertHelmet = new ItemDesertArmour(Strings.Items.GILDED_DESERT_HELMET_NAME, EquipmentSlotType.HEAD);
    public static final ItemDesertArmour gildedDesertChestpiece = new ItemDesertArmour(Strings.Items.GILDED_DESERT_CHESTPIECE_NAME, EquipmentSlotType.CHEST);
    public static final ItemDesertArmour gildedDesertLegging = new ItemDesertArmour(Strings.Items.GILDED_DESERT_LEGGING_NAME, EquipmentSlotType.LEGS);
    public static final ItemDesertArmour gildedDesertFeet = new ItemDesertArmour(Strings.Items.GILDED_DESERT_FEET_NAME, EquipmentSlotType.FEET);

    public static void registerIHUDItems(){
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.ANPUTS_GROUNDING, new GroundingBowHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.ANUBIS_WRATH, new AnubisWrathHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.HORUS_SOARING, new HorusSoaringHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.ISIS_DIVISION, new IsisDivisionHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.MONTUS_BLAST, new MontusBlastHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.RAS_FURY, new RasFuryHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.SETHS_VENOM, new SethsVenomHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.SHUS_BREATH, new ShusBreathHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.TEFNUTS_RAIN, new TefnutsRainHUDItem());
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AtumItems.SHORT_BOW, BowHUDItem.INSTANCE);
    }
}
