package weissmoon.random.compat.aquaculture;

import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;
import com.teammetallurgy.aquaculture.api.fishing.Hook;
import com.teammetallurgy.aquaculture.api.fishing.Hooks;
import com.teammetallurgy.aquaculture.init.AquaItems;
import com.teammetallurgy.aquaculture.item.AquaFishingRodItem;
import com.teammetallurgy.aquaculture.item.BaitItem;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.random.client.hud.BowHUDItem;

import java.util.Map;

/**
 * Created by Weissmoon on 5/31/21.
 */
public class AquacultureCompatClient{

    private static final Map<Hook, ItemStack> hookMap = Maps.newHashMap();

    public static void registerIHUDItems(){
        IHUDItem fishingHud = new IHUDItem() {
            @Override
            public boolean isSimple(ItemStack stack){
                return false;
            }

            @Override
            public boolean doRenderHUD(ItemStack stack){
                return true;
            }

            @Override
            public void renderHUD(MatrixStack matrixStack, ItemStack hudItemStack) {
                if(hudItemStack.getItem() instanceof AquaFishingRodItem){
                    ItemStack baitStack = AquaFishingRodItem.getBait(hudItemStack);
                    Hook hook = AquaFishingRodItem.getHookType(hudItemStack);
                    if(!baitStack.isEmpty() && baitStack.getItem() instanceof BaitItem && hook != Hooks.EMPTY){
                        int durability;
                        int colour;
                        if(baitStack.getItem().isDamageable()){
                            durability = baitStack.getMaxDamage() - baitStack.getDamage();
                            colour = AquacultureCompat.gildedFishingRod.getRGBDurabilityForDisplay(baitStack);
                        }else{
                            durability = 0;
                            colour = 16777215;
                        }
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, baitStack, String.valueOf(durability), colour);
                    }else if(!baitStack.isEmpty() && baitStack.getItem() instanceof BaitItem && hook == Hooks.EMPTY){
                        int durability;
                        int colour;
                        if(baitStack.getItem().isDamageable()){
                            durability = baitStack.getMaxDamage() - baitStack.getDamage();
                            colour = AquacultureCompat.gildedFishingRod.getRGBDurabilityForDisplay(baitStack);
                        }else{
                            durability = 0;
                            colour = 16777215;
                        }
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, baitStack, String.valueOf(durability), colour);
                    }else if(hook != Hooks.EMPTY){
                        if(hookMap.get(hook) == null)
                            hookMap.put(hook, new ItemStack(hook.getItem()));
                        HUDHelper.renderItemOppositeOffhandHudNoString(matrixStack, hookMap.get(hook));
                    }
                }
            }

        };
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquacultureCompat.gildedFishingRod, fishingHud);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquaItems.IRON_FISHING_ROD, fishingHud);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquaItems.GOLD_FISHING_ROD, fishingHud);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquaItems.DIAMOND_FISHING_ROD, fishingHud);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquaItems.NEPTUNIUM_FISHING_ROD, fishingHud);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(AquaItems.NEPTUNIUM_BOW, BowHUDItem.INSTANCE);
    }
}
