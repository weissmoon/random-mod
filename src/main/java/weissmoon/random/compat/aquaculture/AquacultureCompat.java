package weissmoon.random.compat.aquaculture;

import com.teammetallurgy.aquaculture.Aquaculture;
import com.teammetallurgy.aquaculture.api.AquacultureAPI;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import weissmoon.random.item.equipment.aqua.ItemGildedFishFilletKnife;
import weissmoon.random.item.equipment.aqua.ItemGildedFishingRod;
import weissmoon.random.item.equipment.tools.daggers.ItemDagger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 1/5/20.
 */
public class AquacultureCompat {

    public static final ItemDagger neptuniumDagger = new ItemDagger(AquacultureAPI.MATS.NEPTUNIUM, -12, "neptunium"){
        @Override
        public java.util.Collection<ItemGroup> getCreativeTabs()
        {
            List<ItemGroup> list = new ArrayList<>();
            list.add(Aquaculture.GROUP);
            list.add(ItemGroup.COMBAT);
            return list;
        }
    };
    public static final Item gildedFilletKnife = new ItemGildedFishFilletKnife();
    public static final Item gildedFishingRod = new ItemGildedFishingRod();

    //public static final Hook gildedHook = new Hook.HookBuilder("gilded").setModID(Reference.MOD_ID).setDurabilityChance(0.07).build();
}
