package weissmoon.random.compat.aquaculture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tags.FluidTags;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import weissmoon.random.Randommod;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 1/25/20.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID)
public class AquacultureEvent {

    @SubscribeEvent
    public static void onAttack(LivingHurtEvent event) {
        if(Randommod.aquacultureLoaded) {
            Entity source = event.getSource().getTrueSource();
            if (source instanceof LivingEntity) {
                LivingEntity living = (LivingEntity) source;
                if (living.areEyesInFluid(FluidTags.WATER)) {
                    ItemStack heldStack = living.getHeldItemMainhand();
                    if (heldStack.getItem() == AquacultureCompat.neptuniumDagger) {
                        event.setAmount(event.getAmount() * 1.5F);
                    }
                }
            }
        }
    }
}
