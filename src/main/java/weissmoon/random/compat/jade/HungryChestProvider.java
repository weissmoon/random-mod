package weissmoon.random.compat.jade;

import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import snownee.jade.Renderables;
import weissmoon.random.block.tile.TileHungryChest;
import weissmoon.random.item.model.ModelItems;

import java.util.List;

/**
 * Created by Weissmoon on 4/22/22.
 */
public class HungryChestProvider implements IComponentProvider{

    public void appendBody(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileHungryChest){
            TileHungryChest chest = (TileHungryChest)tileEntity;
            if(chest.isLocked()){
                tooltip.add(Renderables.item(new ItemStack(ModelItems.lock)));
            }
        }
    }
}
