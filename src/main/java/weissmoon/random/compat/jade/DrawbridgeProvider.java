package weissmoon.random.compat.jade;

import mcp.mobius.waila.Waila;
import mcp.mobius.waila.api.*;
import mcp.mobius.waila.utils.ModIdentification;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import weissmoon.random.block.tile.TileDrawbrigde;

import java.util.List;

import static mcp.mobius.waila.addons.core.HUDHandlerBlocks.MOD_NAME_TAG;
import static mcp.mobius.waila.addons.core.HUDHandlerBlocks.OBJECT_NAME_TAG;

/**
 * Created by Weissmoon on 4/22/22.
 */
public class DrawbridgeProvider implements IComponentProvider{

    public ItemStack getStack(IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileDrawbrigde){
            TileDrawbrigde drawbrigde = (TileDrawbrigde)tileEntity;
            return drawbrigde.getCamoflageBlock();
        }
        return ItemStack.EMPTY;
    }

    public void appendHead(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileDrawbrigde){
            TileDrawbrigde drawbrigde = (TileDrawbrigde)tileEntity;
            if(drawbrigde.getCamoflageBlock() != ItemStack.EMPTY)
                ((ITaggableList)tooltip).setTag(OBJECT_NAME_TAG, new StringTextComponent(String.format((Waila.CONFIG.get()).getFormatting().getBlockName(), drawbrigde.getCamoflageBlock().getDisplayName().getString())));
        }
    }

    public void appendBody(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {

    }

    public void appendTail(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileDrawbrigde){
            TileDrawbrigde drawbrigde = (TileDrawbrigde)tileEntity;
            String name = ModIdentification.getModName(drawbrigde.getCamoflageBlock().getItem().getCreatorModId(drawbrigde.getCamoflageBlock()));
            name = String.format((Waila.CONFIG.get()).getFormatting().getModName(), name);
            ((ITaggableList)tooltip).setTag(MOD_NAME_TAG, new StringTextComponent(name));
        }
    }
}
