package weissmoon.random.compat.jade;

import mcp.mobius.waila.Waila;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import net.minecraft.block.JukeboxBlock;
import net.minecraft.item.Item;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.override.tile.TileJukebox;

import java.util.List;

/**
 * Created by Weissmoon on 4/23/22.
 */
public class JukeboxProvider implements IComponentProvider{

    static final ResourceLocation CONFIG_JUKEBOX = new ResourceLocation("jukebox");

    public void appendBody(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        if (config.get(CONFIG_JUKEBOX) && accessor.getBlock() == ModBlocks.jukebox) {
            if (accessor.getBlockState().get(JukeboxBlock.HAS_RECORD)){
                TileJukebox jukebox = (TileJukebox)accessor.getTileEntity();

                try {
                    Item item = jukebox.getRecord().getItem();
                    if (item instanceof MusicDiscItem) {
                        tooltip.add(new TranslationTextComponent("record.nowPlaying", ((MusicDiscItem)item).getDescription()));
                    }
                } catch (Exception var7) {
                    Waila.LOGGER.catching(var7);
                }
            } else {
                tooltip.add(new TranslationTextComponent("tooltip.waila.empty"));
            }
        }
    }
}
