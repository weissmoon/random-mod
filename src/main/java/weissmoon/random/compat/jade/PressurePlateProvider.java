package weissmoon.random.compat.jade;

import mcp.mobius.waila.Waila;
import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import mcp.mobius.waila.api.ITaggableList;
import mcp.mobius.waila.api.event.WailaRenderEvent;
import mcp.mobius.waila.utils.ModIdentification;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.block.plates.WeissRandomBasePlate;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateBase;

import java.util.List;

import static mcp.mobius.waila.addons.core.HUDHandlerBlocks.MOD_NAME_TAG;
import static mcp.mobius.waila.addons.core.HUDHandlerBlocks.OBJECT_NAME_TAG;

/**
 * Created by Weissmoon on 4/22/22.
 */
//@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class PressurePlateProvider implements IComponentProvider{

    public static final PressurePlateProvider INSTANCE = new PressurePlateProvider();

    static{
        MinecraftForge.EVENT_BUS.register(INSTANCE);
    }

    @SubscribeEvent
    public void preRender(WailaRenderEvent.Pre event){
        Block bLock = event.getAccessor().getBlock();
        if(bLock instanceof weissmoon.random.block.plates.WeissRandomBasePlate){
            if(((WeissRandomBasePlate)bLock).isInvisible())
                event.setCanceled(true);
        }
    }

    public void appendHead(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileWoodenPressurePlateBase){
            TileWoodenPressurePlateBase plate = (TileWoodenPressurePlateBase)tileEntity;
            if(plate.getPlateBlockStack() != ItemStack.EMPTY){
                String name = plate.getPlateBlockStack().getDisplayName().getString();
                name = String.format((Waila.CONFIG.get()).getFormatting().getBlockName(), new TranslationTextComponent(accessor.getBlock().getTranslationKey(), name).getString());
                ((ITaggableList)tooltip).setTag(OBJECT_NAME_TAG, new StringTextComponent(String.format((Waila.CONFIG.get()).getFormatting().getBlockName(), name)));
            }
        }
    }

    public void appendTail(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {

        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileWoodenPressurePlateBase){
            TileWoodenPressurePlateBase plate = (TileWoodenPressurePlateBase)tileEntity;
            String name = ModIdentification.getModName(plate.getPlateBlockStack().getItem().getCreatorModId(plate.getPlateBlockStack()));
            name = String.format((Waila.CONFIG.get()).getFormatting().getModName(), name);
            ((ITaggableList)tooltip).setTag(MOD_NAME_TAG, new StringTextComponent(name));
        }
    }
}
