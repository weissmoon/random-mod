package weissmoon.random.compat.jade;

import mcp.mobius.waila.api.IRegistrar;
import mcp.mobius.waila.api.IWailaPlugin;
import mcp.mobius.waila.api.TooltipPosition;
import mcp.mobius.waila.api.WailaPlugin;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 4/22/22.
 */
@WailaPlugin
public class JadeCompat implements IWailaPlugin{

    @Override
    public void register(IRegistrar registrar){
        DrawbridgeProvider drawbridgeProvider = new DrawbridgeProvider();
        registrar.registerStackProvider(drawbridgeProvider, ModBlocks.drawbridge.getClass());
        registrar.registerComponentProvider(drawbridgeProvider, TooltipPosition.HEAD, ModBlocks.drawbridge.getClass());
        registrar.registerComponentProvider(drawbridgeProvider, TooltipPosition.TAIL, ModBlocks.drawbridge.getClass());
        registrar.registerComponentProvider(PressurePlateProvider.INSTANCE, TooltipPosition.HEAD, ModBlocks.woodenPressurePlateS.getClass());
        registrar.registerComponentProvider(PressurePlateProvider.INSTANCE, TooltipPosition.TAIL, ModBlocks.woodenPressurePlateS.getClass());
        registrar.registerComponentProvider(new HungryChestProvider(), TooltipPosition.BODY, ModBlocks.hungryChest.getClass());
        registrar.registerComponentProvider(new BarrelProvider(), TooltipPosition.BODY, ModBlocks.barrelBlock.getClass());
        registrar.registerComponentProvider(new JukeboxProvider(), TooltipPosition.BODY, ModBlocks.jukebox.getClass());
    }

}
