package weissmoon.random.compat.jade;

import mcp.mobius.waila.api.IComponentProvider;
import mcp.mobius.waila.api.IDataAccessor;
import mcp.mobius.waila.api.IPluginConfig;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import snownee.jade.Renderables;
import weissmoon.random.block.tile.TileBarrel;

import java.util.List;

/**
 * Created by Weissmoon on 4/23/22.
 */
public class BarrelProvider implements IComponentProvider{

    public void appendBody(List<ITextComponent> tooltip, IDataAccessor accessor, IPluginConfig config) {
        TileEntity tileEntity = accessor.getTileEntity();
        if(tileEntity instanceof TileBarrel){
            TileBarrel barrel = (TileBarrel)tileEntity;
            int timer = barrel.getTimer();
            if(Minecraft.getInstance().player.isSneaking())
                if(timer > 0)
                    tooltip.add(Renderables.of(Renderables.item(new ItemStack(Items.CLOCK), 0.75F, 0), Renderables.offsetText(new TranslationTextComponent("jade.brewingStand.time", timer / 20), 0, 3)));
        }
    }
}
