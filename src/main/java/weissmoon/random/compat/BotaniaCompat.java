package weissmoon.random.compat;

import vazkii.botania.api.BotaniaAPI;
import weissmoon.random.item.equipment.tools.daggers.ItemDaggerElementium;
import weissmoon.random.item.equipment.tools.daggers.ItemDaggerMana;

/**
 * Created by Weissmoon on 10/28/19.
 */
public class BotaniaCompat {

    public static final ItemDaggerMana manasteelDagger = new ItemDaggerMana(BotaniaAPI.instance().getManasteelItemTier(), 60, -2, "manasteel");
    public static final ItemDaggerMana elementiumDagger = new ItemDaggerElementium(BotaniaAPI.instance().getElementiumItemTier(), 60, -2,  "b_elementium");
    public static final ItemDaggerMana terrasteelDagger = new ItemDaggerMana(BotaniaAPI.instance().getTerrasteelItemTier(), 100, -2,  "terrasteel");
}
