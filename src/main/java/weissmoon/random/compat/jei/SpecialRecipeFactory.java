package weissmoon.random.compat.jei;

import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.item.IPlankHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 2/1/22.
 */
public class SpecialRecipeFactory{


    public static List<IShapedRecipe<?>> createProjectTableRecipe(){

        List<IShapedRecipe<?>> recipes = new ArrayList();
        String group = "weissrandom.wooden.dagger";

        ResourceLocation chestResource = new ResourceLocation("forge:chests/wooden");
        ITag<Item> chestsTag = ItemTags.getCollection().get(chestResource);
        ResourceLocation plankResourcee = new ResourceLocation("minecraft", "planks");
        ITag<Item> plankTag = ItemTags.getCollection().get(plankResourcee);


        ItemStack plank = new ItemStack(Blocks.CRAFTING_TABLE);
        Ingredient tableIngredient = Ingredient.fromStacks(plank);
        Ingredient plankIngredient = Ingredient.fromTag(plankTag);
        Ingredient chestIngredient = Ingredient.fromTag(chestsTag);
        NonNullList<Ingredient> inputs = NonNullList.from(Ingredient.EMPTY, tableIngredient, plankIngredient, chestIngredient);
        ItemStack output = new ItemStack(ModBlocks.projectTable);
        if (output.getItem() instanceof IPlankHolder) {
            ((IPlankHolder) output.getItem()).setPlank(output, new ItemStack(Blocks.OAK_PLANKS).setDisplayName(new StringTextComponent(plankResourcee.toString())));
        }
        ResourceLocation id = new ResourceLocation("weissrandom", "projecttable");
        ShapedRecipe recipe = new ShapedRecipe(id, group, 1, 3, inputs, output);
        recipes.add(recipe);

        return recipes;
    }
}
