package weissmoon.random.compat.jei;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import it.unimi.dsi.fastutil.ints.Int2ObjectArrayMap;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.gui.ingredient.ITooltipCallback;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.recipe.RecipeBarrel;

/**
 * Created by Weissmoon on 5/18/21.
 */
public class BarrelRecipeCategory implements IRecipeCategory<RecipeBarrel>{

    public static final ResourceLocation UID = new ResourceLocation("weissmoon:recipe_barrel");
    private static final TranslationTextComponent catalystTooltip = (TranslationTextComponent)new TranslationTextComponent("tooltip.weissrandom.jei.catalystIngredient").mergeStyle(TextFormatting.GREEN);
    public static final ITooltipCallback catalystToolTip = (ITooltipCallback<ItemStack>)(slotIndex, input, ingredient, tooltip) -> {
        if(slotIndex == 2){
            tooltip.add(catalystTooltip);
        }
    };
    private static final Int2ObjectArrayMap<IDrawable> arrows = new Int2ObjectArrayMap<>(3);

    public final String title;
    private final IDrawable background;
    private final IDrawable icon;
    private final IGuiHelper guiHelper;
//    private final IDrawable arrow;

    BarrelRecipeCategory(IGuiHelper guiHelper){
        super();
        this.title = I18n.format("weissrandom.jei.barrelCrafting");
        this.background = guiHelper.createBlankDrawable(96, 34);
        this.icon = guiHelper.createDrawableIngredient(new ItemStack(ModBlocks.barrelBlock));
        this.guiHelper = guiHelper;
        arrows.put(500, guiHelper.drawableBuilder(new ResourceLocation("jei", "textures/gui/gui_vanilla.png"), 82, 128, 24, 17).buildAnimated(200, IDrawableAnimated.StartDirection.LEFT, false));
        arrows.put(0, guiHelper.drawableBuilder(new ResourceLocation("jei", "textures/gui/gui_vanilla.png"), 82, 128, 24, 17).build());
    }

    @Override
    public ResourceLocation getUid(){
        return UID;
    }

    @Override
    public Class<? extends RecipeBarrel> getRecipeClass(){
        return RecipeBarrel.class;
    }

    @Override
    public String getTitle(){
        return title;
    }

    @Override
    public IDrawable getBackground(){
        return background;
    }

    @Override
    public IDrawable getIcon(){
        return icon;
    }

    @Override
    public void setIngredients(RecipeBarrel recipeBarrel, IIngredients iIngredients){
        ItemStack[] array = new ItemStack[]{recipeBarrel.ingredient1, recipeBarrel.ingredient2};
        iIngredients.setInputs(VanillaTypes.ITEM, Lists.newArrayList(array));
        iIngredients.setOutput(VanillaTypes.ITEM, recipeBarrel.output);
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, RecipeBarrel recipeBarrel, IIngredients iIngredients){
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, false, 70, 8);
        guiItemStacks.init(1, true, 8, 8);
        guiItemStacks.init(2, true, 26, 8);
        guiItemStacks.set(0, iIngredients.getOutputs(VanillaTypes.ITEM).get(0));
        guiItemStacks.set(1, iIngredients.getInputs(VanillaTypes.ITEM).get(0));
        guiItemStacks.set(2, iIngredients.getInputs(VanillaTypes.ITEM).get(1));
        if(recipeBarrel.isCatalyst)
            guiItemStacks.addTooltipCallback(catalystToolTip);
    }

    @Override
    public void draw(RecipeBarrel recipe, MatrixStack matrixStack, double mouseX, double mouseY) {
        IDrawable arrow = this.getArrow(recipe);
        arrow.draw(matrixStack, 44, 10);
    }

    protected IDrawable getArrow(RecipeBarrel recipe) {
        int time = recipe.time;
        if(!arrows.containsKey(time))
            arrows.put(time, guiHelper.drawableBuilder(new ResourceLocation("jei", "textures/gui/gui_vanilla.png"), 82, 128, 24, 17).buildAnimated((int)(time/2.5), IDrawableAnimated.StartDirection.LEFT, false));
        return arrows.get(time);
    }
}
