package weissmoon.random.compat.jei;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.random.block.ModBlocks;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 1/30/22.
 */
public class WoodInvisibletoSilentInvisiblePlateRecipeFactory{

    public static List<IShapedRecipe<?>> createWoodSilentRecipes(){

        List<IShapedRecipe<?>> recipes = new ArrayList();
        String group = "weissrandom.wooden.plate.silent";

        ResourceLocation woolResource = new ResourceLocation("minecraft", "wool");
        ITag<Item> wooltag = ItemTags.getCollection().get(woolResource);
        ResourceLocation stickResource = new ResourceLocation("minecraft:wooden_pressure_plates");
        ITag<Item> plateTag = ItemTags.getCollection().get(stickResource);

        List<Item> plates = plateTag.getAllElements();

        for(Item item:plates){
            ItemStack plate = new ItemStack(item);
            ItemStack input = new ItemStack(ModBlocks.woodenPressurePlateI);
            if (input.getItem() instanceof IItemHolderItem) {
                IItemHolderItem.setHolderItem(input, plate);
            }
            Ingredient plateIngredient = Ingredient.fromStacks(input);
            Ingredient woolIngredient = Ingredient.fromTag(wooltag);
            NonNullList<Ingredient> inputs = NonNullList.from(Ingredient.EMPTY, plateIngredient, woolIngredient);
            ItemStack output = new ItemStack(ModBlocks.woodenPressurePlateSI);
            if (output.getItem() instanceof IItemHolderItem) {
                IItemHolderItem.setHolderItem(output, plate);
            }
            ResourceLocation id = new ResourceLocation("weissrandom", "wooden.plate.silent." + item.getTranslationKey());
            ShapedRecipe recipe = new ShapedRecipe(id, group, 1, 2, inputs, output);
            recipes.add(recipe);
        }
        return recipes;
    }
}
