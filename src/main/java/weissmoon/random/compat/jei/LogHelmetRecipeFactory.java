package weissmoon.random.compat.jei;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ModItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 1/30/22.
 */
public class LogHelmetRecipeFactory{

    public static List<IShapedRecipe<?>> createLogHelmet(){

        List<IShapedRecipe<?>> recipes = new ArrayList();
        String group = "weissrandom.wooden.helmet";

        ResourceLocation logResource = new ResourceLocation("minecraft:logs");
        ITag<Item> logTag = ItemTags.getCollection().get(logResource);

        List<Item> logs = logTag.getAllElements();

        for(Item item:logs){
            ItemStack log = new ItemStack(item);
            Ingredient logIngredient = Ingredient.fromStacks(log);
            NonNullList<Ingredient> inputs = NonNullList.from(Ingredient.EMPTY, logIngredient, logIngredient, logIngredient, logIngredient, Ingredient.EMPTY, logIngredient);
            ItemStack output = new ItemStack(ModItems.woodHelmet);
            if (output.getItem() instanceof IPlankHolder) {
                IPlankHolder.setPlankS(output, log);
            }
            ResourceLocation id = new ResourceLocation("weissrandom", "wooden.helmet." + item.getTranslationKey());
            ShapedRecipe recipe = new ShapedRecipe(id, group, 3, 2, inputs, output);
            recipes.add(recipe);
        }
        return recipes;
    }
}
