package weissmoon.random.compat.jei;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.item.model.ModelItems;
import weissmoon.random.recipe.ItemOnBlockUsage;

/**
 * Created by Weissmoon on 4/5/22.
 */
public class IOBFactory implements IRecipeCategory<ItemOnBlockUsage>{

    public static final ResourceLocation UID = new ResourceLocation("weissmoon:recipe_iob");

    public final String title;
    private final IDrawable background;
    private final IDrawable icon;
    private final IGuiHelper guiHelper;
    private final IDrawable sneak;
    private final IDrawable mouseLeft;
    private final IDrawable mouseRight;

    IOBFactory(IGuiHelper guiHelper){
        super();
        this.title = I18n.format("weissrandom.jei.iob");
        this.background = guiHelper.createBlankDrawable(80, 44);
        this.icon = guiHelper.createDrawableIngredient(new ItemStack(ModelItems.mouse));
        sneak = guiHelper.createDrawableIngredient(new ItemStack(ModelItems.sneak));
        mouseLeft = guiHelper.createDrawableIngredient(new ItemStack(ModelItems.mouseLeft));
        mouseRight = guiHelper.createDrawableIngredient(new ItemStack(ModelItems.mouseRight));
        this.guiHelper = guiHelper;
    }


    @Override
    public ResourceLocation getUid(){
        return UID;
    }

    @Override
    public Class<? extends ItemOnBlockUsage> getRecipeClass(){
        return ItemOnBlockUsage.class;
    }

    @Override
    public String getTitle(){
        return title;
    }

    @Override
    public IDrawable getBackground(){
        return background;
    }

    @Override
    public IDrawable getIcon(){
        return icon;
    }

    @Override
    public void setIngredients(ItemOnBlockUsage recipe, IIngredients ingredients){
        ItemStack[] array;
        if(recipe.item.isEmpty())
            array = new ItemStack[]{recipe.block};
        else
            array = new ItemStack[]{recipe.block, recipe.item};
        ingredients.setInputs(VanillaTypes.ITEM, Lists.newArrayList(array));
//        ingredients.setOutput(VanillaTypes.ITEM, recipe.output);
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, ItemOnBlockUsage recipe, IIngredients ingredients){
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, true, 16,  24);
        guiItemStacks.set(0, ingredients.getInputs(VanillaTypes.ITEM).get(0));
        guiItemStacks.init(1, true, 8, 4);
        if(!recipe.item.isEmpty())
            guiItemStacks.set(1, ingredients.getInputs(VanillaTypes.ITEM).get(1));
    }

    @Override
    public void draw(ItemOnBlockUsage recipe, MatrixStack matrixStack, double mouseX, double mouseY) {
//        arrow.draw(matrixStack, 28, 10);
        IDrawable output;
        IDrawable result;
        output = guiHelper.createDrawableIngredient(recipe.output);
        result = guiHelper.createDrawableIngredient(recipe.result);
        output.draw(matrixStack, 54, 25);
        result.draw(matrixStack, 54, 5);
        if(recipe.right)
            mouseRight.draw(matrixStack, 24, 5);
        else
            mouseLeft.draw(matrixStack, 24, 5);
        if(recipe.sneak)
            sneak.draw(matrixStack, 0, 25);
    }
}
