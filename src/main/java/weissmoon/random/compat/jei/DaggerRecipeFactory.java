package weissmoon.random.compat.jei;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.tags.ITag;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ModItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 1/28/22.
 */
public class DaggerRecipeFactory{

    public static List<IShapedRecipe<?>> createDaggerRecipes(){

        List<IShapedRecipe<?>> recipes = new ArrayList();
        String group = "weissrandom.wooden.dagger";

        ResourceLocation stickResource = new ResourceLocation("forge:rods/wooden");
        ITag<Item> stickTag = ItemTags.getCollection().get(stickResource);
        ResourceLocation plankResourcee = new ResourceLocation("minecraft", "planks");
        ITag<Item> plankTag = ItemTags.getCollection().get(plankResourcee);

        List<Item> planks = plankTag.getAllElements();

        for(Item item:planks){
            ItemStack plank = new ItemStack(item);
            Ingredient plankIngredient = Ingredient.fromStacks(plank);
            Ingredient stickIngredient = Ingredient.fromTag(stickTag);
            NonNullList<Ingredient> inputs = NonNullList.from(Ingredient.EMPTY, plankIngredient, stickIngredient);
            ItemStack output = new ItemStack(ModItems.woodDagger);
            if (output.getItem() instanceof IPlankHolder) {
                ((IPlankHolder) output.getItem()).setPlank(output, plank);
            }
            ResourceLocation id = new ResourceLocation("weissrandom", "wooden.dagger." + item.getTranslationKey());
            ShapedRecipe recipe = new ShapedRecipe(id, group, 1, 2, inputs, output);
            recipes.add(recipe);
        }
        return recipes;
    }
}
