package weissmoon.random.compat.jei;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.ingredient.IGuiItemStackGroup;
import mezz.jei.api.gui.ingredient.ITooltipCallback;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import weissmoon.random.recipe.RecipeCrumbs;

import java.util.List;

/**
 * Created by Weissmoon on 3/10/22.
 */
public class CrumbsRecipeCategory implements IRecipeCategory<RecipeCrumbs>{

    public static final ResourceLocation UID = new ResourceLocation("weissmoon:recipe_crumbs");

    public final String title;
    private final IDrawable background;
    private final IDrawable icon;
    private final IGuiHelper guiHelper;
    private final IDrawable arrow;


    CrumbsRecipeCategory(IGuiHelper guiHelper){
        super();
        this.title = I18n.format("weissrandom.jei.crumbs");
        this.background = guiHelper.createBlankDrawable(80, 34);
        this.icon = guiHelper.createDrawableIngredient(new ItemStack(Items.MELON_SEEDS));
        this.guiHelper = guiHelper;
        arrow = guiHelper.drawableBuilder(new ResourceLocation("jei", "textures/gui/gui_vanilla.png"), 60, 79, 24, 17).build();
    }

    @Override
    public ResourceLocation getUid(){
        return UID;
    }

    @Override
    public Class<? extends RecipeCrumbs> getRecipeClass(){
        return RecipeCrumbs.class;
    }

    @Override
    public String getTitle(){
        return title;
    }

    @Override
    public IDrawable getBackground(){
        return background;
    }

    @Override
    public IDrawable getIcon(){
        return icon;
    }

    @Override
    public void setIngredients(RecipeCrumbs recipe, IIngredients ingredients){
        ItemStack[] array = new ItemStack[]{recipe.ingredient1};
        ingredients.setInputs(VanillaTypes.ITEM, Lists.newArrayList(array));
        ingredients.setOutput(VanillaTypes.ITEM, recipe.output);
    }

    @Override
    public void setRecipe(IRecipeLayout recipeLayout, RecipeCrumbs recipe, IIngredients ingredients){
        IGuiItemStackGroup guiItemStacks = recipeLayout.getItemStacks();
        guiItemStacks.init(0, false, 54, 8);
        guiItemStacks.init(1, true, 8, 8);
        guiItemStacks.set(0, ingredients.getOutputs(VanillaTypes.ITEM).get(0));
        guiItemStacks.set(1, ingredients.getInputs(VanillaTypes.ITEM).get(0));
        guiItemStacks.addTooltipCallback(new TooltioCallback(recipe.chance, recipe.rolls));
    }

    @Override
    public void draw(RecipeCrumbs recipe, MatrixStack matrixStack, double mouseX, double mouseY) {
        arrow.draw(matrixStack, 28, 10);
    }


    public final class TooltioCallback implements ITooltipCallback<ItemStack>{

        public final TranslationTextComponent chanceTooltip;
        public final TranslationTextComponent rollsTooltip;

        public TooltioCallback(float chance, int rolls){
            this.chanceTooltip = (TranslationTextComponent)new TranslationTextComponent("tooltip.weissrandom.jei.chancePercent", (chance*100),"%").mergeStyle(TextFormatting.GREEN);
            this.rollsTooltip = (TranslationTextComponent)new TranslationTextComponent("tooltip.weissrandom.jei.rolls", rolls).mergeStyle(TextFormatting.GREEN);
        }

        public void onTooltip(int slotIndex, boolean input, ItemStack ingredient, List<ITextComponent> tooltip){
            if(slotIndex == 0)
                tooltip.add(chanceTooltip);
            if(slotIndex == 1)
                tooltip.add(rollsTooltip);
        }
    };
}
