package weissmoon.random.compat.jei;

import mezz.jei.api.IModPlugin;
import mezz.jei.api.JeiPlugin;
import mezz.jei.api.constants.VanillaRecipeCategoryUid;
import mezz.jei.api.registration.*;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.client.gui.ProjectTableGui;
import weissmoon.random.container.ContainerProjectTable;
import weissmoon.random.item.ItemBlockProjectTable;
import weissmoon.random.recipe.ItemOnBlockUsage;
import weissmoon.random.recipe.RecipeBarrel;
import weissmoon.random.recipe.RecipeCrumbs;
import weissmoon.random.recipe.WeissSpecialRecipe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 11/21/19.
 */
@JeiPlugin
public class JEICompat implements IModPlugin {

    @Override
    public ResourceLocation getPluginUid() {
        return new ResourceLocation("weissrandom:jei");
    }

    @Override
    public void registerCategories(IRecipeCategoryRegistration registry) {
        registry.addRecipeCategories(new BarrelRecipeCategory(registry.getJeiHelpers().getGuiHelper()));
        registry.addRecipeCategories(new CrumbsRecipeCategory(registry.getJeiHelpers().getGuiHelper()));
        registry.addRecipeCategories(new IOBFactory(registry.getJeiHelpers().getGuiHelper()));
    }

    @Override
    public void registerRecipeCatalysts(IRecipeCatalystRegistration registry) {
        ItemStack tableStack = new ItemStack(ModBlocks.projectTable);
        ((ItemBlockProjectTable) tableStack.getItem()).setPlank(tableStack, new ItemStack(Blocks.OAK_PLANKS));
        registry.addRecipeCatalyst(tableStack, VanillaRecipeCategoryUid.CRAFTING);
        registry.addRecipeCatalyst(new ItemStack(ModBlocks.barrelBlock), BarrelRecipeCategory.UID);
    }

    @Override
    public void registerRecipeTransferHandlers(IRecipeTransferRegistration recipeTransferRegistry) {
        recipeTransferRegistry.addRecipeTransferHandler(ContainerProjectTable.class, VanillaRecipeCategoryUid.CRAFTING, 1, 9, 11, 53);
    }

    @Override
    public void registerGuiHandlers(IGuiHandlerRegistration registration) {
        registration.addRecipeClickArea(ProjectTableGui.class, 88, 32, 28, 23, VanillaRecipeCategoryUid.CRAFTING);
    }

    @Override
    public void registerRecipes(IRecipeRegistration registration) {
        WeissSpecialRecipe.reloadIngredients();
        registration.addRecipes(WeissSpecialRecipe.getRecipes(), VanillaRecipeCategoryUid.CRAFTING);
        List<RecipeBarrel> barrelRecipes = new ArrayList<>(5);
        barrelRecipes.add(new RecipeBarrel(null));
        try{
            barrelRecipes.addAll(1, RecipeBarrel.getRecipes(Minecraft.getInstance().world));
        }catch (Exception e){
            e.printStackTrace();
        }
        registration.addRecipes(barrelRecipes, BarrelRecipeCategory.UID);
        List<RecipeCrumbs> crumbsRecipes = new ArrayList<>(5);
        try{
            crumbsRecipes.addAll(0, RecipeCrumbs.getRecipes(Minecraft.getInstance().world));
        }catch (Exception e){
            e.printStackTrace();
        }
        registration.addRecipes(crumbsRecipes, CrumbsRecipeCategory.UID);
        List<ItemOnBlockUsage> iobRecipes = new ArrayList<>(5);
        try{
            iobRecipes.addAll(0, ItemOnBlockUsage.getRecipes(Minecraft.getInstance().world));
        }catch (Exception e){
            e.printStackTrace();
        }
        registration.addRecipes(iobRecipes, IOBFactory.UID);
        registration.addRecipes(SpecialRecipeFactory.createProjectTableRecipe(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(LogHelmetRecipeFactory.createLogHelmet(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(LogBootsRecipeFactory.createLogBoots(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(DaggerRecipeFactory.createDaggerRecipes(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(WoodSilentPlateRecipeFactory.createWoodSilentRecipes(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(WoodInvisiblePlateRecipeFactory.createWoodInvisibleRecipes(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(WoodSilenttoSilentInvisiblePlateRecipeFactory.createWoodInvisibleRecipes(), VanillaRecipeCategoryUid.CRAFTING);
        registration.addRecipes(WoodInvisibletoSilentInvisiblePlateRecipeFactory.createWoodSilentRecipes(), VanillaRecipeCategoryUid.CRAFTING);
    }

//        IIngredientBlacklist blacklist = registry.getJeiHelpers().getIngredientBlacklist();
//        blacklist.addIngredientToBlacklist(new ItemStack(ModBlocks.projectTable));
//        blacklist.addIngredientToBlacklist(new ItemStack(ModBlocks.lantern));
//        blacklist.addIngredientToBlacklist(new ItemStack(ModItems.plungerLauncher));
//        blacklist.addIngredientToBlacklist(new ItemStack(ModItems.sandal));
//        blacklist.addIngredientToBlacklist(new ItemStack(ModItems.dualFish));
//    }
}
