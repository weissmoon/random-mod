package weissmoon.random.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 5/25/19.
 */
public class ItemBlockCharcoal extends WeissBlockItem {

    public ItemBlockCharcoal() {
        super(ModBlocks.charcoalBlock, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS));
    }

    @Override
    public int getBurnTime(ItemStack itemStack)
    {
        return 16000;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIconWeiss = iconRegister.registerIcon(this, getModID()+":"+RegName);
    }
}
