package weissmoon.random.item.bag;

import weissmoon.core.item.WeissItem;

/**
 * Created by Weissmoon on 11/27/21.
 */
public class BagItemBase extends WeissItem{

    static{

    }

    public BagItemBase(String name){
        super(name);
    }

    public BagItemBase(String name, Properties properties){
        super(name, properties);
    }
}
