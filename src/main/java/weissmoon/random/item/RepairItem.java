package weissmoon.random.item;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Weissmoon on 7/25/21.
 */
public class RepairItem extends WeissItem implements IPlankHolder{


    public RepairItem(){
        super(Strings.Items.REPAIR_ITEM, new Properties().group(ItemGroup.TOOLS));
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> list){
        if(tab == ItemGroup.TOOLS){
            ItemStack outputasdfrwe = new ItemStack(this);
            ((RepairItem)outputasdfrwe.getItem()).getPlank(outputasdfrwe);
            list.add(outputasdfrwe);
        }
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        CompoundNBT tags = stack.getTag();
        if (tags != null) {
            ItemStack repair = getPlank(stack);
            if(!repair.isEmpty())
                tooltip.add(repair.getDisplayName());
//            else
//                tooltip.add(new TranslationTextComponent("selectWorld.gameMode.creative"));

        }
    }
}
