package weissmoon.random.item;

import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/19/18.
 * see Mary Poppins
 */
public class ItemParasol extends WeissItem{
    public ItemParasol() {
        super(Strings.Items.PARASOL_NAME);
    }
}
