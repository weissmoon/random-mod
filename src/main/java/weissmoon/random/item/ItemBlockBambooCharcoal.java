package weissmoon.random.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 2/27/22.
 */
public class ItemBlockBambooCharcoal extends WeissBlockItem{

    public ItemBlockBambooCharcoal(){
        super(ModBlocks.bambooCharcoalBlock, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS));
    }

    @Override
    public int getBurnTime(ItemStack itemStack){
        return 8000;
    }
}
