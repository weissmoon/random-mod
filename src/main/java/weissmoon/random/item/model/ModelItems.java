package weissmoon.random.item.model;

import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.core.item.ModelItem;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 2/12/16.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModelItems{

    public static final ModelItem lid = new Lid();
    public static final ModelItem lock = new ModelItem(Strings.Models.LOCK);
    public static final ModelItem unlock = new ModelItem(Strings.Models.UNLOCK);
    public static final ModelItem sneak = new ModelItem(Strings.Models.SNEAK);
    public static final ModelItem mouseLeft = new ModelItem(Strings.Models.MOUSE_LEFT);
    public static final ModelItem mouseRight = new ModelItem(Strings.Models.MOUSE_RIGHT);
    public static final ModelItem mouse = new ModelItem(Strings.Models.MOUSE);


    //Atum models
    public static final ModelItem soulOrb = new ModelItem(Strings.Models.SOUL_ORB_MODEL);
    public static final ModelItem arrowDouble = new ModelItem(Strings.Models.ATUM_DOUBLE);
    public static final ModelItem arrowExplosive = new ModelItem(Strings.Models.ATUM_EXPLOSIVE);
    public static final ModelItem arrowFire = new ModelItem(Strings.Models.ATUM_FIRE);
    public static final ModelItem arrowPoison = new ModelItem(Strings.Models.ATUM_POISON);
    public static final ModelItem arrowQuick = new ModelItem(Strings.Models.ATUM_QUICK);
    public static final ModelItem arrowRain = new ModelItem(Strings.Models.ATUM_RAIN);
    public static final ModelItem arrowSlow = new ModelItem(Strings.Models.ATUM_SLOW);
    public static final ModelItem arrowStrai= new ModelItem(Strings.Models.ATUM_STRAIGHT);



    public static void init (){
        MinecraftForge.EVENT_BUS.register(ModelItems.class);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> evt) {

        IForgeRegistry<Item> GameRegistry = evt.getRegistry();


        GameRegistry.register(lid);
        GameRegistry.register(lock);
        GameRegistry.register(unlock);
        GameRegistry.register(sneak);
        GameRegistry.register(mouseLeft);
        GameRegistry.register(mouseRight);
        GameRegistry.register(mouse);

        //Atum
        GameRegistry.register(soulOrb);
        GameRegistry.register(arrowDouble);
        GameRegistry.register(arrowExplosive);
        GameRegistry.register(arrowFire);
        GameRegistry.register(arrowPoison);
        GameRegistry.register(arrowQuick);
        GameRegistry.register(arrowRain);
        GameRegistry.register(arrowSlow);
        GameRegistry.register(arrowStrai);
    }
}
