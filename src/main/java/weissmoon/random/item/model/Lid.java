package weissmoon.random.item.model;

import weissmoon.core.item.ModelItem;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 2/12/16.
 */
public class Lid extends ModelItem{
    public Lid(){
        super(Strings.Models.LID_MODEL);
    }
}
