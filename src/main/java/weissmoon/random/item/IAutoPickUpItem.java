package weissmoon.random.item;

import net.minecraft.item.ItemStack;

/**
 * Created by Weissmoon on 11/27/21.
 */
public interface IAutoPickUpItem{

    /**
     * @return Should bag perform puck up oprations
     */
    default boolean shouldPickUp(ItemStack bag){
        return false;
    }

    /**
     * ItemStack to be confirmed
     * @param bag ItemStack of the back to perform pick up operation
     * @param toPick ItemStack to be picked up
     * @return can the item ben inserted into the bag
     */
    default boolean canPickUp(ItemStack bag, ItemStack toPick){
        return false;
    }

    /**
     * Insertion operation to bag inventory
     * @param bad ItemStack of bag to insert Item into
     * @param toInsert ItemStack of item to insert
     * @return remaining ItemStack that could not fit in bag
     *         used for adding items to be added to  player inventory
     */
    default ItemStack insertStack(ItemStack bad, ItemStack toInsert){
        return toInsert;
    }
}
