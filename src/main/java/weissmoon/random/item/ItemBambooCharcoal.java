package weissmoon.random.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 12/18/21.
 */
public class ItemBambooCharcoal extends WeissItem{
    public ItemBambooCharcoal(){
        super(Strings.Items.BAMBOO_CHARCOAL_NAME, new Item.Properties().group(ItemGroup.MISC));
    }

    @Override
    public int getBurnTime(ItemStack stack){
        return 800;
    }
}
