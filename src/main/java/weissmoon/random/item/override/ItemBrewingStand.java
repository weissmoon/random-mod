package weissmoon.random.item.override;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeissOverride;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 12/24/19.
 */
public class ItemBrewingStand extends BlockItem implements IItemWeissOverride{

    private final String ModId;
    protected final String RegName;
    protected IIcon itemIconWeiss;

    public ItemBrewingStand() {
        super(ModBlocks.brewingStand, new Item.Properties().group(ItemGroup.BREWING));
        this.ModId = "minecraft";
        this.RegName = ModBlocks.brewingStand.getWeissName();
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public String getCreatorModId(ItemStack itemStack) {
        return Reference.MOD_ID;
    }

    @Override
    public String getTrueModID(ItemStack itemStack){
        return Reference.MOD_ID;
    }

    @Override
    public String getModID(){
        return "minecraft";
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack) {
        return this.itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }
}
