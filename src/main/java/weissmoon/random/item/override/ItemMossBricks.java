package weissmoon.random.item.override;

import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IBlockItemWeiss;
import weissmoon.core.item.IItemWeissOverride;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 1/11/20.
 */
public class ItemMossBricks extends BlockItem implements IItemWeissOverride, IBlockItemWeiss {

    private final String ModId;
    protected final String RegName;
    protected IIcon itemIconWeiss;

    public ItemMossBricks() {
        super(ModBlocks.mossyCobblestone, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS));
        this.ModId = "minecraft";
        this.RegName = ModBlocks.mossyCobblestone.getWeissName();
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public String getModID(){
        return "minecraft";
    }

    @Override
    public String getWeissName() {
        return RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack) {
        return itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public String getCreatorModId(ItemStack itemStack) {
        return Reference.MOD_ID;
    }

    @Override
    public String getTrueModID(ItemStack itemStack){
        return Reference.MOD_ID;
    }
}
