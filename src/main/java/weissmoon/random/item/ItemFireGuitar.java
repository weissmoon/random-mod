package weissmoon.random.item;

import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/6/18.
 * see Mad Max
 */
public class ItemFireGuitar extends WeissItem {
    public ItemFireGuitar() {
        super(Strings.Items.FIRE_GUITAR_NAME);
    }
}
