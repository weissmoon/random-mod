package weissmoon.random.item;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.common.PlantType;
import net.minecraftforge.common.IPlantable;
import weissmoon.core.item.WeissItem;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 11/12/19.
 */
public class ItemMelonAirSeed extends WeissItem implements IPlantable {

    public ItemMelonAirSeed() {
        super(Strings.Items.MELON_AIR_SEED_NAME, new Item.Properties().group(ItemGroup.MISC));
//        this.setCreativeTab(CreativeTabs.MISC);
    }

    @Override
    public ActionResultType onItemUse(ItemUseContext context)
    {
        PlayerEntity player = context.getPlayer();
        BlockPos pos = context.getPos();
        Hand hand = context.getHand();
        World worldIn = context.getWorld();
        Direction facing = context.getFace();
        ItemStack itemstack = player.getHeldItem(hand);
        net.minecraft.block.BlockState state = worldIn.getBlockState(pos);
        if (facing == Direction.UP && player.canPlayerEdit(pos.offset(facing), facing, itemstack) && state.getBlock().canSustainPlant(state, worldIn, pos, Direction.UP, this) && worldIn.isAirBlock(pos.up()))
        {
            worldIn.setBlockState(pos.up(), ModBlocks.stemAir.getDefaultState());

            if (player instanceof ServerPlayerEntity)
            {
                CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) player, pos.up(), itemstack);
            }

            itemstack.shrink(1);
            return ActionResultType.SUCCESS;
        }
        else
        {
            return ActionResultType.FAIL;
        }
    }

    @Override
    public PlantType getPlantType(IBlockReader world, BlockPos pos) {
        return PlantType.CROP;
    }

    @Override
    public BlockState getPlant(IBlockReader world, BlockPos pos) {
        return ModBlocks.stemAir.getDefaultState();
    }
}
