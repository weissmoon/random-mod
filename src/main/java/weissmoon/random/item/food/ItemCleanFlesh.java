package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemCleanFlesh extends WeissItemFood{
    public ItemCleanFlesh(){
        super(3, 0.1F, true, false, false, Strings.Items.CLEAN_FLESH_NAME);
    }
}
