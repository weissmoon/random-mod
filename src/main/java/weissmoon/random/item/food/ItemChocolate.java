package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/6/18.
 */
public class ItemChocolate extends WeissItemFood {

    public ItemChocolate() {
        super(2, 0.5F, false, false, false, Strings.Items.CHOCOLATE_NAME);
    }


}
