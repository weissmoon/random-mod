package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemSaltedPork extends WeissItemFood{
    public ItemSaltedPork() {
        super(16, 1.6F, true, false, false, Strings.Items.SALTED_PORK_NAME);
    }
}
