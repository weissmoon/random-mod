package weissmoon.random.item.food;

import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Effects;
import net.minecraft.world.World;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemHoneyBread extends WeissItemFood{
    public ItemHoneyBread(){
        super(8, 0.7F, false, false, false, Strings.Items.HONEY_BREAD_NAME);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving){
        super.onItemUseFinish(stack, worldIn, entityLiving);

        if (!worldIn.isRemote) {
            entityLiving.removePotionEffect(Effects.POISON);
        }

        return stack;
    }
}
