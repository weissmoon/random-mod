package weissmoon.random.item.food;

import net.minecraft.item.ItemStack;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/9/22.
 */
public class ItemFriedPotatoWedgeRaw extends WeissItemFood{
    public ItemFriedPotatoWedgeRaw(){
        super(1, 0.125F, false, false, false, Strings.Items.FRIED_POTATO_WEDGE_RAW_NAME);
    }

    public int getUseDuration(ItemStack stack) {
        return 24;
    }
}
