package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/8/21.
 */
public class ItemMeatBunBeef extends WeissItemFood{
    public ItemMeatBunBeef(){
        super(7, 0.9F, true, false, false, Strings.Items.MEAT_BUN_BEEF);
    }
}
