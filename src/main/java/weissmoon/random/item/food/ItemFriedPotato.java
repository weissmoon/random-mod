package weissmoon.random.item.food;

import net.minecraft.item.ItemStack;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/12/22.
 */
public class ItemFriedPotato extends WeissItemFood{
    public ItemFriedPotato(){
        super(1, 0.65F, false, false, true, Strings.Items.FRIED_POTATO_NAME);
    }

    public int getUseDuration(ItemStack stack) {
        return 12;
    }
}