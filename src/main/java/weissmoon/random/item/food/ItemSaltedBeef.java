package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/30/21.
 */
public class ItemSaltedBeef extends WeissItemFood {
    public ItemSaltedBeef() {
        super(16, 1.6F, true, false, false, Strings.Items.SALTED_BEEF_NAME);
    }
}
