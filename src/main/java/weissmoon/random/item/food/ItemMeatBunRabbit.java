package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemMeatBunRabbit extends WeissItemFood{
    public ItemMeatBunRabbit(){
        super(9, 0.9F, true, false, false, Strings.Items.MEAT_BUN_RABBIT);
    }
}
