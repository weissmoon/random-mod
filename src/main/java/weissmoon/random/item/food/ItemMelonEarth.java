package weissmoon.random.item.food;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.potion.Effects;
import net.minecraft.util.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.stats.Stats;
import net.minecraft.util.SoundCategory;
import net.minecraft.world.World;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.core.helper.RNGHelper;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 11/12/19.
 */
public class ItemMelonEarth extends WeissItemFood {
    public ItemMelonEarth() {
        super(2, 0.3F, false, false, false, Strings.Items.MELON_EARTH_NAME);
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack stack, World worldIn, LivingEntity entityLiving)
    {
        if (entityLiving instanceof PlayerEntity)
        {
            PlayerEntity entityplayer = (PlayerEntity)entityLiving;
            if(!worldIn.isRemote) {
                if (RNGHelper.getFloat() < 0.6F) {
                    entityplayer.addPotionEffect(new EffectInstance(Effects.STRENGTH, 160, 1, false, true));
                    if (RNGHelper.getFloat() < 0.15F) {
                        entityplayer.addPotionEffect(new EffectInstance(Effects.SLOWNESS, 80, 2, false, true));
                    }
                }
            }
            entityplayer.getFoodStats().addStats(2, 0.3F);
            worldIn.playSound(null, entityplayer.getPosX(), entityplayer.getPosY(), entityplayer.getPosZ(), SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.PLAYERS, 0.5F, worldIn.rand.nextFloat() * 0.1F + 0.9F);
            entityplayer.addStat(Stats.ITEM_USED.get(this));
            if(entityplayer instanceof ServerPlayerEntity) {
                CriteriaTriggers.CONSUME_ITEM.trigger((ServerPlayerEntity) entityplayer, stack);
            }
            stack.shrink(1);
            if (RNGHelper.getFloat() < 0.05){
                InventoryHelper.givePlayerOrDropItemStack(new ItemStack(ModItems.melonEarthSeed), entityplayer);
            }
        }

        return stack;
    }
}
