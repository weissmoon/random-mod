package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 5/12/21.
 */
public class ItemSalt extends WeissItemFood{
    public ItemSalt(){
        super(0, 0.0F, false, true, true, Strings.Items.SALT_NAME);
    }
}
