package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemMeatBunMutton extends WeissItemFood{
    public ItemMeatBunMutton(){
        super(9, 0.9F, true, false, false, Strings.Items.MEAT_BUN_MUTTON);
    }
}
