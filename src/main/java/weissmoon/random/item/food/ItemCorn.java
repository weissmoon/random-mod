package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/2/19.
 */
public class ItemCorn extends WeissItemFood {
    public ItemCorn() {
        super(2, 3, false, false, false, Strings.Items.CORN_FOOD_NAME);
    }
}
