package weissmoon.random.item.food;

import net.minecraft.item.ItemStack;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/9/22.
 */
public class ItemFriedPotatoWedge extends WeissItemFood{
    public ItemFriedPotatoWedge(){
        super(2, 0.625F, false, false, true, Strings.Items.FRIED_POTATO_WEDGE_NAME);
    }

    public int getUseDuration(ItemStack stack) {
        return 24;
    }
}
