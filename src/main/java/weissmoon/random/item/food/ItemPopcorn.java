package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/7/19.
 */
public class ItemPopcorn extends WeissItemFood {
    public ItemPopcorn() {
        super(5, 7.2F, false, false, false, Strings.Items.POPCORN_NAME);
    }
}
