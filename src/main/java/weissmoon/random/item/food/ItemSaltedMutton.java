package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/15/21.
 */
public class ItemSaltedMutton extends WeissItemFood{
    public ItemSaltedMutton(){
        super(12, 1.6F, true, false, false, Strings.Items.SALTED_MUTTON_NAME);
    }
}
