package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/7/19.
 */
public class ItemCornCob extends WeissItemFood {
    public ItemCornCob() {
        super(4, 5.3F, false, false, false, Strings.Items.CORN_COB_NAME);
    }
}
