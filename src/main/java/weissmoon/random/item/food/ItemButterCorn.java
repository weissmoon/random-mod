package weissmoon.random.item.food;

import weissmoon.core.item.WeissItemFood;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 3/12/19.
 */
public class ItemButterCorn extends WeissItemFood {
    public ItemButterCorn() {
        super(6, 3.2F, false, false, false, Strings.Items.BUTTER_CORN_NAME);
    }
}
