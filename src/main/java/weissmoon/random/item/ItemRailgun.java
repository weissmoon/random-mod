package weissmoon.random.item;

import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Items;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
//import org.lwjgl.input.Keyboard;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.world.World;
import weissmoon.core.entity.BulletProperties;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.core.item.WeissItem;
import weissmoon.core.utils.NBTHelper;
//import weissmoon.random.entity.EntityRailBullet;
import weissmoon.random.lib.*;

import javax.annotation.Nullable;
import java.util.*;

public class ItemRailgun extends WeissItem{

    private List<ValidRailAmmo> activeAmmo;
    private String READY = "ready";


    public ItemRailgun (){
        super(Strings.Items.RAILGUN_NAME);
        //this.maxStackSize = 1;
        //setNoRepair();
    }

    @Override
    public void inventoryTick (ItemStack stack, World world, Entity entity, int i, boolean flag){
        if ((stack != null) && (stack.getItem() != Items.AIR)){
            int cool = getCooldown(stack);
            if (cool > 0){
                setCooldown(stack, cool - 1);
            }else{
                if (entity instanceof PlayerEntity){
                    int item = ((PlayerEntity)entity).inventory.currentItem;
                    /*Dual Hotbar Support*/
                    if ((item == 8) || (item == 17)){
                        item--;
                    }else{
                        item++;
                    }
                    ItemStack stack1;
                    try{
                        stack1 = ((PlayerEntity)entity).inventory.getStackInSlot(item);
                    }catch( ArrayIndexOutOfBoundsException e ){
                        stack1 = null;
                    }
                    if ((stack1 == null) || (stack1.getItem() == Items.AIR)){
                        NBTHelper.setBoolean(stack, READY, false);
                        ((PlayerEntity)entity).inventory.markDirty();
                        return;
                    }
                    List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
                    for (ValidRailAmmo e : list){
                        List<ItemStack> stacks = e.getNugetlist();
                        for (ItemStack es : stacks){
                            if (stack1.isItemEqual(es)){
                                NBTHelper.setBoolean(stack, READY, true);
                                return;
                            }
                        }
                    }
                }
                NBTHelper.setBoolean(stack, READY, false);
            }
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand hand){
        ItemStack stack = player.getHeldItem(hand);
        player.stopActiveHand();
        if ((player.getHeldItem(Hand.MAIN_HAND).getItem() instanceof ItemRailgun)){
            //if (NBTHelper.getBoolean(stack, READY)){
                if ((getCooldown(stack) <= 0)){
                    if ((!player.world.isRemote)){
                        int item = player.inventory.currentItem;
                        if ((item == 8) || (item == 17)){
                            item--;
                        }else{
                            item++;
                        }
                        ItemStack stack1 = player.inventory.getStackInSlot(item);
                        if ((stack1 != ItemStack.EMPTY) || (stack1.getItem() != Items.AIR)){
                            //EntityRailBullet bullet = new EntityRailBullet(player.world, player, getBullet(player, player.inventory.currentItem), stack1);
//                            EntityRailBullet bullet = new EntityRailBullet(player.world, player, ValidRailAmmo.GOLD, new ItemStack(Items.GOLD_NUGGET));
                            //bullet.nuggetIndex = NBTHelper.getByte(stack, INDEX);
//                            InventoryHelper.consumeItemInSlot(player, item, false);
//                            player.world.addEntity((Entity) bullet);
                        }
                    }
                    setCooldownDefault(stack);
                    NBTHelper.setBoolean(stack, READY, false);
                }
                return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
            //}
        }
        return new ActionResult<ItemStack>(ActionResultType.FAIL, stack);
    }

//    private ItemStack tryArming (ItemStack stack, EntityPlayer player){
//        int item = player.inventory.currentItem;
//
//        /**Dual Hotbar Support*/
//        if ((item == 8) || (item == 17)){
//            item--;
//        }else{
//            item++;
//        }
//        ItemStack stack1 = player.inventory.getStackInSlot(item);
//        if ((stack1 == null) || (stack1.getItem() == null)){
//            return stack;
//        }
//        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
//        for (ValidRailAmmo e : list){
//            List<ItemStack> stacks = e.getNugetlist();
//            int p = 0;
//            for (ItemStack es : stacks){
//                if (stack1.isItemEqual(es)){
//                    //setActiveAmmo(stack, e);
//                    setCooldownDefault(stack);
//                    //NBTHelper.setBoolean(stack, LOADED, true);
//                    //NBTHelper.setByte(stack, INDEX, (byte)p);
//                    return stack;
//                }
//                p++;
//            }
//        }
//        return stack;
//    }

    @Override
    public double getDurabilityForDisplay (ItemStack stack){
        if ((stack.getItem() instanceof ItemRailgun)){
            Integer chamber = ((ItemRailgun)stack.getItem()).getCooldown(stack);
            return (getCooldownDefault(stack) - chamber) / getCooldownDefault(stack);
        }
        return 1.0D;
    }

    public ValidRailAmmo getBullet (PlayerEntity player, int item){
        //int item = player.inventory.currentItem;

        if (item == -1){
            return ValidRailAmmo.UNKNOWN;
        }
        /*Dual Hotbar Support*/
        if ((item == 8) || (item == 17)){
            item--;
        }else{
            item++;
        }
        ItemStack stack1 = player.inventory.getStackInSlot(item);
        if ((stack1 == ItemStack.EMPTY) || (stack1.getItem() == Items.AIR)){
            return ValidRailAmmo.UNKNOWN;
        }
        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
        for (ValidRailAmmo e : list){
            List<ItemStack> stacks = e.getNugetlist();
            for (ItemStack es : stacks){
                if (stack1.isItemEqual(es)){
                    if (stack1.isItemEqual(es)){
                        return e;
                    }
                }
            }
        }
        return ValidRailAmmo.UNKNOWN;
    }

    public ValidRailAmmo getBulletFromStack (ItemStack stack1){

        if ((stack1 == null) || (stack1.getItem() == Items.AIR)){
            return ValidRailAmmo.UNKNOWN;
        }
        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
        for (ValidRailAmmo e : list){
            List<ItemStack> stacks = e.getNugetlist();
            for (ItemStack es : stacks){
                if (stack1.isItemEqual(es)){
                    return e;
                }
            }
        }
        return ValidRailAmmo.UNKNOWN;
    }

    public String getBulletStackString (ItemStack stack1){

        if ((stack1 == null) || (stack1.getItem() == Items.AIR)){
            return ValidRailAmmo.UNKNOWN.getDictionaryName();
        }
        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
        for (ValidRailAmmo e : list){
            List<ItemStack> stacks = e.getNugetlist();
            for (ItemStack es : stacks){
                if (stack1.isItemEqual(es)){
                    return e.getDictionaryName();
                }
            }
        }
        return ValidRailAmmo.UNKNOWN.getDictionaryName();
    }

    public int getCooldown (ItemStack stack){
        return NBTHelper.getInt(stack, "coolDown");
    }

    public int getCooldownDefault (ItemStack stack){
        return 24;
    }

    public void setCooldown (ItemStack stack, int cool){
        NBTHelper.setInteger(stack, "coolDown", cool);
    }

    public void setCooldownDefault (ItemStack stack){
        setCooldown(stack, getCooldownDefault(stack));
    }

    @Override
    public UseAction getUseAction (ItemStack stack){
        return UseAction.BOW;
    }

    @Override
    public int getUseDuration (ItemStack stack){
        return 0;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void addInformation (ItemStack stack, @Nullable World world, List<ITextComponent> list, ITooltipFlag flag){
        PlayerEntity player = Minecraft .getInstance().player;
        CompoundNBT tags = stack.getTag();
        if ((tags != null)){
            String returning = "";
            StringBuilder string = new StringBuilder();
            string.append(new TranslationTextComponent("tooltip.weissrandom.itemRailgun").getUnformattedComponentText());
            string.append(' ');
            int slot = -1;
            for (int j = 0; j < player.inventory.mainInventory.size(); j++){
                if ((player.inventory.getStackInSlot(j) != ItemStack.EMPTY) && (player.inventory.getStackInSlot(j) == stack)){
                    slot = j;
                    break;
                }
            }
            if (getBullet(player, slot) == ValidRailAmmo.UNKNOWN || NBTHelper.getBoolean(stack, "READY")){
                string.append(new TranslationTextComponent("empty.weissrandom.itemRailgun").getUnformattedComponentText());
            }else{
                //string.append(ValidRailAmmo.getFromOrdinal(NBTHelper.getByte(stack, "ammo")).getNugetlist().get(0).getDisplayName());
                string.append(this.getBullet(player, slot).getNugetlist().get(0).getDisplayName());
            }
            returning = string.toString();
            list.add(new StringTextComponent(returning));
        }

        //if ((Keyboard.isKeyDown(42)) || (Keyboard.isKeyDown(54))){
            list.add(new StringTextComponent(""));
            TranslationTextComponent ammo = new TranslationTextComponent("valid.weissrandom.itemRailgun");
            list.add(ammo);
            for (ValidRailAmmo e : ValidRailAmmo.values()){
                if (e.isActive()){
                    list.add(e.getNugetlist().get(0).getDisplayName());
                }
            }
        //}
    }

    public List<ValidRailAmmo> getActiveBullets (){
        return this.activeAmmo;
    }

    @Override
    public Collection<ItemGroup> getCreativeTabs()
    {
        return Arrays.asList(getGroup());
    }

//    @Override
//    public CreativeTabs getCreativeTab (){
//        return CreativeTabs.COMBAT;
//    }
}
