package weissmoon.random.item;

import com.google.common.collect.Multimap;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.item.ItemGroup;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.block.Blocks;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

import java.util.List;

/**
 * Created by Weissmoon on 10/5/17.
 */
public class ItemBattleFish extends WeissItem {

    private final float attackDamage = 4.5F;

    public ItemBattleFish(){
        super(Strings.Items.DUAL_WIELDING_FISH_NAME);
    }

    public void addInformation(ItemStack stack, PlayerEntity player, List list, ITooltipFlag flag) {
        super.addInformation(stack, player.world, list, flag);
        String comp = this.getModID() + ":" + this.RegName + ".lore";
        TranslationTextComponent ds = new TranslationTextComponent(this.getModID() + ":" + this.RegName + ".lore");
        if (!ds.getUnformattedComponentText().contentEquals(comp)) {
            list.add(new TranslationTextComponent(this.getModID() + ":" + this.RegName + ".lore").getUnformattedComponentText());
        }
    }
    /**
     * Returns the amount of damage this item will deal. One heart of damage is equal to 2 damage points.
     */
    public float getDamageVsEntity()
    {
        return this.attackDamage;
    }

    public float getStrVsBlock(ItemStack stack, BlockState state)
    {
        Block block = state.getBlock();

        if (block == Blocks.COBWEB)
        {
            return 15.0F;
        }
        else
        {
            Material material = state.getMaterial();
            return material != Material.PLANTS && material != Material.TALL_PLANTS && material != Material.CORAL && material != Material.LEAVES && material != Material.GOURD ? 1.0F : 1.5F;
        }
    }

    /**
     * Current implementations of this method in child classes do not use the entry argument beside ev. They just raise
     * the damage on the stack.
     */
    public boolean hitEntity(ItemStack stack, LivingEntity target, LivingEntity attacker)
    {
        stack.damageItem(1, attacker, (p_213360_0_) -> {
            p_213360_0_.sendBreakAnimation(EquipmentSlotType.CHEST);
        });
        return true;
    }

    /**
     * Called when a Block is destroyed using this Item. Return true to trigger the "Use Item" statistic.
     */
    public boolean onBlockDestroyed(ItemStack stack, World worldIn, BlockState state, BlockPos pos, LivingEntity entityLiving)
    {
        if ((double)state.getBlockHardness(worldIn, pos) != 0.0D)
        {
            stack.damageItem(2, entityLiving, (p_213360_0_) -> {
                p_213360_0_.sendBreakAnimation(EquipmentSlotType.CHEST);
            });
        }

        return true;
    }

    /**
     * Check whether this Item can harvest the given Block
     */
    public boolean canHarvestBlock(BlockState blockIn)
    {
        return blockIn.getBlock() == Blocks.COBWEB;
    }

    /**
     * Returns True is the item is renderer in full 3D when hold.
     */
    @OnlyIn(Dist.CLIENT)
    public boolean isFull3D()
    {
        return true;
    }

    /**
     * Return the enchantability factor of the item, most of the time is based on material.
     */
    public int getItemEnchantability()
    {
        return 12;
    }

    /**
     * Return whether this item is repairable in an anvil.
     */
    public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
    {
        return false;
    }

    @SuppressWarnings("Deprecated")
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlotType equipmentSlot)
    {
        Multimap<Attribute, AttributeModifier> multimap = super.getAttributeModifiers(equipmentSlot);

        if (equipmentSlot == EquipmentSlotType.MAINHAND || equipmentSlot == EquipmentSlotType.OFFHAND)
        {
            multimap.put(Attributes.ATTACK_DAMAGE, new AttributeModifier(ATTACK_DAMAGE_MODIFIER, "Weapon modifier", (double)this.attackDamage, AttributeModifier.Operation.ADDITION));
        }

        return multimap;
    }

    @Override
    public java.util.Collection<ItemGroup> getCreativeTabs()
    {
        return super.getCreativeTabs();
//        return new CreativeTabs[]{
//                CreativeTabs.FOOD,
//                CreativeTabs.COMBAT
//        };
    }

//    @Override
//    public CreativeTabs getCreativeTab (){
//        return null;
//        //return CreativeTabs.FOOD;
//    }
}
