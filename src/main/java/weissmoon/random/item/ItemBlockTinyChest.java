package weissmoon.random.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.client.render.item.RenderItemTinyChest;

/**
 * Created by Weissmoon on 6/30/21.
 */
public class ItemBlockTinyChest extends WeissBlockItem implements IItemRenderCustom{
    public ItemBlockTinyChest(){
        super(ModBlocks.tinyChest, new Item.Properties().group((ItemGroup.DECORATIONS)));
    }

    @Override
    public IItemRenderer getIItemRender(){
        return new RenderItemTinyChest();
    }
}
