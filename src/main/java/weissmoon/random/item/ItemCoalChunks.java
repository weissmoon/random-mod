package weissmoon.random.item;

import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.client.render.renderOverride.CustomRenderRegistry;
import weissmoon.core.item.WeissItem;

/**
 * Created by Weissmoon on 5/24/19.
 */
public class ItemCoalChunks extends WeissItem implements IItemRenderCustom {

    private final boolean charcoal;
    public ItemCoalChunks() {
        super("itemcoalfragment", new Item.Properties().group(ItemGroup.MISC));
        this.charcoal = false;
        //this.hasSubtypes = true;
    }

    public ItemCoalChunks(boolean charcoal) {
        super("itemcharcoalfragment", new Item.Properties().group(ItemGroup.MISC));
        this.charcoal = true;
        //this.hasSubtypes = true;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconArray = new IIcon[2];
        this.itemIconArray[0] = iconRegister.registerIcon(this, "itemcoalfragment");
        this.itemIconArray[1] = iconRegister.registerIcon(this, "itemcharcoalfragment");
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        try{
            return this.charcoal ? this.itemIconArray[1]: this.itemIconArray[0];
        }catch (Exception e) {
            return this.itemIconArray[0];
        }
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack)
    {
        return new TranslationTextComponent(this.charcoal ? "item.weissrandom.itemcharcoalfragment" : "item.weissrandom.itemcoalfragment");
    }

    @Override
    public int getBurnTime(ItemStack itemStack)
    {
        return 200;
    }

    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> items)
    {
        if (this.isInGroup(tab))
        {
            items.add(new ItemStack(this, 1));
        }
    }

    @Override
    public IItemRenderer getIItemRender() {
        return WeissCoreAPI.noRenderer;
    }
}
