package weissmoon.random.item;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileProjectTable;
import weissmoon.random.client.render.item.RenderItemProjectTable;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Weissmoon on 5/9/19.
 */
public class ItemBlockProjectTable extends WeissBlockItem implements IItemRenderCustom, IPlankHolder {

    private static final String PLANK = "plankNBT";

    public ItemBlockProjectTable() {
        super(ModBlocks.projectTable, new Item.Properties().group(ItemGroup.DECORATIONS));
    }

    @Override
    public IItemRenderer getIItemRender() {
        return new RenderItemProjectTable();
    }

    public void setPlank(ItemStack stack, ItemStack plank) {
        CompoundNBT cmp = new CompoundNBT();
        if(plank != null) {
            cmp = plank.serializeNBT();
            cmp.putByte("Count", (byte) 1);
        }
        NBTHelper.setTagCompound(stack, PLANK, cmp);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> list) {
        if(tab == ItemGroup.DECORATIONS) {
            ItemStack outputasdfrwe = new ItemStack(this);
            ((ItemBlockProjectTable) outputasdfrwe.getItem()).setPlank(outputasdfrwe, new ItemStack(Blocks.OAK_PLANKS));
            list.add(outputasdfrwe);//TODO tag list
        }
    }

    public ItemStack getPlank(ItemStack stack){
        CompoundNBT cmp = NBTHelper.getTagCompound(stack, PLANK);
        if(cmp == null)
            return ItemStack.EMPTY;
        return ItemStack.read(cmp);
    }

    @Override
    protected boolean onBlockPlaced(BlockPos pos, World world, @Nullable PlayerEntity player, ItemStack stack, BlockState newState)
    {
//        if (!world.setBlockState(pos, newState, 11)) return false;
        world.setBlockState(pos, newState, 11);

        BlockState state = world.getBlockState(pos);
        if (state.getBlock() == this.getBlock())
        {
            setTileEntityNBT(world, player, pos, stack);
            this.getBlock().onBlockPlacedBy(world, pos, state, player, stack);
            ((TileProjectTable)world.getTileEntity(pos)).setWoodStack(getPlank(stack));
            //world.getTileEntity(pos).markDirty();

            if (player instanceof ServerPlayerEntity)
                CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayerEntity) player, pos, stack);
        }

        return true;
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> list, ITooltipFlag flagIn)
    {
    //public void addInformation (ItemStack stack, EntityPlayer player, List list, boolean flag){
        CompoundNBT tags = stack.getTag();
        if (tags != null) {
            list.add(this.getPlank(stack).getDisplayName());
        }
    }
}
