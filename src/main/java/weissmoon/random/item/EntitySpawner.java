package weissmoon.random.item;

import net.minecraft.item.ItemGroup;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import weissmoon.core.item.WeissItem;
import weissmoon.random.client.render.entity.Cylinder;

public class EntitySpawner extends WeissItem{


    EntitySpawner(){
        super(null);
    }


    public boolean onItemUseFirst (ItemStack stack, PlayerEntity player, World world, int x, int y, int z, int side,
                                   float hitX, float hitY, float hitZ){
        //player.worldObj.playAuxSFX(1005, new BlockPos(x, y, z), 2264);
        //player.worldObj.playSoundEffect(player.posX, player.posY, player.posZ, "random.explode", 4.0F, (1.0F + (player.worldObj.rand.nextFloat() - player.worldObj.rand.nextFloat()) * 0.2F) * 0.7F);

        return true;
    }

    public ItemGroup getCreativeTab (){
        return ItemGroup.MISC;
    }
}
