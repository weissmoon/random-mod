package weissmoon.random.item;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tags.ItemTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.RegistryManager;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.client.render.item.RenderItemWoodenPressurePlate;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Weissmoon on 1/3/22.
 */
public class ItemWoodenPressurePlate extends WeissBlockItem implements IItemHolderItem, IItemRenderCustom{

    public ItemWoodenPressurePlate(Block block, Properties properties){
        super(block, properties);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> list) {
        if(tab == ItemGroup.REDSTONE){
            for(Item item: Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("wooden_pressure_plates"))).getAllElements()){
                ItemStack outputasdfrwe = new ItemStack(this);
                IItemHolderItem.setHolderItem(outputasdfrwe, new ItemStack(item));
                list.add(outputasdfrwe);
            }
        }
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack) {
        return new TranslationTextComponent("block."+this.getBlock().getRegistryName().getNamespace()+"."+this.getBlock().getRegistryName().getPath(), IItemHolderItem.getHolding(stack).getDisplayName());
    }

    @Override
    protected boolean onBlockPlaced(BlockPos pos, World worldIn, @Nullable PlayerEntity player, ItemStack stack, BlockState state) {
        return setTileEntityNBT(worldIn, player, pos, stack);
    }

    public static boolean setTileEntityNBT(World worldIn, @Nullable PlayerEntity player, BlockPos pos, ItemStack stackIn) {
        MinecraftServer minecraftserver = worldIn.getServer();
        if (minecraftserver != null){
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity != null){
                if (player == null)
                    return false;

                CompoundNBT compoundnbt1 = tileentity.write(new CompoundNBT());
                CompoundNBT compoundnbt2 = compoundnbt1.copy();
                CompoundNBT plateStack = new CompoundNBT();
                IItemHolderItem.getHolding(stackIn).write(plateStack);
                compoundnbt1.put("plate", plateStack);
                if (!compoundnbt1.equals(compoundnbt2)) {
                    tileentity.read(worldIn.getBlockState(pos), compoundnbt1);
                    tileentity.markDirty();
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public IIcon getIcon(ItemStack stack){
        try{
            return new IIcon(RegistryManager.ACTIVE.getRegistry(Item.class).getKey(IItemHolderItem.getHolding(stack).getItem()), "inventory");
        }catch(NullPointerException e){
            return this.itemIconWeiss;
        }
    }

    @Override
    public IItemRenderer getIItemRender(){
        return new RenderItemWoodenPressurePlate();
    }
}
