package weissmoon.random.item;

import net.minecraft.client.Minecraft;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.UseAction;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.core.item.WeissItem;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Weissmoon on 5/1/17.
 */
public class ItemPlungerLauncher extends WeissItem {

    private String READY = "ready";


    public ItemPlungerLauncher() {
        super(Strings.Items.PLUNGER_NAME);
    }

    @Override
    public void inventoryTick (ItemStack stack, World world, Entity entity, int i, boolean flag){
        if ((stack != null) && (stack.getItem() != null)){
            int cool = getCooldown(stack);
            if (cool > 0){
                setCooldown(stack, cool - 1);
            }else{
                if (entity instanceof PlayerEntity){
                    int item = i;
                    /**Dual Hotbar Support*/
                    if ((item == 8) || (item == 17)){
                        item--;
                    }else{
                        item++;
                    }
                    ItemStack stack1;
                    try{
                        stack1 = ((PlayerEntity)entity).inventory.getStackInSlot(item);
                    }catch( ArrayIndexOutOfBoundsException e ){
                        stack1 = null;
                    }
                    if ((stack1 == null) || (stack1.getItem() == null)){
                        NBTHelper.setBoolean(stack, READY, false);
                        ((PlayerEntity)entity).inventory.markDirty();
                        return;
                    }
//                    if (stack1.getItem() instanceof RandomDummy) {
//                        if (NBTHelper.getString(stack1, "itemname").matches(Strings.Items.PLUNGER_NAME)) {
//                            NBTHelper.setBoolean(stack, READY, true);
//                            return;
//                        }
//                    }
                }
                NBTHelper.setBoolean(stack, READY, false);
            }
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity player, Hand hand){
        ItemStack stack = player.getHeldItem(hand);
        player.stopActiveHand();
        if ((player.getHeldItem(Hand.MAIN_HAND).getItem() instanceof ItemPlungerLauncher)){
            if (NBTHelper.getBoolean(stack, READY)){
                if ((getCooldown(stack) <= 0)){
                    if ((!player.world.isRemote)){
                        int item = player.inventory.currentItem;
                        if ((item == 8) || (item == 17)){
                            item--;
                        }else{
                            item++;
                        }
                        ItemStack stack1 = player.inventory.getStackInSlot(item);
                        if ((stack1 != null) || (stack1.getItem() != null)){
                            //PlungerEntity bullet = new PlungerEntity(player.world, player);
                            InventoryHelper.consumeItemInSlot(player, item, false);
                            //player.world.spawnEntity(bullet);
                        }
                    }
                    setCooldownDefault(stack);
                    NBTHelper.setBoolean(stack, READY, false);
                }
                return new ActionResult<ItemStack>(ActionResultType.SUCCESS, stack);
            }
        }
        return new ActionResult<ItemStack>(ActionResultType.FAIL, stack);
    }

    public double getDurabilityForDisplay (ItemStack stack){
        if ((stack.getItem() instanceof ItemRailgun)){
            Integer chamber = ((ItemRailgun)stack.getItem()).getCooldown(stack);
            return (getCooldownDefault(stack) - chamber) / getCooldownDefault(stack);
        }
        return 1.0D;
    }

    public boolean hasBullet (PlayerEntity player, int item){
        if (item == -1){
            return false;
        }
        /*Dual Hotbar Support*/
        if ((item == 8) || (item == 17)){
            item--;
        }else{
            item++;
        }
        ItemStack stack1 = player.inventory.getStackInSlot(item);
        if ((stack1 == null) || (stack1.getItem() == null)){
            return false;
        }
//        if (stack1.getItem() instanceof RandomDummy) {
//            if (NBTHelper.getString(stack1, "itemname").matches(Strings.Items.PLUNGER_NAME)) {
//                return true;
//            }
//        }
        return false;
    }
    public int getCooldown (ItemStack stack){
        return NBTHelper.getInt(stack, "coolDown");
    }

    public int getCooldownDefault (ItemStack stack){
        return 24;
    }

    public void setCooldown (ItemStack stack, int cool){
        NBTHelper.setInteger(stack, "coolDown", cool);
    }

    public void setCooldownDefault (ItemStack stack){
        setCooldown(stack, getCooldownDefault(stack));
    }

    public UseAction getItemUseAction (ItemStack stack){
        return UseAction.BOW;
    }

    public int getMaxItemUseDuration (ItemStack stack){
        return 0;
    }

    @OnlyIn(Dist.CLIENT)
    public void addInformation (ItemStack stack, @Nullable World world, List<ITextComponent> list, ITooltipFlag flag) {
        PlayerEntity player = Minecraft.getInstance().player;
        CompoundNBT tags = stack.getTag();
        if ((tags != null) && (tags != null)){
            String returning = "";
            StringBuilder string = new StringBuilder();
            string.append(new TranslationTextComponent("tooltip.weissrandom.itemRailgun").getUnformattedComponentText());
            int slot = -1;
            for (int j = 0; j < player.inventory.mainInventory.size(); j++){
                if ((player.inventory.mainInventory.get(j) != null) && (player.inventory.mainInventory.get(j) == stack)){
                    slot = j;
                    break;
                }
            }
            if (hasBullet(player, slot) || NBTHelper.getBoolean(stack, "READY")){
                string.append(new TranslationTextComponent("ready.weissrandom.itemRailgun").getUnformattedComponentText());
            }else{
                string.append(new TranslationTextComponent("empty.weissrandom.itemRailgun").getUnformattedComponentText());
            }
            returning = string.toString();
            TextComponent textComponent = new StringTextComponent(returning);
            list.add(textComponent);
        }
    }

//    @Override
//    public CreativeTabs getCreativeTab (){
//        return null;
//        //return CreativeTabs.COMBAT;
//    }
}
