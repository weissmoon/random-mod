package weissmoon.random.item;

import net.minecraft.item.*;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.crafting.*;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
//import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.core.item.WeissItem;
import weissmoon.core.item.WeissItemArrow;
import weissmoon.core.item.WeissItemFood;
import weissmoon.random.Randommod;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.compat.*;
import weissmoon.random.compat.aquaculture.AquacultureCompat;
import weissmoon.random.compat.atum.AtumCompat;
import weissmoon.random.container.ModContainers;
import weissmoon.random.item.equipment.*;
import weissmoon.random.item.equipment.armour.*;
import weissmoon.random.item.equipment.tools.daggers.*;
import weissmoon.random.item.equipment.tools.wood.*;
import weissmoon.random.item.food.*;
import weissmoon.random.item.equipment.tools.*;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;
import weissmoon.random.recipe.*;
//import weissmoon.random.recipe.*;


@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModItems{

    public static final WeissItem railgun = new ItemRailgun();
    public static final WeissItem dualFish = new ItemBattleFish();
    public static final WeissItemFood cheerio = new ItemCheerio();
    public static final WeissItem cheerioRing = new ItemCheerioRing();
    public static final WeissItem plungerLauncher = new ItemPlungerLauncher();
    public static final WeissItem sandal = new ItemSandal();
    public static final WeissItemFood chocolate = new ItemChocolate();
    public static final WeissItemArrow sonicArrow = new ItemSonicArrow();
    public static final WeissItem cornSeed = new ItemCornSeed();
    public static final WeissItemFood corn = new ItemCorn();
    public static final WeissItemFood cornCob = new ItemCornCob();
    public static final WeissItemFood popcorn = new ItemPopcorn();
    public static final WeissItemFood butterCorn = new ItemButterCorn();
    public static final ItemArmourWood woodHelmet = new ItemArmourWood(Strings.Items.WOOD_HELMET_NAME, 0, EquipmentSlotType.HEAD);
//    public static final ItemArmourWood woodChestpiece = new ItemArmourWood(Strings.Items.WOOD_CHESTPIECE_NAME, 1, EntityEquipmentSlot.CHEST);
//    public static final ItemArmourWood woodLeggins = new ItemArmourWood(Strings.Items.WOOD_LEGGING, 2, EntityEquipmentSlot.LEGS);
    public static final ItemArmourWood woodFeet = new ItemArmourWood(Strings.Items.WOOD_FEET_NAME, 3, EquipmentSlotType.FEET);
    public static final ItemQuiverBase quiver1 = new ItemQuiver1();
    public static final ItemCoalChunks coalFragments = new ItemCoalChunks();
    public static final ItemActivationArrow activationArrow = new ItemActivationArrow();
    public static final ItemArmourGilded gildedHelmet = new ItemArmourGilded(Strings.Items.GILDED_HELMET_NAME,  EquipmentSlotType.HEAD);
    public static final ItemArmourGilded gildedChestpiece = new ItemArmourGilded(Strings.Items.GILDED_CHESTPIECE_NAME, EquipmentSlotType.CHEST);
    public static final ItemArmourGilded gildedLeggins = new ItemArmourGilded(Strings.Items.GILDED_LEGGING_NAME, EquipmentSlotType.LEGS);
    public static final ItemArmourGilded gildedFeet = new ItemArmourGilded(Strings.Items.GILDED_FEET_NAME, EquipmentSlotType.FEET);
    public static final ItemGildedSpade gildedSpade = new ItemGildedSpade();
    public static final ItemGildedPickaxe gildedPickaxe = new ItemGildedPickaxe();
    public static final ItemGildedAxe gildedAxe = new ItemGildedAxe();
    public static final ItemGildedSword gildedSword = new ItemGildedSword();
    public static final ItemGildedHoe gildedHoe = new ItemGildedHoe();
    public static final ItemWoodSpade woodSpade = new ItemWoodSpade();
//    public static final ItemWoodPickaxe woodPickaxe = new ItemWoodPickaxe();
//    public static final ItemWoodAxe woodAxe = new ItemWoodAxe();
//    public static final ItemWoodHoe woodHoe = new ItemWoodHoe();
//    public static final ItemWoodSword woodSword = new ItemWoodSword();
    public static final ItemDagger woodDagger = new ItemDaggerWood();
    public static final ItemDagger stoneDagger = new ItemDagger(ItemTier.STONE, -2, "stone");
    public static final ItemDagger goldDagger = new ItemDagger(ItemTier.GOLD, 0, "gold");
    public static final ItemDagger ironDagger = new ItemDagger(ItemTier.IRON, -4, "iron");
    public static final ItemDagger diamondDagger = new ItemDagger(ItemTier.DIAMOND, -6, "diamond");
    public static final ItemDagger gildedDagger = new ItemDagger(WeissToolMaterials.GildedToolMaterial, -3.6F, "wmgildedtool");
    public static final ItemDaggerMana manasteelDagger;
    public static final ItemDaggerMana elementiumDagger;
    public static final ItemDaggerMana terrasteelDagger;
//    public static final ItemDagger thaumiumDagger;
//    public static final ItemDagger voidDagger;
//    public static final ItemDagger darkSteelDagger;
//    public static final ItemDagger endSteelDagger;
//    public static final ItemDagger stellarAlloyDagger;
    public static final WeissItemFood melonFire = new ItemMelonFire();
    public static final WeissItemFood melonEarth = new ItemMelonEarth();
    public static final WeissItemFood melonAir = new ItemMelonAir();
    public static final WeissItem melonFireSeed = new ItemMelonFireSeed();
    public static final WeissItem melonEarthSeed = new ItemMelonEarthSeed();
    public static final WeissItem melonAirSeed = new ItemMelonAirSeed();
//    public static final ItemDagger dawnstoneDagger;
    public static final ItemDagger neptuniumDagger;
    public static final ItemCoalChunks charcoalFragments = new ItemCoalChunks(false);
    public static final ItemHorseArmourGilded gildedHorseArmour = new ItemHorseArmourGilded(Strings.Items.GILDED_HORSE_ARMOUR_NAME, 1, "gilded", new Item.Properties().maxStackSize(1).group(ItemGroup.MISC));
    public static final Item gildedFilletKnife;
    public static final Item gildedFishingRod;
    public static final Item meatBunBeef = new ItemMeatBunBeef();
    public static final Item meatBunPork = new ItemMeatBunPork();
    public static final Item meatBunMutton = new ItemMeatBunMutton();
    public static final Item meatBunRabbit = new ItemMeatBunRabbit();
    public static final Item saltedBeef = new ItemSaltedBeef();
    public static final Item saltedPork = new ItemSaltedPork();
    public static final Item saltedMutton = new ItemSaltedMutton();
    public static final Item saltedRabbit = new ItemSaltedRabbit();
    public static final Item cleanFlesh = new ItemCleanFlesh();
    public static final Item honeyBread = new ItemHoneyBread();
    public static final Item salt = new ItemSalt();
    public static final ItemDagger limestoneDagger;
    public static final WeissItemFood mashedPotatoes = new WeissItemFood(8, 0.7F, false, false, false, Strings.Items.MASHED_POTATOES_NAME);
    public static final Item repairItem = new RepairItem();
    public static final ItemDesertArmour gildedDesertHelmet;
    public static final ItemDesertArmour gildedDesertChestpiece;
    public static final ItemDesertArmour gildedDesertLegging;
    public static final ItemDesertArmour gildedDesertFeet;
    public static final Item bambooCharcoal = new ItemBambooCharcoal();
    public static final Item friedPotatoWedgeRaw = new ItemFriedPotatoWedgeRaw();
    public static final Item friedPotatoWedge = new ItemFriedPotatoWedge();
    public static final Item friedPotatoRaw = new ItemFriedPotatoRaw();
    public static final Item friedPotato = new ItemFriedPotato();

    static{
        if(Randommod.botaniaLoaded){
            manasteelDagger = BotaniaCompat.manasteelDagger;
            elementiumDagger = BotaniaCompat.elementiumDagger;
            terrasteelDagger = BotaniaCompat.terrasteelDagger;
        }else{
                manasteelDagger = null;
                elementiumDagger = null;
                terrasteelDagger = null;
        }
//        if(Randommod.thaumcraftLoaded){
//            thaumiumDagger = ThaumcraftCompat.thaumiumDagger;
//            voidDagger = ThaumcraftCompat.voidDagger;
//        }else{
//            thaumiumDagger = null;
//            voidDagger = null;
//        }
//        if(Randommod.enderIOLoaded){
//            darkSteelDagger = EnderIOCompat.darkSteelDagger;
//            endSteelDagger = EnderIOCompat.endSteelDagger;
//            stellarAlloyDagger = EnderIOCompat.stellarAlloyDagger;
//        }else{
//            darkSteelDagger = null;
//            endSteelDagger = null;
//            stellarAlloyDagger = null;
//        }
//        if(Randommod.embersLoaded){
//            dawnstoneDagger = EmbersCompat.dawnstoneDagger;
//        }else{
//            dawnstoneDagger = null;
//        }
        if(Randommod.aquacultureLoaded){
            neptuniumDagger = AquacultureCompat.neptuniumDagger;
            gildedFilletKnife = AquacultureCompat.gildedFilletKnife;
            gildedFishingRod = AquacultureCompat.gildedFishingRod;
        }else{
            neptuniumDagger = null;
            gildedFilletKnife = null;
            gildedFishingRod = null;
        }
        if(Randommod.atumLoaded){
            limestoneDagger = AtumCompat.limestoneDagger;
            gildedDesertHelmet = AtumCompat.gildedDesertHelmet;
            gildedDesertChestpiece = AtumCompat.gildedDesertChestpiece;
            gildedDesertLegging = AtumCompat.gildedDesertLegging;
            gildedDesertFeet = AtumCompat.gildedDesertFeet;
        }else{
            limestoneDagger = null;
            gildedDesertHelmet = null;
            gildedDesertChestpiece = null;
            gildedDesertLegging = null;
            gildedDesertFeet = null;
        }
    }

    //public static final WeissDummy dummy = new RandomDummy();
    //public static final WeissItem effect = new EntitySpawner();

    public static void init (){
        MinecraftForge.EVENT_BUS.register(ModItems.class);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> evt) {
        IForgeRegistry<Item> GameRegistry = evt.getRegistry();


        GameRegistry.register(railgun);
        GameRegistry.register(cheerio);
        GameRegistry.register(cheerioRing);
        GameRegistry.register(plungerLauncher);
        GameRegistry.register(sandal);
        GameRegistry.register(chocolate);
        GameRegistry.register(dualFish);
        GameRegistry.register(sonicArrow);
        GameRegistry.register(cornSeed);
        GameRegistry.register(corn);
        //MinecraftForge.addGrassSeed(new ItemStack(cornSeed),1 );
        GameRegistry.register(cornCob);
        GameRegistry.register(popcorn);
        GameRegistry.register(butterCorn);
        GameRegistry.register(woodHelmet);
//        GameRegistry.register(woodChestpiece);
//        GameRegistry.register(woodLeggins);
        GameRegistry.register(woodFeet);
        GameRegistry.register(quiver1);
        GameRegistry.register(coalFragments);
        GameRegistry.register(charcoalFragments);
        GameRegistry.register(activationArrow);
        GameRegistry.register(gildedSpade);
        GameRegistry.register(gildedPickaxe);
        GameRegistry.register(gildedAxe);
        GameRegistry.register(gildedSword);
        GameRegistry.register(gildedHoe);
        GameRegistry.register(gildedHelmet);
        GameRegistry.register(gildedChestpiece);
        GameRegistry.register(gildedLeggins);
        GameRegistry.register(gildedFeet);
        GameRegistry.register(woodSpade);
//        GameRegistry.register(woodPickaxe);
//        GameRegistry.register(woodAxe);
//        GameRegistry.register(woodHoe);
//        GameRegistry.register(woodSword);
        GameRegistry.register(woodDagger);
        GameRegistry.register(stoneDagger);
        GameRegistry.register(ironDagger);
        GameRegistry.register(goldDagger);
        GameRegistry.register(diamondDagger);
        GameRegistry.register(gildedDagger);
        GameRegistry.register(melonFire);
        GameRegistry.register(melonEarth);
        GameRegistry.register(melonAir);
        GameRegistry.register(melonFireSeed);
        ModBlocks.stemFire.setSeedItem(melonFireSeed);
        GameRegistry.register(melonEarthSeed);
        ModBlocks.stemEarth.setSeedItem(melonEarthSeed);
        GameRegistry.register(melonAirSeed);
        ModBlocks.stemAir.setSeedItem(melonAirSeed);
        GameRegistry.register(gildedHorseArmour);
        GameRegistry.register(meatBunBeef);
        GameRegistry.register(meatBunPork);
        GameRegistry.register(meatBunMutton);
        GameRegistry.register(meatBunRabbit);
        GameRegistry.register(saltedBeef);
        GameRegistry.register(saltedPork);
        GameRegistry.register(saltedMutton);
        GameRegistry.register(saltedRabbit);
        GameRegistry.register(cleanFlesh);
        GameRegistry.register(honeyBread);
        GameRegistry.register(salt);
        GameRegistry.register(mashedPotatoes);
        GameRegistry.register(repairItem);
        GameRegistry.register(bambooCharcoal);
        GameRegistry.register(friedPotatoWedgeRaw);
        GameRegistry.register(friedPotatoWedge);
        GameRegistry.register(friedPotatoRaw);
        GameRegistry.register(friedPotato);

        if(Randommod.botaniaLoaded){
//            manasteelDagger = BotaniaCompat.manasteelDagger;
//            elementiumDagger = BotaniaCompat.elementiumDagger;
//            terrasteelDagger = BotaniaCompat.terrasteelDagger;
            GameRegistry.register(manasteelDagger);
            GameRegistry.register(elementiumDagger);
            GameRegistry.register(terrasteelDagger);
        }
//        if(Randommod.thaumcraftLoaded){
//            GameRegistry.register(thaumiumDagger);
//            GameRegistry.register(voidDagger);
//        }
//        if(Randommod.enderIOLoaded){
//            GameRegistry.register(darkSteelDagger);
//            GameRegistry.register(endSteelDagger);
//            //if(Loader.isModLoaded("enderioendergy"))
//                GameRegistry.register(stellarAlloyDagger);
//        }
//        if(Randommod.embersLoaded){
//            GameRegistry.register(dawnstoneDagger);
//        }
        if(Randommod.aquacultureLoaded){
//            neptuniumDagger = AquacultureCompat.neptuniumDagger;
            GameRegistry.register(neptuniumDagger);
//            gildedFilletKnife = AquacultureCompat.gildedFilletKnife;
//            gildedFishingRod = AquacultureCompat.gildedFishingRod;
            GameRegistry.register(gildedFilletKnife);
            GameRegistry.register(gildedFishingRod);
        }
        if(Randommod.atumLoaded){
            GameRegistry.register(limestoneDagger);
            GameRegistry.register(gildedDesertHelmet);
            GameRegistry.register(gildedDesertChestpiece);
            GameRegistry.register(gildedDesertLegging);
            GameRegistry.register(gildedDesertFeet);
        }
        //GameRegistry.registerItem(effect, "itemWeissSpawner");
    }

    @SubscribeEvent
    public static void addRecipes(RegistryEvent.Register<IRecipeSerializer<?>> event){
        IForgeRegistry<IRecipeSerializer<?>> GameRegistry = event.getRegistry();
//        //GameRegistry.register();
//        //GameRegistry.register("weissrandom:woodHelmet", WoodHelmetRecipe.class, RecipeSorter.Category.SHAPED, "");
        ModContainers.register(GameRegistry, WoodHelmetRecipe.SERIALIZER, WoodHelmetRecipe.location);
        ModContainers.register(GameRegistry, WoodBootsRecipe.SERIALIZER, WoodBootsRecipe.location);
        ModContainers.register(GameRegistry, ProjectTableRecipe.SERIALIZER, ProjectTableRecipe.location);
//        GameRegistry.remove(new ResourceLocation("minecraft:coal"));
//        net.minecraftforge.fml.common.registry.GameRegistry.addShapedRecipe(
//                new ResourceLocation("minecraft:coal"),
//                new ResourceLocation("coal"),
//                new ItemStack(Items.COAL),
//                "C", 'C', Blocks.COAL_BLOCK);
//        GameRegistry.remove(new ResourceLocation("minecraft:wooden_shovel"));
        ModContainers.register(GameRegistry, WoodSpadeRecipe.SERIALIZER, WoodSpadeRecipe.location);
        ModContainers.register(GameRegistry, WoodDaggerRecipe.SERIALIZER, WoodDaggerRecipe.location);
        ModContainers.register(GameRegistry, RepairRecipe.SERIALIZER, RepairRecipe.location);
        ModContainers.register(GameRegistry, CreativeRepairItemRecipe.SERIALIZER, CreativeRepairItemRecipe.location);
        ModContainers.register(GameRegistry, WoodSilentPressurePlateRecipe.SERIALIZER, WoodSilentPressurePlateRecipe.location);
        ModContainers.register(GameRegistry, WoodInvisiblePoressurePlateRecipe.SERIALIZER, WoodInvisiblePoressurePlateRecipe.location);
        ModContainers.register(GameRegistry, WoodSilenttoSilentInvisiblePoressurePlateRecipe.SERIALIZER, WoodSilenttoSilentInvisiblePoressurePlateRecipe.location);
        ModContainers.register(GameRegistry, WoodInvisiblet0SilentInvisiblePressurePlateRecipe.SERIALIZER, WoodInvisiblet0SilentInvisiblePressurePlateRecipe.location);
    }

    public static void initRecipies (){
        //GameRegistry.addRecipe(new ShapedOreRecipe(railgun, "IcI", "BBB", "tII", 'I', Blocks.IRON_BLOCK, 'c', WeissCore.newDummyItemStack(CoreDummyItems.lamp), 't', Blocks.STONE_BUTTON, 'B', Blocks.GOLDEN_RAIL));
    }

    public static void initDummyItems (){
        //dummy.addDummyItem(Strings.Items.PLUNGER_NAME);
    }
}
