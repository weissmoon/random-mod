package weissmoon.random.item;

import weissmoon.core.item.WeissItem;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 8/12/17.
 */
public class ItemSandal extends WeissItem {
    public ItemSandal() {
        super(Strings.Items.SANDLE_NAME);
    }
}
