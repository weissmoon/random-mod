package weissmoon.random.item.equipment;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.world.World;
import weissmoon.core.item.WeissItem;


/**
 * Created by Weissmoon on 5/18/19.
 */
//@Optional.InterfaceList({
//        @Optional.Interface(modid = "baubles", iface = "baubles.api.IBauble", striprefs = true),
//        @Optional.Interface(modid = "baubles", iface = "baubles.api.BaubleType", striprefs = true)
//})
public abstract class ItemQuiverBase extends WeissItem{//} implements IBauble {

    private int InventorySize;
    protected static final String ITEM_INVENTORY_NBT = "itemInv";

    public ItemQuiverBase(String name){
        super(name, new Item.Properties().group(ItemGroup.COMBAT).maxStackSize(1));
    }

    public abstract IInventory getQuiverInventory(ItemStack stack);

    public static IInventory getQuiverInventoryS(ItemStack stack){
        return ((ItemQuiverBase)stack.getItem()).getQuiverInventory(stack);
    }

    public static boolean isQuiver(ItemStack stack){
        return stack.getItem() instanceof ItemQuiverBase;
    }

    public static boolean hasArrowStatic(ItemStack quiverStack, ItemQuiverBase item){
        if(quiverStack.getItem() == item)
            return item.hasArrow(quiverStack);
        return false;
    }

    @Override
    public abstract ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand);
//    {
//        ItemStack stack = player.getHeldItem(hand);
//
//        if(hand == EnumHand.MAIN_HAND && player.getHeldItemOffhand().getItem() instanceof ItemBow){
//            return new ActionResult<ItemStack>(EnumActionResult.PASS, player.getHeldItem(hand));
//        }
//        if(hand == EnumHand.OFF_HAND && player.getHeldItemMainhand().getItem() instanceof ItemBow){
//            return new ActionResult<ItemStack>(EnumActionResult.PASS, player.getHeldItem(hand));
//        }
//
//        if(!world.isRemote) {
//            player.openGui();
//        }
//
//        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, stack);
//    }

    protected abstract boolean hasArrow(ItemStack quiverStack);

    public abstract ItemStack getActiveArrow(ItemStack quiverStack);

    protected static boolean isAmmo(ItemStack stack)
    {
        return stack.getItem() instanceof ArrowItem;
    }

//    @Override
//    public BaubleType getBaubleType(ItemStack itemstack) {
//        return BaubleType.BODY;
//    }

//    @Override
//    public boolean willAutoSync(ItemStack itemstack, LivingEntity player) {
//        return true;
//    }
}
