//package weissmoon.random.item.equipment.tools.daggers;
//
//import com.enderio.core.api.client.gui.IAdvancedTooltipProvider;
//import com.enderio.core.client.handlers.SpecialTooltipHandler;
//import com.enderio.core.common.interfaces.IOverlayRenderAware;
//import com.enderio.core.common.util.ItemUtil;
//import crazypants.enderio.api.capacitor.ICapacitorKey;
//import crazypants.enderio.api.upgrades.IDarkSteelItem;
//import crazypants.enderio.api.upgrades.IDarkSteelUpgrade;
//import crazypants.enderio.api.upgrades.IEquipmentData;
//import crazypants.enderio.base.capacitor.CapacitorKey;
//import crazypants.enderio.base.config.config.DarkSteelConfig;
//import crazypants.enderio.base.handler.darksteel.DarkSteelTooltipManager;
//import crazypants.enderio.base.handler.darksteel.SwordHandler;
//import crazypants.enderio.base.item.darksteel.upgrade.energy.EnergyUpgrade;
//import crazypants.enderio.base.item.darksteel.upgrade.energy.EnergyUpgradeManager;
//import crazypants.enderio.base.item.darksteel.upgrade.travel.TravelUpgrade;
//import crazypants.enderio.base.render.itemoverlay.PowerBarOverlayRenderHelper;
//import info.loenwind.autoconfig.factory.IValue;
//import net.minecraft.entity.LivingEntity;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.item.ItemStack;
//import net.minecraft.util.ActionResult;
//import net.minecraft.util.ActionResultType;
//import net.minecraft.util.Hand;
//import net.minecraft.world.World;
//import weissmoon.core.compat.OreDictionaryHelper;
//
//import javax.annotation.Nonnull;
//import javax.annotation.Nullable;
//import java.util.List;
//
///**
// * Created by Weissmoon on 11/9/19.
// */
//public class ItemDaggerEnder extends ItemDagger implements IDarkSteelItem,  IAdvancedTooltipProvider, IOverlayRenderAware {
//
//    private final @Nonnull IEquipmentData data;
//
//    public ItemDaggerEnder(@Nonnull IEquipmentData data, String name) {
//        super(data.getToolMaterial(), name);
//        this.data = data;
//    }
//
//    @Override
//    @Nonnull
//    public ActionResult<ItemStack> onItemRightClick(@Nonnull World world, @Nonnull PlayerEntity player, @Nonnull Hand hand) {
//        if (player.isSneaking()) {
//            if (!world.isRemote) {
//                //openUpgradeGui(player, hand);
//            }
//            return new ActionResult<>(ActionResultType.SUCCESS, player.getHeldItem(hand));
//        }
//
//        return super.onItemRightClick(world, player, hand);
//    }
//
//    @Override
//    public boolean hitEntity(@Nonnull ItemStack stack, @Nonnull LivingEntity entity, @Nonnull LivingEntity playerEntity) {
//
//        if (playerEntity instanceof PlayerEntity) {
//
//            PlayerEntity player = (PlayerEntity) playerEntity;
//
//            // Durability damage
//            EnergyUpgrade.EnergyUpgradeHolder eu = EnergyUpgradeManager.loadFromItem(stack);
//            if (eu != null && eu.isAbsorbDamageWithPower() && eu.getEnergy() > 0) {
//                eu.extractEnergy(getPowerPerDamagePoint(stack), false);
//
//            } else {
//                super.hitEntity(stack, entity, playerEntity);
//            }
//
//            // sword hit
//            if (eu != null) {
//                eu.writeToItem();
//
//                if (eu.getEnergy() >= DarkSteelConfig.darkSteelSwordPowerUsePerHit.get()) {
//                    extractInternal(player.getHeldItemMainhand(), DarkSteelConfig.darkSteelSwordPowerUsePerHit);
//                    entity.getPersistentData().putBoolean(SwordHandler.HIT_BY_DARK_STEEL_SWORD, true);
//                }
//
//            }
//
//        }
//        return true;
//    }
//
//    protected int getPowerPerDamagePoint(@Nonnull ItemStack stack) {
//        EnergyUpgrade.EnergyUpgradeHolder eu = EnergyUpgradeManager.loadFromItem(stack);
//        if (eu != null) {
//            return eu.getCapacity() / data.getToolMaterial().getMaxUses();
//        } else {
//            return 1;
//        }
//    }
//    @Override
//    public int getIngotsRequiredForFullRepair() {
//        return 1;
//    }
//
//    @Override
//    public boolean isItemForRepair(@Nonnull ItemStack right) {
//        return OreDictionaryHelper.isOre(right, data.getRepairIngotOredict());
//    }
//
//    @Override
//    public boolean isBlockBreakingTool() {
//        return true;
//    }
//
//    @Override
//    public boolean hasUpgradeCallbacks(@Nonnull IDarkSteelUpgrade upgrade) {
//        return upgrade == TravelUpgrade.INSTANCE;
//    }
//
//    @Nonnull
//    @Override
//    public IEquipmentData getEquipmentData() {
//        return this.data;
//    }
//
//    @Nonnull
//    @Override
//    public ICapacitorKey getEnergyStorageKey(@Nonnull ItemStack stack) {
//        return CapacitorKey.DARK_STEEL_SWORD_ENERGY_BUFFER;
//    }
//
//    @Nonnull
//    @Override
//    public ICapacitorKey getEnergyInputKey(@Nonnull ItemStack stack) {
//        return CapacitorKey.DARK_STEEL_SWORD_ENERGY_INPUT;
//    }
//
//    @Nonnull
//    @Override
//    public ICapacitorKey getEnergyUseKey(@Nonnull ItemStack stack) {
//        return CapacitorKey.DARK_STEEL_SWORD_ENERGY_USE;
//    }
//
//    @Nonnull
//    @Override
//    public ICapacitorKey getAbsorptionRatioKey(@Nonnull ItemStack stack) {
//        return CapacitorKey.DARK_STEEL_SWORD_ABSORPTION_RATIO;
//    }
//
////    @Override
////    public boolean isActive(@Nonnull EntityPlayer ep, @Nonnull ItemStack equipped) {
////        return isTravelUpgradeActive(ep, equipped);
////    }
////
////    private boolean isTravelUpgradeActive(@Nonnull EntityPlayer ep, @Nonnull ItemStack equipped) {
////        return ep.isSneaking() && TravelUpgrade.INSTANCE.hasUpgrade(equipped);
////    }
////
////    @Override
////    public void extractInternal(@Nonnull ItemStack item, int power) {
////        EnergyUpgradeManager.extractEnergy(item, this, power, false);
////    }
////
////    @Override
//    public void extractInternal(@Nonnull ItemStack item, IValue<Integer> power) {
//        EnergyUpgradeManager.extractEnergy(item, this, power, false);
//    }
////
////    @Override
////    public int getEnergyStored(@Nonnull ItemStack item) {
////        return EnergyUpgradeManager.getEnergyStored(item);
////    }
//
////    @Override
////    public void addCommonEntries(@Nonnull ItemStack itemstack, @Nullable PlayerEntity entityplayer, @Nonnull List<String> list, boolean flag) {
////        DarkSteelTooltipManager.addCommonTooltipEntries(itemstack, entityplayer, list, flag);
////    }
//
////    @Override
////    public void addBasicEntries(@Nonnull ItemStack itemstack, @Nullable PlayerEntity entityplayer, @Nonnull List<String> list, boolean flag) {
////        DarkSteelTooltipManager.addBasicTooltipEntries(itemstack, entityplayer, list, flag);
////    }
//
////    @Override
////    public void addDetailedEntries(@Nonnull ItemStack itemstack, @Nullable PlayerEntity entityplayer, @Nonnull List<String> list, boolean flag) {
////        if (!SpecialTooltipHandler.showDurability(flag)) {
////            list.add(ItemUtil.getDurabilityString(itemstack));
////        }
////        String str = EnergyUpgradeManager.getStoredEnergyString(itemstack);
////        if (str != null) {
////            list.add(str);
////        }
////        DarkSteelTooltipManager.addAdvancedTooltipEntries(itemstack, entityplayer, list, flag);
////    }
//
//    @Override
//    public void renderItemOverlayIntoGUI(@Nonnull ItemStack stack, int xPosition, int yPosition) {
//        PowerBarOverlayRenderHelper.instance_upgradeable.render(stack, xPosition, yPosition);
//    }
//}
