package weissmoon.random.item.equipment.tools;

import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.tools.WeissItemHoe;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class ItemGildedHoe extends WeissItemHoe implements IItemWeiss{

    public ItemGildedHoe() {
        super(WeissToolMaterials.GildedToolMaterial, 0, -1.2F, Strings.Items.GILDED_HOE_NAME);
    }
}
