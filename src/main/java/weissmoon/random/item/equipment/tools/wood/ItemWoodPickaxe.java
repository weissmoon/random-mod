package weissmoon.random.item.equipment.tools.wood;

import net.minecraft.item.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;
import weissmoon.random.item.IPlankHolder;

/**
 * Created by Weissmoon on 10/1/19.
 */
public class ItemWoodPickaxe extends PickaxeItem implements IPlankHolder, IItemWeiss {

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ItemWoodPickaxe() {
        super(ItemTier.WOOD, 1, -2.8F, (new Item.Properties()).group(ItemGroup.TOOLS));
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = "wooden_pickaxe";
        this.setRegistryName("minecraft:" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack) {
        return this.itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIconWeiss = iconRegister.registerIcon(this, getUnlocalizedName().substring(getUnlocalizedName().indexOf(".") + 1));
    }

    @Override
    public ITextComponent getDisplayName(ItemStack itemStack){
        if (getPlank(itemStack).getItem() == Items.AIR){
//            return new TextComponentTranslation("item.pickaxeWood.name").getUnformattedText();
        }
        return new TranslationTextComponent(this.getUnlocalizedName() + ".name", this.getPlank(itemStack).getDisplayName());
    }

    public String getUnlocalizedName()
    {
        return "item.weissrandom:itemWoodPickaxe";
    }
}
