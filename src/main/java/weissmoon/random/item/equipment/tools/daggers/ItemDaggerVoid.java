package weissmoon.random.item.equipment.tools.daggers;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.Effects;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
//import thaumcraft.api.items.IWarpingGear;//TODO

import java.util.List;

/**
 * Created by Weissmoon on 11/9/19.
 */
public class ItemDaggerVoid extends ItemDagger{//} implements IWarpingGear {
    public ItemDaggerVoid(ItemTier material, String name) {
        super(material, -2, name);
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int itemSlot, boolean isSelected){
        super.inventoryTick(stack, world, entity, itemSlot, isSelected);
        if(stack.isDamaged() && entity != null && entity.ticksExisted % 20 == 0 && entity instanceof LivingEntity){
            stack.damageItem(-1, (LivingEntity) entity, null);
        }
    }

    @Override
    public boolean hitEntity(ItemStack stack, LivingEntity target, LivingEntity attacker) {
        if(!target.world.isRemote && attacker instanceof PlayerEntity && (!(target instanceof PlayerEntity) || attacker.getServer().isPVPEnabled())) {
            target.addPotionEffect(new EffectInstance(Effects.WEAKNESS, 60));
        }
        return super.hitEntity(stack, target, attacker);
    }

    //@Override//TODO
    public int getWarp(ItemStack itemstack, PlayerEntity player) {
        return 1;
    }

    @OnlyIn(Dist.CLIENT)
    public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
        tooltip.add(new StringTextComponent(TextFormatting.GOLD.toString() + new TranslationTextComponent("enchantment.special.sapless")));
        super.addInformation(stack, worldIn, tooltip, flagIn);
    }
}
