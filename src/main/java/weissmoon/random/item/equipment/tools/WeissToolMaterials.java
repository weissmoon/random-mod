package weissmoon.random.item.equipment.tools;

import net.minecraft.item.IItemTier;
import weissmoon.core.item.tools.WeissItemTier;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class WeissToolMaterials {

    public static final IItemTier GildedToolMaterial = new WeissItemTier(
//            "wmGildedTool",
            3,
            299,
            5.8F,
            1.8F,
            22,
            null
    );

    public static final IItemTier RubyToolMaterial = new WeissItemTier(
//            "wmRuby",
            2,
            500,
            8.0F,
            3.0F,
            12,
            null
    );

    public static final IItemTier EmeraldToolMaterial = new WeissItemTier(
//            "wmEmerald",
            2,
            500,
            8.0F,
            3.0F,
            12,
            null
    );

    public static final IItemTier SapphireToolMaterial = new WeissItemTier(
//            "wmSapphire",
            2,
            500,
            8.0F,
            3.0F,
            12,
            null
    );

}
