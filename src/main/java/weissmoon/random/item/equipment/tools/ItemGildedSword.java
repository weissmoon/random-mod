package weissmoon.random.item.equipment.tools;

import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.tools.WeissItemSword;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class ItemGildedSword extends WeissItemSword implements IItemWeiss {

    public ItemGildedSword() {
        super(WeissToolMaterials.GildedToolMaterial, 3, -2.4F, Strings.Items.GILDED_SWORD_NAME);
    }
}
