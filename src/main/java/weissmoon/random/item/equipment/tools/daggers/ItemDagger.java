package weissmoon.random.item.equipment.tools.daggers;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.BlockState;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.Attributes;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.item.IItemTier;
import weissmoon.core.item.tools.WeissItemSword;

/**
 * Created by Weissmoon on 10/7/19.
 */
public class ItemDagger extends WeissItemSword {

    public final float daggerDamage;


    public ItemDagger(IItemTier material, float attackDamage, String materialName) {
        super(material, (float)((material.getAttackDamage() + 1 + attackDamage) / 2.0) + 1, -1, "itemdagger" + materialName);
        //this.setMaxDamage((int)(material.getMaxUses() * 1.5));//TODO
        this.daggerDamage = (float)((material.getAttackDamage() + 1 - attackDamage) / 2.0) + 1;
    }

    @Override
    public float getAttackDamage()
    {
        return this.daggerDamage;
    }

    @Override
    public float getDestroySpeed(ItemStack stack, BlockState state)
    {
        Material block = state.getMaterial();

        if (block == Material.WEB)
        {
            return 10.0F;
        }
        else
        {
            Material material = state.getMaterial();
            return material != Material.PLANTS && material != Material.TALL_PLANTS && material != Material.CORAL && material != Material.LEAVES && material != Material.GOURD ? 1.0F : 3F;
        }
    }
}
