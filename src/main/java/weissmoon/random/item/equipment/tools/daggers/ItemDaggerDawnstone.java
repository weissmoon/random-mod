package weissmoon.random.item.equipment.tools.daggers;

import net.minecraft.item.IItemTier;

/**
 * Created by Weissmoon on 11/17/19.
 */
public class ItemDaggerDawnstone extends ItemDagger {
    public ItemDaggerDawnstone(IItemTier material) {
        super(material, -2, "dawnstone");
    }
}
