package weissmoon.random.item.equipment.tools;

import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.tools.WeissItemSpade;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class ItemGildedSpade extends WeissItemSpade implements IItemWeiss {

    public ItemGildedSpade() {
        super(WeissToolMaterials.GildedToolMaterial, 3.3F, -3, Strings.Items.GILDED_SHOVEL_NAME);
    }
}
