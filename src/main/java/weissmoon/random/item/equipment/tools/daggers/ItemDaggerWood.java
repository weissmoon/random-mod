package weissmoon.random.item.equipment.tools.daggers;

import net.minecraft.item.ItemGroup;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemTier;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.random.item.IPlankHolder;

/**
 * Created by Weissmoon on 10/26/19.
 */
public class ItemDaggerWood extends ItemDagger implements IPlankHolder {

    public ItemDaggerWood() {
        super(ItemTier.WOOD, 0, "wood");
    }

    @Override
    public ITextComponent getDisplayName(ItemStack itemStack) {
        return new TranslationTextComponent(this.getTranslationKey(), this.getPlank(itemStack).getDisplayName());
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> list) {
        if(tab == ItemGroup.COMBAT) {
            ItemStack out = new ItemStack(this);
            ((ItemDaggerWood) out.getItem()).setPlank(out, new ItemStack(Blocks.OAK_PLANKS));
            list.add(out);
        }
    }
}
