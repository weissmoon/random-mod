package weissmoon.random.item.equipment.tools;

import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.tools.WeissItemsPickaxe;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class ItemGildedPickaxe extends WeissItemsPickaxe implements IItemWeiss {

    public ItemGildedPickaxe() {
        super(WeissToolMaterials.GildedToolMaterial, 2.8F, -2.8F, Strings.Items.GILDED_PICKAXE_NAME);
    }
}
