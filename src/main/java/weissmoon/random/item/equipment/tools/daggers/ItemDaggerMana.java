package weissmoon.random.item.equipment.tools.daggers;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.IItemTier;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import vazkii.botania.api.mana.IManaUsingItem;
import vazkii.botania.api.mana.ManaItemHandler;

/**
 * Created by Weissmoon on 10/26/19.
 */
public class ItemDaggerMana extends ItemDagger implements IManaUsingItem{

    private int manaPerDamage;

    public ItemDaggerMana(IItemTier material, int mana, float attackDamage, String name) {
        super(material, attackDamage, name);
        this.manaPerDamage = mana;
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity player, int par4, boolean par5) {
        if (!world.isRemote && player instanceof PlayerEntity && stack.getDamage() > 0 && ManaItemHandler.instance().requestManaExactForTool(stack, (PlayerEntity) player, getManaPerDamage() * 2, true))
            stack.setDamage(stack.getDamage() - 1);
    }

    @Override
    public boolean usesMana(ItemStack stack) {
        return true;
    }

    public int getManaPerDamage() {
        return manaPerDamage;
    }
}
