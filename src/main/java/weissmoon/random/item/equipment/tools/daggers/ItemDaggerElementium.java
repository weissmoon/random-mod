package weissmoon.random.item.equipment.tools.daggers;

import com.google.common.collect.Multimap;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IItemTier;
import vazkii.botania.common.core.handler.PixieHandler;

import javax.annotation.Nonnull;

/**
 * Created by Weissmoon on 10/28/19.
 */
public class ItemDaggerElementium extends ItemDaggerMana {
    public ItemDaggerElementium(IItemTier material, int mana, float attackDamage, String name) {
        super(material, mana, attackDamage, name);
    }

    @Override
    public Multimap<Attribute, AttributeModifier> getAttributeModifiers(@Nonnull EquipmentSlotType slot) {
        Multimap<Attribute, AttributeModifier> ret = super.getAttributeModifiers(slot);
        if (slot == EquipmentSlotType.MAINHAND) {
            ret.put(PixieHandler.PIXIE_SPAWN_CHANCE, PixieHandler.makeModifier(slot, "Sword modifier", 0.05D));
        }

        return ret;
    }
}
