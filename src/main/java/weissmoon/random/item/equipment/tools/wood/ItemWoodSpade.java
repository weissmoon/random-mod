package weissmoon.random.item.equipment.tools.wood;

import net.minecraft.item.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;
import weissmoon.random.item.IPlankHolder;

/**
 * Created by Weissmoon on 10/1/19.
 */
public class ItemWoodSpade extends ShovelItem implements IPlankHolder, IItemWeiss {

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ItemWoodSpade() {
        super(ItemTier.WOOD, 1.5F, -3.0F, (new Item.Properties()).group(ItemGroup.TOOLS));
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = "wooden_shovel";
        this.setRegistryName("minecraft:" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public final String getModID() {
        return "minecraft";
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    public IIcon getIcon(ItemStack stack) {
        return this.itemIconWeiss;
    }

    @Override
    public void registerIcons(IIconRegister iconRegister) {
        this.itemIconWeiss = iconRegister.registerIcon(this, "minecraft:"+RegName);
    }

    @Override
    public ITextComponent getDisplayName(ItemStack itemStack){
        if (getPlank(itemStack).getItem() == Items.AIR){
            return new TranslationTextComponent("item.minecraft.wooden_shovel");
        }
        return new TranslationTextComponent(this.getUnlocalizedName(), this.getPlank(itemStack).getDisplayName());
    }

    public String getUnlocalizedName()
    {
        return "item.weissrandom.itemwoodspade";
    }
}
