package weissmoon.random.item.equipment.tools;

import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.tools.WeissItemAxe;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 7/31/19.
 */
public class ItemGildedAxe extends WeissItemAxe implements IItemWeiss {

    public ItemGildedAxe() {
        super(WeissToolMaterials.GildedToolMaterial, 7.8F, -3.02F, Strings.Items.GILDED_AXE_NAME);
    }
}
