package weissmoon.random.item.equipment;

import net.minecraft.item.HorseArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;

/**
 * Created by Weissmoon on 1/7/20.
 */
public class ItemHorseArmourGilded extends HorseArmorItem implements IItemWeiss {

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ItemHorseArmourGilded(String name, int p_i50042_1_, String p_i50042_2_, Properties p_i50042_3_) {
        super(p_i50042_1_, new ResourceLocation(ModLoadingContext.get().getActiveContainer().getModId(), "textures/entity/horse/armor/horse_armor_" + p_i50042_2_ + ".png"), p_i50042_3_);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }
}
