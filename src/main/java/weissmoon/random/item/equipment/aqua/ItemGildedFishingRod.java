package weissmoon.random.item.equipment.aqua;

import com.teammetallurgy.aquaculture.Aquaculture;
import com.teammetallurgy.aquaculture.item.AquaFishingRodItem;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;
import weissmoon.random.item.equipment.tools.WeissToolMaterials;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/22/20.
 */
public class ItemGildedFishingRod extends AquaFishingRodItem implements IItemWeiss{

    @OnlyIn(Dist.CLIENT)
    protected IIcon itemIconWeiss;
    @OnlyIn(Dist.CLIENT)
    protected IIcon itemIconCast;
    private final String ModId;
    protected final String RegName;

    public ItemGildedFishingRod() {
        super(WeissToolMaterials.GildedToolMaterial, new Properties().defaultMaxDamage(100).group(Aquaculture.GROUP));
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = Strings.Items.GILDED_FISHING_ROD_NAME;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public IIcon getIcon(ItemStack stack){
        return this.itemIconWeiss;
    }

    @Override
    @OnlyIn(Dist.CLIENT)
    public void registerIcons(IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
        this.itemIconCast = iconRegister.registerIcon(this, ModId+":"+RegName+"cast");
    }
}
