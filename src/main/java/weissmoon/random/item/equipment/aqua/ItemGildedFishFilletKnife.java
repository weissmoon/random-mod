package weissmoon.random.item.equipment.aqua;

import com.teammetallurgy.aquaculture.item.ItemFilletKnife;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.client.render.IIcon;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissItemRegistry;
import weissmoon.core.item.IItemWeiss;
import weissmoon.random.item.equipment.tools.WeissToolMaterials;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/22/20.
 */
public class ItemGildedFishFilletKnife extends ItemFilletKnife implements IItemWeiss {

    protected IIcon itemIconWeiss;
    private final String ModId;
    protected final String RegName;

    public ItemGildedFishFilletKnife() {
        super(WeissToolMaterials.GildedToolMaterial);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = Strings.Items.GILDED_FILLET_KNIFE_NAME;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissItemRegistry.weissItemRegistry.regItem(this);
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IIcon getIcon (ItemStack stack){
        return this.itemIconWeiss;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void registerIcons (IIconRegister iconRegister){
        this.itemIconWeiss = iconRegister.registerIcon(this, ModId+":"+RegName);
    }
}
