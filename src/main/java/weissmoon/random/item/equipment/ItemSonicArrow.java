package weissmoon.random.item.equipment;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.WeissItemArrow;
import weissmoon.random.client.render.item.RenderSonicArrow;
import weissmoon.random.entity.SonicArrowEntity;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/4/19.
 */
public class ItemSonicArrow extends WeissItemArrow implements IItemRenderCustom {

    public ItemSonicArrow() {
        super(Strings.Items.SONIC_ARROW_NAME, new Item.Properties().group(ItemGroup.COMBAT));
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        return new SonicArrowEntity(worldIn, shooter);
    }

//    @SideOnly(Side.CLIENT)
//    public void registerIcons (IIconRegister iconRegister){
//        //this.itemIconWeiss = iconRegister.registerIcon(this, "itemsonicarrow");
//    }

    @Override
    public IItemRenderer getIItemRender() {
        return new RenderSonicArrow();
    }
}
