package weissmoon.random.item.equipment;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import weissmoon.core.item.WeissItemArrow;
import weissmoon.random.entity.ActivationArrowEntity;
import weissmoon.random.lib.Strings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 7/10/19.
 */
public class ItemActivationArrow extends WeissItemArrow {

    public ItemActivationArrow() {
        super(Strings.Items.ACTIVATION_ARROW_NAME);
    }

    @Override
    public AbstractArrowEntity createArrow(World worldIn, ItemStack stack, LivingEntity shooter) {
        return new ActivationArrowEntity(worldIn, shooter);
    }

    @Override
    public java.util.Collection<ItemGroup> getCreativeTabs()
    {
        List<ItemGroup> list  = new ArrayList<>();
        list.add(ItemGroup.COMBAT);
        list.add(ItemGroup.REDSTONE);
        return list;
    }
}
