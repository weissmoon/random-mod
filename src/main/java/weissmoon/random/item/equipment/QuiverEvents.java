package weissmoon.random.item.equipment;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.util.*;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.player.ArrowLooseEvent;
import net.minecraftforge.event.entity.player.ArrowNockEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.helper.RNGHelper;
import weissmoon.random.Randommod;
import weissmoon.random.compat.BaublesCompat;
import weissmoon.random.compat.CuriosCompat;

import static weissmoon.random.item.equipment.ItemQuiverBase.*;

/**
 * Created by Weissmoon on 8/30/19.
 */
public class QuiverEvents {

    @SubscribeEvent
    public void onArrowNock(ArrowNockEvent event){

        //activate bow if quiver and ammo is available
        PlayerEntity player = event.getPlayer();

        if (isQuiver(player.getHeldItem(Hand.OFF_HAND)))
        {
            ItemStack offHand = player.getHeldItem(Hand.OFF_HAND);
            if(hasArrowStatic(offHand, (ItemQuiverBase)offHand.getItem())){
                event.setAction(new ActionResult<>(ActionResultType.SUCCESS, event.getBow()));
                player.setActiveHand(Hand.MAIN_HAND);
                return;
//            }else{
//                do NOT open Gui if quiver is empty
//                event.setAction(new ActionResult<ItemStack>(ActionResultType.PASS, event.getBow()));
            }
        }
        if (isQuiver(player.getHeldItem(Hand.MAIN_HAND)))
        {
            ItemStack mainHand = player.getHeldItem(Hand.MAIN_HAND);
            if(hasArrowStatic(mainHand, (ItemQuiverBase)mainHand.getItem())){
                event.setAction(new ActionResult<>(ActionResultType.SUCCESS, event.getBow()));
                player.setActiveHand(Hand.OFF_HAND);
                return;
            }
        }
        if(Randommod.baublesLoaded)
        {
            if(BaublesCompat.hasQuiverBauble(player)){
                if(hasArrowStatic(BaublesCompat.getFirstQuiver(player), (ItemQuiverBase) BaublesCompat.getFirstQuiver(player).getItem())) {
                    event.setAction(new ActionResult<>(ActionResultType.SUCCESS, event.getBow()));
                    player.setActiveHand(event.getHand());
                    return;
                }
            }
        }
        if(Randommod.curiosLoaded)
        {
            if(CuriosCompat.hasQuiverCurio(player)){
                if(hasArrowStatic(CuriosCompat.getFirstQuiver(player), (ItemQuiverBase) CuriosCompat.getFirstQuiver(player).getItem())) {
                    event.setAction(new ActionResult<>(ActionResultType.SUCCESS, event.getBow()));
                    player.setActiveHand(event.getHand());
                    return;
                }
            }
        }
        {
            for (int i = 0; i < player.inventory.getSizeInventory(); ++i)
            {
                ItemStack itemstack = player.inventory.getStackInSlot(i);

                if (isQuiver(itemstack))
                {
                    if(hasArrowStatic(itemstack, (ItemQuiverBase)itemstack.getItem())){
                        event.setAction(new ActionResult<>(ActionResultType.SUCCESS, event.getBow()));
                        player.setActiveHand(event.getHand());
                        return;
                    }
                }
            }
        }
    }

    @SubscribeEvent
    public void onArrowLoose(ArrowLooseEvent event){

        //set up variables
        PlayerEntity entityplayer = event.getPlayer();
        World worldIn = event.getWorld();
        int i = event.getCharge();
        boolean flag = event.hasAmmo();
        ItemStack bowStack = event.getBow();
        ItemStack arrowStack = ItemStack.EMPTY;



        ItemStack quiverStack = ItemStack.EMPTY;

        boolean usedQuiver = false;

        //find first quiver or arrow
        quiver:
        {
            if (isQuiver(entityplayer.getHeldItem(Hand.OFF_HAND))){
                ItemStack offHandStack = entityplayer.getHeldItem(Hand.OFF_HAND);
                if(hasArrowStatic(offHandStack, (ItemQuiverBase)offHandStack.getItem())){
                    arrowStack = ((ItemQuiverBase)offHandStack.getItem()).getActiveArrow(offHandStack);
                    quiverStack = offHandStack;
                    usedQuiver = true;
                    break quiver;
                }
            }else if(isAmmo(entityplayer.getHeldItem(Hand.OFF_HAND))){
                //arrowStack = entityplayer.getHeldItem(Hand.OFF_HAND);
                return;
            }
            if(isQuiver(entityplayer.getHeldItem(Hand.MAIN_HAND))){
                ItemStack mainHandStack = entityplayer.getHeldItem(Hand.MAIN_HAND);
                if(hasArrowStatic(mainHandStack, (ItemQuiverBase)mainHandStack.getItem())){
                    arrowStack = ((ItemQuiverBase)mainHandStack.getItem()).getActiveArrow(mainHandStack);
                    quiverStack = mainHandStack;
                    usedQuiver = true;
                    break quiver;
                    //event.setAction(new ActionResult<ItemStack>(ActionResultType.SUCCESS, event.getBow()));
                }
            }else if(isAmmo(entityplayer.getHeldItem(Hand.MAIN_HAND))){
                //arrowStack = entityplayer.getHeldItem(Hand.MAIN_HAND);
                return;
            }
//            if(Randommod.baublesLoaded) {
//                quiverStack = BaublesCompat.getFirstQuiver(entityplayer);
//                if (quiverStack != ItemStack.EMPTY) {
//                    arrowStack = ((ItemQuiverBase) quiverStack.getItem()).getActiveArrow(quiverStack);
//                    usedQuiver = true;
//                    break quiver;
//                }
//            }
            if(Randommod.curiosLoaded) {
                quiverStack = CuriosCompat.getFirstQuiver(entityplayer);
                if (quiverStack != ItemStack.EMPTY) {
                    arrowStack = ((ItemQuiverBase) quiverStack.getItem()).getActiveArrow(quiverStack);
                    usedQuiver = true;
                    break quiver;
                }
            }
            for (int inv = 0; inv < entityplayer.inventory.getSizeInventory(); ++inv){
                ItemStack itemStack = entityplayer.inventory.getStackInSlot(inv);

                if (isQuiver(itemStack)){
                    if(hasArrowStatic(itemStack, (ItemQuiverBase)itemStack.getItem())){
                        arrowStack = ((ItemQuiverBase)itemStack.getItem()).getActiveArrow(itemStack);
                        quiverStack = itemStack;
                        usedQuiver = true;
                        //event.setAction(new ActionResult<ItemStack>(ActionResultType.SUCCESS, event.getBow()));
                        break quiver;
                    }
                }else if(isAmmo(itemStack)){
//                    arrowStack = itemStack;
//                    break;
                    return;
                }
            }
        }



        if (i < 0)
            return;

        if (!arrowStack.isEmpty() || flag)
        {
            if (arrowStack.isEmpty())
            {
                arrowStack = new ItemStack(Items.ARROW);
            }

            float f = BowItem.getArrowVelocity(i);

            if ((double)f >= 0.1D)
            {
                boolean flag1 = entityplayer.abilities.isCreativeMode || (arrowStack.getItem() instanceof ArrowItem && ((ArrowItem) arrowStack.getItem()).isInfinite(arrowStack, bowStack, entityplayer));

                if (!worldIn.isRemote)
                {
                    ArrowItem itemarrow = (ArrowItem)(arrowStack.getItem() instanceof ArrowItem ? arrowStack.getItem() : Items.ARROW);
                    AbstractArrowEntity entityarrow = itemarrow.createArrow(worldIn, arrowStack, entityplayer);
                    entityarrow =((BowItem) bowStack.getItem()).customArrow(entityarrow);
                    entityarrow.shoot(entityplayer.getPosX(), entityarrow.getPosY(), entityarrow.getPosZ(), f * 3.0F, 0F);

                    if (f == 1.0F)
                    {
                        entityarrow.setIsCritical(true);
                    }

                    int j = EnchantmentHelper.getEnchantmentLevel(Enchantments.POWER, bowStack);

                    if (j > 0)
                    {
                        entityarrow.setDamage(entityarrow.getDamage() + (double)j * 0.5D + 0.5D);
                    }

                    int k = EnchantmentHelper.getEnchantmentLevel(Enchantments.PUNCH, bowStack);

                    if (k > 0)
                    {
                        entityarrow.setKnockbackStrength(k);
                    }

                    if (EnchantmentHelper.getEnchantmentLevel(Enchantments.FLAME, bowStack) > 0)
                    {
                        entityarrow.setFire(100);
                    }

                    if(bowStack.getItem().isDamageable()) {
                        bowStack.damageItem(1, entityplayer, handler -> {
                            handler.sendBreakAnimation(entityplayer.getActiveHand());
                        });
                    }

                    if (flag1 || entityplayer.abilities.isCreativeMode && (arrowStack.getItem() == Items.SPECTRAL_ARROW || arrowStack.getItem() == Items.TIPPED_ARROW))
                    {
                        entityarrow.pickupStatus = AbstractArrowEntity.PickupStatus.CREATIVE_ONLY;
                    }

                    worldIn.addEntity(entityarrow);
                }

                worldIn.playSound(null, entityplayer.getPosX(), entityplayer.getPosY(), entityplayer.getPosZ(), SoundEvents.ENTITY_ARROW_SHOOT, SoundCategory.PLAYERS, 1.0F, 1.0F / (RNGHelper.getFloat() * 0.4F + 1.2F) + f * 0.5F);

                if (!flag1 && !entityplayer.abilities.isCreativeMode)
                {
                    if(usedQuiver){
                        IInventory quiverInventory = ((ItemQuiverBase)quiverStack.getItem()).getQuiverInventory(quiverStack);
                        for(int qi = 0;qi < quiverInventory.getSizeInventory(); qi++){
                            if(!quiverInventory.getStackInSlot(qi).isEmpty()){
                                quiverInventory.decrStackSize(qi, 1);
                                break;
                            }
                        }
//                    }else{
//                        arrowStack.shrink(1);
//
//                        if (arrowStack.isEmpty()) {
//                            entityplayer.inventory.deleteStack(arrowStack);
//                        }
                    }
                }

                entityplayer.addStat(Stats.ITEM_USED.get(event.getBow().getItem()));
                event.setCharge(0);
            }
        }
    }

}
