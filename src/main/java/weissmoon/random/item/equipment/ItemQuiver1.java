package weissmoon.random.item.equipment;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.inventory.container.SimpleNamedContainerProvider;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.NetworkHooks;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.Randommod;
import weissmoon.random.container.ContainerQuiver1;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 5/18/19.
 */
public class ItemQuiver1 extends ItemQuiverBase{

    public ItemQuiver1() {
        super(Strings.Items.QUIVER_1_NAME);
    }

    @Override
    public IInventory getQuiverInventory(ItemStack stack) {
        return new Quiver1Inventory(stack);
    }

    @Override
    protected boolean hasArrow(ItemStack quiverStack) {
        return !(new Quiver1Inventory(quiverStack).isEmpty());
    }

    @Override
    public ItemStack getActiveArrow(ItemStack quiverStack) {
        for (ItemStack itemStack:new Quiver1Inventory(quiverStack).chestContents){
            if (!itemStack.isEmpty()){
                return itemStack;
            }
        }
        return ItemStack.EMPTY;
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, PlayerEntity player, Hand hand) {

        ItemStack stack = player.getHeldItem(hand);

        //Do not open Gui if also holding bow
        if(hand == Hand.MAIN_HAND && player.getHeldItemOffhand().getItem() instanceof BowItem){
            return new ActionResult(ActionResultType.PASS, player.getHeldItem(hand));
        }
        if(hand == Hand.OFF_HAND && player.getHeldItemMainhand().getItem() instanceof BowItem){
            return new ActionResult(ActionResultType.PASS, player.getHeldItem(hand));
        }

        if(!world.isRemote) {
            SimpleNamedContainerProvider container = new SimpleNamedContainerProvider(
                    (id, playerInventory, playerEntity) -> new ContainerQuiver1(id, playerInventory, getQuiverInventoryS(player.getHeldItem(hand))),
                    stack.getDisplayName());
            player.openContainer(container);
            NetworkHooks.openGui((ServerPlayerEntity) player, container);
        }

        return new ActionResult(ActionResultType.SUCCESS, stack);
    }


    public class Quiver1Inventory implements IInventory {

        protected NonNullList<ItemStack> chestContents;
        private ItemStack itemStack;

//        public Quiver1Inventory(){
//
//        }

        public Quiver1Inventory(ItemStack stack){
            this.itemStack = stack;

            CompoundNBT compound = NBTHelper.getTagCompound(stack, ITEM_INVENTORY_NBT);

            this.chestContents = NonNullList.withSize(this.getSizeInventory(), ItemStack.EMPTY);

            ItemStackHelper.loadAllItems(compound, this.chestContents);
        }

        @Override
        public int getSizeInventory() {
            return 3;
        }

        @Override
        public boolean isEmpty() {

            for (ItemStack itemstack : this.chestContents)
            {
                if (!itemstack.isEmpty())
                {
                    return false;
                }
            }

            return true;
        }

        @Override
        public ItemStack getStackInSlot(int index) {
            return chestContents.get(index);
        }

        @Override
        public ItemStack decrStackSize(int index, int count) {
            ItemStack itemstack = ItemStackHelper.getAndSplit(chestContents, index, count);

            if (!itemstack.isEmpty())
            {
                this.markDirty();
            }

            return itemstack;
        }

        @Override
        public ItemStack removeStackFromSlot(int index) {
            ItemStack itemstack = this.chestContents.get(index);

            if (itemstack.isEmpty())
            {
                return ItemStack.EMPTY;
            }
            else
            {
                this.chestContents.set(index, ItemStack.EMPTY);
                return itemstack;
            }
        }

        @Override
        public void setInventorySlotContents(int index, ItemStack stack) {
            chestContents.set(index, stack);

            if (!stack.isEmpty() && stack.getCount() > this.getInventoryStackLimit())
            {
                stack.setCount(this.getInventoryStackLimit());
            }

            this.markDirty();
        }

        @Override
        public int getInventoryStackLimit() {
            return 64;
        }

        @Override
        public void markDirty() {
            CompoundNBT compound = new CompoundNBT();

            ItemStackHelper.saveAllItems(compound, this.chestContents);

            NBTHelper.setTagCompound(this.itemStack, ITEM_INVENTORY_NBT, compound);
        }

        @Override
        public boolean isUsableByPlayer(PlayerEntity player) {
            return true;
        }

        @Override
        public void openInventory(PlayerEntity player) {

        }

        @Override
        public void closeInventory(PlayerEntity player) {

        }

        @Override
        public boolean isItemValidForSlot(int index, ItemStack stack) {
            return (stack.getItem() instanceof ArrowItem);
        }


        @Override
        public void clear() {

        }

        //@Override
        public String getName() {
            return null;
        }

        //@Override
        public boolean hasCustomName() {
            return false;
        }

        //@Override
        public ITextComponent getDisplayName() {
            return null;
        }
    }
}
