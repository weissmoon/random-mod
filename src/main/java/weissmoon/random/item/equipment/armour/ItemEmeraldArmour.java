package weissmoon.random.item.equipment.armour;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;

import javax.annotation.Nonnull;

/**
 * Created by Weissmoon on 2/15/19.
 */
public class ItemEmeraldArmour extends ArmorItem {//} implements ISpecialArmor {
    public ItemEmeraldArmour(IArmorMaterial materialIn, int renderIndexIn, EquipmentSlotType equipmentSlotIn) {
        super(materialIn, equipmentSlotIn, new Properties());
    }

//    @Override
//    public ArmorProperties getProperties(LivingEntity player, @Nonnull ItemStack armor, DamageSource source, double damage, int slot) {
//        return null;
//    }
//
//    @Override
//    public int getArmorDisplay(PlayerEntity player, @Nonnull ItemStack armor, int slot) {
//        return 0;
//    }
//
//    @Override
//    public void damageArmor(LivingEntity entity, @Nonnull ItemStack stack, DamageSource source, int damage, int slot) {
//
//    }
}
