package weissmoon.random.item.equipment.armour;

import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.item.ItemGroup;
import net.minecraft.entity.LivingEntity;
import net.minecraft.block.Blocks;
import net.minecraft.util.SoundEvents;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.armour.ItemArmourBase;
import weissmoon.core.item.armour.WeissArmorMaterial;
import weissmoon.random.client.render.armour.WoodArmourModel;
import weissmoon.random.client.render.item.RenderItemWoodHelmet;
import weissmoon.random.item.IPlankHolder;

/**
 * Created by Weissmoon on 4/5/19.
 */
public class ItemArmourWood extends ItemArmourBase implements IItemRenderCustom, IPlankHolder{

    private static WoodArmourModel model;

    private static final IArmorMaterial material = new WeissArmorMaterial(
            "wmwood",
            5,
            new int[]{1, 1, 2, 1},
            20,
            SoundEvents.ITEM_ARMOR_EQUIP_LEATHER,
            0.0F,
            null);


    public ItemArmourWood(String name, int renderIndexIn, EquipmentSlotType equipmentSlotIn) {
        super(name, material, equipmentSlotIn);
    }

    @Override
    public ITextComponent getDisplayName(ItemStack itemStack){
        return new TranslationTextComponent(this.getTranslationKey() , this.getPlank(itemStack).getDisplayName());
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public BipedModel getArmorModel(LivingEntity entityLiving, ItemStack itemStack, EquipmentSlotType armorSlot, BipedModel original) {
        if(model == null)
            model = new WoodArmourModel(1);
        model.setEntity(entityLiving);
        return model;
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public void fillItemGroup(ItemGroup tab, NonNullList<ItemStack> list){
        if(tab == ItemGroup.COMBAT) {
            ItemStack outputasdfrwe = new ItemStack(this);
            ((ItemArmourWood) outputasdfrwe.getItem()).setPlank(outputasdfrwe, new ItemStack(Blocks.OAK_LOG));
            list.add(outputasdfrwe);
        }
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public IItemRenderer getIItemRender() {
        return new RenderItemWoodHelmet();
    }
}
