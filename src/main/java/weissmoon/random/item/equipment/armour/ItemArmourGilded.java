package weissmoon.random.item.equipment.armour;

import net.minecraft.entity.Entity;
import net.minecraft.util.SoundEvents;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.ItemStack;
import weissmoon.core.item.armour.ItemArmourBase;
import weissmoon.core.item.armour.WeissArmorMaterial;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 7/30/19.
 */
public class ItemArmourGilded extends ItemArmourBase {

    static final IArmorMaterial material = new WeissArmorMaterial(
            "wmGilded",
            16,
            new int[]{2, 5, 6, 2},
            23,
            SoundEvents.ITEM_ARMOR_EQUIP_IRON,
            0.0F,
            null);


    public ItemArmourGilded(String name, EquipmentSlotType equipmentSlotIn){
        super(name, material, equipmentSlotIn);
    }

    @Nullable
    public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type){
        return slot.getIndex() != 1 ? "weissrandom:textures/armor/gildedlayer1.png" : "weissrandom:textures/armor/gildedlayer2.png";
    }
}
