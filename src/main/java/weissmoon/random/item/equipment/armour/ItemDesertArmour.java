package weissmoon.random.item.equipment.armour;

import com.teammetallurgy.atum.api.IFogReductionItem;
import net.minecraft.entity.Entity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.IDyeableArmorItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 9/10/21.
 */
public class ItemDesertArmour extends ItemArmourGilded implements IFogReductionItem, IDyeableArmorItem{
    public ItemDesertArmour(String name, EquipmentSlotType equipmentSlotIn){
        super(name, equipmentSlotIn);
    }

    @Override
    public float getFogReduction(float fogDensity, ItemStack armorItem){
        return fogDensity / 2F;
    }

    @Override
    public int getColor(@Nonnull ItemStack stack) {
        CompoundNBT nbt = stack.getChildTag("display");
        return nbt != null && nbt.contains("color", 99) ? nbt.getInt("color") : 14869989;
    }

    @Nullable
    public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlotType slot, String type){
        if(type == null)
            return slot.getIndex() != 1 ? "weissrandom:textures/armor/desertgildedlayer1.png" : "weissrandom:textures/armor/desertgildedlayer2.png";
        return slot.getIndex() != 1 ? "weissrandom:textures/armor/desertgildedoverlay1.png" : "weissrandom:textures/armor/desertgildedoverlay2.png";
    }
}
