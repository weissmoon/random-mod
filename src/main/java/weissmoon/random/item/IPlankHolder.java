package weissmoon.random.item;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.text.ITextComponent;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.core.utils.NBTHelper;

/**
 * Created by Weissmoon on 10/1/19.
 */
//TODO move to core/ rename
public interface IPlankHolder extends IItemHolderItem{

    String PLANK = "plankNBT";

    public ITextComponent getDisplayName(ItemStack itemStack);
//    {
//        return new TextComponentTranslation(itemStack.getItem().getUnlocalizedName() + ".name", this.getPlank(itemStack).getDisplayName()).getUnformattedText();
//    }

    default void setPlank(ItemStack stack, ItemStack plank) {
        CompoundNBT cmp = new CompoundNBT();
        if(plank != null) {
            cmp = plank.serializeNBT();
            cmp.putByte("Count", (byte) 1);
        }
        NBTHelper.setTagCompound(stack, PLANK, cmp);
    }


    static void setPlankS(ItemStack stack, ItemStack plank) {
        CompoundNBT cmp = new CompoundNBT();
        if(plank != null) {
            cmp = plank.serializeNBT();
            cmp.putByte("Count", (byte) 1);
        }
        NBTHelper.setTagCompound(stack, PLANK, cmp);
    }

    default ItemStack getPlank(ItemStack stack){
        CompoundNBT cmp = NBTHelper.getTagCompound(stack, PLANK);
        if(cmp == null)
            return null;
        return ItemStack.read(cmp);
    }

    static ItemStack getPlankS(ItemStack stack){
        CompoundNBT cmp = NBTHelper.getTagCompound(stack, PLANK);
        if(cmp == null)
            return null;
        return ItemStack.read(cmp);
    }
}
