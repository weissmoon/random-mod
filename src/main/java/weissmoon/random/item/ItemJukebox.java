package weissmoon.random.item;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import weissmoon.core.item.IItemWeiss;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 2/22/20.
 */
public class ItemJukebox extends WeissBlockItem implements IItemWeiss {
    public ItemJukebox(){
        super(ModBlocks.jukebox, new Item.Properties().group(ItemGroup.DECORATIONS));
    }

    @Override
    public ITextComponent getDisplayName(ItemStack stack)
    {
        return new TranslationTextComponent("block.minecraft.jukebox");
    }
}
