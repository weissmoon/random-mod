package weissmoon.random.item;

import java.util.List;

/**
 * Created by Weissmoon on 5/23/19.
 */
public interface IRadial{

    int getRadialSize();

    List getRadialContentList();
}
