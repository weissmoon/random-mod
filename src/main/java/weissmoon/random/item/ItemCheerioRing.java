package weissmoon.random.item;

//import baubles.api.BaubleType;
//import baubles.api.IBauble;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.ItemGroup;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.core.helper.PlayerHelper;
import weissmoon.core.item.WeissItem;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 3/27/17.
 */
//@Optional.InterfaceList({
//        @Optional.Interface(modid = "Baubles", iface = "baubles.api.IBauble", striprefs = true),
//        @Optional.Interface(modid = "Baubles", iface = "baubles.api.BaubleType", striprefs = true)
//})
public class ItemCheerioRing extends WeissItem{//} implements IBauble {

    public ItemCheerioRing() {
        super(Strings.Items.CHEERIO_RING_NAME, new Properties().maxStackSize(1));
    }

    @Override
    public void inventoryTick (ItemStack stack, World world, Entity entity, int i, boolean flag) {
        if ((stack != null) && (stack.getItem() != Items.AIR)) {
            int cool = getCooldown(stack);
            if (cool > 0) {
                setCooldown(stack, cool - 1);
            }else{
                if(NBTHelper.getBoolean(stack, "flag")) {
                    if (entity instanceof PlayerEntity) {
                        PlayerEntity player = (PlayerEntity) entity;
                        if (NBTHelper.getString(stack, "player").matches(player.getName().getUnformattedComponentText()) && !player.isCreative()) {
                            if(NBTHelper.getBoolean(stack, "active")){
                                if(!(InventoryHelper.isPlayerInventoryFull(player)) || InventoryHelper.hasItem(player, ModItems.cheerio)){
                                    int rgiven = NBTHelper.getInt(stack, "given");
                                    if(player.experienceTotal != 0) {
                                        PlayerHelper.removeExperience(player, 1);
                                        int count = NBTHelper.getInt(stack, "counter");
                                        NBTHelper.setInteger(stack, "counter", count + 1);

                                        if (player.experienceLevel >= 32) {
                                            if (count >= 80) {
                                                count -= 80;
                                                InventoryHelper.givePlayerOrDropItemStack(new ItemStack(ModItems.cheerio), player);
                                                NBTHelper.setInteger(stack, "given", rgiven + 1);
                                            }
                                        } else {
                                            if (count >= 40) {
                                                count -= 40;
                                                InventoryHelper.givePlayerOrDropItemStack(new ItemStack(ModItems.cheerio), player);
                                                NBTHelper.setInteger(stack, "given", rgiven + 1);
                                            }
                                        }
                                        rgiven = NBTHelper.getInt(stack, "given");
                                    }else{
                                        NBTHelper.setInteger(stack, "given", 0);
                                    }
                                    if(rgiven >= NBTHelper.getInt(stack, "level")){
                                        NBTHelper.setInteger(stack, "given", 0);
                                        NBTHelper.setBoolean(stack, "active", false);
                                        this.setCooldown(stack, 60);
                                    }
                                }
                            }else{
                                if(!InventoryHelper.hasItem(player, ModItems.cheerio)){
                                    int levels = player.experienceLevel;
                                    if (levels > 64) levels = 64;
                                    if (levels == 0) return;
                                    NBTHelper.setInteger(stack, "level", levels);
                                    NBTHelper.setBoolean(stack, "active", true);
                                }
                            }
                        }else{
                            int levels = player.experienceLevel;
                            if (levels > 64) levels = 64;
                            NBTHelper.setInteger(stack, "level", levels);
                            NBTHelper.setString(stack, "player", player.getName().getUnformattedComponentText());
                            NBTHelper.setBoolean(stack, "active", true);
                        }
                    }
                }
            }
        }
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand hand)
    {
        ItemStack itemStackIn = playerIn.getHeldItem(hand);
        NBTHelper.toggleBoolean(itemStackIn, "flag");
        return new ActionResult<>(ActionResultType.SUCCESS, itemStackIn);
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean hasEffect (ItemStack stack){
        boolean g = NBTHelper.getBoolean(stack, "flag");
        return g;
    }

    public int getCooldown (ItemStack stack){
        return NBTHelper.getInt(stack, "coolDown");
    }

    public void setCooldown (ItemStack stack, int cool){
        NBTHelper.setInteger(stack, "coolDown", cool);
    }

    @Override
    public void addInformation (ItemStack stack, @Nullable World world, List<ITextComponent> list, ITooltipFlag flag) {
        super.addInformation(stack, world, list, flag);
        String comp = this.getModID() + "." + this.RegName + ".lore";
        TranslationTextComponent ds = new TranslationTextComponent(this.getModID() + "." + this.RegName + ".lore");
        if (!ds.getUnformattedComponentText().contentEquals(comp)){
            list.add(new TranslationTextComponent(this.getModID() + "." + this.RegName + ".lore"));
            list.add(new TranslationTextComponent(this.getModID() + "." + this.RegName + ".lore2"));
        }
    }

    @Override
    public java.util.Collection<ItemGroup> getCreativeTabs()
    {
        List<ItemGroup> list = new ArrayList<>();
        list.add(ItemGroup.FOOD);
        list.add(ItemGroup.MISC);
        return list;
    }

//    @Override
//    public CreativeTabs getCreativeTab()
//    {
//        return CreativeTabs.MISC;
//    }

//    @Override
//    public BaubleType getBaubleType(ItemStack itemstack) {
//        return BaubleType.RING;
//    }
//
//    @Override
//    public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
//        this.onUpdate(itemstack, player.world, player, 0, false);
//    }
//
//    @Override
//    public void onEquipped(ItemStack itemstack, EntityLivingBase player) {
//        NBTHelper.setBoolean(itemstack, "flag", true);
//    }
//
//    @Override
//    public void onUnequipped(ItemStack itemstack, EntityLivingBase player) {
//        NBTHelper.setBoolean(itemstack, "flag", false);
//    }

}
