package weissmoon.random.item;

import net.minecraft.item.ItemGroup;
import weissmoon.core.api.client.item.IItemRenderCustom;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 3/30/22.
 */
public class ItemBlockHungryChest extends WeissBlockItem{//} implements IItemRenderCustom{

    public ItemBlockHungryChest(){
        super(ModBlocks.hungryChest, new Properties().group((ItemGroup.DECORATIONS)));
    }
//
//    @Override
//    public IItemRenderer getIItemRender(){
//        return new RenderItemHungryChest();
//    }
}
