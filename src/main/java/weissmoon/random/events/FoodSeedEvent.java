package weissmoon.random.events;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.core.helper.RNGHelper;
import weissmoon.random.recipe.CrumbsInventory;
import weissmoon.random.recipe.RecipeCrumbs;
import weissmoon.random.recipe.RecipeTypes;

import java.util.Optional;

/**
 * Created by Weissmoon on 3/28/19.
 */
public class FoodSeedEvent {

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onFoodEat(LivingEntityUseItemEvent.Finish event){
        if(event.getEntityLiving() instanceof PlayerEntity){
            PlayerEntity player = (PlayerEntity)event.getEntityLiving();
            CrumbsInventory.INSTANCE.setFood(event.getItem());
            Optional<RecipeCrumbs> maybeRecipe = player.world.getRecipeManager().getRecipe(RecipeTypes.CRUMBS_TYPE, CrumbsInventory.INSTANCE, player.world);
            maybeRecipe.ifPresent((recipeCrumbs) -> {
                int i = 0;
                while(i < recipeCrumbs.getRolls()){
                    if(recipeCrumbs.chance>RNGHelper.getFloat())
                        InventoryHelper.givePlayerOrDropItemStack(recipeCrumbs.getRecipeOutput(), player);
                    i++;
                }
            });
        }
//            if(event.getItem().isItemEqual(new ItemStack(Items.MELON)))
//                if (RNGHelper.getFloat() < 0.05)
//                    InventoryHelper.givePlayerOrDropItemStack(new ItemStack(Items.MELON_SEEDS), (PlayerEntity)event.getEntityLiving());
    }
}
