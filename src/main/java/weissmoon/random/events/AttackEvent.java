package weissmoon.random.events;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.Hand;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.combat.PlayerSupplementaryDamageSource;
import weissmoon.random.item.ItemBattleFish;

/**
 * Created by Weissmoon on 10/5/17.
 */
public class AttackEvent {

    private PlayerEntity attacker;
    private Entity target;

    //Setup
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerAttack(AttackEntityEvent event){
        attacker = event.getPlayer();
        target = event.getTarget();
    }

    //Execute
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onLivingAttack(LivingAttackEvent event){
        DamageSource source = event.getSource();
        if(source instanceof EntityDamageSource && source.getDamageType().contains("player") && source.getClass() != PlayerSupplementaryDamageSource.class){
            PlayerEntity attacker = (PlayerEntity)((EntityDamageSource)source).getTrueSource();
            ItemStack stack = attacker.getHeldItem(Hand.MAIN_HAND);
            if(stack != null && stack.getItem() instanceof ItemBattleFish){
                event.getEntityLiving().attackEntityFrom(new PlayerSupplementaryDamageSource(attacker), 4);
                return;
            }
            stack = attacker.getHeldItem(Hand.OFF_HAND);
            if(stack != null && stack.getItem() instanceof ItemBattleFish){
                event.getEntityLiving().attackEntityFrom(new PlayerSupplementaryDamageSource(attacker), 4);
            }
        }
    }
}
