package weissmoon.random.events;

import net.minecraft.block.Blocks;
import net.minecraft.tileentity.MobSpawnerTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.event.world.BlockEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * Created by Weissmoon on 2/5/20.
 */
public class BreakSpawnerEvent {

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void spawnerBreakEvent(BlockEvent.BreakEvent event){
        if(!event.isCanceled() && event.getState().getBlock() == Blocks.SPAWNER){
            if(event.getPlayer().abilities.isCreativeMode)
                return;
            TileEntity tileEntity = event.getWorld().getTileEntity(event.getPos());
            if(tileEntity instanceof MobSpawnerTileEntity){
                ((MobSpawnerTileEntity) tileEntity).getSpawnerBaseLogic().spawnDelay = 0;
                ((MobSpawnerTileEntity) tileEntity).tick();
            }
        }
    }
}
