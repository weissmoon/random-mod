package weissmoon.random.events;

import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.item.IAutoPickUpItem;

/**
 * Created by Weissmoon on 11/27/21.
 */
//TODO move to core
public class ItemPickUpEvent{

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void onItemPickUpEVent(PlayerEvent.ItemPickupEvent event){
        PlayerEntity player = event.getPlayer();
        if(doesPlayerHavePickUpItem(player))
            for (int inv = 0; inv < player.inventory.getSizeInventory(); ++inv){
                ItemStack bagStack = player.inventory.getStackInSlot(inv);

                if (isPickItem(bagStack))
                    if(((IAutoPickUpItem)bagStack.getItem()).canPickUp(bagStack, event.getStack())){
                        ItemStack remainingStack = ((IAutoPickUpItem)bagStack.getItem()).insertStack(bagStack, event.getStack());

                        if(!remainingStack.isEmpty()){
                            ItemEntity entityToAdd = new ItemEntity(player.world, player.getPosX(), player.getPosY(), player.getPosZ(), remainingStack);
                            entityToAdd.setPickupDelay(0);
                            player.world.addEntity(entityToAdd);
                            return;
                        }
                    }
        }
    }

    public boolean doesPlayerHavePickUpItem(PlayerEntity player){
        for (int i = 0; i < player.inventory.getSizeInventory(); ++i)
            if (isPickItem(player.inventory.getStackInSlot(i)))
                return true;

        return false;
    }

    public boolean isPickItem(ItemStack stack){
        return stack.getItem() instanceof IAutoPickUpItem;
    }
}
