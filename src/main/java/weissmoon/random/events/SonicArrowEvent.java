package weissmoon.random.events;

import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.monster.SkeletonEntity;
import net.minecraft.entity.monster.StrayEntity;
import net.minecraft.entity.monster.WitherSkeletonEntity;
import net.minecraft.entity.monster.IMob;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.helper.RNGHelper;
import weissmoon.random.item.ModItems;

/**
 * Created by Weissmoon on 3/25/19.
 */
public class SonicArrowEvent {

    @SubscribeEvent
    public void skeletonDrops(LivingDropsEvent event){
        if (event.getEntityLiving() instanceof IMob){
            if ((event.getEntityLiving() instanceof SkeletonEntity) &&
                    (RNGHelper.getFloat() > 0.99D)){
                ItemStack itemStack = new ItemStack(ModItems.sonicArrow);
                event.getDrops().add(createItemEntity(event.getEntityLiving().world, event.getEntityLiving(), itemStack));
            }
            if ((event.getEntityLiving() instanceof WitherSkeletonEntity) &&
                    (RNGHelper.getFloat() > 0.90D)){
                ItemStack itemStack = new ItemStack(ModItems.sonicArrow);
                event.getDrops().add(createItemEntity(event.getEntityLiving().world, event.getEntityLiving(), itemStack));
            }
            if ((event.getEntityLiving() instanceof StrayEntity) &&
                    (RNGHelper.getFloat() > 0.85D)){
                ItemStack itemStack = new ItemStack(ModItems.sonicArrow);
                event.getDrops().add(createItemEntity(event.getEntityLiving().world, event.getEntityLiving(), itemStack));
            }
        }
    }

    private static ItemEntity createItemEntity(World world, Entity entity, ItemStack stack){
        return new ItemEntity(world, entity.getPosX(), entity.getPosY(), entity.getPosZ(),stack);
    }
}
