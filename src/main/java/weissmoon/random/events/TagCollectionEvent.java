package weissmoon.random.events;

import com.google.gson.*;
import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.item.Item;
import net.minecraft.item.SwordItem;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.AddReloadListenerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryManager;

import java.util.Map;

/**
 * Created by Weissmoon on 4/9/22.
 * NOOP
 */
public class TagCollectionEvent{

    @SubscribeEvent
    public void registerGoal(AddReloadListenerEvent event){
        event.addListener(new Listener());
    }

    class Listener extends JsonReloadListener{
        public Listener(){
            super(new GsonBuilder().create(), "tags");
        }

        @Override
        protected void apply(Map<ResourceLocation, JsonElement> objectIn, IResourceManager resourceManagerIn, IProfiler profilerIn){
//            ITag<Item> swordsTag = event.getTagManager().getItemTags().get(new ResourceLocation("weissrandom:all_sword_class"));
            JsonObject tagElement = new JsonObject();
            JsonElement object = objectIn.getOrDefault(new ResourceLocation("weissrandom:items/all_sword_class"), tagElement);
            tagElement.addProperty("replace", false);
            JsonArray valuesArray = new JsonArray();


            IForgeRegistry<Item> items = RegistryManager.ACTIVE.getRegistry(Item.class);
            items.getValues().forEach(item -> {
                if(item instanceof SwordItem){
                    valuesArray.add(item.getRegistryName().toString());
                }
            });
            tagElement.add("values", valuesArray);
            objectIn.put(new ResourceLocation("weissrandom:items/all_sword_class"), tagElement);
        }
    }
}
