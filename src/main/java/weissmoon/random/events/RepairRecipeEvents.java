package weissmoon.random.events;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.item.RepairItem;

/**
 * Created by Weissmoon on 7/26/21.
 */
public class RepairRecipeEvents{

    @SubscribeEvent
    public void repairRecipeExperienceCheck(PlayerEvent.ItemCraftedEvent event){
        ItemStack toolStack = null;
        ItemStack repairItem = null;

        IInventory inv = event.getInventory();

        for(int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if(item.isDamageable() && item.isDamaged())
                toolStack = item.copy();
            else if(item.getItem() instanceof RepairItem)
                repairItem = item;
            else if(!item.isEmpty())
                return;
        }


        if(toolStack != null && repairItem != null){
            int cost = event.getCrafting().getRepairCost();
            PlayerEntity player = event.getPlayer();

            if(player != null)
                if(player.experienceLevel > cost){
                    event.getPlayer().addExperienceLevel(-cost);
                }else{
                    int overCost = cost;
                    while(player.experienceLevel < overCost)
                        overCost--;
                    player.experience = 0;
                    player.experienceLevel = 0;
                    player.experienceTotal = 0;
                    toolStack.setRepairCost(((overCost * cost) + cost));
                }
        }
    }
}
