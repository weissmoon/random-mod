package weissmoon.random.events;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EntityDamageSource;
import net.minecraft.util.Hand;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.core.helper.RNGHelper;

/**
 * Created by Weissmoon on 5/31/19.
 */
public class ArrowMeleeEvent {

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerAttack(LivingHurtEvent event){
        DamageSource source = event.getSource();
        if(source instanceof EntityDamageSource && source.damageType.equals("player") && source.getTrueSource() != null){
            PlayerEntity attacker = (PlayerEntity) source.getTrueSource();
            ItemStack heldItem = attacker.getHeldItem(Hand.MAIN_HAND);
            if(!heldItem.isEmpty() && heldItem.getItem() == Items.ARROW){
                event.setAmount(event.getAmount() + 3);
                if(RNGHelper.getFloat() > 0.90)
                    heldItem.shrink(1);
            }
        }
    }

}
