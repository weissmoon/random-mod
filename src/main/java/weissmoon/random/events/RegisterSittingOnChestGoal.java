package weissmoon.random.events;

import net.minecraft.entity.passive.CatEntity;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.ai.CatSitOnChestGoal;

/**
 * Created by Weissmoon on 3/31/22.
 */
public class RegisterSittingOnChestGoal{

    @SubscribeEvent
    public void registerGoal(EntityJoinWorldEvent event){
        if(event.getEntity() instanceof CatEntity){
            CatEntity cat = (CatEntity)event.getEntity();
            cat.goalSelector.addGoal(7, new CatSitOnChestGoal(cat, 0.8D));
        }
    }
}
