package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ModItems;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Weissmoon on 10/1/19.
 */
public class WoodSpadeRecipe extends WeissSpecialRecipe implements IShapedRecipe<CraftingInventory> {

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(WoodSpadeRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("wooden_shovel");

    public WoodSpadeRecipe(ResourceLocation o) {
        super(o, 1, 3);
//        super(new ResourceLocation("minecraft:wooden_shovel"), new ItemStack(ModItems.woodSpade),"p", "s", "s", 'p', "plankWood", 's', "stickWood");
//        this.group = group;
//        this.width = 1;
//        this.height = 3;
//        this.mirrored = false;
//        this.setRegistryName("minecraft:wooden_shovel");
    }

    @Nullable
    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ItemStack planks = null;
        for (int x = 0; x < inv.getSizeInventory(); x++){
            boolean match = false;
                ItemStack stack = inv.getStackInSlot(x);
                if (stack != ItemStack.EMPTY) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("logWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                break;
//                }
                    ResourceLocation plankTag = new ResourceLocation("minecraft", "planks");
                    Item unknownItem = stack.getItem();
                    boolean isInGroup = ItemTags.getCollection().get(plankTag).contains(unknownItem);
                    if(isInGroup){
                        planks = stack;
                        break;
                    }
                }
        }
        ItemStack outputasdfrwe = new ItemStack(ModItems.woodSpade);
        if (outputasdfrwe.getItem() instanceof IPlankHolder) {
            ((IPlankHolder) outputasdfrwe.getItem()).setPlank(outputasdfrwe, planks);
        }
        return outputasdfrwe;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation() {
        return location;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ModItems.woodSpade);
    }

    @Override
    public final int getRecipeWidth(){
        return this.width;
    }

    @Override
    public final int getRecipeHeight(){
        return this.height;
    }

    @Override
    protected void reloadIngredientList() {
        NonNullList<Ingredient> ingredientslist = NonNullList.withSize(width * height, Ingredient.EMPTY);
        ingredientslist.set(0, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:planks")))));
        ingredientslist.set(1, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("forge:rods/wooden")))));
        ingredientslist.set(2, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("forge:rods/wooden")))));
        recipeItems = ingredientslist;
    }
}
