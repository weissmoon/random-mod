package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ModItems;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Weissmoon on 10/26/19.
 */
public class WoodDaggerRecipe extends WeissSpecialRecipe implements IShapedRecipe<CraftingInventory> {

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(WoodDaggerRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weissrandom:wood_dagger");

    public WoodDaggerRecipe(ResourceLocation o) {
        super(o, 1, 2);
        //super(new ResourceLocation("weissrandom:wooddagger"), new ItemStack(ModItems.woodDagger),"p", "s", 'p', "plankWood", 's', "stickWood");
//        this.group = group;
//        this.width = 1;
//        this.height = 2;
//        this.mirrored = false;
//        this.setRegistryName("weissrandom:wooddagger");
    }

    @Nullable
    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ItemStack planks = null;
        for (int x = 0; x < inv.getSizeInventory(); x++){
            boolean match = false;
            ItemStack stack = inv.getStackInSlot(x);
            if (stack != ItemStack.EMPTY) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("plankWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                    break;
//                }
                ResourceLocation plankTag = new ResourceLocation("minecraft", "planks");
                Item unknownItem = stack.getItem();
                boolean isInGroup = ItemTags.getCollection().get(plankTag).contains(unknownItem);
                if(isInGroup){
                    planks = stack;
                    break;
                }
            }
        }
        ItemStack outputasdfrwe = new ItemStack(ModItems.woodDagger);
        if (outputasdfrwe.getItem() instanceof IPlankHolder) {
            ((IPlankHolder) outputasdfrwe.getItem()).setPlank(outputasdfrwe, planks);
        }
        return outputasdfrwe;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation() {
        return location;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ModItems.woodDagger);
    }

    @Override
    public final int getRecipeWidth(){
        return this.width;
    }

    @Override
    public final int getRecipeHeight(){
        return this.height;
    }

    @Override
    protected void reloadIngredientList() {
        NonNullList<Ingredient> ingredientslist = NonNullList.withSize(width * height, Ingredient.EMPTY);
        ingredientslist.set(0, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:planks")))));
        ingredientslist.set(1, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("forge:rods/wooden")))));
        recipeItems = ingredientslist;
    }
}
