package weissmoon.random.recipe;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import static weissmoon.random.recipe.RecipeTypes.IOB_JEI_SERIALIZER;

/**
 * Created by Weissmoon on 4/5/22.
 */
public class ItemOnBlockUsage implements IRecipe<IOBInventory>{

    public static final ResourceLocation TYPE = new ResourceLocation("weissrandom:itemonblockjei");

    public final ResourceLocation recipeID;
    public final ItemStack item;
    public final ItemStack block;
    public final ItemStack output;
    public final ItemStack result;
    public final boolean sneak;
    public final boolean right;
    private static List<ItemOnBlockUsage> recipes = new ArrayList<>(4);

    public ItemOnBlockUsage(ResourceLocation id, ItemStack item, ItemStack block, ItemStack output, ItemStack result, boolean sneaking, boolean right){
        this.recipeID = id;
        this.block = block;
        this.item = item;
        this.output = output;
        this.result = result;
        this.sneak = sneaking;
        this.right = right;
        recipes.add(this);
    }

    @Override
    public boolean matches(IOBInventory inv, World worldIn){
        return false;
    }

    @Override
    public ItemStack getCraftingResult(IOBInventory inv){
        return new ItemStack(Items.ICE);
    }

    @Override
    public boolean canFit(int width, int height){
        return false;
    }

    @Override
    public ItemStack getRecipeOutput(){
        return new ItemStack(Items.ICE);
    }

    @Override
    public ResourceLocation getId(){
        return recipeID;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return IOB_JEI_SERIALIZER;
    }

    @Override
    public IRecipeType<?> getType(){
        return Registry.RECIPE_TYPE.getOptional(TYPE).get();
    }

    public static List<ItemOnBlockUsage> getRecipes(World world){
        return world.getRecipeManager().getRecipesForType(RecipeTypes.IOB_JEI_TYPE);
    }

    public static final class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<ItemOnBlockUsage>{
        @Override
        public ItemOnBlockUsage read(ResourceLocation recipeId, JsonObject json){
            ItemStack item = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "item"));
            ItemStack block = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "block"));
            ItemStack output;
            try{
                output = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "output"));
            }catch (Throwable e){
                output = ItemStack.EMPTY;
            }
            ItemStack result;
            try{
                result = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "result"));
            }catch (Throwable e){
                result = ItemStack.EMPTY;
            }
            boolean sneaking = JSONUtils.getBoolean(json, "sneaking", false);
            boolean right = JSONUtils.getBoolean(json, "right", false);
            return new ItemOnBlockUsage(recipeId, item, block, output, result, sneaking, right);
        }

        @Nullable
        @Override
        public ItemOnBlockUsage read(ResourceLocation recipeId, PacketBuffer buffer){
            ResourceLocation id = buffer.readResourceLocation();
            ItemStack item = buffer.readItemStack();
            ItemStack block = buffer.readItemStack();
            ItemStack output = buffer.readItemStack();
            ItemStack result = buffer.readItemStack();
            boolean sneak = buffer.readBoolean();
            boolean right = buffer.readBoolean();
            return new ItemOnBlockUsage(id, item, block, output, result, sneak, right);
        }

        @Override
        public void write(PacketBuffer buffer, ItemOnBlockUsage recipe){
            buffer.writeResourceLocation(recipe.recipeID);
            buffer.writeItemStack(recipe.item);
            buffer.writeItemStack(recipe.block);
            buffer.writeItemStack(recipe.output);
            buffer.writeItemStack(recipe.result);
            buffer.writeBoolean(recipe.sneak);
            buffer.writeBoolean(recipe.right);
        }
    }
}
