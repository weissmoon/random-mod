package weissmoon.random.recipe;

import com.google.gson.JsonObject;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.item.crafting.ShapedRecipe;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static weissmoon.random.recipe.RecipeTypes.CRUMBS_SERIALIZER;

/**
 * Created by Weissmoon on 3/7/22.
 */
public class RecipeCrumbs implements IRecipe<CrumbsInventory>{

    public static final ResourceLocation TYPE = new ResourceLocation("weissrandom:crumbs");

    public final ResourceLocation recipeID;
    public final ItemStack output;
    public final ItemStack ingredient1;
    public final float chance;
    public final int rolls;
    private static List<RecipeCrumbs> recipes = new ArrayList<>(4);


    public RecipeCrumbs(ResourceLocation id, ItemStack food, ItemStack crumbs, float chance, int rolls){
        this.recipeID = id;
        this.output = crumbs;
        this.ingredient1 = food;
        this.chance = chance;
        this.rolls = rolls;
        recipes.add(this);
    }

    public int getRolls(){
        return Math.max(1, rolls);
    }

    @Override
    public boolean matches(CrumbsInventory inv, World worldIn){
        return (ingredient1.isItemEqual(inv.getFood()));
    }

    @Override
    public ItemStack getCraftingResult(CrumbsInventory inv){
        return output.copy();
    }

    @Override
    public boolean canFit(int width, int height){
        return false;//NO-OP
    }

    @Override
    public ItemStack getRecipeOutput(){
        return output.copy();
    }

    @Override
    public ResourceLocation getId(){
        return recipeID;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return CRUMBS_SERIALIZER;
    }

    @Override
    public IRecipeType<?> getType(){
        return Registry.RECIPE_TYPE.getOptional(TYPE).get();
    }

    public static final class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<RecipeCrumbs>{
        @Override
        public RecipeCrumbs read(ResourceLocation recipeId, JsonObject json){
            ItemStack ingredient1 = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "food"));
            ItemStack output = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "crumbs"));
            float chance = JSONUtils.getFloat(json, "chance", 0.5F);
            int rolls = Math.max(JSONUtils.getInt(json, "rolls", 1), 1);
            return new RecipeCrumbs(recipeId, ingredient1, output, chance, rolls);
        }

        @Nullable
        @Override
        public RecipeCrumbs read(ResourceLocation recipeId, PacketBuffer buffer){
            ResourceLocation id = buffer.readResourceLocation();
            ItemStack ingredient1 = buffer.readItemStack();
            ItemStack output = buffer.readItemStack();
            float chance = buffer.readFloat();
            int rolls = buffer.readInt();
            return new RecipeCrumbs(id, ingredient1, output, chance, rolls);
        }

        @Override
        public void write(PacketBuffer buffer, RecipeCrumbs recipe){
            buffer.writeResourceLocation(recipe.recipeID);
            buffer.writeItemStack(recipe.ingredient1);
            buffer.writeItemStack(recipe.output);
            buffer.writeFloat(recipe.chance);
            buffer.writeInt(recipe.rolls);
        }
    }

    public static List<RecipeCrumbs> getRecipes(World world){
        return world.getRecipeManager().getRecipesForType(RecipeTypes.CRUMBS_TYPE);
    }
}
