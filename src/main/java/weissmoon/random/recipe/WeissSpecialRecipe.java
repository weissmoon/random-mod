package weissmoon.random.recipe;

import net.minecraft.block.Blocks;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipe;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 12/26/19.
 */
public abstract class WeissSpecialRecipe extends SpecialRecipe {

    protected int width, height;
    protected NonNullList<Ingredient> recipeItems;
    private static List<WeissSpecialRecipe> recipes = new ArrayList<>(4);
    private static boolean loaded;

    public WeissSpecialRecipe(ResourceLocation o, int recipeWidth, int recipeHeight) {
        super(o);
        this.width = recipeWidth;
        this.height = recipeHeight;
        recipes.add(this);
    }


    @Override
    public boolean matches(CraftingInventory inv, World worldIn) {
//        if(!loaded){
//            reloadIngredients();
//            loaded = true;
//        }
        try {
            for (int i = 0; i <= inv.getWidth() - this.width; ++i) {
                for (int j = 0; j <= inv.getHeight() - this.height; ++j) {
                    if (this.checkMatch(inv, i, j, true)) {
                        return true;
                    }

                    if (this.checkMatch(inv, i, j, false)) {
                        return true;
                    }
                }
            }
        }catch (NullPointerException e){
            reloadIngredients();
        }

        return false;
    }

    @Override
    public boolean canFit(int width, int height) {
        return this.height * this.width >= 2;
    }

    public final int getWidth(){
        return this.width;
    }

    public final int getHeight(){
        return this.height;
    }

    protected boolean checkMatch(CraftingInventory craftingInventory, int p_77573_2_, int p_77573_3_, boolean p_77573_4_) {
        for(int i = 0; i < craftingInventory.getWidth(); ++i) {
            for(int j = 0; j < craftingInventory.getHeight(); ++j) {
                int k = i - p_77573_2_;
                int l = j - p_77573_3_;
                Ingredient ingredient = Ingredient.EMPTY;
                if (k >= 0 && l >= 0 && k < this.width && l < this.height) {
                    if (p_77573_4_) {
                        ingredient = this.recipeItems.get(this.width - k - 1 + l * this.width);
                    } else {
                        ingredient = this.recipeItems.get(k + l * this.width);
                    }
                }

                if (!ingredient.test(craftingInventory.getStackInSlot(i + j * craftingInventory.getWidth()))) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public NonNullList<Ingredient> getIngredients() {
        return recipeItems;
    }

    @Override
    public abstract IRecipeSerializer<?> getSerializer();

    public abstract ResourceLocation getLocation();

    @Override
    public abstract ItemStack getCraftingResult(CraftingInventory inv);

    protected abstract void reloadIngredientList();

    public static void reloadIngredients(){
         for (WeissSpecialRecipe recipe : recipes) {
             recipe.reloadIngredientList();
         }
    }

    public static List<WeissSpecialRecipe> getRecipes(){
        return recipes;
    }
}
