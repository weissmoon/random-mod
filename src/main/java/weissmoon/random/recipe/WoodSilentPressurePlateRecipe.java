package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.random.block.ModBlocks;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Weissmoon on 1/13/22.
 */
public class WoodSilentPressurePlateRecipe extends WeissSpecialRecipe implements IShapedRecipe<CraftingInventory>{

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(WoodSilentPressurePlateRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weissrandom:wooden_pressure_plate_silent");

    public WoodSilentPressurePlateRecipe(ResourceLocation o) {
        super(o, 1, 2);
        //super(new ResourceLocation("weissrandom:wooddagger"), new ItemStack(ModItems.woodDagger),"p", "s", 'p', "plankWood", 's', "stickWood");
//        this.group = group;
//        this.width = 1;
//        this.height = 2;
//        this.mirrored = false;
//        this.setRegistryName("weissrandom:wooddagger");
    }

    @Nullable
    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ItemStack plate = null;
        for (int x = 0; x < inv.getSizeInventory(); x++){
            ItemStack stack = inv.getStackInSlot(x);
            if (stack != ItemStack.EMPTY) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("plankWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                    break;
//                }
                ResourceLocation plankTag = new ResourceLocation("minecraft", "wooden_pressure_plates");
                Item unknownItem = stack.getItem();
                boolean isInGroup = ItemTags.getCollection().get(plankTag).contains(unknownItem);
                if(isInGroup){
                    ResourceLocation ignoreTag = new ResourceLocation("weissrandom", "wooden_pressure_plates_ignore");
                    boolean ignoreGroup = ItemTags.getCollection().get(ignoreTag).contains(unknownItem);
                    if(ignoreGroup)
                        continue;
                    plate = stack;
                    break;
                }
            }
        }
        ItemStack outputasdfrwe = new ItemStack(ModBlocks.woodenPressurePlateS);
        if (outputasdfrwe.getItem() instanceof IItemHolderItem)
            IItemHolderItem.setHolderItem(outputasdfrwe, plate);
        return outputasdfrwe;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation() {
        return location;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ModBlocks.woodenPressurePlateS);
    }

    @Override
    public final int getRecipeWidth(){
        return this.width;
    }

    @Override
    public final int getRecipeHeight(){
        return this.height;
    }

    @Override
    protected void reloadIngredientList() {
        NonNullList<Ingredient> ingredientslist = NonNullList.withSize(width * height, Ingredient.EMPTY);
        ingredientslist.set(0, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:wooden_pressure_plates")))));
        ingredientslist.set(1, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:wool")))));
        recipeItems = ingredientslist;
    }
}
