//package weissmoon.random.recipe;
//
//import net.minecraft.inventory.CraftingInventory;
//import net.minecraft.item.ItemStack;
//import net.minecraft.item.crafting.ShapedRecipe;
//import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.oredict.OreDictionary;
//import net.minecraftforge.oredict.ShapedOreRecipe;
//import weissmoon.random.item.IPlankHolder;
//import weissmoon.random.item.ModItems;
//
//import javax.annotation.Nullable;
//import java.util.Iterator;
//
///**
// * Created by Weissmoon on 10/1/19.
// */
//public class WoodPickaxeRecipe extends ShapedRecipe {
//
//    public WoodPickaxeRecipe() {
//        super(new ResourceLocation("minecraft:wooden_shovel"),null, 1, 3,  "p", "s", "s", 'p', "plankWood", 's', "stickWood", new ItemStack(ModItems.woodSpade));
////        this.group = group;
////        this.width = 1;
////        this.height = 3;
////        this.mirrored = false;
////        this.setRegistryName("minecraft:wooden_shovel");
//    }
//
//    @Nullable
//    @Override
//    public ItemStack getCraftingResult(CraftingInventory inv) {
//        ItemStack planks = null;
//        for (int x = 0; x < inv.getSizeInventory(); x++){
//            boolean match = false;
//            ItemStack stack = inv.getStackInSlot(x);
//            if (stack != null) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("plankWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                    break;
//                }
//            }
//        }
//        ItemStack outputasdfrwe = new ItemStack(ModItems.woodSpade);
//        if (outputasdfrwe.getItem() instanceof IPlankHolder) {
//            ((IPlankHolder) outputasdfrwe.getItem()).setPlank(outputasdfrwe, planks);
//        }
//        return outputasdfrwe;
//    }
//}//TODO
