package weissmoon.random.recipe;

import net.minecraft.item.crafting.IRecipe;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.util.registry.Registry;
import net.minecraftforge.event.RegistryEvent;

/**
 * Created by Weissmoon on 4/19/21.
 */
public class RecipeTypes{

    public static final IRecipeType<RecipeBarrel> BARREL_TYPE = new RecipeTypes.RecipeType();
    public static final IRecipeSerializer<RecipeBarrel> BARREL_SERIALIZER = new weissmoon.random.recipe.RecipeBarrel.Serializer();
    public static final IRecipeType<RecipeCrumbs> CRUMBS_TYPE = new RecipeTypes.RecipeType();
    public static final IRecipeSerializer<RecipeCrumbs> CRUMBS_SERIALIZER = new weissmoon.random.recipe.RecipeCrumbs.Serializer();
    public static final IRecipeType<ItemOnBlockUsage> IOB_JEI_TYPE = new RecipeTypes.RecipeType();
    public static final IRecipeSerializer<ItemOnBlockUsage> IOB_JEI_SERIALIZER = new weissmoon.random.recipe.ItemOnBlockUsage.Serializer();
    public static final IRecipeType<CraftingShapedWithDamageRecipe> CRAFTING_SHAPED_WITH_DAMAGE_TYPE = new RecipeTypes.RecipeType();
    public static final IRecipeSerializer<CraftingShapedWithDamageRecipe> CRAFTING_SHAPED_WITH_DAMAGE_SERIALIZER = CraftingShapedWithDamageRecipe.SERIALIZER;

    public static void register(RegistryEvent.Register<IRecipeSerializer<?>> event) {
        Registry.register(Registry.RECIPE_TYPE, RecipeBarrel.TYPE, BARREL_TYPE);
        event.getRegistry().register(BARREL_SERIALIZER.setRegistryName(RecipeBarrel.TYPE));
        Registry.register(Registry.RECIPE_TYPE, RecipeCrumbs.TYPE, CRUMBS_TYPE);
        event.getRegistry().register(CRUMBS_SERIALIZER.setRegistryName(RecipeCrumbs.TYPE));
        Registry.register(Registry.RECIPE_TYPE, ItemOnBlockUsage.TYPE, IOB_JEI_TYPE);
        event.getRegistry().register(IOB_JEI_SERIALIZER.setRegistryName(ItemOnBlockUsage.TYPE));
        Registry.register(Registry.RECIPE_TYPE, CraftingShapedWithDamageRecipe.TYPE, CRAFTING_SHAPED_WITH_DAMAGE_TYPE);
        event.getRegistry().register(CRAFTING_SHAPED_WITH_DAMAGE_SERIALIZER.setRegistryName(CraftingShapedWithDamageRecipe.TYPE));
    }
    
    private static class RecipeType<T extends IRecipe<?>> implements IRecipeType<T>{
        private RecipeType() {
        }

        public String toString() {
            return Registry.RECIPE_TYPE.getKey(this).toString();
        }
    }
}
