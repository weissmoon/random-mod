package weissmoon.random.recipe;

import com.google.gson.JsonObject;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.item.crafting.*;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.JSONUtils;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import weissmoon.random.item.ModItems;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

import static weissmoon.random.recipe.RecipeTypes.BARREL_SERIALIZER;

/**
 * Created by Weissmoon on 4/19/21.
 */
public class RecipeBarrel implements IRecipe<IInventory>{

    public static final ResourceLocation TYPE = new ResourceLocation("weissrandom:barrel");
    public static final JsonObject defaultData;

    static{
        defaultData = new JsonObject();
        defaultData.addProperty("isCatalyst", false);
        defaultData.addProperty("time", 500);
    }

    public final ResourceLocation recipeID;
    public final ItemStack output;
    public final ItemStack ingredient1;
    public final ItemStack ingredient2;
    public final boolean isCatalyst;
    public final int time;
    public final static Ingredient EMPTY = Ingredient.EMPTY;
    private static List<RecipeBarrel> recipes = new ArrayList<>(4);

    public RecipeBarrel(RecipeBarrel id){
        this.recipeID = new ResourceLocation("weissrandom:specialSaltWaterRecipeThatYouShouldNotMessWithButYouCantMessWithAnywaysOkaySeeYouBye".toLowerCase());
        this.output = new ItemStack(ModItems.salt);
        this.ingredient1 = new ItemStack(Items.WATER_BUCKET);
        this.ingredient2 = new ItemStack(Items.WATER_BUCKET);
        this.isCatalyst = false;
        this.time = 0;
    }

    public RecipeBarrel(ResourceLocation id, ItemStack ingredient1, ItemStack ingredient2, ItemStack output, boolean catalyst, int time){
        this.recipeID = id;
        this.output = output;
        this.ingredient1 = ingredient1;
        this.ingredient2 = ingredient2;
        this.isCatalyst = catalyst;
        this.time = time;
        recipes.add(this);
    }

    @Override
    public boolean matches(IInventory inv, World worldIn){
        ItemStack item1 = inv.getStackInSlot(0);
        ItemStack item2 = inv.getStackInSlot(1);
        if(itemStackMatchesSize(ingredient1, item1, false)){
            return itemStackMatchesSize(ingredient2, item2, true);
        }else if(itemStackMatchesSize(ingredient2, item1, true)){
            return itemStackMatchesSize(ingredient1, item2, false);
        }
        return false;
    }

    public boolean itemStackMatchesSize(ItemStack ingredient, ItemStack input, boolean testAsCatalyst){
        if(ingredient.isItemEqual(input)){
            if(testAsCatalyst)
                return true;
            return ingredient.getCount() <= input.getCount();
        }
        return false;
    }

    @Override
    public ItemStack getCraftingResult(IInventory inv){
        return output.copy();
    }

    @Override
    public boolean canFit(int width, int height){
        return false;
    }

    @Override
    public ItemStack getRecipeOutput(){
        return output;
    }

    @Override
    public ResourceLocation getId(){
        return recipeID;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return BARREL_SERIALIZER;
    }

    @Override
    public IRecipeType<?> getType(){
        return Registry.RECIPE_TYPE.getOptional(TYPE).get();
    }

    public static final class Serializer extends ForgeRegistryEntry<IRecipeSerializer<?>> implements IRecipeSerializer<RecipeBarrel>{
        @Override
        public RecipeBarrel read(ResourceLocation recipeId, JsonObject json){
            ItemStack ingredient1 = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "ingredient"));
            ItemStack ingredient2 = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "catalyst"));
            ItemStack output = ShapedRecipe.deserializeItem(JSONUtils.getJsonObject(json, "output"));
            JsonObject data = JSONUtils.getJsonObject(json, "data", defaultData);
            boolean catalyst = JSONUtils.getBoolean(data, "isCatalyst", false);
            int time = JSONUtils.getInt(data, "time", 500);
            return new RecipeBarrel(recipeId, ingredient1, ingredient2, output, catalyst, time);
        }

        @Nullable
        @Override
        public RecipeBarrel read(ResourceLocation recipeId, PacketBuffer buffer){
            ResourceLocation id = buffer.readResourceLocation();
            ItemStack ingredient1 = buffer.readItemStack();
            ItemStack ingredient2 = buffer.readItemStack();
            ItemStack output = buffer.readItemStack();
            boolean catalyst = buffer.readBoolean();
            int time = buffer.readInt();
            return new RecipeBarrel(id, ingredient1, ingredient2, output, catalyst, time);
        }

        @Override
        public void write(PacketBuffer buffer, RecipeBarrel recipe){
            buffer.writeResourceLocation(recipe.recipeID);
            buffer.writeItemStack(recipe.ingredient1);
            buffer.writeItemStack(recipe.ingredient2);
            buffer.writeItemStack(recipe.output);
            buffer.writeBoolean(recipe.isCatalyst);
            buffer.writeInt(recipe.time);
        }
    }

    public static List<RecipeBarrel> getRecipes(World world){
        return world.getRecipeManager().getRecipesForType(RecipeTypes.BARREL_TYPE);
    }
}
