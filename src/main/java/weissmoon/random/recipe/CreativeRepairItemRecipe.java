package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.item.RepairItem;

import javax.annotation.Nonnull;

/**
 * Created by Weissmoon on 7/28/21.
 */
public class CreativeRepairItemRecipe extends WeissSpecialRecipe{

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(CreativeRepairItemRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weissrandom:creative_repair_recipe");

    public CreativeRepairItemRecipe(ResourceLocation o){
        super(o, 2, 2);
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation(){
        return location;
    }

    @Override
    protected boolean checkMatch(CraftingInventory inv, int startX, int startY, boolean mirror){
        ItemStack toolStack = ItemStack.EMPTY;
        ItemStack repairItem = ItemStack.EMPTY;
        ItemStack creativeRepairItem = ItemStack.EMPTY;

        for(int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if(toolStack.isEmpty() && item.isDamageable()){
                toolStack = item;
                continue;
            }else if(creativeRepairItem.isEmpty() && item.getItem() instanceof RepairItem && (((RepairItem)item.getItem()).getPlank(item).isEmpty())){
                creativeRepairItem = item;
                continue;
            }else if(repairItem.isEmpty() && !item.isEmpty() && !(item.getItem() instanceof RepairItem)){
                repairItem = item;
                continue;
            }else if(item.isEmpty()){
                continue;
            }
            return false;
        }

        if(!repairItem.isEmpty() && !creativeRepairItem.isEmpty()){
            return toolStack.getItem().getIsRepairable(toolStack, repairItem);
        }
        return false;
    }

    @Override
    public ItemStack getCraftingResult(CraftingInventory inv){
        ItemStack repairItem = ItemStack.EMPTY;
        ItemStack creativeRepairItem = ItemStack.EMPTY;

        for(int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if(item.getItem() instanceof RepairItem && creativeRepairItem.isEmpty()  && ((RepairItem)item.getItem()).getPlank(item).isEmpty()){
                creativeRepairItem = item;
                continue;
            }else if(repairItem.isEmpty() && !item.isDamageable()){
                repairItem = item;
                continue;
            }else if(item.isEmpty() || item.isDamageable()){
                continue;
            }
            return ItemStack.EMPTY;
        }

        if(!repairItem.isEmpty() && !creativeRepairItem.isEmpty()){
            ItemStack holdStack = creativeRepairItem.copy();
            ((RepairItem)holdStack.getItem()).setPlank(holdStack, repairItem);
            holdStack.setCount(1);
            return holdStack;
        }
        return ItemStack.EMPTY;
    }

    @Nonnull
    public NonNullList<ItemStack> getRemainingItems(@Nonnull CraftingInventory inv) {
        NonNullList<ItemStack> nonnulllist = NonNullList.withSize(inv.getSizeInventory(), ItemStack.EMPTY);

        for(int i = 0; i < inv.getSizeInventory(); ++i){
            ItemStack item = inv.getStackInSlot(i);
            if(!item.isEmpty() && item.isDamageable()){
                nonnulllist.set(i, item.copy());
                break;
            }
        }

        return nonnulllist;
    }

    @Override
    protected void reloadIngredientList(){

    }
}
