package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.item.ModItems;
import weissmoon.random.item.equipment.armour.ItemArmourWood;

import javax.annotation.Nullable;
import java.util.Objects;


/**
 * Created by Weissmoon on 4/12/19.
 */
public class WoodHelmetRecipe extends WeissSpecialRecipe implements IShapedRecipe<CraftingInventory> {

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(WoodHelmetRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weissrandom:wood_helmet");

    public WoodHelmetRecipe(ResourceLocation o) {
        super(o, 3, 2);
//        super(new ResourceLocation("weissrandom:woodhelmet"), new ItemStack(ModItems.woodHelmet),"lll", "l l", 'l', "logWood");
//        this.group = group;
//        this.width = 3;
//        this.height = 2;
//        this.mirrored = false;
//        this.setRegistryName("weissrandom:woodhelmet");
    }

    @Override
    protected boolean checkMatch(CraftingInventory inv, int startX, int startY, boolean mirror)
    {
        ItemStack log = ItemStack.EMPTY;
        for (int x = 0; x < inv.getWidth(); x++)
        {
            for (int y = 0; y < inv.getHeight(); y++)
            {
                int subX = x - startX;
                int subY = y - startY;
                Ingredient target = Ingredient.EMPTY;

                if (subX >= 0 && subY >= 0 && subX < width && subY < height)
                {
                    if (mirror) {
                        target = this.recipeItems.get(this.width - subX - 1 + subY * this.width);
                    } else {
                        target = this.recipeItems.get(subX + subY * this.width);
                    }
                }

                ItemStack working = inv.getStackInSlot(x + y * inv.getWidth());
                if (!target.test(working)) {
                    return false;
                }

                if(log.isEmpty()){
                    log = working;
                }else if(!working.isEmpty()){
                    if(!log.isItemEqual(working)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    @Nullable
    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ItemStack planks = null;
        for (int x = 0; x < inv.getSizeInventory(); x++){
            boolean match = false;
            ItemStack stack = inv.getStackInSlot(x);
            if (stack != ItemStack.EMPTY) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("logWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                break;
//                }
                ResourceLocation plankTag = new ResourceLocation("minecraft", "logs");
                Item unknownItem = stack.getItem();
                boolean isInGroup = ItemTags.getCollection().get(plankTag).contains(unknownItem);
                if(isInGroup){
                    planks = stack;
                    break;
                }
            }
        }
        ItemStack outputasdfrwe = new ItemStack(ModItems.woodHelmet);
        if (outputasdfrwe.getItem() instanceof ItemArmourWood) {
            ((ItemArmourWood) outputasdfrwe.getItem()).setPlank(outputasdfrwe, planks);
        }
        return outputasdfrwe;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation() {
        return location;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ModItems.woodHelmet);
    }

    @Override
    public final int getRecipeWidth(){
        return this.width;
    }

    @Override
    public final int getRecipeHeight(){
        return this.height;
    }

    @Override
    protected void reloadIngredientList() {
        NonNullList<Ingredient> ingredientslist = NonNullList.withSize(width * height, Ingredient.EMPTY);
        Ingredient ingredient = Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:logs"))));
        ingredientslist.set(0, ingredient);
        ingredientslist.set(1, ingredient);
        ingredientslist.set(2, ingredient);
        ingredientslist.set(3, ingredient);
        ingredientslist.set(5, ingredient);
        recipeItems = ingredientslist;
    }
}
