package weissmoon.random.recipe;

import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraft.item.crafting.SpecialRecipeSerializer;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.item.RepairItem;

/**
 * Created by Weissmoon on 7/25/21.
 */
public class RepairRecipe extends WeissSpecialRecipe{

    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(RepairRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weiss_repair_recipe");

    public RepairRecipe(ResourceLocation o){
        super(o, 2, 2);
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ResourceLocation getLocation(){
        return location;
    }

    @Override
    protected boolean checkMatch(CraftingInventory inv, int startX, int startY, boolean mirror){
        ItemStack toolStack = ItemStack.EMPTY;
        ItemStack repairItem = ItemStack.EMPTY;

        for(int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if(item.isDamageable() && item.isDamaged() && toolStack.isEmpty()){
                toolStack = item;
                continue;
            }else if(item.getItem() instanceof RepairItem && repairItem.isEmpty()){
                repairItem = item;
                continue;
            }else if(item.isEmpty()){
                continue;
            }
            return false;
        }

        if(!toolStack.isEmpty() && !repairItem.isEmpty()){
            return true;
        }
        return false;
    }

    @Override
    public ItemStack getCraftingResult(CraftingInventory inv){
        ItemStack toolStack = null;
        ItemStack repairItem = null;

        for(int i = 0; i < inv.getSizeInventory(); ++i) {
            ItemStack item = inv.getStackInSlot(i);
            if(item.isDamageable() && item.isDamaged()){
                toolStack = item.copy();
            }else if(item.getItem() instanceof RepairItem){
                repairItem = item;
            }
        }

        if(toolStack != null && repairItem != null){
            ItemStack material = ((RepairItem)repairItem.getItem()).getPlank(repairItem);
            if(toolStack.getItem().getIsRepairable(toolStack, material)){
                int repairCost = toolStack.getRepairCost();
                if(repairCost >= 40)
                    return ItemStack.EMPTY;
                int minimumRepair = Math.min(toolStack.getDamage(), toolStack.getMaxDamage() / 4);//TODO configure
                if (minimumRepair <= 0) {
                    return ItemStack.EMPTY;
                }else{
                    toolStack.setDamage(toolStack.getDamage() - minimumRepair);
                }
                ++repairCost;
                toolStack.setRepairCost(repairCost);
                return toolStack;
            }
        }

        return ItemStack.EMPTY;
    }


    @Override
    protected void reloadIngredientList(){

    }
}
