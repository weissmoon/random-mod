package weissmoon.random.recipe;

import net.minecraft.block.Blocks;
import net.minecraft.inventory.CraftingInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.*;
import net.minecraft.tags.ItemTags;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.crafting.IShapedRecipe;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.item.ItemBlockProjectTable;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * Created by Weissmoon on 5/11/19.
 */
//TODO repackage special/ handlers
public class ProjectTableRecipe extends WeissSpecialRecipe implements IShapedRecipe<CraftingInventory> {

    //private static NonNullList<Ingredient> recipeItems;
    public static final IRecipeSerializer<?> SERIALIZER = new SpecialRecipeSerializer<>(ProjectTableRecipe::new);
    public static final ResourceLocation location = new ResourceLocation("weissrandom:project_tables");

    public ProjectTableRecipe(ResourceLocation o) {
        super(o, 1, 3);
//        super(new ResourceLocation("weissrandom:projecttable"), new ItemStack(ModBlocks.projectTable),"w", "p", "c", 'w', "workbench", 'p', "plankWood", 'c', "chestWood");
//        this.group = group;
//        this.width = 1;
//        this.height = 3;
//        this.mirrored = false;
//        this.setRegistryName("weissrandom:projecttable");
    }

    @Nullable
    @Override
    public ItemStack getCraftingResult(CraftingInventory inv) {
        ItemStack planks = null;
        for (int x = 0; x < inv.getSizeInventory(); x++){
            boolean match = false;
            ItemStack stack = inv.getStackInSlot(x);
            if (stack != ItemStack.EMPTY) {
//                Iterator<ItemStack> itr = (OreDictionary.getOres("plankWood")).iterator();
//                while (itr.hasNext() && !match) {
//                    match = OreDictionary.itemMatches(itr.next(), stack, false);
//                }
//                if(match) {
//                    planks = stack;
//                    break;
//                }
                ResourceLocation plankTag = new ResourceLocation("minecraft", "planks");
                Item unknownItem = stack.getItem();
                boolean isInGroup = ItemTags.getCollection().get(plankTag).contains(unknownItem);
                if(isInGroup){
                    planks = stack;
                    break;
                }
            }
        }
        ItemStack outputasdfrwe = new ItemStack(ModBlocks.projectTable);
        if (outputasdfrwe.getItem() instanceof ItemBlockProjectTable) {
            ((ItemBlockProjectTable)outputasdfrwe.getItem()).setPlank(outputasdfrwe, planks);
        }
        return outputasdfrwe;
    }

    @Override
    public boolean canFit(int width, int height) {
        return width >= 1 && height >= 3;
    }

    @Override
    public IRecipeSerializer<?> getSerializer(){
        return SERIALIZER;
    }

    @Override
    public ItemStack getRecipeOutput() {
        return new ItemStack(ModBlocks.projectTable);
    }

    @Override
    public final int getRecipeWidth(){
        return this.width;
    }

    @Override
    public final int getRecipeHeight(){
        return this.height;
    }

    @Override
    public ResourceLocation getLocation() {
        return location;
    }

    @Override
    protected void reloadIngredientList() {
        NonNullList<Ingredient> ingredientslist = NonNullList.withSize(width * height, Ingredient.EMPTY);
        //ingredientslist.set(0, Ingredient.fromTag(ItemTags.getCollection().getOrCreate(new ResourceLocation("forge:workbench"))));
        ingredientslist.set(0, Ingredient.fromStacks(new ItemStack(Blocks.CRAFTING_TABLE)));
        ingredientslist.set(1, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("minecraft:planks")))));
        ingredientslist.set(2, Ingredient.fromTag(Objects.requireNonNull(ItemTags.getCollection().get(new ResourceLocation("forge:chests/wooden")))));
        recipeItems = ingredientslist;
    }
}
