package weissmoon.random.recipe;

import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;

import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * Created by Weissmoon on 5/4/21.
 */
public class IngredientWithSize extends Ingredient{

    private final Ingredient.IItemList[] acceptedItems;
    private ItemStack[] matchingStacks;
    private int count;

    protected IngredientWithSize(Stream<? extends Ingredient.IItemList> itemLists, int count){
        super(Stream.of());
        this.acceptedItems = itemLists.toArray(Ingredient.IItemList[]::new);
    }

    public static Ingredient fromItemListStream(Stream<? extends Ingredient.IItemList> stream, int count) {
        IngredientWithSize ingredient = new IngredientWithSize(stream, count);
        return ingredient.acceptedItems.length == 0 ? Ingredient.EMPTY : ingredient;
    }

    @Override
    public boolean test(ItemStack stack){
        return false;
    }
}
