package weissmoon.random.recipe;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

/**
 * Created by Weissmoon on 3/10/22.
 */
public class CrumbsInventory implements IInventory{

    public static ItemStack foodStack = ItemStack.EMPTY;
    public static final CrumbsInventory INSTANCE = new CrumbsInventory();

    @Override
    public int getSizeInventory(){
        return 1;
    }

    public ItemStack getFood(){
        return foodStack;
    }

    public void  setFood(ItemStack stack){
        foodStack = stack;
    }


    @Override
    public boolean isEmpty(){
        return false;
    }

    @Override
    public ItemStack getStackInSlot(int index){
        return null;
    }

    @Override
    public ItemStack decrStackSize(int index, int count){
        return ItemStack.EMPTY;
    }

    @Override
    public ItemStack removeStackFromSlot(int index){
        return ItemStack.EMPTY;
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack){}

    @Override
    public void markDirty(){}

    @Override
    public boolean isUsableByPlayer(PlayerEntity player){
        return false;
    }

    @Override
    public void clear(){}
}
