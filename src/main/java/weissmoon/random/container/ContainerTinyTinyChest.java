package weissmoon.random.container;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileTinyChest;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 6/30/21.
 */
public class ContainerTinyTinyChest extends Container{

    private TileTinyChest tileTinyChest;


    @ObjectHolder(Reference.MOD_ID+":"+ Strings.Blocks.TINY_CHEST+"tiny")
    public static ContainerType<ContainerTinyTinyChest> TYPE;


    public static ContainerTinyTinyChest fromNetwork(int windowId, PlayerInventory playerInventory, PacketBuffer packetBuffer) {
        BlockPos pos = packetBuffer.readBlockPos();
        TileTinyChest tile = (TileTinyChest)playerInventory.player.world.getTileEntity(pos);
        return new ContainerTinyTinyChest(windowId, playerInventory, tile);
    }

    public ContainerTinyTinyChest(int id, PlayerInventory playerInventory, IInventory chestInventory){
        super(TYPE, id);

        this.tileTinyChest = (TileTinyChest) chestInventory;

        this.addSlot(new Slot(chestInventory, 0, 80,  20));

        for (int l = 0; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 8 + k * 18, l * 18 + 51));

        for (int i1 = 0; i1 < 9; ++i1)
            this.addSlot(new Slot(playerInventory, i1, 8 + i1 * 18, 109));

        if(!playerInventory.player.world.isRemote)
            tileTinyChest.openInventory(playerInventory.player);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        return  ContainerHelper.transferStackInSlot(playerIn, index, inventorySlots, 0, 0, 1, 37);
//        ItemStack itemstack = ItemStack.EMPTY;
//        Slot slot = inventorySlots.get(index);
//        if (slot != null && slot.getHasStack()) {
//            ItemStack itemstack1 = slot.getStack();
//            itemstack = itemstack1.copy();
//
//            if(index == 0){
//                if(!mergeItemStack(itemstack1, 1, 37, false))
//                    return ItemStack.EMPTY;
//            }else if(index >= 3 && index < 39){
//                if(!mergeItemStack(itemstack1, 0, 1, false))
//                    return ItemStack.EMPTY;
//            }
//
//            if(itemstack1.isEmpty())
//                slot.putStack(ItemStack.EMPTY);
//            else
//                slot.onSlotChanged();
//
//            if(itemstack1.getCount() == itemstack.getCount())
//                return ItemStack.EMPTY;
//        }
//        return itemstack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn){
        return (tileTinyChest.getWorld().getTileEntity(tileTinyChest.getPos()) == tileTinyChest &&
                isWithinUsableDistance(IWorldPosCallable.of(tileTinyChest.getWorld(), tileTinyChest.getPos()), playerIn, ModBlocks.tinyChest));
    }

    public void onContainerClosed(PlayerEntity playerIn) {
        super.onContainerClosed(playerIn);
        if(!playerIn.world.isRemote)
        tileTinyChest.closeInventory(playerIn);
    }
}
