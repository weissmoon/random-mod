package weissmoon.random.container;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.crafting.RecipeItemHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.*;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.CraftingResultSlot;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.ICraftingRecipe;
import net.minecraft.item.crafting.IRecipeType;
import net.minecraft.network.PacketBuffer;
import net.minecraft.network.play.server.SSetSlotPacket;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileProjectTable;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

import java.util.Optional;

/**
 * Created by Weissmoon on 5/7/19.
 */
public class ContainerProjectTable extends Container {

    private final ContainerProjectTable inst;
    public CraftingInventory craftMatrix;
    public TileProjectTable chest;
    public CraftResultInventory craftResult;
    private final PlayerEntity player;
    @ObjectHolder(Reference.MOD_ID+":"+Strings.Blocks.PROJECT_TABLE)
    public static ContainerType<ContainerProjectTable> TYPE;

    public static ContainerProjectTable fromNetwork(int windowId, PlayerInventory playerInventory, PacketBuffer packetBuffer) {
        BlockPos pos = packetBuffer.readBlockPos();
        TileProjectTable tile = (TileProjectTable)playerInventory.player.world.getTileEntity(pos);
        return new ContainerProjectTable(windowId, playerInventory, tile, playerInventory.player);
    }

    public ContainerProjectTable(int id, IInventory playerInventory, IInventory chestInventory, PlayerEntity player){
        super(TYPE, id);
        this.inst = this;
        this.craftMatrix = new SharedCraftingInventory(this);
        this.chest = (TileProjectTable)chestInventory;
        this.craftResult = new CraftResultInventory(){
            @Override
            public void markDirty(){
                chest.markDirty();
                BlockState state = chest.getWorld().getBlockState(chest.getPos());
                chest.getWorld().notifyBlockUpdate(chest.getPos(), state, state, 3);
            }
        };
        this.player = player;


        this.addSlot(new CraftingResultSlot(this.player, craftMatrix, this.craftResult, 0, 124, 35));

        for (int i = 0; i < 3; ++i)
            for (int j = 0; j < 3; ++j)
                this.addSlot(new Slot(craftMatrix, j + i * 3, 30 + j * 18, 17 + i * 18));

        for (int l = 1; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(chestInventory, k + l * 9, 8 + k * 18, l * 18 + 69));

        for (int l = 0; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 8 + k * 18, l * 18 + 133));

        for (int i1 = 0; i1 < 9; ++i1)
            this.addSlot(new Slot(playerInventory, i1, 8 + i1 * 18, 191));
        onCraftMatrixChanged(craftMatrix);
    }

    @Override
    public void onCraftMatrixChanged(IInventory inventoryIn)
    {
        this.slotChangedCraftingGrid(player.world, this.player, this.craftMatrix, this.craftResult);
    }

//    @Override
    protected void slotChangedCraftingGrid(World world, PlayerEntity player, CraftingInventory inv, CraftResultInventory result) {
        if(!world.isRemote){
            ServerPlayerEntity serverplayerentity = (ServerPlayerEntity)player;
            ItemStack itemstack = ItemStack.EMPTY;
            ICraftingRecipe irecipe;
            Optional<ICraftingRecipe> optional = world.getServer().getRecipeManager().getRecipe(IRecipeType.CRAFTING, inv, world);

            if(optional.isPresent()){
                irecipe = optional.get();
                result.setRecipeUsed(irecipe);
                itemstack = irecipe.getCraftingResult(inv);
            }

            result.setInventorySlotContents(0, itemstack);
            for (ServerPlayerEntity playerw:serverplayerentity.getServer().getPlayerList().getPlayers())
                if(playerw.openContainer instanceof ContainerProjectTable)
                    if(((ContainerProjectTable) playerw.openContainer).chest == this.chest)
                        playerw.connection.sendPacket(new SSetSlotPacket(playerw.openContainer.windowId, 0, itemstack));
        }
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn) {
        return (chest.getWorld().getTileEntity(chest.getPos()) == chest &&
                isWithinUsableDistance(IWorldPosCallable.of(chest.getWorld(), chest.getPos()), playerIn, ModBlocks.projectTable));
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);
        if(slot != null && slot.getHasStack()){
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();
            if(index == 0){
                itemstack1.getItem().onCreated(itemstack1, playerIn.world, playerIn);
                if(!mergeItemStack(itemstack1, 28, 63, true))
                    return ItemStack.EMPTY;

                slot.onSlotChange(itemstack1, itemstack);
            }else if(index<9){//1-9
                if(!mergeItemStack(itemstack1, 10, 27, false))
                    return ItemStack.EMPTY;
            }else if(index < 27){//10-27
                if(!mergeItemStack(itemstack1, 28, 63, false))
                    return ItemStack.EMPTY;
            }else if(index < 64){//28-64
                if(!mergeItemStack(itemstack1, 10, 27, false))
                    return ItemStack.EMPTY;
            }else if(!mergeItemStack(itemstack1, 28, 63, false))//???
                return ItemStack.EMPTY;

            if(itemstack1.isEmpty())
                slot.putStack(ItemStack.EMPTY);
            else
                slot.onSlotChanged();

            if(itemstack1.getCount() == itemstack.getCount())
                return ItemStack.EMPTY;

            ItemStack itemstack2 = slot.onTake(playerIn, itemstack1);
            if(index == 0)
                playerIn.dropItem(itemstack2, false);
        }
        return itemstack;
    }


    public class SharedCraftingInventory extends CraftingInventory{

        public SharedCraftingInventory(Container eventHandlerIn){
            super(eventHandlerIn, 3, 3);
        }

        @Override
        public ItemStack getStackInSlot(int index){
            return index >= this.getSizeInventory() ? ItemStack.EMPTY : chest.getStackInSlot(index);
        }

        @Override
        public int getSizeInventory(){
            return 9;
        }

        @Override
        public boolean isEmpty(){
            for(int i = 0; i<9; i++)
                if(chest.getStackInSlot(i).isEmpty())
                    return false;

            return true;
        }

        @Override
        public ItemStack removeStackFromSlot(int index){
            inst.onCraftMatrixChanged(this);
            return chest.removeStackFromSlot(index);
        }

        @Override
        public ItemStack decrStackSize(int index, int count){
            ItemStack itemstack = chest.decrStackSize(index, count);

            if (!itemstack.isEmpty())
                inst.onCraftMatrixChanged(this);

            return itemstack;
        }

        /**
         * Sets the given item stack to the specified slot in the inventory (can be crafting or armor sections).
         */
        @Override
        public void setInventorySlotContents(int index, ItemStack stack){
            chest.setInventorySlotContents(index, stack);
            inst.onCraftMatrixChanged(this);
        }

        @Override
        public void openInventory(PlayerEntity player){
            super.openInventory(player);
        }

        @Override
        public void closeInventory(PlayerEntity player){
            super.closeInventory(player);
        }

        @Override
        public void clear(){
            //this.stackList.clear();
        }

        @Override
        public void fillStackedContents(RecipeItemHelper helper){
            for (int i = 0; i<9; i++){
                ItemStack itemstack = chest.getStackInSlot(i);
                helper.accountStack(itemstack);
            }
        }
    }
}
