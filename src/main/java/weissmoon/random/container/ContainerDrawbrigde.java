package weissmoon.random.container;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.core.container.CommonSlots;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileDrawbrigde;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/6/19.
 */
public class ContainerDrawbrigde extends Container {

    private TileDrawbrigde tileDrawbrigde;

    @ObjectHolder(Reference.MOD_ID+":"+ Strings.Blocks.DRAWBRIGDE_NAME)
    public static ContainerType<ContainerDrawbrigde> TYPE;

    public static ContainerDrawbrigde fromNetwork(int windowId, PlayerInventory playerInventory, PacketBuffer packetBuffer) {
        BlockPos pos = packetBuffer.readBlockPos();
        TileDrawbrigde tile = (TileDrawbrigde)playerInventory.player.world.getTileEntity(pos);
        return new ContainerDrawbrigde(windowId, playerInventory, tile);
    }

    public ContainerDrawbrigde(int id, IInventory playerInventory, IInventory chestInventory){
        super(TYPE, id);

        this.tileDrawbrigde = (TileDrawbrigde) chestInventory;
        this.addSlot(new CommonSlots.BlockSlot(chestInventory, 0, 80, 20));
        this.addSlot(new CommonSlots.SizeLimitedSlot(chestInventory, 1, 134, 20, 1));

        for (int l = 0; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 8 + k * 18, l * 18 + 51));

        for (int i1 = 0; i1 < 9; ++i1)
            this.addSlot(new Slot(playerInventory, i1, 8 + i1 * 18, 109));
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn){
        return (tileDrawbrigde.getWorld().getTileEntity(tileDrawbrigde.getPos()) == tileDrawbrigde &&
                isWithinUsableDistance(IWorldPosCallable.of(tileDrawbrigde.getWorld(), tileDrawbrigde.getPos()), playerIn, ModBlocks.drawbridge));
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index){
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = this.inventorySlots.get(index);

        if(slot != null && slot.getHasStack()){
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index < 2){
                if(!this.mergeItemStack(itemstack1, 2, this.inventorySlots.size(), true))
                    return ItemStack.EMPTY;
            }else if(!this.mergeItemStack(itemstack1, 0, 1, false))
                return ItemStack.EMPTY;

            if (itemstack1.isEmpty())
                slot.putStack(ItemStack.EMPTY);
            else
                slot.onSlotChanged();
        }

        return itemstack;
    }
}
