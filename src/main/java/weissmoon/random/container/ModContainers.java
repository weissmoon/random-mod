package weissmoon.random.container;

import net.minecraft.client.gui.ScreenManager;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.IForgeRegistryEntry;
import weissmoon.random.client.gui.*;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 12/23/19.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModContainers {

    public static final ContainerType<ContainerQuiver1> quiver = IForgeContainerType.create(ContainerQuiver1::fromNetwork);
    public static final ContainerType<ContainerProjectTable> table = IForgeContainerType.create(ContainerProjectTable::fromNetwork);
    public static final ContainerType<ContainerDrawbrigde> drawbridge = IForgeContainerType.create(ContainerDrawbrigde::fromNetwork);
    public static final ContainerType<ContainerTinyChest> tinyChest = IForgeContainerType.create(ContainerTinyChest::fromNetwork);
    public static final ContainerType<ContainerTinyTinyChest> tinyTinyChest = IForgeContainerType.create(ContainerTinyTinyChest::fromNetwork);

    @SubscribeEvent
    public static void registerContainers(RegistryEvent.Register<ContainerType<?>> evt) {
        IForgeRegistry<ContainerType<?>> containerTypes = evt.getRegistry();
        register(containerTypes, quiver, new ResourceLocation(Reference.MOD_ID+":"+Strings.Items.QUIVER_1_NAME));

        register(containerTypes, table, new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.PROJECT_TABLE));

        register(containerTypes, drawbridge, new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.DRAWBRIGDE_NAME));
        register(containerTypes, tinyChest, new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.TINY_CHEST));
        register(containerTypes, tinyTinyChest, new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.TINY_CHEST+"tiny"));

        DistExecutor.runWhenOn(Dist.CLIENT, () -> ModContainers::registerScreenFactories);
    }

    @OnlyIn(Dist.CLIENT)
    public static void registerScreenFactories(){
        ScreenManager.registerFactory(quiver, Quiver1Gui::new);
        ScreenManager.registerFactory(table, ProjectTableGui::new);
        ScreenManager.registerFactory(drawbridge, DrawbrigdeGui::new);
        ScreenManager.registerFactory(tinyChest, TinyChestGui::new);
        ScreenManager.registerFactory(tinyTinyChest, TinyTinyChestGui::new);
    }

    public static <V extends IForgeRegistryEntry<V>> void register(IForgeRegistry<V> reg, IForgeRegistryEntry<V> thing, ResourceLocation name) {
        reg.register(thing.setRegistryName(name));
    }
}
