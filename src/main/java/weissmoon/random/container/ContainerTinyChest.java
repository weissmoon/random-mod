package weissmoon.random.container;

import net.minecraft.block.Blocks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.IWorldPosCallable;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileTinyChest;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 6/30/21.
 */
public class ContainerTinyChest extends Container{

    private TileTinyChest tileTinyChest;

    @ObjectHolder(Reference.MOD_ID+":"+ Strings.Blocks.TINY_CHEST)
    public static ContainerType<ContainerTinyChest> TYPE;

    public static ContainerTinyChest fromNetwork(int windowId, PlayerInventory playerInventory, PacketBuffer packetBuffer) {
        BlockPos pos = packetBuffer.readBlockPos();
        TileTinyChest tile = (TileTinyChest)playerInventory.player.world.getTileEntity(pos);
        return new ContainerTinyChest(windowId, playerInventory, tile);
    }

    public ContainerTinyChest(int id, PlayerInventory playerInventory, IInventory chestInventory){
        super(TYPE, id);

        this.tileTinyChest = (TileTinyChest) chestInventory;

        for (int l = 0; l < 9; ++l)
            for (int k = 0; k < 12; ++k)
                this.addSlot(new Slot(chestInventory, k + l * 12, 8 + k * 18, l * 18 + 17));

        for (int l = 0; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 35 + k * 18, l * 18 + 190));

        for (int i1 = 0; i1 < 9; ++i1)
            this.addSlot(new Slot(playerInventory, i1, 35 + i1 * 18, 249));

        if(!playerInventory.player.world.isRemote)
            tileTinyChest.openInventory(playerInventory.player);
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        return  ContainerHelper.transferStackInSlot(playerIn, index, inventorySlots, 0, 107, 108, 144);
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn){
        return (tileTinyChest.getWorld().getTileEntity(tileTinyChest.getPos()) == tileTinyChest &&
                isWithinUsableDistance(IWorldPosCallable.of(tileTinyChest.getWorld(), tileTinyChest.getPos()), playerIn, ModBlocks.tinyChest));
    }

    public void onContainerClosed(PlayerEntity playerIn) {
        super.onContainerClosed(playerIn);
        if(!playerIn.world.isRemote)
        tileTinyChest.closeInventory(playerIn);
    }
}
