package weissmoon.random.container;

import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Slot;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Hand;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.core.container.CommonSlots;
import weissmoon.random.item.equipment.ItemQuiver1;
import weissmoon.random.item.equipment.ItemQuiverBase;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 5/18/19.
 */
public class ContainerQuiver1 extends Container {

    @ObjectHolder(Reference.MOD_ID+":"+ Strings.Items.QUIVER_1_NAME)
    public static ContainerType<ContainerQuiver1> TYPE;

    public static ContainerQuiver1 fromNetwork(int windowId, PlayerInventory player, PacketBuffer inventory){
        IInventory quiver;
        if (player.getCurrentItem().getItem() instanceof ItemQuiver1)
            quiver = ItemQuiverBase.getQuiverInventoryS(player.getCurrentItem());
        else
            quiver = ItemQuiverBase.getQuiverInventoryS(player.offHandInventory.get(0));
        return new ContainerQuiver1(windowId, player, quiver);
    }

    public ContainerQuiver1(int id, PlayerInventory playerInventory, IInventory chestInventory){
        super(ModContainers.quiver, id);
        this.addSlot(new ArrowSlot(chestInventory, 0, 62, 20));
        this.addSlot(new ArrowSlot(chestInventory, 1, 80, 20));
        this.addSlot(new ArrowSlot(chestInventory, 2, 98, 20));

        for (int l = 0; l < 3; ++l)
            for (int k = 0; k < 9; ++k)
                this.addSlot(new Slot(playerInventory, k + l * 9 + 9, 8 + k * 18, l * 18 + 51));

        for (int i1 = 0; i1 < 9; ++i1)
            if(i1 == playerInventory.currentItem)
                this.addSlot(new CommonSlots.LockedSlot(playerInventory, i1, 8 + i1 * 18, 109));
            else
                this.addSlot(new Slot(playerInventory, i1, 8 + i1 * 18, 109));
    }

    @Override
    public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);
        if(slot != null && slot.getHasStack()){
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if (index >= 0 && index < 3){
                if (!mergeItemStack(itemstack1, 3, 39, false))
                    return ItemStack.EMPTY;
            }else if(index < 39){
                if (!mergeItemStack(itemstack1, 0, 3, false))
                    return ItemStack.EMPTY;
            }

            if(itemstack1.isEmpty())
                slot.putStack(ItemStack.EMPTY);
            else
                slot.onSlotChanged();

            if(itemstack1.getCount() == itemstack.getCount())
                return ItemStack.EMPTY;
        }
        return itemstack;
    }

    @Override
    public boolean canInteractWith(PlayerEntity playerIn){
        return true;
    }

    private static class ArrowSlot extends Slot{

        public ArrowSlot(IInventory inventoryIn, int index, int xPosition, int yPosition){
            super(inventoryIn, index, xPosition, yPosition);
        }

        @Override
        public boolean isItemValid(ItemStack stack){
            return (stack.getItem() instanceof ArrowItem);
        }
    }
}
