package weissmoon.random.client;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.ItemColors;
import weissmoon.random.Randommod;
import weissmoon.random.item.ModItems;
import weissmoon.random.item.equipment.armour.ItemDesertArmour;

import static com.teammetallurgy.aquaculture.client.ClientHandler.registerFishingRodModelProperties;

/**
 * Created by Weissmoon on 3/16/21.
 */
public class ClientHandler{

    public static void setupClient() {
        if(Randommod.aquacultureLoaded)
            registerFishingRodModelProperties(ModItems.gildedFishingRod);
        if(Randommod.atumLoaded){
            ItemColors itemColor = Minecraft.getInstance().getItemColors();
            itemColor.register((stack, tintIndex) -> tintIndex>0 ? -1:((ItemDesertArmour)stack.getItem()).getColor(stack), ModItems.gildedDesertHelmet, ModItems.gildedDesertChestpiece, ModItems.gildedDesertLegging, ModItems.gildedDesertFeet);
        }
    }
}
