package weissmoon.random.client.hud;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.ListNBT;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;

/**
 * Created by Weissmoon on 1/29/22.
 */
public class CrossbowHUDItem implements IHUDItem{

    public static CrossbowHUDItem INSTANCE;

    static{
        INSTANCE = new CrossbowHUDItem();
    }

    @Override
    public boolean isSimple(ItemStack stack){
        return false;
    }

    @Override
    public boolean doRenderHUD(ItemStack stack){
        return true;
    }

    @Override
    public void renderHUD(MatrixStack matrixStack, ItemStack hudItemStack){
        CompoundNBT stackTag = hudItemStack.getTag();
        if (stackTag != null && stackTag.contains("ChargedProjectiles", 9)) {
            ListNBT projectiles = stackTag.getList("ChargedProjectiles", 10);
            ItemStack quiver = ItemStack.read(projectiles.getCompound(0));
            HUDHelper.renderItemOppositeOffhandHud(matrixStack, quiver, "", 16777215);
        }
    }
}
