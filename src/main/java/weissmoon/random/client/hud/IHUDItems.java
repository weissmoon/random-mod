package weissmoon.random.client.hud;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import weissmoon.core.api.WeissCoreAPI;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ModItems;
import weissmoon.random.item.RepairItem;
import weissmoon.random.item.equipment.ItemQuiver1;
import weissmoon.random.item.equipment.ItemQuiverBase;


/**
 * Created by Weissmoon on 1/5/20.
 */
public class IHUDItems {
    public static void register() {
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(Items.BOW, BowHUDItem.INSTANCE);
        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(Items.CROSSBOW, CrossbowHUDItem.INSTANCE);

        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(ModItems.quiver1, new IHUDItem() {
            @Override
            public boolean isSimple(ItemStack stack){
                return true;
            }

            @Override
            public boolean doRenderHUD(ItemStack stack){
                if(stack.getItem() instanceof ItemQuiver1){
                    return ItemQuiver1.hasArrowStatic(stack, (ItemQuiverBase) stack.getItem());
                }
                return false;
            }

            @Override
            public ItemStack getDisplayStack(ItemStack stack) {
                return ((ItemQuiverBase) stack.getItem()).getActiveArrow(stack);
            }
        });

        WeissCoreAPI.hudItemRegistry.registerIIHUDItem(ModItems.repairItem, new IHUDItem() {
            @Override
            public boolean isSimple(ItemStack stack){
                return false;
            }

            @Override
            public boolean doRenderHUD(ItemStack stack){
                if(stack.getItem() instanceof RepairItem){
                    CompoundNBT tags = stack.getTag();
                    if (tags != null){
                        ItemStack repair = IPlankHolder.getPlankS(stack);
                        if(!repair.isEmpty())
                            return true;
                    }
                }
                return false;
            }

            @Override
            public void renderHUD(MatrixStack matrixStack, ItemStack stack){
                CompoundNBT tags = stack.getTag();
                if (tags == null)
                    return;
                ItemStack renderStack = IPlankHolder.getPlankS(stack);
                HUDHelper.renderItemOppositeOffhandHudNoString(matrixStack, renderStack);
            }
        });
    }
}
