package weissmoon.random.client.hud;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.MainWindow;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TippedArrowItem;
import net.minecraft.util.Hand;
import net.minecraft.util.HandSide;
import weissmoon.core.api.client.helper.HUDHelper;
import weissmoon.core.api.client.item.IHUDItem;
import weissmoon.random.Randommod;
import weissmoon.random.compat.BaublesCompat;
import weissmoon.random.compat.CuriosCompat;
import weissmoon.random.item.equipment.ItemQuiverBase;

import static weissmoon.random.item.equipment.ItemQuiverBase.hasArrowStatic;
import static weissmoon.random.item.equipment.ItemQuiverBase.isQuiver;

/**
 * Created by Weissmoon on 5/31/21.
 */
public class BowHUDItem implements IHUDItem{

    public static BowHUDItem INSTANCE;

    static{
        INSTANCE = new BowHUDItem();
    }

    @Override
    public boolean isSimple(ItemStack stack){
        return false;
    }

    @Override
    public boolean doRenderHUD(ItemStack stack){
        PlayerEntity player = Minecraft.getInstance().player;
        for(int i = 0; i < player.inventory.getSizeInventory(); ++i){
            ItemStack itemstack = player.inventory.getStackInSlot(i);

            if(itemstack.getItem() instanceof ArrowItem || itemstack.getItem() instanceof ItemQuiverBase)
                return true;
        }
        if (Randommod.baublesLoaded)
            if (BaublesCompat.hasQuiverBauble(player)) {
                ItemStack quiverBauble = BaublesCompat.getFirstQuiver(player);
                return hasArrowStatic(quiverBauble, (ItemQuiverBase) quiverBauble.getItem());
            }
        if(Randommod.curiosLoaded)
            if (CuriosCompat.hasQuiverCurio(player)) {
                ItemStack quiverCurio = CuriosCompat.getFirstQuiver(player);
                return hasArrowStatic(quiverCurio, (ItemQuiverBase) quiverCurio.getItem());
            }
        return false;
    }

    ItemStack arrowStack = null;
    ArrowItem arrowItem = null;
    int count = 0;
    int countNext = 0;
    boolean next = false;

    @Override
    public void renderHUD(MatrixStack matrixStack, ItemStack hudItemStack) {
        PlayerEntity player = Minecraft.getInstance().player;
        arrowStack = null;
        arrowItem = null;
        count = 0;
        countNext = 0;
        next = false;

        primaryHandle:
        {
            ItemStack offHand = player.getHeldItem(Hand.OFF_HAND);
            if(isQuiver(offHand)){
                if(hasArrowStatic(offHand, (ItemQuiverBase) offHand.getItem())){
                    ItemStack quiver = ((ItemQuiverBase) offHand.getItem()).getActiveArrow(offHand);
                    HUDHelper.renderItemOppositeOffhandHud(matrixStack, quiver, String.valueOf(quiver.getCount()), 16777215);
                    return;
                }
            }else if(offHand.getItem() instanceof ArrowItem){
                if(offHand.getItem() instanceof TippedArrowItem)
                    arrowStack = offHand;
                arrowItem = (ArrowItem)offHand.getItem();
                count += offHand.getCount();
                break primaryHandle;
            }
            ItemStack mainHand = player.getHeldItem(Hand.MAIN_HAND);
            if(isQuiver(player.getHeldItem(Hand.MAIN_HAND))){
                if(hasArrowStatic(mainHand, (ItemQuiverBase) mainHand.getItem())){
                    ItemStack quiver = ((ItemQuiverBase) mainHand.getItem()).getActiveArrow(mainHand);
                    HUDHelper.renderItemOppositeOffhandHud(matrixStack, quiver, String.valueOf(quiver.getCount()), 16777215);
                    return;
                }
            }else if(mainHand.getItem() instanceof ArrowItem){
                if(mainHand.getItem() instanceof TippedArrowItem)
                    arrowStack = mainHand;
                arrowItem = (ArrowItem)mainHand.getItem();
                count += mainHand.getCount();
                break primaryHandle;
            }
            if(Randommod.baublesLoaded)
                if(BaublesCompat.hasQuiverBauble(player)){
                    ItemStack quiverBauble = CuriosCompat.getFirstQuiver(player);
                    if(hasArrowStatic(quiverBauble, (ItemQuiverBase) quiverBauble.getItem())){
                        ItemStack quiver = ((ItemQuiverBase) quiverBauble.getItem()).getActiveArrow(quiverBauble);
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, quiver, String.valueOf(quiver.getCount()), 16777215);
                        return;
                    }
                }
            if(Randommod.curiosLoaded)
                if(CuriosCompat.hasQuiverCurio(player)){
                    ItemStack quiverCurio = CuriosCompat.getFirstQuiver(player);
                    if(hasArrowStatic(quiverCurio, (ItemQuiverBase) quiverCurio.getItem())){
                        ItemStack quiver = ((ItemQuiverBase) quiverCurio.getItem()).getActiveArrow(quiverCurio);
                        HUDHelper.renderItemOppositeOffhandHud(matrixStack, quiver, String.valueOf(quiver.getCount()), 16777215);
                        return;
                    }
                }
        }
        for(int i = 0; i < player.inventory.getSizeInventory() - 1; ++i){
            ItemStack itemstack = player.inventory.getStackInSlot(i);

            if(arrowItem == null && isQuiver(itemstack))
                if(hasArrowStatic(itemstack, (ItemQuiverBase) itemstack.getItem())){
                    arrowStack = ((ItemQuiverBase) itemstack.getItem()).getActiveArrow(itemstack);
                    count = arrowStack.getCount();
                    break;
                }
            if(itemstack.getItem() instanceof ArrowItem || isQuiver(itemstack)){
                if(itemstack.getItem() instanceof TippedArrowItem && arrowItem == null){
                    arrowStack = itemstack;
                    count = itemstack.getCount();
                    break;
                }
                if(arrowItem == null && !isQuiver(itemstack))
                    arrowItem = (ArrowItem)itemstack.getItem();
                if(itemstack.getItem() == arrowItem){
                    count += itemstack.getCount();
                }else if(itemstack.getItem() != arrowItem && !next){
                    if(isQuiver(itemstack))
                        if(!hasArrowStatic(itemstack, (ItemQuiverBase)itemstack.getItem()))
                            continue;
                    countNext += count;
                    next = true;
                }
            }
        }
//        TODO
//        if(30 != ClientEvents.getTickTImeUp())
//            matrixStack.translate(0, 30-ClientEvents.getTickTImeUp()+ClientEvents.getPartialTick(), 0);
        render(matrixStack, hudItemStack);
    }

    @Override
    public void renderPreviousHUD(MatrixStack matrixStack, ItemStack heldStack){
//        TODO
//        if(30 != ClientEvents.getTickTImeUp())
//            matrixStack.translate(0, 30-ClientEvents.getTickTImeUp()-ClientEvents.getPartialTick(), 0);
        render(matrixStack, heldStack);
    }

    private void render(MatrixStack matrixStack, ItemStack heldStack){
        HUDHelper.renderItemOppositeOffhandHud(matrixStack, arrowStack == null ? new ItemStack(arrowItem) : arrowStack, String.valueOf(count), 16777215);
        if(!next)
            return;
        Minecraft minecraft = Minecraft.getInstance();
        HandSide side = minecraft.player.getPrimaryHand().opposite();
        String countS = String.valueOf(countNext);
        MainWindow sr = minecraft.mainWindow;
        int i = sr.getScaledWidth() / 2;
        int l1 = sr.getScaledHeight() - 19;
        GlStateManager.disableDepthTest();
        if(side == HandSide.RIGHT){
            if(count > 1000)
                i -= 6;
            minecraft.fontRenderer.drawStringWithShadow(matrixStack, countS, i - 122 - minecraft.fontRenderer.getStringWidth(countS), l1 + 6 + 3, 16777215);
        }else{
            if(countNext < 10)
                i += 6;
            else if(countNext < 100)
                i += 12;
            else if(countNext < 1000)
                i += 18;
            else
                i += 24;
            minecraft.fontRenderer.drawStringWithShadow(matrixStack, countS, i + 124 - minecraft.fontRenderer.getStringWidth(countS), l1 + 6 + 3, 16777215);
        }
        GlStateManager.enableDepthTest();
    }
}
