package weissmoon.random.client.event;

import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.event.TickEvent;
import org.lwjgl.opengl.GL11;

/**
 * Created by Weissmoon on 5/23/19.
 */
public class RadialEventHandler{

    public boolean showRadial = false;

    //@SubscribeEvent
    public void renderTickEvent(TickEvent.RenderTickEvent event){
        if(true)
        return;
        GL11.glPushMatrix();
        try{
            float radius = 50f;

            int SLICES = 50;
            float degrees = 7.2f;
            float radians = 0.13f;

            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

            GL11.glMatrixMode(GL11.GL_MODELVIEW);
            GL11.glPushMatrix();
            GL11.glLoadIdentity();

            GL11.glMatrixMode(GL11.GL_PROJECTION);
            GL11.glPushMatrix();
            GL11.glLoadIdentity();

            final int stencilBit = MinecraftForgeClient.reserveStencilBit();
            final int stencilMask = 1 << stencilBit;
            GL11.glStencilMask(stencilMask);
            GL11.glStencilFunc(GL11.GL_ALWAYS, 0, stencilMask);
            GL11.glBegin(GL11.GL_TRIANGLE_FAN);
            for(int lo = 0;lo < SLICES;lo++){
                float heading = lo * radians;
                GL11.glVertex3d(Math.cos(heading) * radius, Math.sin(heading) * radius, 0.0D);
            }
            GL11.glEnd();
            MinecraftForgeClient.releaseStencilBit(stencilBit);
            GL11.glPopMatrix();
            GL11.glMatrixMode(GL11.GL_MODELVIEW);

            GL11.glPopMatrix();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_TEXTURE_2D);


        }finally {
            GL11.glPopMatrix();
        }

    }

    public void drawCircle(int slices, float radius, double x, double y, double z){
        GL11.glPushMatrix();
        try{
            //float radius = 20f;
            //int SLICES = 50;
            //float degrees = 7.2f;
            //float radians = 0.13f;

            float radians = (float)slices / 360f;

            GL11.glBegin(GL11.GL_LINE_LOOP);
            for(int lo = 0;lo < slices;lo++){
                float heading = lo * radians;
                GL11.glVertex3d((Math.cos(heading) * radius) + x, (Math.sin(heading) * radius) + y, 5 + z);
            }
            GL11.glEnd();
        }finally {
            GL11.glPopMatrix();
        }
    }
}
