package weissmoon.random.client.event;

import net.minecraft.client.entity.player.AbstractClientPlayerEntity;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.util.Hand;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import weissmoon.random.item.ModItems;

public class SetItemUsage{

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    //public void renderSecondItem (RenderPlayerEvent.Pre event){
//        if (event.renderer != null){
//            if (event.entityPlayer != null){
//                if (event.entityPlayer.getHeldItem() != null && event.entityPlayer.getHeldItem().getItem() == ModItems.railgun){
//                    //event.renderer.modelBipedMain.heldItemRight = 3;
//                    event.renderer.modelArmorChestplate.aimedBow = event.renderer.modelArmor.aimedBow = event.renderer.modelBipedMain.aimedBow = true;
//                    //event.renderer.modelArmorChestplate.heldItemLeft = event.renderer.modelArmor.heldItemLeft = event.renderer.modelBipedMain.heldItemLeft = 0;
//                }
//            }
//        }
        //if (event.renderer != null){
            //if (event.getEntityPlayer() != null){
                //if (event.getEntityPlayer().getHeldItem(Hand.MAIN_HAND) != null && event.getEntityPlayer().getHeldItem(Hand.MAIN_HAND).getItem() == ModItems.railgun){
                    //event.renderer.modelBipedMain.heldItemRight = 3;
                    //event.renderer.modelArmorChestplate.aimedBow = event.renderer.modelArmor.aimedBow = event.renderer.modelBipedMain.aimedBow = true;
                    //event.renderer.modelArmorChestplate.heldItemLeft = event.renderer.modelArmor.heldItemLeft = event.renderer.modelBipedMain.heldItemLeft = 0;
                    //event.getEntityPlayer().activeItemStackUseCount = 1;
                //}
            //}
        //}
    //}


    public void renderRailItem(RenderLivingEvent.Pre event){
        if(event.getEntity() instanceof AbstractClientPlayerEntity) {
            if (((AbstractClientPlayerEntity)event.getEntity()).getHeldItem(Hand.MAIN_HAND) != null && ((AbstractClientPlayerEntity)event.getEntity()).getHeldItem(Hand.MAIN_HAND).getItem() == ModItems.railgun) {
                ((PlayerRenderer)event.getRenderer()).getEntityModel().rightArmPose = BipedModel.ArmPose.BOW_AND_ARROW;
            }
        }
    }
}
