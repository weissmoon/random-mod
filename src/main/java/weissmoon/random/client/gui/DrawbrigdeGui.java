package weissmoon.random.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import weissmoon.random.container.ContainerDrawbrigde;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/6/19.
 */
public class DrawbrigdeGui extends ContainerScreen<ContainerDrawbrigde> {

    private static final ResourceLocation DRAWBRIGDE_GUI_TEXTURE = new ResourceLocation("weissrandom:textures/gui/drawbrigde1.png");


    public DrawbrigdeGui(ContainerDrawbrigde inventorySlotsIn, PlayerInventory playerInventory, ITextComponent title) {
        super(inventorySlotsIn, Minecraft.getInstance().player.inventory, new TranslationTextComponent("block.weissrandom." + Strings.Blocks.DRAWBRIGDE_NAME));
        ySize = 133;
        playerInventoryTitleY = 39;
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks){
        renderBackground(matrixStack);
        drawGuiContainerBackgroundLayer(matrixStack, partialTicks, mouseX, mouseX);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        renderHoveredTooltip(matrixStack, mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        Minecraft.getInstance().getTextureManager().bindTexture(DRAWBRIGDE_GUI_TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        this.blit(matrixStack, i, j, 0, 0, this.xSize, this.ySize);
//        this.drawTexturedModalRect(i, j, 0, 0, this.xSize, 7 * 18 + 17);
//        this.drawTexturedModalRect(i, j + 7 * 18 + 17, 0, 126, this.xSize, 96);
    }
}
