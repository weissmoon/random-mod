package weissmoon.random.client.render;

import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.color.IBlockColor;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockDisplayReader;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 11/13/19.
 */
public class BlockColorHandler{

    public static class FireMelon implements IBlockColor {
        @Override
        public int getColor(BlockState state, @Nullable IBlockDisplayReader worldIn, @Nullable BlockPos pos, int tintIndex) {
            return 10441252;
        }
    }

    public static class EarthMelon implements IBlockColor {
        @Override
        public int getColor(BlockState state, @Nullable IBlockDisplayReader worldIn, @Nullable BlockPos pos, int tintIndex) {
            return 6704179;
        }
    }

    public static class AirMelon implements IBlockColor {
        @Override
        public int getColor(BlockState state, @Nullable IBlockDisplayReader worldIn, @Nullable BlockPos pos, int tintIndex) {
            return 5013401;
        }
    }
}
