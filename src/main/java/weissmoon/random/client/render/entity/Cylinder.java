package weissmoon.random.client.render.entity;

import net.minecraft.entity.EntityType;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.world.World;

public class Cylinder extends net.minecraft.entity.Entity{
    private float rotY;

    public Cylinder (World par1World, float x, float y, float z){
        super(EntityType.ARROW, par1World);
        setPosition(x, y, z);
    }


    protected void entityInit (){
    }


    protected void readAdditional (CompoundNBT nbttagcompound){
    }


    protected void writeAdditional (CompoundNBT nbttagcompound){
    }


    public void tick (){
        if (!this.world.isRemote){
            this.rotY += 0.05F;
        }
    }

    public float getRotY (){
        return this.rotY;
    }

    @Override
    protected void registerData() {

    }


    @Override
    public IPacket<?> createSpawnPacket() {
        return null;
    }
}
