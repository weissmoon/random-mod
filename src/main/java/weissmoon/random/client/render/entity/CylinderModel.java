package weissmoon.random.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.model.Model;

import java.util.function.Function;


public class CylinderModel extends Model{
    public CylinderModel() {
        super(RenderType::getEntitySolid);
    }
    //IModelCustom Cylinder;

    public void render (float rotY){
        float rot = (float)Math.toRadians(rotY);
        GL11.glRotatef(rot, 0.0F, 1.0F, 0.0F);
        //this.Cylinder.renderAll();
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder iVertexBuilder, int i, int i1, float v, float v1, float v2, float v3) {

    }
}
