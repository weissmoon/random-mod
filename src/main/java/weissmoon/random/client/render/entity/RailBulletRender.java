//package weissmoon.random.client.render.entity;
//
//import com.mojang.blaze3d.matrix.MatrixStack;
//import net.minecraft.client.Minecraft;
//import net.minecraft.client.renderer.BufferBuilder;
//import com.mojang.blaze3d.platform.GlStateManager;
//import net.minecraft.client.renderer.IRenderTypeBuffer;
//import net.minecraft.client.renderer.entity.EntityRendererManager;
//import net.minecraft.client.renderer.texture.TextureAtlasSprite;
//import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
//import org.lwjgl.opengl.GL11;
//import net.minecraft.client.renderer.Tessellator;
//import net.minecraft.client.renderer.entity.EntityRenderer;
//import net.minecraft.client.renderer.texture.AtlasTexture;
//import net.minecraft.item.Items;
//import net.minecraft.item.ItemStack;
//import net.minecraft.util.*;
//import weissmoon.random.entity.EntityRailBullet;
//
//public class RailBulletRender extends EntityRenderer<EntityRailBullet> {
//
//    public RailBulletRender(EntityRendererManager renderManager){
//        super(renderManager);
//        this.shadowSize = 0.1F;
//    }
//
//
////    public void doRender (Entity p_76986_1_, double p_76986_2_, double p_76986_4_, double p_76986_6_, float p_76986_8_, float p_76986_9_)
////    {
////        doRender((EntityRailBullet)p_76986_1_, p_76986_2_, p_76986_4_, p_76986_6_, p_76986_8_, p_76986_9_);
////    }
//
//    @Override
//    public void render (EntityRailBullet te, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn)
//    {
//        GL11.glPushMatrix();
////        bindEntityTexture(te);
//        double x, y, z;
//        x = te.posX;
//        y = te.posY;
//        z = te.posZ;
//        GL11.glTranslated(x, y, z);
//        GL11.glEnable(32826);
//        float f2 = 0.76F;
//        GL11.glScalef(f2 / 1.0F, f2 / 1.0F, f2 / 1.0F);
//        GL11.glScalef(1.0F, 1.0F, 1.0F);
////        List<ItemStack> list = te.ammo.getNugetlist();
//        ItemStack stack = te.bulletStack;//(ItemStack)list.get(te.nuggetIndex);
//        TextureAtlasSprite iicon;
//        if ((stack == null) || (stack.getItem() == Items.AIR)){
//            iicon = Minecraft.getInstance().getItemRenderer().getItemModelMesher().getParticleIcon(Items.GOLD_NUGGET);
//        }else{
//            iicon = Minecraft.getInstance().getItemRenderer().getItemModelMesher().getItemModel(stack).getParticleTexture();
//        }
//        Tessellator tessellator = Tessellator.getInstance();
//        BufferBuilder vertexBuffer = tessellator.getBuffer();
//        float f3 = iicon.getMinU();
//        float f4 = iicon.getMaxU();
//        float f5 = iicon.getMinV();
//        float f6 = iicon.getMaxV();
////        float f7 = 1.0F;
////        float f8 = 0.5F;
////        float f9 = 0.25F;
//        GL11.glRotatef(180.0F - this.renderManager.getCameraOrientation().getY(), 0.0F, 1.0F, 0.0F);
//        GL11.glRotatef(-this.renderManager.getCameraOrientation().getX(), 1.0F, 0.0F, 0.0F);
//        //tessellator.startDrawingQuads();
//        //tessellator.setNormal(0.0F, 1.0F, 0.0F);
////        if (this.renderOutlines){
////            GlStateManager.enableColorMaterial();
////            GlStateManager.setupSolidRenderingTextureCombine(this.getTeamColor(te));
////        }
//        vertexBuffer.begin(7, DefaultVertexFormats.POSITION_COLOR_TEX_LIGHTMAP);
////        tessellator.addVertexWithUV(0.0F - f8, 0.0F - f9, 0.0D, f3, f6);
////        tessellator.addVertexWithUV(f7 - f8, 0.0F - f9, 0.0D, f4, f6);
////        tessellator.addVertexWithUV(f7 - f8, 1.0F - f9, 0.0D, f4, f5);
////        tessellator.addVertexWithUV(0.0F - f8, 1.0F - f9, 0.0D, f3, f5);
//        vertexBuffer.pos(-0.5D, -0.25D, 0.0D).tex(f3, f6).normal(0.0F, 1.0F, 0.0F).endVertex();
//        vertexBuffer.pos(0.5D, -0.25D, 0.0D).tex(f4, f6).normal(0.0F, 1.0F, 0.0F).endVertex();
//        vertexBuffer.pos(0.5D, 0.75D, 0.0D).tex(f4, f5).normal(0.0F, 1.0F, 0.0F).endVertex();
//        vertexBuffer.pos(-0.5D, 0.75D, 0.0D).tex(f3, f5).normal(0.0F, 1.0F, 0.0F).endVertex();
//        tessellator.draw();
////        if (this.renderOutlines){
////            GlStateManager.tearDownSolidRenderingTextureCombine();
////            GlStateManager.disableColorMaterial();
////        }
//        GL11.glDisable(32826);
//        GL11.glPopMatrix();
//    }
//
//
////    protected ResourceLocation getEntityTexture (Entity p_110775_1_){
////        return TextureMap.LOCATION_BLOCKS_TEXTURE;
////    }
//
//    public ResourceLocation getEntityTexture (EntityRailBullet p_110775_1_){
//        return AtlasTexture.LOCATION_BLOCKS_TEXTURE;
//    }
//}
