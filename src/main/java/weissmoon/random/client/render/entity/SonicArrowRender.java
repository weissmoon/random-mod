package weissmoon.random.client.render.entity;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.entity.SonicArrowEntity;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 2/13/19.
 */
public class SonicArrowRender extends ArrowRenderer<SonicArrowEntity> {

    public static final ResourceLocation RES_ARROW = new ResourceLocation("textures/entity/projectiles/arrow.png");

    public SonicArrowRender(EntityRendererManager manager)
    {
        super(manager);
    }

    @Override
    public void render(SonicArrowEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn){
        super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
    }

    @Nullable
    @Override
    public ResourceLocation getEntityTexture(SonicArrowEntity entity) {
        return RES_ARROW;
    }
}
