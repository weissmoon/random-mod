package weissmoon.random.client.render.entity;

import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;
import weissmoon.random.entity.ActivationArrowEntity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 7/11/19.
 */
public class ActivationArrowRender extends ArrowRenderer<ActivationArrowEntity> {

    public static final ResourceLocation RES_ARROW = new ResourceLocation("weissrandom:textures/entity/activationarrowprojectile.png");
    public static final ResourceLocation NORMAL_ARROW = new ResourceLocation("textures/entity/projectiles/arrow.png");

    public ActivationArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

//    @Override
    public void doRender(@Nonnull ActivationArrowEntity entity, double x, double y, double z, float entityYaw, float partialTicks){
//        super.doRender(entity, x, y, z, entityYaw, partialTicks);
    }

    @Nullable
    @Override
    public ResourceLocation getEntityTexture(ActivationArrowEntity entity) {
        if(entity.isDust()){
            return RES_ARROW;
        }
        return NORMAL_ARROW;
    }
}
