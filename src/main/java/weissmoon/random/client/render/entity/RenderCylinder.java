package weissmoon.random.client.render.entity;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.fml.client.FMLClientHandler;

public class RenderCylinder extends EntityRenderer<Cylinder> {
    private CylinderModel model;

    public RenderCylinder(EntityRendererManager renderManager){
        super(renderManager);
        this.model = new CylinderModel();
    }

    //@Override
    public void doRender (Cylinder entity, double d0, double d1, double d2, float f, float f1){
        renderCylinder((Cylinder)entity, d0, d1, d2, f, f1);
    }

    @Override
    public ResourceLocation getEntityTexture (Cylinder entity){
        return null;
    }

    public void renderCylinder (Cylinder entity, double x, double y, double z, float yaw, float partialTickTimer){
        GL11.glPushMatrix();
        GL11.glTranslatef((float)x, (float)y, (float)z);
        GL11.glScalef(-1.0F, -1.0F, 1.0F);

        //FMLClientHandler.instance().getClient().renderEngine.bindTexture(ResourceLocationHelper.getResourceLocation("RWBY".toLowerCase(), "textures/items/itemCrescentRoseT.png"));

        this.model.render(entity.getRotY());

        GL11.glPopMatrix();
    }
}
