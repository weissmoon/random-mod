package weissmoon.random.client.render.item;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

/**
 * Created by Weissmoon on 8/23/19.
 */
public class ItemRenderer {

    /**
     * Renders an item held in hand as a 2D texture with thickness
     * Adapted from ItemRender.renderItemIn2D from 1.7.10
     */
    public static void renderItemIn2D(float maxU, float minV, float minU, float maxV, int width, int height, float thickness)
    {
        //tessellator and vertex buffer separated
        Tessellator tessellator = Tessellator.getInstance();
        BufferBuilder p_78439_0_ = tessellator.getBuffer();

        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(0.0F, 0.0F, 1.0F);
        //back
        p_78439_0_.pos(0.0D, 0.0D, 0.0D).tex(maxU, maxV).normal(0.0F, 0.0F, 1.0F).endVertex();
        p_78439_0_.pos(1.0D, 0.0D, 0.0D).tex(minU, maxV).normal(0.0F, 0.0F, 1.0F).endVertex();
        p_78439_0_.pos(1.0D, 1.0D, 0.0D).tex(minU, minV).normal(0.0F, 0.0F, 1.0F).endVertex();
        p_78439_0_.pos(0.0D, 1.0D, 0.0D).tex(maxU, minV).normal(0.0F, 0.0F, 1.0F).endVertex();
        tessellator.draw();
        //p_78439_0_.draw();
        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(0.0F, 0.0F, -1.0F);
        //front
        p_78439_0_.pos(0.0D, 1.0D, (double)(0.0F - thickness)).tex(maxU, minV).normal(0.0F, 0.0F, -1.0F).endVertex();
        p_78439_0_.pos(1.0D, 1.0D, (double)(0.0F - thickness)).tex(minU, minV).normal(0.0F, 0.0F, -1.0F).endVertex();
        p_78439_0_.pos(1.0D, 0.0D, (double)(0.0F - thickness)).tex(minU, maxV).normal(0.0F, 0.0F, -1.0F).endVertex();
        p_78439_0_.pos(0.0D, 0.0D, (double)(0.0F - thickness)).tex(maxU, maxV).normal(0.0F, 0.0F, -1.0F).endVertex();
        tessellator.draw();
        //p_78439_0_.draw();
        float f5 = 0.5F * (maxU - minU) / (float)width;
        float f6 = 0.5F * (maxV - minV) / (float)height;
        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(-1.0F, 0.0F, 0.0F);
        //right
        int k;
        float f7;
        float f8;

        for (k = 0; k < width; ++k)
        {
            f7 = (float)k / (float)width;
            f8 = maxU + (minU - maxU) * f7 - f5;
            p_78439_0_.pos((double)f7, 0.0D, (double)(0.0F - thickness)).tex(f8, maxV).normal(-1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f7, 0.0D, 0.0D).tex(f8, maxV).normal(-1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f7, 1.0D, 0.0D).tex(f8, minV).normal(-1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f7, 1.0D, (double)(0.0F - thickness)).tex(f8, minV).normal(-1.0F, 0.0F, 0.0F).endVertex();
        }

        tessellator.draw();
        //p_78439_0_.draw();
        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(1.0F, 0.0F, 0.0F);
        //left
        float f9;

        for (k = 0; k < width; ++k)
        {
            f7 = (float)k / (float)width;
            f8 = maxU + (minU - maxU) * f7 - f5;
            f9 = f7 + 1.0F / (float)width;
            p_78439_0_.pos((double)f9, 1.0D, (double)(0.0F - thickness)).tex(f8, minV).normal(1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f9, 1.0D, 0.0D).tex(f8, minV).normal(1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f9, 0.0D, 0.0D).tex(f8, maxV).normal(1.0F, 0.0F, 0.0F).endVertex();
            p_78439_0_.pos((double)f9, 0.0D, (double)(0.0F - thickness)).tex(f8, maxV).normal(1.0F, 0.0F, 0.0F).endVertex();
        }

        tessellator.draw();
        //p_78439_0_.draw();
        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(0.0F, 1.0F, 0.0F);
        //top

        for (k = 0; k < height; ++k)
        {
            f7 = (float)k / (float)height;
            f8 = maxV + (minV - maxV) * f7 - f6;
            f9 = f7 + 1.0F / (float)height;
            p_78439_0_.pos(0.0D, (double)f9, 0.0D).tex(maxU, f8).normal(0.0F, 1.0F, 0.0F).endVertex();
            p_78439_0_.pos(1.0D, (double)f9, 0.0D).tex(minU, f8).normal(0.0F, 1.0F, 0.0F).endVertex();
            p_78439_0_.pos(1.0D, (double)f9, (double)(0.0F - thickness)).tex(minU, f8).normal(0.0F, 1.0F, 0.0F).endVertex();
            p_78439_0_.pos(0.0D, (double)f9, (double)(0.0F - thickness)).tex(maxU, f8).normal(0.0F, 1.0F, 0.0F).endVertex();
        }

        tessellator.draw();
        //p_78439_0_.draw();
        p_78439_0_.begin(7, DefaultVertexFormats.POSITION_TEX_COLOR_NORMAL);
        //p_78439_0_.startDrawingQuads();
        //p_78439_0_.setNormal(0.0F, -1.0F, 0.0F);
        //bottom

        for (k = 0; k < height; ++k)
        {
            f7 = (float)k / (float)height;
            f8 = maxV + (minV - maxV) * f7 - f6;
            p_78439_0_.pos(1.0D, (double)f7, 0.0D).tex(minU, f8).normal(0.0F, -1.0F, 0.0F).endVertex();
            p_78439_0_.pos(0.0D, (double)f7, 0.0D).tex(maxU, f8).normal(0.0F, -1.0F, 0.0F).endVertex();
            p_78439_0_.pos(0.0D, (double)f7, (double)(0.0F - thickness)).tex(maxU, f8).normal(0.0F, -1.0F, 0.0F).endVertex();
            p_78439_0_.pos(1.0D, (double)f7, (double)(0.0F - thickness)).tex(minU, f8).normal(0.0F, -1.0F, 0.0F).endVertex();
        }

        tessellator.draw();
        //p_78439_0_.draw();
    }
}
