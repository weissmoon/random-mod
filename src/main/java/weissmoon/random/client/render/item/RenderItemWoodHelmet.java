package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.random.item.equipment.armour.ItemArmourWood;

/**
 * Created by Weissmoon on 4/12/19.
 */
public class RenderItemWoodHelmet implements IItemRenderer {

    @Override
    public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType) {
        return cameraTransformType == ItemCameraTransforms.TransformType.HEAD;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data) {
        Item itemArmourWood = item.getItem();
        GL11.glPushMatrix();
        try {
            if (cameraTransformType == ItemCameraTransforms.TransformType.HEAD) {
                if (itemArmourWood instanceof ItemArmourWood) {
                    ItemStack log = ((ItemArmourWood) itemArmourWood).getPlank(item);
                    GlStateManager.enableLighting();
                    IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
                    Minecraft.getInstance().getItemRenderer().renderItem(log, cameraTransformType, 15728880, 0, matrixStack, irendertypebuffer);
                    GlStateManager.enableBlend();
                }
            }
        }finally {
            GL11.glPopMatrix();
        }
    }
}
