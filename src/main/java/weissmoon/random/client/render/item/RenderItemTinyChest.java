package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileTinyChest;
import weissmoon.random.client.render.tile.TinyChestTESR;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND;
import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND;

/**
 * Created by Weissmoon on 6/30/21.
 */
public class RenderItemTinyChest implements IItemRenderer{

    private final TileTinyChest chest = new TileTinyChest();

    @Override
    public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType) {
        return false;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, IItemRenderer.ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data) {
        matrixStack.push();
//        GlStateManager.enableLighting();
//        TileEntityRendererDispatcher.instance.renderItem(chest, matrixStack, (IRenderTypeBuffer)data[0], (Integer)data[1], 655360);
//        TinyChestTESR.renderProjectTable(matrixStack,(IRenderTypeBuffer)data[0], 1600000, 655360);
//        Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(Blocks.CHEST), ItemCameraTransforms.TransformType.GUI, 16000, OverlayTexture.NO_OVERLAY, matrixStack, (IRenderTypeBuffer)data[0]);
        matrixStack.pop();
//        TinyChestTESR.renderProjectTable(matrixStack, (IRenderTypeBuffer)data[0], (Integer)data[1], OverlayTexture.NO_OVERLAY);
    }
}
