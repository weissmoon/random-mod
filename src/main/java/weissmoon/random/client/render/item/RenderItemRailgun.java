package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.RenderMaterial;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.utils.NBTHelper;
import weissmoon.random.client.render.models.ModelItemRailgun;


public class RenderItemRailgun implements IItemRenderer {

    private static ModelItemRailgun model;
    private static RenderMaterial material;


    public RenderItemRailgun (){
        model = new ModelItemRailgun();
        material = new RenderMaterial(AtlasTexture.LOCATION_BLOCKS_TEXTURE, new ResourceLocation("weissrandom", "textures/items/itemrailgunm.png"));
    }

    @Override
    public boolean handleRenderType (ItemStack item, ItemCameraTransforms.TransformType type){
        switch(type){
            case GUI:
                return false;
        }

        return true;
    }

    @Override
    public boolean shouldUseRenderHelper (ItemCameraTransforms.TransformType type, ItemStack item, ItemRendererHelper helper){
        return helper != ItemRendererHelper.EQUIPT_ANIMATION;
    }

    @Override
    public void renderItem (ItemCameraTransforms.TransformType type, ItemStack item, MatrixStack matrixStack, Object... data){

        matrixStack.push();
        switch(type){
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                matrixStack.scale(-1.0F, -1.0F, 1.0F);
                matrixStack.translate(-0.68F, -2.75F, 0.88F);
                matrixStack.rotate(Vector3f.YP.rotationDegrees(135.0F));
                matrixStack.rotate(Vector3f.XP.rotationDegrees(-70.0F));


                break;


            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                matrixStack.scale(-1.0F, -1.0F, 1.0F);
                matrixStack.translate(1.0F, -1.5F, 1.5F);
                matrixStack.rotate(Vector3f.YP.rotationDegrees(220.0F));
                matrixStack.rotate(Vector3f.XP.rotationDegrees(6.0F));


                break;

            case GROUND:
                matrixStack.scale(-0.5F, -0.5F, 0.5F);
                matrixStack.translate(0.0F, -2.0F, 0.0F);
                matrixStack.translate(0.0F, 0.0F, -2.1F);
                break;
        }


        //Minecraft.getInstance().getRenderManager().textureManager.bindTexture(new ResourceLocation("weissrandom", "textures/items/itemrailgunm.png"));
        CompoundNBT tag = item.getTag();
        IRenderTypeBuffer irendertypebuffer = (IRenderTypeBuffer.Impl)data[0];
        IVertexBuilder builder = material.getBuffer(irendertypebuffer, RenderType::getEntitySolid);
//        IVertexBuilder builder = irendertypebuffer$impl.getBuffer(RenderTypeLookup.func_239219_a_(item, false));
        int overlay = OverlayTexture.NO_OVERLAY;
        if (tag != null){
            if (NBTHelper.getBoolean(item, "ready")){
                model.render(matrixStack, builder, (Integer)data[1], overlay, true);
            }else{
                model.render(matrixStack, builder, (Integer)data[1], overlay, false);
            }
        }else{
            model.render(matrixStack, builder, (Integer)data[1], overlay, false);
        }
        matrixStack.scale(-1.0F, 1.0F, 1.0F);
        matrixStack.pop();
    }

    public void renderRose (float x, float y, float z){
    }
}
