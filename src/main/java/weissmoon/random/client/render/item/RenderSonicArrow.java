package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.client.render.IIcon;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 3/24/19.
 */
public class RenderSonicArrow implements IItemRenderer {

    ItemStack stack = new ItemStack(Items.ARROW);
    IIcon iIcon = new IIcon("minecraft:arrow", "inventory");
    ResourceLocation pick = new ResourceLocation(Reference.MOD_ID + ":itemgildedpickaxe");
    ResourceLocation sol = new ResourceLocation(Reference.MOD_ID + ":items/itemsonicarrowl");

    @Override
    public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType) {
        switch(cameraTransformType){
            case FIRST_PERSON_RIGHT_HAND:
                return true;
        }
        return false;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType transformType, ItemStack itemStack, ItemRendererHelper itemRendererHelper) {
        return itemRendererHelper == ItemRendererHelper.EQUIPT_ANIMATION;
    }

    @Override
    public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data) {
        GL11.glPushMatrix();

        TextureAtlasSprite base = Minecraft.getInstance().getModelManager().getAtlasTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE).getSprite(pick);
        TextureAtlasSprite light = Minecraft.getInstance().getModelManager().getAtlasTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE).getSprite(sol);
        //ItemRenderer.renderItemIn2D(base.getMaxU(), base.getMinV(), base.getMinU(), base.getMaxV(), base.getWidth(), base.getHeight(), 1F/16F);
        boolean lighting = GL11.glGetBoolean(GL11.GL_LIGHTING);
        //if(lighting)
            //RenderHelper.disableStandardItemLighting();
        //GL11.glLightf(16385, 4610, 16385);
        //ItemRenderer.renderItemIn2D(light.getMaxU(), light.getMinV(), light.getMinU(), light.getMaxV(), light.getIconWidth(), light.getIconHeight(), 1F/16F);
        //if(!lighting)
            //RenderHelper.enableStandardItemLighting();

        GL11.glPopMatrix();
    }
}
