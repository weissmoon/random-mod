package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Quaternion;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.random.client.render.tile.ProjectTableTESR;
import weissmoon.random.item.ItemBlockProjectTable;
import weissmoon.random.item.ItemWoodenPressurePlate;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND;
import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND;

/**
 * Created by Weissmoon on 1/5/22.
 */
public class RenderItemWoodenPressurePlate implements IItemRenderer{
    @Override
    public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType){
        return cameraTransformType == FIRST_PERSON_RIGHT_HAND || cameraTransformType == FIRST_PERSON_LEFT_HAND;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, ItemRendererHelper helper){
        return false;
    }

    @Override
    public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data){
        if(item.getItem() instanceof ItemWoodenPressurePlate){
            matrixStack.push();
            try{
//                if(cameraTransformType != FIRST_PERSON_RIGHT_HAND && cameraTransformType != FIRST_PERSON_LEFT_HAND){
//                }else
                {
                    matrixStack.translate(.018, .65, .018);
                    ProjectTableTESR.renderProjectTable(0, 0, 0, IItemHolderItem.getHolding(item), matrixStack, (IRenderTypeBuffer)data[0], (Integer)data[1], OverlayTexture.NO_OVERLAY);
                }
            }finally{
                matrixStack.pop();
            }
        }
    }
}
