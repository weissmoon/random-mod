package weissmoon.random.client.render.item;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.item.ItemStack;
import weissmoon.core.api.client.item.IItemRenderer;
import weissmoon.random.client.render.tile.ProjectTableTESR;
import weissmoon.random.item.ItemBlockProjectTable;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.*;

/**
 * Created by Weissmoon on 5/9/19.
 */
public class RenderItemProjectTable implements IItemRenderer {

    @Override
    public boolean handleRenderType(ItemStack item, ItemCameraTransforms.TransformType cameraTransformType) {
        return cameraTransformType == FIRST_PERSON_RIGHT_HAND || cameraTransformType == FIRST_PERSON_LEFT_HAND;
    }

    @Override
    public boolean shouldUseRenderHelper(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, ItemRendererHelper helper) {
        return true;
    }

    @Override
    public void renderItem(ItemCameraTransforms.TransformType cameraTransformType, ItemStack item, MatrixStack matrixStack, Object... data) {
        if(item.getItem() instanceof ItemBlockProjectTable) {
            ProjectTableTESR.renderProjectTable(0, 0, 0, ((ItemBlockProjectTable) item.getItem()).getPlank(item), matrixStack, (IRenderTypeBuffer)data[0], (Integer)data[1], OverlayTexture.NO_OVERLAY);
        }
    }
}
