package weissmoon.random.client.render.armour;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.entity.model.AgeableModel;
import net.minecraft.client.renderer.entity.model.BipedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Quaternion;
import org.lwjgl.opengl.GL11;
import weissmoon.random.item.equipment.armour.ItemArmourWood;

import javax.annotation.Nonnull;
import java.util.Objects;

/**
 * Created by Weissmoon on 4/12/19.
 */
public class WoodArmourModel extends BipedModel<LivingEntity> {

    private LivingEntity entity;
    private float headPitch, netHeadYaw;
    public WoodArmourModel(float p_i1148_1_) {
        super(p_i1148_1_);
    }

//    @Override
//    public void render(@Nonnull LivingEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
//        renderL(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
//    }
//
//    public void renderL(MatrixStack matrixStack, VertexBuffer vertexBuffer,  @Nonnull LivingEntity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
//        super.render(matrixStack, vertexBuffer, entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
//        this.entity = entity;
//        this.headPitch = headPitch;
//        this.netHeadYaw = netHeadYaw;
//    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder vertexBuilder, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha) {
        ItemStack headStack = entity.getItemStackFromSlot(EquipmentSlotType.HEAD);
        IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();

        if(!headStack.isEmpty() && headStack.getItem() instanceof ItemArmourWood) {
//            GL11.glPushMatrix();
            matrixStack.push();
            if (entity.isSneaking())
            {
                matrixStack.translate(0.0F, 0.20F, 0.0F);
            }
//            matrixStack.rotate(new Quaternion(entity.rotationPitch, entity.rotationYaw, 0 , 0));
            matrixStack.rotate(new Quaternion(0, bipedHead.rotateAngleY, 0, false));
            matrixStack.rotate(new Quaternion(entity.rotationPitch, 0, 0, true));
            matrixStack.translate(0, -0.21, 0);
            matrixStack.scale(0.6f, 0.6f, 0.6f);
            Minecraft.getInstance().getItemRenderer().renderItem(((ItemArmourWood) headStack.getItem()).getPlank(headStack), ItemCameraTransforms.TransformType.NONE, packedLightIn, packedOverlayIn, matrixStack, irendertypebuffer);
//            GL11.glScaled(1/0.6, 1/0.6, 1/0.6);
//            GL11.glTranslated(0, 0.21, 0);
//            GL11.glRotatef(-headPitch, 1, 0, 0);
//            GL11.glRotatef(-netHeadYaw, 0, 1, 0);
//            if (entity.isSneaking())
//            {
//                GL11.glTranslated(0.0F, -0.20F, 0.0F);
//            }
            matrixStack.pop();
//            GL11.glPopMatrix();
        }
        ItemStack chestStack = entity.getItemStackFromSlot(EquipmentSlotType.CHEST);
        if(!chestStack.isEmpty() && chestStack.getItem() instanceof ItemArmourWood) {
            Minecraft.getInstance().getItemRenderer().renderItem(((ItemArmourWood) chestStack.getItem()).getPlank(chestStack), ItemCameraTransforms.TransformType.NONE, packedLightIn, packedOverlayIn, matrixStack, irendertypebuffer);
        }
        ItemStack legStack = entity.getItemStackFromSlot(EquipmentSlotType.LEGS);
        if(!legStack.isEmpty() && legStack.getItem() instanceof ItemArmourWood) {
            Minecraft.getInstance().getItemRenderer().renderItem(((ItemArmourWood) legStack.getItem()).getPlank(legStack), ItemCameraTransforms.TransformType.NONE, packedLightIn, packedOverlayIn, matrixStack, irendertypebuffer);
        }
        ItemStack feetStack = entity.getItemStackFromSlot(EquipmentSlotType.FEET);
        if(!feetStack.isEmpty() && feetStack.getItem() instanceof ItemArmourWood) {
            matrixStack.push();
            matrixStack.translate(0, 0.75, 0);
            if (entity.isSneaking())
            {
                matrixStack.translate(0.0F, 0.0F, 0.25F);
            }

            matrixStack.rotate(new Quaternion((float)(bipedRightLeg.rotateAngleX * (180 / Math.PI)), 1, 0, true));
            matrixStack.rotate(new Quaternion(0, (float)(this.bipedRightLeg.rotateAngleY * (180 / Math.PI)), 0, true));
            matrixStack.rotate(new Quaternion(0, 0, (float)(this.bipedRightLeg.rotateAngleZ * (180 / Math.PI)), true));
            matrixStack.scale(0.36F, 0.36F, 0.36F);
            matrixStack.translate(-0.34, 1.7, 0);
            Minecraft.getInstance().getItemRenderer().renderItem(((ItemArmourWood) feetStack.getItem()).getPlank(feetStack), ItemCameraTransforms.TransformType.NONE, packedLightIn, packedOverlayIn, matrixStack, irendertypebuffer);
            matrixStack.translate(0.34, -1.7, 0);
//            GL11.glScaled(1/0.36, 1/0.36, 1/0.36);
            matrixStack.rotate(new Quaternion(0, 0, (float)-(this.bipedRightLeg.rotateAngleZ * (180 / Math.PI)), true));
            matrixStack.rotate(new Quaternion(0, (float)-(this.bipedRightLeg.rotateAngleY * (180 / Math.PI)), 0, true));
            matrixStack.rotate(new Quaternion((float)-(this.bipedRightLeg.rotateAngleX * (180 / Math.PI)), 0, 0, true));
//            GL11.glTranslated(0, -0.75, 0);
//
//            GL11.glTranslated(0, 0.75, 0);
            matrixStack.rotate(new Quaternion((float)(bipedLeftLeg.rotateAngleX * (180 / Math.PI)), 1, 0, true));
            matrixStack.rotate(new Quaternion(0, (float)(this.bipedLeftLeg.rotateAngleY * (180 / Math.PI)), 0, true));
            matrixStack.rotate(new Quaternion(0, 0, (float)(this.bipedLeftLeg.rotateAngleZ * (180 / Math.PI)), true));
//            matrixStack.rotate(new Quaternion(this.bipedLeftLeg.rotateAngleX * (180 / Math.PI), 1, 0, 0);
//            matrixStack.rotate(new Quaternion(this.bipedLeftLeg.rotateAngleY * (180 / Math.PI), 0, 1, 0);
//            matrixStack.rotate(new Quaternion(this.bipedLeftLeg.rotateAngleZ * (180 / Math.PI), 0, 0, 1);
//            GL11.glScaled(0.36, 0.36, 0.36);
            matrixStack.translate(0.34, 1.7, 0);
            Minecraft.getInstance().getItemRenderer().renderItem(((ItemArmourWood) feetStack.getItem()).getPlank(feetStack), ItemCameraTransforms.TransformType.NONE, packedLightIn, packedOverlayIn, matrixStack, irendertypebuffer);
//            GL11.glTranslated(-0.34, -1.7, 0);
//            GL11.glScaled(1/0.36, 1/0.36, 1/0.36);
//            GL11.glRotated(-(this.bipedLeftLeg.rotateAngleZ * (180 / Math.PI)), 0, 0, 1);
//            GL11.glRotated(-(this.bipedLeftLeg.rotateAngleY * (180 / Math.PI)), 0, 1, 0);
//            GL11.glRotated(-(this.bipedLeftLeg.rotateAngleX * (180 / Math.PI)), 1, 0, 0);
//            if (entity.isSneaking())
//            {
//                GL11.glTranslated(0.0F, 0.0F, -0.25F);
//            }
//            GL11.glTranslated(0, -0.75, 0);
            matrixStack.pop();
        }


    }


    public void setEntity(LivingEntity entity){
        this.entity = entity;
    }
}
