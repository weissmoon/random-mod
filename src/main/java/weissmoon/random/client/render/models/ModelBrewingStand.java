//package weissmoon.random.client.render.models;
//
//import net.minecraft.client.model.ModelBase;
//import net.minecraft.client.model.ModelRenderer;
//import org.lwjgl.opengl.GL11;
//
///**
// * Created by Weissmoon on 4/30/19.
// */
//@Deprecated
//public class ModelBrewingStand extends ModelBase {
//    public ModelRenderer base1;
//    public ModelRenderer base2;
//    public ModelRenderer base3;
//
//    public ModelRenderer standu;
//    public ModelRenderer standn;
//    public ModelRenderer stands;
//    public ModelRenderer standw;
//    public ModelRenderer stande;
//    public ModelRenderer tube1;
//    public ModelRenderer tube2;
//    public ModelRenderer tube3;
//
//    public ModelBrewingStand(){
//        textureHeight = 16;
//        textureWidth = 16;
//        this.base1 = new ModelRenderer(this, 9, 5);
//        this.base1.setRotationPoint(0.0F, 0.0F, 0.0F);
//        this.base1.addBox(9, 0, 5, 6, 2, 6, 0.0F);
//        this.base2 = new ModelRenderer(this, 2, 1);
//        this.base2.setRotationPoint(0,0, 0);
//        this.base2.addBox(2, 0, 1, 6, 2, 6);
//        this.base3 = new ModelRenderer(this, 2, 8);
//        this.base3.setRotationPoint(0,0, 0);
//        this.base3.addBox(2, 0, 9, 6, 2, 6);
//        this.standu = new ModelRenderer(this, 7, 7);
//        this.standu.setRotationPoint(0,0, 0);
//        this.standu.addBox(7, 14, 7, 2, 0, 2);
//        this.standn = new ModelRenderer(this, 7, 2);
//        this.standn.setRotationPoint(2,0, -2);
//        this.standn.addBox(0, 0, 0, 2, 14, 0, false);
//        this.tube1 = new ModelRenderer(this, 6, 0);
//        this.tube1.setRotationPoint(0,0, 0);
//        this.tube1.addBox(0, 0, 0, 6, 16, 0);
//        this.tube2 = new ModelRenderer(this, 6, 0);
//        this.tube2.setRotationPoint(0,0, 0);
//        this.tube2.addBox(0, 0, 0, 6, 16, 0, true);
//    }
//
//    public void renderBase(){
//        float ff = 0.0625F;
//        this.base1.render(ff);
//        this.base2.render(ff);
//        this.base3.render(ff);
//    }
//
//    public void renderStand(){
//        float ff = 0.0625F;
//        int i = 0;
//        this.standu.render(ff);
//        //this.tube1.render(ff);
//
//        this.tube2 = new ModelRenderer(this, 8, 0);
//        this.tube2.setRotationPoint(0,0, 0);
//        this.tube2.addBox(0, 0, 0, 6, 16, 0, false);
//        GL11.glTranslated(0.5, (14F / 16F), 0.5);
//        GL11.glRotated(180, 0, 0, 1);
//        while(i < 4) {
//            GL11.glRotatef(90F * i, 0, 1, 0);
//            //this.standn.rotateAngleY = (1.5707963267948966F * (float)i);
//            this.standn.render(ff);
//            i++;
//        }
//        GL11.glTranslated(0, -(2F/16F), 0);
//        this.tube2.render(ff);
//    }
//}
