package weissmoon.random.client.render.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
/**
 * Created by Weissmoon on 5/16/21.
 */
public class LidModel extends Model{


    public ModelRenderer rail1;
    public ModelRenderer rail2;


    public LidModel(){
        super(RenderType::getEntitySolid);
        rail1 = new ModelRenderer(this, 2, 2);
        rail1.addBox(0, 0, 0, 14, 1, 14, 0.0F);
        rail2 = new ModelRenderer(this, 2, 2);
        rail2.setRotationPoint(0, 0, -0.5F);
        rail2.addBox(6, 1, 4, 4, 2, 1, 0.0F);
    }

    @Override
    public void render(MatrixStack matrixStackIn, IVertexBuilder bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha){
        rail1.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
        rail2.render(matrixStackIn, bufferIn, packedLightIn, packedOverlayIn);
    }
}
