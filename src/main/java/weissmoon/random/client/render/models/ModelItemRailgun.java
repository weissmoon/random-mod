package weissmoon.random.client.render.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GLX;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import org.lwjgl.opengl.GL11;
import net.minecraft.client.renderer.*;
import net.minecraft.entity.Entity;

public class ModelItemRailgun extends Model {

    public ModelRenderer shape28;
    public ModelRenderer shape29;
    public ModelRenderer rail1;
    public ModelRenderer rail2;
    public ModelRenderer tieL1;
    public ModelRenderer tieL2;
    public ModelRenderer tieL3;
    public ModelRenderer tieL4;
    public ModelRenderer tieL5;
    public ModelRenderer tieLB1;
    public ModelRenderer tieLB2;
    public ModelRenderer tieLB3;
    public ModelRenderer tieLB4;
    public ModelRenderer tieR1;
    public ModelRenderer tieR2;
    public ModelRenderer tieR3;
    public ModelRenderer tieR4;
    public ModelRenderer tieR5;
    public ModelRenderer tieRB1;
    public ModelRenderer tieRB2;
    public ModelRenderer tieRB3;
    public ModelRenderer tieRB4;
    public ModelRenderer triggerGuardF;
    public ModelRenderer TriggerGuardU;
    public ModelRenderer TriggerGuardB;
    public ModelRenderer trigger;


    public ModelItemRailgun (){
        super(RenderType::getEntitySolid);
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.tieRB4 = new ModelRenderer(this, 0, 0);
        this.tieRB4.setRotationPoint(2.0F, 8.4F, 41.5F);
        this.tieRB4.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieRB4, 2.401573F, 0.0F, 0.0F);
        this.triggerGuardF = new ModelRenderer(this, 0, 10);
        this.triggerGuardF.setRotationPoint(0.5F, 12.0F, 42.0F);
        this.triggerGuardF.addBox(0.0F, 0.0F, -1.0F, 2, 5, 1, 0.0F);
        setRotateAngle(this.triggerGuardF, -0.61086524F, 0.0F, 0.0F);
        this.tieRB1 = new ModelRenderer(this, 0, 0);
        this.tieRB1.setRotationPoint(2.0F, 8.4F, 10.7F);
        this.tieRB1.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieRB1, 2.401573F, 0.0F, 0.0F);
        this.tieR4 = new ModelRenderer(this, 0, 0);
        this.tieR4.setRotationPoint(2.0F, 8.4F, 31.2F);
        this.tieR4.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieR4, 0.74351025F, 0.0F, 0.0F);
        this.shape28 = new ModelRenderer(this, 0, 30);
        this.shape28.setRotationPoint(0.0F, 0.0F, 28.9F);
        this.shape28.addBox(-0.1F, -0.1F, 0.0F, 3, 4, 5, 0.0F);
        this.shape29 = new ModelRenderer(this, 0, 21);
        this.shape29.setRotationPoint(0.0F, 0.0F, 28.9F);
        this.shape29.addBox(-0.1F, -0.1F, 0.0F, 3, 4, 5, 0.0F);
        this.tieL3 = new ModelRenderer(this, 0, 0);
        this.tieL3.setRotationPoint(0.0F, 8.4F, 20.9F);
        this.tieL3.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieL3, 0.74351025F, 0.0F, 0.0F);
        this.tieLB2 = new ModelRenderer(this, 0, 0);
        this.tieLB2.setRotationPoint(0.0F, 8.4F, 20.9F);
        this.tieLB2.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieLB2, 2.401573F, 0.0F, 0.0F);
        this.tieLB3 = new ModelRenderer(this, 0, 0);
        this.tieLB3.setRotationPoint(0.0F, 8.4F, 31.2F);
        this.tieLB3.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieLB3, 2.401573F, 0.0F, 0.0F);
        this.rail2 = new ModelRenderer(this, 0, 0);
        this.rail2.setRotationPoint(0.0F, 8.0F, 0.0F);
        this.rail2.addBox(0.0F, 0.0F, 0.0F, 3, 4, 50, 0.0F);
        this.TriggerGuardU = new ModelRenderer(this, 0, 14);
        this.TriggerGuardU.setRotationPoint(0.5F, 15.1F, 33.1F);
        this.TriggerGuardU.addBox(0.0F, 0.0F, 0.0F, 2, 1, 6, 0.0F);
        this.tieR3 = new ModelRenderer(this, 0, 0);
        this.tieR3.setRotationPoint(2.0F, 8.4F, 20.9F);
        this.tieR3.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieR3, 0.74351025F, 0.0F, 0.0F);
        this.tieR1 = new ModelRenderer(this, 0, 0);
        this.tieR1.setRotationPoint(2.0F, 8.4F, 0.4F);
        this.tieR1.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieR1, 0.74351025F, 0.0F, 0.0F);
        this.tieLB4 = new ModelRenderer(this, 0, 0);
        this.tieLB4.setRotationPoint(0.0F, 8.4F, 41.5F);
        this.tieLB4.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieLB4, 2.401573F, 0.0F, 0.0F);
        this.trigger = new ModelRenderer(this, 0, 0);
        this.trigger.setRotationPoint(1.5F, 12.0F, 34.8F);
        this.trigger.addBox(0.0F, 0.0F, 0.0F, 1, 2, 1, 0.0F);
        setRotateAngle(this.trigger, 0.34906584F, 0.0F, 0.0F);
        this.TriggerGuardB = new ModelRenderer(this, 0, 10);
        this.TriggerGuardB.setRotationPoint(0.5F, 12.0F, 33.1F);
        this.TriggerGuardB.addBox(0.0F, 0.0F, 0.0F, 2, 4, 1, 0.0F);
        this.tieRB3 = new ModelRenderer(this, 0, 0);
        this.tieRB3.setRotationPoint(2.0F, 8.4F, 31.2F);
        this.tieRB3.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieRB3, 2.401573F, 0.0F, 0.0F);
        this.tieL2 = new ModelRenderer(this, 0, 0);
        this.tieL2.setRotationPoint(0.0F, 8.4F, 10.7F);
        this.tieL2.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieL2, 0.74351025F, 0.0F, 0.0F);
        this.tieL4 = new ModelRenderer(this, 0, 0);
        this.tieL4.setRotationPoint(0.0F, 8.4F, 31.2F);
        this.tieL4.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieL4, 0.74351025F, 0.0F, 0.0F);
        this.tieL5 = new ModelRenderer(this, 0, 0);
        this.tieL5.setRotationPoint(0.0F, 8.4F, 41.5F);
        this.tieL5.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieL5, 0.74351025F, 0.0F, 0.0F);
        this.tieRB2 = new ModelRenderer(this, 0, 0);
        this.tieRB2.setRotationPoint(2.0F, 8.4F, 20.9F);
        this.tieRB2.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieRB2, 2.401573F, 0.0F, 0.0F);
        this.rail1 = new ModelRenderer(this, 0, 0);
        this.rail1.mirror = true;
        this.rail1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.rail1.addBox(-3.0F, -4.0F, 0.0F, 3, 4, 50, 0.0F);
        setRotateAngle(this.rail1, 0.0F, 0.0F, 3.1415927F);
        this.tieL1 = new ModelRenderer(this, 0, 0);
        this.tieL1.setRotationPoint(0.0F, 8.4F, 0.4F);
        this.tieL1.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieL1, 0.74351025F, 0.0F, 0.0F);
        this.tieLB1 = new ModelRenderer(this, 0, 0);
        this.tieLB1.setRotationPoint(0.0F, 8.4F, 10.7F);
        this.tieLB1.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieLB1, 2.401573F, 0.0F, 0.0F);
        this.tieR2 = new ModelRenderer(this, 0, 0);
        this.tieR2.setRotationPoint(2.0F, 8.4F, 10.7F);
        this.tieR2.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieR2, 0.74351025F, 0.0F, 0.0F);
        this.tieR5 = new ModelRenderer(this, 0, 0);
        this.tieR5.setRotationPoint(2.0F, 8.4F, 41.5F);
        this.tieR5.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        setRotateAngle(this.tieR5, 0.74351025F, 0.0F, 0.0F);
    }

    //@Override
    public void render (MatrixStack var1, IVertexBuilder var2, int var3, int var4, float var5, float var6, float var7, float var8){
    }

    public void render (MatrixStack matrixStack, IVertexBuilder ivertexbuilder, int packedLight, int packedOverlayIn, boolean ready){
        matrixStack.push();
        this.tieRB4.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.triggerGuardF.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieRB1.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieR4.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
//        float lightX = GLX.lastBrightnessX;
//        float lightY = GLX.lastBrightnessY;
        RenderHelper.disableStandardItemLighting();
        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, 200.0F, 200.0F);
        if (ready){
//            GL11.glTranslatef(this.shape29.offsetX, this.shape29.offsetY, this.shape29.offsetZ);
            matrixStack.translate(this.shape29.rotationPointX, this.shape29.rotationPointY, this.shape29.rotationPointZ);
            matrixStack.scale(1.1F, 1.0F, 1.0F);
//            GL11.glTranslatef(-this.shape29.offsetX, -this.shape29.offsetY, -this.shape29.offsetZ);
            matrixStack.translate(-this.shape29.rotationPointX, -this.shape29.rotationPointY, -this.shape29.rotationPointZ);
            this.shape29.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        }else{
//            GL11.glTranslatef(this.shape28.offsetX, this.shape28.offsetY, this.shape28.offsetZ);
            matrixStack.translate(this.shape28.rotationPointX, this.shape28.rotationPointY, this.shape28.rotationPointZ);
            matrixStack.scale(1.1F, 1.0F, 1.0F);
//            GL11.translate(-this.shape28.offsetX, -this.shape28.offsetY, -this.shape28.offsetZ);
            matrixStack.translate(-this.shape28.rotationPointX, -this.shape28.rotationPointY, -this.shape28.rotationPointZ);
            this.shape28.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        }
        RenderHelper.enableStandardItemLighting();
        //OpenGlHelper.setLightmapTextureCoords(OpenGlHelper.lightmapTexUnit, lightX, lightY);
        this.tieL3.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieLB2.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieLB3.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.rail2.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.TriggerGuardU.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieR3.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieR1.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieLB4.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.trigger.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.TriggerGuardB.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieRB3.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieL2.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieL4.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieL5.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieRB2.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.rail1.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieL1.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieLB1.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieR2.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        this.tieR5.render(matrixStack, ivertexbuilder, packedLight, packedOverlayIn, 1.0F, 1.0F, 1.0F, 1.0F);
        matrixStack.pop();
    }

    public void setRotateAngle (ModelRenderer modelRenderer, float x, float y, float z){
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
