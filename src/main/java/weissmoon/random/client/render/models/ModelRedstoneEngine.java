package weissmoon.random.client.render.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelRenderer;

/**
 * Created by Weissmoon on 6/3/19.
 */
public class ModelRedstoneEngine extends Model {

    public ModelRenderer shape28;
    public ModelRenderer shape29;
    public ModelRenderer shape30;


    public ModelRedstoneEngine () {
        super(RenderType::getEntitySolid);
        this.textureWidth = 128;
        this.textureHeight = 64;
        this.shape28 = new ModelRenderer(this, 0, 0);
        this.shape28.setRotationPoint(0.0F, 0F, 0F);
        this.shape28.addBox(0.0F, 0.0F, 0.0F, 16, 16, 10, 0.0F);
        setRotateAngle(this.shape28, 0F, 0.0F, 0.0F);
        this.shape29 = new ModelRenderer(this, 0, 0);
        this.shape29.setRotationPoint(0.0F, 0F, 0F);
        this.shape29.addBox(0.0F, 0.0F, 0.0F, 2, 2, 6, 0.0F);
        setRotateAngle(this.shape29, 0F, 0.0F, 0.0F);
        this.shape30 = new ModelRenderer(this, 0, 0);
        this.shape30.setRotationPoint(0.0F, 0F, 0F);
        this.shape30.addBox(0.0F, 0.0F, 0.0F, 2, 2, 6, 0.0F);
        setRotateAngle(this.shape30, 0F, 0.0F, 45.0F);
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder iVertexBuilder, int i, int i1, float v, float v1, float v2, float v3) {
//    }
//    public void renderModel(){
        float ff = 0.0625F;
//        shape28.render(ff);
//        shape29.render(ff);
//        shape30.render(ff);
    }

    public void setRotateAngle (ModelRenderer modelRenderer, float x, float y, float z){
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
}
