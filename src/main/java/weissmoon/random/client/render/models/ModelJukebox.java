package weissmoon.random.client.render.models;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.model.Model;

/**
 * Created by Weissmoon on 2/22/20.
 */
public class ModelJukebox extends Model {

    public ModelRenderer bottom;
    public ModelRenderer top;
    public ModelRenderer holder;
    public ModelRenderer stemBase;
    public ModelRenderer plate;
    public ModelRenderer plate1;
    public ModelRenderer plate2;
    public ModelRenderer stem;
    public ModelRenderer needle;

    public ModelJukebox(){
        super(RenderType::getEntitySolid);
        this.textureWidth = 16;
        this.textureHeight = 16;

//        bottom = new ModelRenderer(this, 0, 0);
//        bottom.addBox(0.0F, -0.5F, 0.0F, 1, 1, 7, 0.0F);
        plate = new ModelRenderer(this, 1, 1);
        plate.addBox(0.0F, 0.0F, 0.0F, 12, 1, 14, 0.0F);
        plate1 = new ModelRenderer(this, 0, 0);
        plate1.addBox(0.0F, 0.0F, 0.0F, 1, 1, 12, 0.0F);
        plate.addChild(plate1);
        plate2 = new ModelRenderer(this, 0, 0);
        plate2.addBox(0.0F, 0.0F, 0.0F, 1, 1, 12, 0.0F);
        plate.addChild(plate2);
        stem = new ModelRenderer(this, 0, 0);
        stem.addBox(0.0F, 0.0F, 0.0F, 1, 1, 8, 0.0F);
        needle = new ModelRenderer(this, 0, 0);
        needle.addBox(0.0F, 0.0F, 0.0F, 1, 1, 8, 0.0F);
        stem.addChild(needle);
    }

    @Override
    public void render(MatrixStack matrixStack, IVertexBuilder iVertexBuilder, int i, int i1, float v, float v1, float v2, float v3) {

    }
}
