package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.Quaternion;
import org.lwjgl.opengl.GL11;
import weissmoon.random.block.BlockDrawbrigde;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileDrawbrigde;

/**
 * Created by Weissmoon on 4/6/19.
 */
public class DrawbridgeTESR extends TileEntityRenderer<TileDrawbrigde> {

    public DrawbridgeTESR(TileEntityRendererDispatcher manager) {
        super(manager);
    }

    @Override
    public void render(TileDrawbrigde te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
    {
//        ITextComponent itextcomponent = te.getDisplayName();

//        if (itextcomponent != null && this.rendererDispatcher.cameraHitResult != null && te.getPos().equals(this.rendererDispatcher.cameraHitResult.getBlockPos()))
//        {
//            this.setLightmapDisabled(true);
//            this.drawNameplate(te, itextcomponent.getFormattedText(), x, y, z, 12);
//            this.setLightmapDisabled(false);
//        }
        float x, y, z;
        x = te.getPos().getX();
        y = te.getPos().getY();
        z = te.getPos().getZ();
        this.renderDrawbrigde(x, y, z, te, matrixStack, bufferIn, combinedLightIn, combinedOverlayIn);
    }

    public void renderDrawbrigde(double x, double y, double z, TileDrawbrigde tileDrawbrigde, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn){
        matrixStack.push();
        try {
            matrixStack.translate(.5, .5, .5);
            Direction facing = Minecraft.getInstance().world.getBlockState(tileDrawbrigde.getPos()).get(BlockDrawbrigde.FACING);
            switch(facing) {
                case UP:
                    matrixStack.rotate(new Quaternion(90 , 0, 0, true));
                    break;
                case DOWN:
                    matrixStack.rotate(new Quaternion(270, 0, 0, true));
                    break;
                case NORTH:
                    matrixStack.rotate(new Quaternion(0  , 0, 0, true));
                    break;
                case WEST:
                    matrixStack.rotate(new Quaternion(0, 90 , 0, true));
                    break;
                case SOUTH:
                    matrixStack.rotate(new Quaternion(0, 180, 0, true));
                    break;
                case EAST:
                    matrixStack.rotate(new Quaternion(0, 270, 0, true));
                    break;
            }
            //IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
            if (tileDrawbrigde.getCamoflageBlock().isEmpty()) {
                Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(ModBlocks.drawbridge), ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            }else{
                Minecraft.getInstance().getItemRenderer().renderItem(tileDrawbrigde.getCamoflageBlock(), ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            }
            //ModelLoader.getInventoryVariant();
        }finally {
            matrixStack.pop();
        }
    }
}
