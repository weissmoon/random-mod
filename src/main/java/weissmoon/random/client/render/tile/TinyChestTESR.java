package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.client.renderer.model.RenderMaterial;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Vector3f;
import weissmoon.random.block.BlockTinyChest;
import weissmoon.random.block.tile.TileTinyChest;

import java.util.Calendar;

import static net.minecraft.client.renderer.Atlases.CHEST_MATERIAL;
import static net.minecraft.client.renderer.Atlases.CHEST_XMAS_MATERIAL;

/**
 * Created by Weissmoon on 6/27/21.
 */
public class TinyChestTESR extends TileEntityRenderer<TileTinyChest>{

    public static TinyChestTESR INSTANCE;

    private final ModelRenderer singleLid;
    private final ModelRenderer singleBottom;
    private final ModelRenderer singleLatch;
    private boolean isChristmas;

    public TinyChestTESR(TileEntityRendererDispatcher rendererDispatcherIn){
        super(rendererDispatcherIn);
        Calendar calendar = Calendar.getInstance();
        this.isChristmas = calendar.get(Calendar.MONTH)+1 == 12 && calendar.get(Calendar.DATE) >= 24 && calendar.get(Calendar.DATE)<=26;

        this.singleBottom = new ModelRenderer(64, 64, 0, 19);
        this.singleBottom.addBox(1.0F, 0.0F, 1.0F, 14.0F, 10.0F, 14.0F, 0.0F);
        this.singleLid = new ModelRenderer(64, 64, 0, 0);
        this.singleLid.addBox(1.0F, 0.0F, 0.0F, 14.0F, 5.0F, 14.0F, 0.0F);
        this.singleLid.rotationPointY = 9.0F;
        this.singleLid.rotationPointZ = 1.0F;
        this.singleLatch = new ModelRenderer(64, 64, 0, 0);
        this.singleLatch.addBox(7.0F, -1.0F, 15.0F, 2.0F, 4.0F, 1.0F, 0.0F);
        this.singleLatch.rotationPointY = 8.0F;
        INSTANCE = this;
    }

    @Override
    public void render(TileTinyChest tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn){
        float angle = tileEntityIn.getLidAngle(partialTicks);
        matrixStackIn.push();
        float rotate = tileEntityIn.getWorld().getBlockState(tileEntityIn.getPos()).get(BlockTinyChest.FACING).getHorizontalAngle();
        ItemStack tinyStack = tileEntityIn.gettinyStack();
        renderStack:
        if(!tinyStack.isEmpty() && tileEntityIn.isTiny()){
            if(angle == 0)
                break renderStack;
            matrixStackIn.push();
            matrixStackIn.translate(0.5, (1.5 * angle), 0.5);
            matrixStackIn.scale(angle, angle, angle);
            matrixStackIn.rotate(Vector3f.YP.rotationDegrees(-rotate));
            Minecraft.getInstance().getItemRenderer().renderItem(tinyStack, ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
            matrixStackIn.pop();
        }
        matrixStackIn.translate(0.5, 0, 0.5);
        matrixStackIn.rotate(Vector3f.YP.rotationDegrees(-rotate));
        matrixStackIn.translate(-0.5, 0, -0.5);
        render( matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn, angle);
        matrixStackIn.pop();
    }

    public void render(MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float angle){
        RenderMaterial rendermaterial = getMaterial();
        IVertexBuilder ivertexbuilder = rendermaterial.getBuffer(bufferIn, RenderType::getEntityCutout);

        matrixStackIn.translate(233/1088f, 0, 233/1088f);
        matrixStackIn.scale(1/14f*8,1/14f*8,1/14f*8);

        singleLid.rotateAngleX = -(angle * ((float)Math.PI / 2F));
        singleLatch.rotateAngleX = singleLid.rotateAngleX;
        singleLid.render(matrixStackIn, ivertexbuilder, combinedLightIn, combinedOverlayIn);
        singleLatch.render(matrixStackIn, ivertexbuilder, combinedLightIn, combinedOverlayIn);
        singleBottom.render(matrixStackIn, ivertexbuilder, combinedLightIn, combinedOverlayIn);
    }

    public RenderMaterial getMaterial(){
        return isChristmas ? CHEST_XMAS_MATERIAL:CHEST_MATERIAL;
    }

    public static void renderProjectTable(MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn){
        INSTANCE.render(matrixStack, bufferIn, combinedLightIn, combinedOverlayIn, 0);
    }
}
