package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.BrewingStandTileEntity;
import net.minecraft.util.math.vector.Quaternion;
import net.minecraft.util.text.ITextComponent;
import org.lwjgl.opengl.GL11;
import weissmoon.random.block.override.tile.TileBrewingStand;

/**
 * Created by Weissmoon on 4/29/19.
 */
public class BrewingStandTESR extends TileEntityRenderer<BrewingStandTileEntity> {

    public BrewingStandTESR(TileEntityRendererDispatcher p_i226006_1_){
        super(p_i226006_1_);
        //this.model = new ModelBrewingStand();
    }

    @Override
    public void render(BrewingStandTileEntity te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
    {
        //ITextComponent itextcomponent = te.getDisplayName();

        matrixStack.push();
        try{
            //GL11.glTranslated(x + .5, y + .5, z + .5);
//            float x, y, z;
//            x = te.getPos().getX();
//            y = te.getPos().getY();
//            z = te.getPos().getZ();
//            IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
//            matrixStack.glTranslated(x, y, z);
            ItemStack item = te.getStackInSlot(0);
            matrixStack.translate(0.291, 0.3, 0.291);
            matrixStack.rotate(new Quaternion(0, -45, 0, true));
            Minecraft.getInstance().getItemRenderer().renderItem(item, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            matrixStack.rotate(new Quaternion(0, 45, 0, true));
            matrixStack.translate(-0.291, 0, -0.291);
            item = te.getStackInSlot(1);
            matrixStack.translate(0.291, 0, 0.709);
            matrixStack.rotate(new Quaternion(0, 45, 0,true));
            Minecraft.getInstance().getItemRenderer().renderItem(item, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            matrixStack.rotate(new Quaternion(0, -45, 0, true));
            matrixStack.translate(-0.291, 0, -0.709);
            item = te.getStackInSlot(2);
            matrixStack.translate(0.765, 0, 0.5);
            //GL11.glRotated(180, 0, 1, 0);
            Minecraft.getInstance().getItemRenderer().renderItem(item, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            matrixStack.translate(-0.765, 0, -0.5);
            item = te.getStackInSlot(3);
            matrixStack.translate(0.5, 0.7, 0.5);//ingredient
            long angle = (System.currentTimeMillis() / 25) % 360;
            matrixStack.rotate(new Quaternion(0, angle, 0, true));
            Minecraft.getInstance().getItemRenderer().renderItem(item, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
//            item = te.brewingItemStacks.get(4);//blaze
//            GL11.glTranslated(0.5, 0.5, 0.5);
//            GL11.glRotated(-angle * 2, 0, 1, 0);
//            Minecraft.getMinecraft().getRenderItem().renderItem(item, ItemCameraTransforms.TransformType.GROUND);
//            this.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
        }finally {
            matrixStack.pop();
        }
    }
}
