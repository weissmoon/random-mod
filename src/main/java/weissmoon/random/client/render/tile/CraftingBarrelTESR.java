package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import weissmoon.random.block.tile.TileBarrel;
import weissmoon.random.item.model.ModelItems;

/**
 * Created by Weissmoon on 5/16/21.
 */
public class CraftingBarrelTESR extends TileEntityRenderer<TileBarrel>{

    private final ItemStack lid = new ItemStack(ModelItems.lid);

    public CraftingBarrelTESR(TileEntityRendererDispatcher manager){
        super(manager);
    }

    @Override
    public void render(TileBarrel tileEntityIn, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn){
        if(tileEntityIn.getLidPosition() == 0)
            return;
        if(tileEntityIn.getLidPosition() == 1){
            matrixStackIn.translate(.5, .9, .5);
        }else{
            matrixStackIn.translate(.5, .5, .5);
        }
        Minecraft.getInstance().getItemRenderer().renderItem(lid, ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
    }
}
