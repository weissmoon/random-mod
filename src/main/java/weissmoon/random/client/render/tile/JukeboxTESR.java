package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.vector.Quaternion;
import org.lwjgl.opengl.GL11;
import weissmoon.random.block.override.tile.TileJukebox;
import weissmoon.random.block.tile.TileDrawbrigde;

/**
 * Created by Weissmoon on 2/22/20.
 */
public class JukeboxTESR extends TileEntityRenderer<TileJukebox> {

    public JukeboxTESR(TileEntityRendererDispatcher manager) {
        super(manager);
    }

    @Override
    public void render(TileJukebox te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
    {
//        ITextComponent itextcomponent = te.getDisplayName();

//        if (itextcomponent != null && this.rendererDispatcher.cameraHitResult != null && te.getPos().equals(this.rendererDispatcher.cameraHitResult.getBlockPos()))
//        {
//            this.setLightmapDisabled(true);
//            this.drawNameplate(te, itextcomponent.getFormattedText(), x, y, z, 12);
//            this.setLightmapDisabled(false);
//        }
        float x, y, z;
        x = te.getPos().getX();
        y = te.getPos().getY();
        z = te.getPos().getZ();
        this.render(te, x, y, z, matrixStack, bufferIn, combinedLightIn, combinedOverlayIn);
    }

    public void render(TileJukebox tileEntityIn, double x, double y, double z, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        matrixStack.push();
        try{
            //matrixStack.glTranslated(x, y, z);
            ItemStack item = tileEntityIn.getRecord();
            matrixStack.translate(0.494, 0.645, 0.74);
            matrixStack.rotate(new Quaternion(-90, 0, 0, true));
            if(600 > ((System.currentTimeMillis()) % 1200)) {
                matrixStack.translate(-0.048, 0.0, 0.0);
                matrixStack.rotate(new Quaternion(0, -180, 0, true));
            }
            matrixStack.scale(1.5F, 1.5F, 1.2F);
            IRenderTypeBuffer.Impl irendertypebuffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
            Minecraft.getInstance().getItemRenderer().renderItem(item, ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, irendertypebuffer);
        }finally {
            matrixStack.pop();
        }
    }
}
