package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.state.properties.BlockStateProperties;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateSilent;

/**
 * Created by Weissmoon on 1/3/22.
 */
public class WoodenPressurePlateTESR extends TileEntityRenderer<TileWoodenPressurePlateSilent>{
    public WoodenPressurePlateTESR(TileEntityRendererDispatcher dispatcher){
        super(dispatcher);
    }

    @Override
    public void render(TileWoodenPressurePlateSilent tileWoodenPressurePlate, float v, MatrixStack matrixStack, IRenderTypeBuffer iRenderTypeBuffer, int i, int i1){
        try{
            boolean powered = tileWoodenPressurePlate.getWorld().getBlockState(tileWoodenPressurePlate.getPos()).get(BlockStateProperties.POWERED);

            BlockState renderState = tileWoodenPressurePlate.getStateforRender(powered);

            Minecraft.getInstance().getBlockRendererDispatcher().renderBlock(renderState, matrixStack, iRenderTypeBuffer, i, i1);
        }catch (Error ignored){

        }
    }
}
