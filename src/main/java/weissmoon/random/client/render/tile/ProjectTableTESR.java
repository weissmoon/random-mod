package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.block.Blocks;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.ITextComponent;
import org.lwjgl.opengl.GL11;
import weissmoon.core.WeissCore;
import weissmoon.core.proxy.ClientProxy;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.override.tile.TileJukebox;
import weissmoon.random.block.tile.TileProjectTable;

/**
 * Created by Weissmoon on 5/9/19.
 */
public class ProjectTableTESR extends TileEntityRenderer<TileProjectTable> {

    public ProjectTableTESR(TileEntityRendererDispatcher manager) {
        super(manager);
    }

    @Override
    public void render(TileProjectTable te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn)
    {
        ITextComponent itextcomponent = te.getDisplayName();

//        if (itextcomponent != null && this.rendererDispatcher.cameraHitResult != null && te.getPos().equals(this.rendererDispatcher.cameraHitResult.getBlockPos()))
//        {
//            this.setLightmapDisabled(true);
//            this.drawNameplate(te, itextcomponent.getFormattedText(), x, y, z, 12);
//            this.setLightmapDisabled(false);
//        }
        float x, y, z;
        x = te.getPos().getX();
        y = te.getPos().getY();
        z = te.getPos().getZ();
        renderProjectTable(x, y, z, te.getWoodStack(), matrixStack, bufferIn, combinedLightIn, combinedOverlayIn);
    }

    public static void renderProjectTable(double x, double y, double z, ItemStack woodStack, MatrixStack matrixStack, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn){
        matrixStack.push();
        try {
            matrixStack.translate(.5, .5, .5);
            matrixStack.scale(0.999F, 0.999F, 0.999F);
            if (woodStack.isEmpty()) {
                Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(Blocks.OAK_PLANKS), ItemCameraTransforms.TransformType.FIXED, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            }else{
                Minecraft.getInstance().getItemRenderer().renderItem(woodStack, ItemCameraTransforms.TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            }
            matrixStack.scale(1/0.999F, 1/0.999F, 1/0.999F);
            //Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(ModBlocks.projectTable), ItemCameraTransforms.TransformType.GROUND, combinedLightIn, combinedOverlayIn, matrixStack, bufferIn);
            //IBakedModel model = Minecraft.getInstance().getItemRenderer().getItemModelMesher().getModelManager().getModel(new ModelResourceLocation("weissrandom:block/blockprojecttable"));
            //((ClientProxy) WeissCore.proxy).weissRenderItem.renderModel(model, -1, new ItemStack(ModBlocks.projectTable));
        }finally{
            matrixStack.pop();
        }
    }
}
