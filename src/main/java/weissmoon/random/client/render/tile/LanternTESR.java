package weissmoon.random.client.render.tile;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.block.Blocks;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.item.ItemStack;
import org.lwjgl.opengl.GL11;
import weissmoon.random.block.tile.TileLantern;

/**
 * Created by Weissmoon on 4/11/19.
 */
public class LanternTESR extends TileEntityRenderer<TileLantern> {

    public LanternTESR(TileEntityRendererDispatcher p_i226006_1_) {
        super(p_i226006_1_);
    }

    @Override
    public void render(TileLantern te, float partialTicks, MatrixStack matrixStack, IRenderTypeBuffer buffer, int packedLight, int combinedOverlay) {
        float x, y, z;
        x = te.getPos().getX();
        y = te.getPos().getY();
        z = te.getPos().getZ();
        renderDrawbrigde(x, y, z, te, matrixStack, buffer, packedLight, combinedOverlay);
    }

    public void renderDrawbrigde(double x, double y, double z, TileLantern tileDrawbrigde, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn, int combinedOverlayIn){
        GL11.glPushMatrix();
        try {
            GL11.glTranslated(x + .5, y + .5, z + .5);
//            IBakedModel model = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getModelManager().get;
//            ItemStack stack = new ItemStack(Blocks.SEA_LANTERN);
//            IBakedModel ibakedmodel = Minecraft.getMinecraft().getRenderItem().getItemModelMesher().getItemModel(stack);
//            ibakedmodel = ibakedmodel.getOverrides().handleItemState(ibakedmodel, stack, null, null);
//            ((ClientProxy)WeissCore.proxy).weissRenderItem.renderModel(model, -1, ItemStack.EMPTY);
//            int color = 0;
//            if(tileDrawbrigde != null){
//                color -= EnumDyeColor.byMetadata(tileDrawbrigde.getBlockMeta()).getColorValue();
//            }
//            ((ClientProxy)WeissCore.proxy).weissRenderItem.renderModel(ibakedmodel, color, stack);
            Minecraft.getInstance().getItemRenderer().renderItem(new ItemStack(Blocks.SEA_LANTERN), ItemCameraTransforms.TransformType.NONE, packedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        }finally {
            GL11.glPopMatrix();
        }
    }
}
