package weissmoon.random.api;


import weissmoon.random.api.activationarrow.*;

/**
 * Created by Weissmoon on 8/11/19.
 */
public class WeissRandomAPI {

    /**
     * this is the registry that you use to add you own ArrowHitBehaviour objects
     * you can add to it anytime after the init event
     * <b>DO NOT OVERRIDE THIS</b>
     * @see IActivationArrowRegistry
     */
    public static IActivationArrowRegistry renderRegistry = new DummyActivationArrowRegistry();
}
