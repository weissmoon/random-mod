package weissmoon.random.api.activationarrow;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;

/**
 * Created by Weissmoon on 8/11/19.
 */
public class DummyActivationArrowRegistry implements IActivationArrowRegistry {

    @Override
    public void registerBlockBehaviour(Block block, IHitWithArrowBehaviour.IBlockHit behaviour){

    }
    @Override
    public void registerEntityBehaviour(Class entity, IHitWithArrowBehaviour.IEntityHit behaviour){

    }
}
