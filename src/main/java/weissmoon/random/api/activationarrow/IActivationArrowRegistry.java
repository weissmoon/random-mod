package weissmoon.random.api.activationarrow;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;

/**
 * Created by Weissmoon on 8/11/19.
 */
public interface IActivationArrowRegistry {
    void registerBlockBehaviour(Block block, IHitWithArrowBehaviour.IBlockHit behaviour);
    void registerEntityBehaviour(Class<Entity> entity, IHitWithArrowBehaviour.IEntityHit behaviour);
}
