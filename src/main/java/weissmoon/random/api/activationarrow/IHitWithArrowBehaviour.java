package weissmoon.random.api.activationarrow;

import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.world.World;
import weissmoon.random.entity.ActivationArrowEntity;

/**
 * Created by Weissmoon on 8/11/19.
 */
public interface IHitWithArrowBehaviour<V> {

    /**
     * @param arrow
     * @param world
     * @param raytraceResult
     * @return redstone removed from arrow
     */
    boolean onHit(ActivationArrowEntity arrow, World world, V raytraceResult);

    interface IEntityHit extends IHitWithArrowBehaviour<EntityRayTraceResult>{}

    interface IBlockHit extends IHitWithArrowBehaviour<BlockRayTraceResult>{}
}
