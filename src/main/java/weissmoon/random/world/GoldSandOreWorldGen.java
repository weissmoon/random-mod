package weissmoon.random.world;

import com.mojang.serialization.Codec;
import net.minecraft.block.BlockState;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.ISeedReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import weissmoon.random.block.ModBlocks;

import java.util.Random;
import java.util.function.Function;

/**
 * Created by Weissmoon on 1/16/20.
 */
public class GoldSandOreWorldGen extends Feature<NoFeatureConfig> {
    public GoldSandOreWorldGen(Codec p_i49878_1_) {
        super(p_i49878_1_);
    }

    @Override
    public boolean generate(ISeedReader world, ChunkGenerator generator, Random rand, BlockPos pos, NoFeatureConfig config) {

        if (rand.nextFloat() > .01){
            return false;
        }

        boolean generated = false;
        for(int i = 0; i < 1; ++i) {
            int x = pos.getX() + rand.nextInt(16);
            int z = pos.getZ() + rand.nextInt(16);
            int y = world.getHeight(Heightmap.Type.MOTION_BLOCKING_NO_LEAVES, x, z);
            BlockState ore = ModBlocks.goldSandOre.getDefaultState();

            BlockPos pos2 = null;
            for(int j = 0; j < 2; ++j) {
                for(int k = 0; k < 2; ++k) {
                    for (int l = 0; l < 1; ++l) {
                        int x1 = x - j;
                        int y1 = y - l;
                        int z1 = z - k;
                        pos2 = new BlockPos(x1, y1, z1);
                        if (world.isAirBlock(pos2) && (world.getWorld().getDimensionKey() != World.THE_NETHER || y1 < 127) && ore.isValidPosition(world, pos2)) {
                            world.setBlockState(pos2, ore, 2);
                            generated = true;
                        }
                    }
                }
            }
            BlockPos pos3 = new BlockPos(pos2).up();
            world.getBlockState(pos2).updatePostPlacement(Direction.DOWN, world.getBlockState(pos3), world, pos2, pos3);
        }
        return generated;
    }
}
