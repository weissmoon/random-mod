package weissmoon.random.world;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.blockplacer.SimpleBlockPlacer;
import net.minecraft.world.gen.blockstateprovider.SimpleBlockStateProvider;
import net.minecraft.world.gen.feature.BlockClusterFeatureConfig;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.Features;
import net.minecraft.world.gen.feature.NoFeatureConfig;
import net.minecraft.world.gen.placement.ChanceConfig;
import net.minecraft.world.gen.placement.IPlacementConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.random.lib.Reference;

import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by Weissmoon on 1/16/20.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModWorldGen {

    private static final Feature ironOre = new IronGravelOreWorldGen(NoFeatureConfig.field_236558_a_);
    private static final Feature goldOre = new GoldSandOreWorldGen(NoFeatureConfig.field_236558_a_);
    private static final BlockClusterFeatureConfig melon = (new BlockClusterFeatureConfig.Builder(new SimpleBlockStateProvider(Blocks.MELON.getDefaultState()), new SimpleBlockPlacer())).tries(64).whitelist(ImmutableSet.of(Blocks.GRASS_BLOCK.getBlock())).func_227317_b_().build();
    public static final Set<Type> TYPE_BLACKLIST = ImmutableSet.of(BiomeDictionary.Type.NETHER, BiomeDictionary.Type.END, BiomeDictionary.Type.WASTELAND, BiomeDictionary.Type.VOID);

    @SubscribeEvent
    public static void registerFeatures(RegistryEvent.Register<Feature<?>> evt) {
        IForgeRegistry WorldGenRegistry = evt.getRegistry();

        WorldGenRegistry.register(ironOre.setRegistryName(new ResourceLocation(Reference.MOD_ID+":irongravelore")));
        WorldGenRegistry.register(goldOre.setRegistryName(new ResourceLocation(Reference.MOD_ID+":goldsandore")));
//        WorldGenRegistry.register(melon.setRegistryName(new ResourceLocation(Reference.MOD_ID+":melonpatch")));
    }

    public static void addWorldgen(BiomeLoadingEvent event) {
        Biome.Category category = event.getCategory();
        if (!TYPE_BLACKLIST.contains(category)) {
            event.getGeneration().withFeature(GenerationStage.Decoration.TOP_LAYER_MODIFICATION, ironOre.withConfiguration(new NoFeatureConfig()).withPlacement(Placement.NOPE.configure(IPlacementConfig.NO_PLACEMENT_CONFIG)));
            event.getGeneration().withFeature(GenerationStage.Decoration.TOP_LAYER_MODIFICATION, goldOre.withConfiguration(new NoFeatureConfig()).withPlacement(Placement.NOPE.configure(IPlacementConfig.NO_PLACEMENT_CONFIG)));
            event.getGeneration().withFeature(GenerationStage.Decoration.VEGETAL_DECORATION, Feature.RANDOM_PATCH.withConfiguration(melon).withPlacement(Features.Placements.PATCH_PLACEMENT.chance(32)));
        }
    }
}
