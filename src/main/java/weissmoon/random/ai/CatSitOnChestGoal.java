package weissmoon.random.ai;

import net.minecraft.block.BlockState;
import net.minecraft.entity.ai.goal.CatSitOnBlockGoal;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.tile.TileHungryChest;

/**
 * Created by Weissmoon on 3/31/22.
 */
public class CatSitOnChestGoal extends CatSitOnBlockGoal{

    public CatSitOnChestGoal(CatEntity cat, double speed){
        super(cat, speed);
    }

    /**
     * Return true to set given position as destination
     */
    protected boolean shouldMoveTo(IWorldReader worldIn, BlockPos pos) {
        if(!worldIn.isAirBlock(pos.up())){
            return false;
        }else{
            BlockState blockstate = worldIn.getBlockState(pos);
            if(blockstate.isIn(ModBlocks.hungryChest)){
                return TileHungryChest.getPlayersUsing(worldIn, pos) < 1;
            }
            return false;
        }
    }
}
