package weissmoon.random.lib;

public class Reference{
    public static final String MOD_ID = "weissrandom";
    public static final String MOD_NAME = "Random Mod";
    public static final String VERSION = "0.0.4.17";
    public static final String GUI_FACTORY_CLASS = "weissmoon.random.client.gui.GuiFactory";
    public static final String CLIENT_PROXY = "weissmoon.random.proxy.ClientProxy";
    public static final String SERVER_PROXY = "weissmoon.random.proxy.ServerProxy";
}
