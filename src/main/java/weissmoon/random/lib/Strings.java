package weissmoon.random.lib;


public class Strings{
    public static final String RESOURCE_PREFIX = "weissrandom:";

    public static final class Items{
        public static final String RAILGUN_NAME = "itemrailgun";
        public static final String CHEERIO_NAME = "itemcheerio";
        public static final String CHEERIO_RING_NAME = "itemcheerioring";
        public static final String PLUNGER_NAME = "itemplunger";
        public static final String PLUNGER_LAUNCHER_NAME = "itemplungerlauncher";
        public static final String DUAL_WIELDING_FISH_NAME = "itemdualwieldingfish";
        public static final String SANDLE_NAME = "itemsandal";
        public static final String CHOCOLATE_NAME = "itemchocolate";
        public static final String FIRE_GUITAR_NAME = "itemfireguitar";
        public static final String PARASOL_NAME = "itemfireguitar";
        public static final String SONIC_ARROW_NAME = "itemsonicarrow";
        public static final String CORN_SEED_NAME = "itemcornseed";
        public static final String CORN_FOOD_NAME = "itemcornfood";
        public static final String CORN_COB_NAME = "itemcorncob";
        public static final String POPCORN_NAME = "itempopcorn";
        public static final String BUTTER_CORN_NAME = "itembuttercorn";
        public static final String WOOD_HELMET_NAME = "itemwoodhelmet";
        public static final String WOOD_CHESTPIECE_NAME = "itemwoodchestpiece";
        public static final String WOOD_LEGGING_NAME = "itemwoodleggings";
        public static final String WOOD_FEET_NAME = "itemwoodfeet";
        public static final String QUIVER_1_NAME = "itemquiver1";
        public static final String ACTIVATION_ARROW_NAME = "itemactivationarrow";
        public static final String GILDED_SHOVEL_NAME = "itemgildedspade";
        public static final String GILDED_PICKAXE_NAME = "itemgildedpickaxe";
        public static final String GILDED_AXE_NAME = "itemgildedaxe";
        public static final String GILDED_SWORD_NAME = "itemgildedsword";
        public static final String GILDED_HOE_NAME = "itemgildedhoe";
        public static final String GILDED_HELMET_NAME = "itemgildedhelmet";
        public static final String GILDED_CHESTPIECE_NAME = "itemgildedchestpiece";
        public static final String GILDED_LEGGING_NAME = "itemgildedleggings";
        public static final String GILDED_FEET_NAME = "itemgildedfeet";
        public static final String MELON_FIRE_NAME = "itemmelonfire";
        public static final String MELON_EARTH_NAME = "itemmelonearth";
        public static final String MELON_AIR_NAME = "itemmelonair";
        public static final String MELON_FIRE_SEED_NAME = "itemmelonfireseed";
        public static final String MELON_EARTH_SEED_NAME = "itemmelonearthseed";
        public static final String MELON_AIR_SEED_NAME = "itemmelonairseed";
        public static final String GILDED_HORSE_ARMOUR_NAME = "itemgildedhorsearmour";
        public static final String GILDED_FILLET_KNIFE_NAME =  "itemgildedfilletknife";
        public static final String GILDED_FISHING_ROD_NAME = "itemgildedfishingrod";
        public static final String MEAT_BUN = "itemmeatbun";
        public static final String MEAT_BUN_BEEF = "itemmeatbunbeef";
        public static final String MEAT_BUN_PORK = "itemmeatbunpork";
        public static final String MEAT_BUN_MUTTON = "itemmeatbunmutton";
        public static final String MEAT_BUN_RABBIT = "itemmeatbunrabbit";
        public static final String SALTED_BEEF_NAME = "itemsaltedbeef";
        public static final String SALTED_PORK_NAME = "itemsaltedpork";
        public static final String SALTED_MUTTON_NAME = "itemsaltedmutton";
        public static final String CLEAN_FLESH_NAME = "itemcleanflesh";
        public static final String SALTED_RABBIT_NAME = "itemsaltedrabbit";
        public static final String HONEY_BREAD_NAME = "itemhoneybread";
        public static final String SALT_NAME = "itemsalt";
        public static final String MASHED_POTATOES_NAME = "itemmashedpotatoes";
        public static final String REPAIR_ITEM = "itemtoolrepair";
        public static final String GILDED_DESERT_HELMET_NAME = "itemgildeddeserthelmet";
        public static final String GILDED_DESERT_CHESTPIECE_NAME = "itemgildeddesertchestpiece";
        public static final String GILDED_DESERT_LEGGING_NAME = "itemgildeddesertlegging";
        public static final String GILDED_DESERT_FEET_NAME = "itemgildeddesertfeet";
        public static final String BAMBOO_CHARCOAL_NAME = "itembamboocharcoal";
        public static final String FRIED_POTATO_WEDGE_RAW_NAME = "itemfriedpotatowedgeraw";
        public static final String FRIED_POTATO_WEDGE_NAME = "itemfriedpotatowedge";
        public static final String FRIED_POTATO_RAW_NAME = "itemfriedpotatoraw";
        public static final String FRIED_POTATO_NAME = "itemfriedpotato";
    }

    public static final class Blocks{
        public static final String IGNITER_NAME = "blockigniter";
        public static final String CORN_NAME = "blockcorn";
        public static final String CORN_TOP_NAME = "blockcorntop";
        public static final String DRAWBRIGDE_NAME = "blockdrawbrigde";
        public static final String COLOURED_SEA_LANTERN = "blockcolouredsealantern";
        public static final String PROJECT_TABLE = "blockprojecttable";
        public static final String OBSIDIAN_PRESSSURE_PLATE = "blockobsidianpressureplate";
        public static final String CHARCOAL_BLOCK_NAME = "blockcharcoal";
        public static final String REDSTONE_ENGINE_NAME = "blockredstoneengine";
        public static final String MELON_FIRE_NAME = "blockmelonfire";
        public static final String MELON_EARTH_NAME = "blockmelonearth";
        public static final String MELON_AIR_NAME = "blockmelonair";
        public static final String IRON_GRAVEL_ORE = "blockirongravelore";
        public static final String GOLD_SAND_ORE = "blockgoldsandore";
        public static final String JUKEBOX_NAME = "blockjukebox";
        public static final String OBSIDIAN_PRESSSURE_SILENT_PLATE = "blockobsidianpressureplatesilent";
        public static final String OBSIDIAN_PRESSSURE_SILENT_INVISIBLE_PLATE = "blockobsidianpressureplatesilentinvisible";
        public static final String OBSIDIAN_PRESSSURE_INVISIBLE_PLATE = "blockobsidianpressureplateinvisible";
        public static final String MOSSY_PRESSSURE_PLATE = "blockmossypressureplate";
        public static final String MOSSY_PRESSSURE_SILENT_PLATE = "blockmossypressureplatesilent";
        public static final String MOSSY_PRESSSURE_SILENT_INVISIBLE_PLATE = "blockmossypressureplatesilentinvisible";
        public static final String MOSSY_PRESSSURE_INVISIBLE_PLATE = "blockmossypressureplateinvisible";
        public static final String PICKLING_BARREL_NAME = "blockpicklingbarrel";
        public static final String TINY_CHEST = "blocktinychest";
        public static final String WOODEN_PRESSURE_PLATE_SILENT = "blockwoodenpressureplatesilent";
        public static final String WOODEN_PRESSURE_PLATE_INVISIBLE = "blockwoodenpressureplateinvisible";
        public static final String WOODEN_PRESSURE_PLATE_SILENT_INVISIBLE = "blockwoodenpressureplatesilentinvisible";
        public static final String BAMBOO_CHARCOAL_BLOCK_NAME = "blockbamboocharcoal";
        public static final String HUNGRY_CHEST_NAME = "blockhungrychest";
    }

    public static final class Models{
        public static final String LID_MODEL = "barrellid";
        public static final String SOUL_ORB_MODEL = "soulorb";
        public static final String LOCK = "lock";
        public static final String UNLOCK = "unlock";
        public static final String SNEAK = "sneak";
        public static final String MOUSE_LEFT = "mouseleft";
        public static final String MOUSE_RIGHT = "mouseright";
        public static final String MOUSE = "mouse";


        public static final String ATUM_DOUBLE = "itematumarrowdouble";
        public static final String ATUM_EXPLOSIVE = "itematumarrowexplosive";
        public static final String ATUM_FIRE = "itematumarrowfire";
        public static final String ATUM_POISON = "itematumarrowpoison";
        public static final String ATUM_QUICK = "itematumarrowquickdraw";
        public static final String ATUM_RAIN = "itematumarrowrain";
        public static final String ATUM_SLOW = "itematumarrowslow";
        public static final String ATUM_STRAIGHT = "itematumarrowstraight";
    }
}
