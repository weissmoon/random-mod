package weissmoon.random.lib;

import net.minecraft.item.ItemStack;
import weissmoon.core.lib.ItemEffects;

import java.lang.reflect.Array;
import java.util.*;


public enum ValidRailAmmo{
    UNKNOWN(0.0F, 0.0F, 0.0F, 0.0F, "UNKNOWN"),
    GOLD(1.0F, 8.0F, 4.0F, 0.1F, "nuggetGold"),
    //Thermal
    IRON(1.3F, 6.0F, 6.0F, 0.1F, "nuggetIron"),
    COPPER(1.5F, 5.0F, 6.0F, 0.1F, "nuggetCopper"),
    TIN(1.8F, 2.0F, 4.0F, 0.3F, "nuggetTin"),
    SILVER(1.4F, 7.0F, 7.0F, 0.2F, "nuggetSilver"),
    LEAD(0.9F, 9.0F, 7.0F, 0.2F, "nuggetLead"),
    NICKEL(1.0F, 5.0F, 5.0F, 0.2F, "nuggetNickel"),
    PLATINUM(1.9F, 6.0F, 9.0F, 0.1F, "nuggetPlatinum"),
    MITHRIL(2.0F, 6.0F, 8.0F, 0.0F, "nuggetMithril"),
    ELECTRUM(1.4F, 6.4F, 6.0F, 0.14F, "nuggetElectrum"),
    INVAR(1.5F, 5.0F, 5.65F, 0.15F, "nuggetInvar"),
    BRONZE(1.4F, 4.0F, 5.7F, 0.14F, "nuggetBronze"),
    SIGNALUM(0.8F, 6.0F, 6.0F, 0.19F, "nuggetSignalum"),
    LIMIUM(1.7F, 3.0F, 5.0F, 0.21F, "nuggetLumium"),
    ENDERIUM(1.9F, 4.0F, 8.0F, 0.1F, "nuggetEnderium"),
    //arsenal
    ELECTRUMFLUX(1.6F, 6.1F, 7.0F, 0.13F, "nuggetElectrumFlux"),
    //botania
    MANASTEEL(1.5F, 6.0F, 7.0F, 0.08F, "nuggetManasteel"),
    TERRASTEEL(1.7F, 6.0F, 10.0F, 0.05F, "nuggetTerrasteel"),
    ELEMENTIUM(1.6F, 6.0F, 9.0F, 0.07F, "nuggetElvenElementium"),
    //ender IO
    PULSATINGIRON(1.4F, 7.0F, 7.0F, 0.9F, "nuggetPulsatingIron"),
    VIRBRANTALLOY(1.0F, 8.0F, 5.0F, 0.9F, "nuggetVibrantAlloy"),
    //Extra Utils
    UNSTABLE(1.5F, 8.0F, 8.0F, 0.0F, "nuggetUnstable"),
    //MineFactory
    MEATRAW(2.0F, 3.0F, 1.0F, 0.0F, "nuggetMeatRaw"),
    MEAT(2.0F, 3.0F, 1.5F, 0.0F, "nuggetMeat"),
    //Steel
    STEEL(1.4F, 6.4F, 7.0F, 0.08F, "nuggetSteel"),
    //Thaumcraft
    THAUMIUM(1.35F, 6.2F, 7.0F, 0.08F, "nuggetThaumium"),
    VOID(2.0F, 7.0F, 10.0F, 0.0F, "nuggetVoid"),
    //Flax's Steam
    ZINC(1.5F, 3.0F, 4.0F, 0.15F, "nuggetZinc"),
    BRASS(1.4F, 4.0F, 5.7F, 0.14F, "nuggetBrass");


    private float speed;
    //penetration
    private float weight;
    //attack
    private float bias;
    private float accuracy;
    private String dictionaryName;
    //unused
    private Array effects;
    List<ItemStack> nuggetList = new ArrayList();
    boolean active = false;

    ValidRailAmmo (float speed, float weight, float bias, float accuracy, String name){
        this.speed = speed;
        this.weight = weight;
        this.bias = bias;
        this.accuracy = accuracy;
        this.dictionaryName = name;
    }

    public float getSpeed (){
        return this.speed;
    }

    public float getWeight (){
        return this.weight;
    }

    public float getBias (){
        return this.bias;
    }

    public float getAccuracy (){
        return this.accuracy;
    }

    public String getDictionaryName (){
        return this.dictionaryName;
    }

    public List<ItemEffects> getEffects (){
        List list = Collections.singletonList(this.effects);
        return list;
    }

    public static ValidRailAmmo getAmmoFromString (String string){
        for (ValidRailAmmo e : ValidRailAmmo.values()){
            if (e.getDictionaryName() == string){
                return e;
            }
        }
        return UNKNOWN;
    }

    public void addTolist (ItemStack stack){
        this.nuggetList.add(stack);
    }

    public List<ItemStack> getNugetlist (){
        return this.nuggetList;
    }

    public boolean isActive (){
        return this.active;
    }

    public void setActive (){
        this.active = true;
    }

    public static ValidRailAmmo getFromOrdinal (int ord){
        for (ValidRailAmmo e : ValidRailAmmo.values()){
            if (e.ordinal() == ord){
                return e;
            }
        }
        return UNKNOWN;
    }

    public boolean isOrdinal (int ord){
        return (this.ordinal() == ord);
    }
}
