package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.item.Items;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import weissmoon.core.block.WeissBlockContainer;
import weissmoon.random.Randommod;
import weissmoon.random.block.tile.TileProjectTable;
import weissmoon.random.item.IPlankHolder;
import weissmoon.random.item.ItemBlockProjectTable;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 5/7/19.
 */
public class BlockProjectTable extends WeissBlockContainer {

    public static final BooleanProperty PLACED = BooleanProperty.create("placed");

    public BlockProjectTable() {
        super(Strings.Blocks.PROJECT_TABLE, Properties.create(Material.WOOD).hardnessAndResistance(2.5F).notSolid());
        this.setDefaultState(this.getDefaultState().with(PLACED, false));
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return new TileProjectTable();
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(PLACED);
    }


    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean moving)
    {
        TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileProjectTable)
        {
            InventoryHelper.dropInventoryItems(worldIn, pos, (TileProjectTable)tileentity);
            ItemStack table = new ItemStack(ModBlocks.projectTable);
            IPlankHolder.setPlankS(table, ((TileProjectTable) tileentity).getWoodStack());
            InventoryHelper.spawnItemStack(worldIn, pos.getX(), pos.getY(), pos.getZ(), table);
            worldIn.updateComparatorOutputLevel(pos, this);
        }

        super.onReplaced(state, worldIn, pos, newState, moving);
    }

    public void harvestBlock(World worldIn, PlayerEntity player, BlockPos pos, BlockState state, @Nullable TileEntity te, ItemStack stack)
    {
        if (te instanceof TileProjectTable)
        {
            player.addStat(Stats.BLOCK_MINED.get(this));
            player.addExhaustion(0.005F);

            if (worldIn.isRemote)
            {
                return;
            }

            int i = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
            Item item = this.asItem();//state, (ServerWorld)worldIn, pos, te);

            if (item == Items.AIR)
            {
                return;
            }

            ItemStack itemstack = new ItemStack(item);//, this.(worldIn.rand));
            //itemstack.setStackDisplayName(((IWorldNameable)te).getName());
            ((ItemBlockProjectTable)itemstack.getItem()).setPlank(itemstack, ((TileProjectTable)te).getWoodStack());
            spawnAsEntity(worldIn, pos, itemstack);
        }
        else
        {
            super.harvestBlock(worldIn, player, pos, state, null, stack);
        }
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity playerIn, Hand hand, BlockRayTraceResult hit) {
        if (worldIn.isRemote) {
            return ActionResultType.SUCCESS;
        } else {
            TileProjectTable ilockablecontainer = ((TileProjectTable)worldIn.getTileEntity(pos));

            if (ilockablecontainer != null) {
                playerIn.openContainer(ilockablecontainer);
                NetworkHooks.openGui((ServerPlayerEntity) playerIn, ilockablecontainer, ilockablecontainer.getPos());
            }

            return ActionResultType.SUCCESS;
        }
    }
    @Override
    public BlockRenderType getRenderType(BlockState state)
    {
        return BlockRenderType.MODEL;
    }

//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer(){
//        return BlockRenderLayer.TRANSLUCENT;
//    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        ItemStack itemstack = new ItemStack(ModBlocks.projectTable);
        ItemStack plank = ((TileProjectTable)worldIn.getTileEntity(pos)).getWoodStack();
        IPlankHolder.setPlankS(itemstack, plank);
        return itemstack;
    }
}
