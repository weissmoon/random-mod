package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.state.EnumProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 2/27/22.
 */
public class BlockBambooCharcoal extends WeissBlock{

    public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.AXIS;

    public BlockBambooCharcoal(){
        super(Strings.Blocks.BAMBOO_CHARCOAL_BLOCK_NAME, Properties.create(Material.ROCK, MaterialColor.BROWN).hardnessAndResistance(5.0F, 6.0F).sound(SoundType.WOOD));
        this.setDefaultState(this.getDefaultState().with(AXIS, Direction.Axis.Y));
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        switch(rot) {
            case COUNTERCLOCKWISE_90:
            case CLOCKWISE_90:
                switch(state.get(AXIS)) {
                    case X:
                        return state.with(AXIS, Direction.Axis.Z);
                    case Z:
                        return state.with(AXIS, Direction.Axis.X);
                    default:
                        return state;
                }
            default:
                return state;
        }
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(AXIS);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(AXIS, context.getFace().getAxis());
    }
}
