package weissmoon.random.block.override;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;

/**
 * Created by Weissmoon on 1/11/20.
 */
public class IMoss {

    protected static boolean isDark(IWorldReader world, BlockPos blockpos) {
        float i = world.getLight(blockpos);
        return i < 9;
    }

    protected static boolean isFlowingWater(IWorldReader world, BlockPos blockpos) {
        BlockState state = world.getBlockState(blockpos);
        if (Blocks.WATER == state.getBlock()){
            return !state.getFluidState().isSource();
        }
        return false;
    }
}
