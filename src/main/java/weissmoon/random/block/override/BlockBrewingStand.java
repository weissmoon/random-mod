package weissmoon.random.block.override;

import net.minecraft.block.Block;
import net.minecraft.block.BrewingStandBlock;
import net.minecraft.block.material.Material;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.block.BlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import weissmoon.core.block.IBlockWeissOverride;
import weissmoon.random.block.override.tile.TileBrewingStand;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 4/30/19.
 */
public class BlockBrewingStand extends BrewingStandBlock implements IBlockWeissOverride{

    public BlockBrewingStand(){
        super(Block.Properties.create(Material.IRON).hardnessAndResistance(0.5F).setLightLevel((state) -> 1));
        setRegistryName("minecraft:brewing_stand");
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader reader){
        return new TileBrewingStand();
    }

    @Override
    public String getModID(){
        return "minecraft";
    }

    @Override
    public String getWeissName() {
        return "brewing_stand";
    }

    @Override
    public String getTrueModID(ItemStack itemStack){
        return Reference.MOD_ID;
    }
}
