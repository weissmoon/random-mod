package weissmoon.random.block.override;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemStack;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import weissmoon.core.block.IBlockWeissOverride;
import weissmoon.random.lib.Reference;

import java.util.Random;

/**
 * Created by Weissmoon on 1/11/20.
 */
public class BlockMossCobblestone extends Block implements IBlockWeissOverride{

    protected final String RegName = "mossy_cobblestone";

    public BlockMossCobblestone() {
        super(Block.Properties.create(Material.ROCK).tickRandomly().hardnessAndResistance(2.0F, 6.0F));
        setRegistryName("minecraft:" + RegName);
    }

//    @Override
//    public int tickRate(IWorldReader worldIn) {
//        return 50;
//    }

    @Override
    public void randomTick(BlockState state, ServerWorld worldIn, final BlockPos pos, Random random) {
        if (!worldIn.isRemote) {
            if (!worldIn.isAreaLoaded(pos, 3)) return; // Forge: prevent loading unloaded chunks when checking neighbor's light and spreading
            if (IMoss.isDark(worldIn, pos)){
                BlockPos uppos = new BlockPos(pos).up();
                BlockState upstate = worldIn.getBlockState(uppos);
                if (isAir(upstate, worldIn, uppos)){
                    for(Direction side: BlockStateProperties.HORIZONTAL_FACING.getAllowedValues()){
                        BlockPos brickPos = new BlockPos(pos).offset(side);
                        BlockState stoneBricks = worldIn.getBlockState(brickPos);
                        if (Blocks.COBBLESTONE == stoneBricks.getBlock()){
                            for(Direction sides: BlockStateProperties.HORIZONTAL_FACING.getAllowedValues()){
                                if(IMoss.isFlowingWater(worldIn, new BlockPos(brickPos).offset(sides))) {
                                    worldIn.setBlockState(brickPos, state);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public String getModID(){
        return "minecraft";
    }

    @Override
    public String getWeissName() {
        return RegName;
    }

    @Override
    public String getTrueModID(ItemStack itemStack){
        return Reference.MOD_ID;
    }
}
