package weissmoon.random.block.override.tile;

import net.minecraft.block.Blocks;
import net.minecraft.block.BrewingStandBlock;
import net.minecraft.block.BlockState;
import net.minecraft.item.Items;
import net.minecraft.inventory.ItemStackHelper;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.BrewingStandTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.server.ServerWorld;

import java.util.Arrays;

/**
 * Created by Weissmoon on 4/30/19.
 */
public class TileBrewingStand extends BrewingStandTileEntity {


    public NonNullList<ItemStack> previousBrewingItemStacks = NonNullList.withSize(5, ItemStack.EMPTY);
    public boolean[] hadStack = new boolean[5];

    @Override
    public void tick(){
        ItemStack itemstack = this.brewingItemStacks.get(4);

        if (this.fuel <= 0 && itemstack.getItem() == Items.BLAZE_POWDER){
            this.fuel = 20;
            itemstack.shrink(1);
            this.markDirty();
        }

        boolean flag = this.canBrew();
        boolean flag1 = this.brewTime > 0;
        ItemStack itemstack1 = this.brewingItemStacks.get(3);

        if (flag1){
            --this.brewTime;
            boolean flag2 = this.brewTime == 0;
            if (flag2 && flag){
                this.brewPotions();
                this.markDirty();
            }else if (!flag){
                this.brewTime = 0;
                this.markDirty();
            }else if (this.ingredientID != itemstack1.getItem()){
                this.brewTime = 0;
                this.markDirty();
            }
        }else if (flag && this.fuel > 0){
            --this.fuel;
            this.brewTime = 400;
            this.ingredientID = itemstack1.getItem();
            this.markDirty();
        }

        if (!this.world.isRemote){
            boolean[] aboolean = this.createFilledSlotsArray();

            if (!Arrays.equals(aboolean, this.filledSlots)){
                this.filledSlots = aboolean;
                BlockState iblockstate = this.world.getBlockState(this.getPos());

                if (!(iblockstate.getBlock() instanceof BrewingStandBlock))
                    return;

                for(int i = 0; i < BrewingStandBlock.HAS_BOTTLE.length; ++i)
                    iblockstate = iblockstate.with(BrewingStandBlock.HAS_BOTTLE[i], aboolean[i]);

                //this.world.setBlockState(this.pos, iblockstate, 2);
            }
        }

        if(world instanceof ServerWorld){
            int i = 0;
            while(i<5){
                if(brewingItemStacks.get(i).isEmpty())
                    if (hadStack[i]){
                        hadStack[i] = false;
                        previousBrewingItemStacks.set(i, ItemStack.EMPTY);
                        SUpdateTileEntityPacket packet = getUpdatePacket();

                        if(packet != null)
                            ((ServerWorld) world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                                    forEach(player -> player.connection.sendPacket(packet));
                        break;
                    }

                if (!(isItemEqual(previousBrewingItemStacks.get(i), brewingItemStacks.get(i)))){
                    previousBrewingItemStacks.set(i, brewingItemStacks.get(i));
                    hadStack[i] = true;
                    SUpdateTileEntityPacket packet = getUpdatePacket();

                    if(packet != null)
                        ((ServerWorld) world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                                forEach(player -> player.connection.sendPacket(packet));
                    break;
                }
                i++;
            }
        }
    }

    @Override
    public CompoundNBT getUpdateTag() {
        // getUpdateTag() is called whenever the chunkdata is sent to the
        // client. In contrast getUpdatePacket() is called when the tile entity
        // itself wants to sync to the client. In many cases you want to send
        // over the same information in getUpdateTag() as in getUpdatePacket().
        CompoundNBT nbtTag = new CompoundNBT();
        this.write(nbtTag);
        return nbtTag;
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        //this.writeToNBT(nbtTag);
        nbtTag.putShort("BrewTime", (short)this.brewTime);
        ItemStackHelper.saveAllItems(nbtTag, this.brewingItemStacks);
        nbtTag.putByte("Fuel", (byte)this.fuel);
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile
        CompoundNBT nbtTag = packet.getNbtCompound();
        this.brewingItemStacks.clear();
        ItemStackHelper.loadAllItems(nbtTag, this.brewingItemStacks);
        this.brewTime = nbtTag.getShort("BrewTime");
        this.fuel = nbtTag.getByte("Fuel");
    }

    public boolean isItemEqual(ItemStack item,ItemStack other){
        return item.getItem() == other.getItem() && item.getDamage() == other.getDamage();
    }

    protected void onContentsChanged(){
        if(world instanceof ServerWorld){
            SUpdateTileEntityPacket packet = getUpdatePacket();

            if(packet != null)
                ((ServerWorld) world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                        forEach(player -> player.connection.sendPacket(packet));
        }
        markDirty();
    }
}
