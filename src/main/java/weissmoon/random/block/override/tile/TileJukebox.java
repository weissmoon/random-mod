package weissmoon.random.block.override.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.BlockState;
import net.minecraft.inventory.IClearable;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 2/22/20.
 */
public class TileJukebox extends TileEntity implements IClearable {

    public static final TileEntityType<TileJukebox> TYPE = new TileEntityType<>(TileJukebox::new, ImmutableSet.of(ModBlocks.jukebox), null);

    private ItemStack record = ItemStack.EMPTY;

    public TileJukebox() {
        super(TYPE);
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if (compound.contains("RecordItem", 10)) {
            this.setRecord(ItemStack.read(compound.getCompound("RecordItem")));
        }

    }

    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        if (!this.getRecord().isEmpty()) {
            compound.put("RecordItem", this.getRecord().write(new CompoundNBT()));
        }

        return compound;
    }

    public ItemStack getRecord() {
        return this.record;
    }

    public void setRecord(ItemStack p_195535_1_) {
        this.record = p_195535_1_;
        this.markDirty();
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT nbtTag = new CompoundNBT();
        this.write(nbtTag);
        return nbtTag;
    }
    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        CompoundNBT nbtTag = new CompoundNBT();
        nbtTag.put("disc", this.record.serializeNBT());
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile
        CompoundNBT nbtTag = packet.getNbtCompound();
        CompoundNBT discTag = nbtTag.getCompound("disc");
        this.record = ItemStack.read(discTag);
    }

    public void clear() {
        this.setRecord(ItemStack.EMPTY);
    }
}
