package weissmoon.random.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.stats.Stats;
import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissBlockRegistry;
import weissmoon.random.block.override.tile.TileJukebox;
import weissmoon.random.lib.Strings;

import static weissmoon.core.helper.BlockHelper.dropStackInWorld;

/**
 * Created by Weissmoon on 2/22/20.
 */
public class BlockJukebox extends JukeboxBlock implements IBlockWeiss {

    protected static final VoxelShape SHAPE = VoxelShapes.or(Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D));

    private final String ModId;
    protected final String RegName;

    public BlockJukebox() {
        super(Block.Properties.create(Material.WOOD, MaterialColor.DIRT).hardnessAndResistance(2.0F, 6.0F));
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = Strings.Blocks.JUKEBOX_NAME;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        setDefaultState(this.stateContainer.getBaseState().with(HAS_RECORD, false));
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);

    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if(state.get(HAS_RECORD)){
            dropRecorda(worldIn, pos);
            state = state.with(HAS_RECORD, false);
            worldIn.setBlockState(pos, state, 2);
        }else{
            ItemStack itemstack = player.getHeldItem(handIn);
            if(!(itemstack.getItem() instanceof MusicDiscItem))
                return ActionResultType.PASS;
            if (!worldIn.isRemote) {
                insertRecord(worldIn, pos, state, itemstack);
                worldIn.playEvent(null, 1010, pos, Item.getIdFromItem(itemstack.getItem()));
                if (player != null) {
                    player.addStat(Stats.PLAY_RECORD);
                    if(!player.abilities.isCreativeMode)
                        itemstack.shrink(1);
                }
            }
            worldIn.setBlockState(pos, state.with(HAS_RECORD, true));
        }
        return ActionResultType.SUCCESS;
    }

    @Override
    public void insertRecord(IWorld worldIn, BlockPos pos, BlockState state, ItemStack recordStack) {
        TileEntity tileentity = worldIn.getTileEntity(pos);
        if (tileentity instanceof TileJukebox) {
            ((TileJukebox)tileentity).setRecord(recordStack.copy());
            worldIn.setBlockState(pos, state.with(HAS_RECORD, true), 2);
        }
    }

    private void dropRecorda(World worldIn, BlockPos pos) {
        if (!worldIn.isRemote) {
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof TileJukebox) {
                TileJukebox jukeboxtileentity = (TileJukebox)tileentity;
                ItemStack itemstack = jukeboxtileentity.getRecord();
                if (!itemstack.isEmpty()) {
                    worldIn.playEvent(1010, pos, 0);
                    jukeboxtileentity.clear();
                    dropStackInWorld(worldIn, pos, itemstack);
                }
            }
        }
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            this.dropRecorda(worldIn, pos);
            super.onReplaced(state, worldIn, pos, newState, isMoving);
        }
    }

    public int getComparatorInputOverride(BlockState blockState, World worldIn, BlockPos pos) {
        TileEntity tileentity = worldIn.getTileEntity(pos);
        if (tileentity instanceof TileJukebox) {
            Item item = ((TileJukebox)tileentity).getRecord().getItem();
            if (item instanceof MusicDiscItem) {
                return ((MusicDiscItem)item).getComparatorValue();
            }
        }

        return 0;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader reader) {
        return new TileJukebox();
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

//    @Override
//    public BlockRenderLayer getRenderLayer() {
//        return BlockRenderLayer.CUTOUT;
//    }

    @Override
    public final String getModID(){
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }
}
