package weissmoon.random.block.plates;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.MonsterEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

/**
 * Created by Weissmoon on 1/12/21.
 */
public class BlockMossyPressurePlate extends WeissRandomBasePlate {

    public BlockMossyPressurePlate(String name, boolean silent, boolean invisible) {
        super(name, null, Properties.create(Material.ROCK,invisible ? MaterialColor.AIR : MaterialColor.STONE).hardnessAndResistance(2.0F, 1200.0F), silent, invisible);
    }

    @Override
    protected List<? extends Entity> getEntitylist(World world, BlockPos pos) {
        AxisAlignedBB axisalignedbb = PRESSURE_AABB.offset(pos);
        return world.getEntitiesWithinAABB(MonsterEntity.class, axisalignedbb);
    }
}
