package weissmoon.random.block.plates;

import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;

/**
 * Created by Weissmoon on 5/24/19.
 */
public class BlockObsidianPressurePlate extends WeissRandomBasePlate {

    public BlockObsidianPressurePlate(String name, boolean silent, boolean invisible) {
        super(name, null, Properties.create(Material.ROCK, invisible ? MaterialColor.AIR : MaterialColor.BLACK).hardnessAndResistance(2.0F, 1200.0F), silent, invisible);
        this.setDefaultState(this.stateContainer.getBaseState().with(POWERED, false));
    }

    @Override
    protected List<? extends Entity> getEntitylist(World world, BlockPos pos) {
        AxisAlignedBB axisalignedbb = PRESSURE_AABB.offset(pos);
        return world.getEntitiesWithinAABB(PlayerEntity.class, axisalignedbb);
    }
}
