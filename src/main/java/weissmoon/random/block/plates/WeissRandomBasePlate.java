package weissmoon.random.block.plates;

import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import weissmoon.core.block.WeissBlockPressurePlate;

/**
 * Created by Weissmoon on 1/12/21.
 */
public abstract class WeissRandomBasePlate extends WeissBlockPressurePlate {

    protected final boolean silent;
    private final boolean invisible;

    public WeissRandomBasePlate(String name, Sensitivity sensitivity, Properties properties, boolean silent, boolean invisible) {
        super(name, sensitivity, properties.doesNotBlockMovement());
        this.silent = silent;
        this.invisible = invisible;
    }

    @Override
    protected void playClickOnSound(IWorld worldIn, BlockPos pos) {
        if(!silent)
            worldIn.playSound(null, pos, SoundEvents.BLOCK_STONE_PRESSURE_PLATE_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.6F);
    }

    @Override
    protected void playClickOffSound(IWorld worldIn, BlockPos pos) {
        if(!silent)
            worldIn.playSound(null, pos, SoundEvents.BLOCK_STONE_PRESSURE_PLATE_CLICK_OFF, SoundCategory.BLOCKS, 0.3F, 0.5F);
    }

    public boolean isInvisible(){
        return invisible;
    }
}
