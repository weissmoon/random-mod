package weissmoon.random.block.plates;

import com.google.common.collect.ImmutableMap;
import com.mojang.serialization.MapCodec;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.Entity;
import net.minecraft.entity.monster.piglin.PiglinTasks;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.Property;
import net.minecraft.state.StateContainer;
import net.minecraft.tags.BlockTags;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import weissmoon.core.item.IItemHolderItem;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateBase;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateInvisible;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateSilent;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateSilentInvisible;

import java.util.List;

/**
 * Created by Weissmoon on 8/26/21.
 */
public class WoodenPressurePlate extends WeissRandomBasePlate{

    private final boolean invisible;
    //Custom BlockState for passing plate color
    protected final StateContainer<Block, BlockState> stateContainerCustom;

    public WoodenPressurePlate(String name, boolean silent, boolean invisible){
        super(name, null, Properties.create(Material.WOOD, MaterialColor.AIR), silent, invisible);
        this.invisible = invisible;
        //Special BlockState only for visible plates
        if(!invisible){
            StateContainer.Builder<Block, BlockState> builder = new StateContainer.Builder<>(this);
            fillStateContainer(builder);
            stateContainerCustom = builder.func_235882_a_(Block::getDefaultState, PlateStateContainer::new);
            setDefaultState(this.stateContainerCustom.getBaseState().with(POWERED, false));
        }else
            stateContainerCustom = null;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world){
        if(silent){
            if(invisible)
                return new TileWoodenPressurePlateSilentInvisible();
            else
                return new TileWoodenPressurePlateSilent();
        }else{
            if(invisible)
                return new TileWoodenPressurePlateInvisible();
            else
                return null;
        }
    }

    @Override
    public boolean hasTileEntity(BlockState state){
        return true;
    }

    @Override
    protected List<? extends Entity> getEntitylist(World world, BlockPos pos){
        AxisAlignedBB axisalignedbb = PRESSURE_AABB.offset(pos);
        return world.getEntitiesWithinAABBExcludingEntity(null, axisalignedbb);
    }

    @Override
    protected void playClickOnSound(IWorld worldIn, BlockPos pos) {
        if(!silent)
            worldIn.playSound(null, pos, SoundEvents.BLOCK_WOODEN_BUTTON_CLICK_ON, SoundCategory.BLOCKS, 0.3F, 0.6F);
    }

    @Override
    protected void playClickOffSound(IWorld worldIn, BlockPos pos) {
        if(!silent)
            worldIn.playSound(null, pos, SoundEvents.BLOCK_WOODEN_BUTTON_CLICK_OFF, SoundCategory.BLOCKS, 0.3F, 0.5F);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.INVISIBLE;
    }

    //Render correct particles when broken directly
    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, BlockState state, PlayerEntity player) {
        TileWoodenPressurePlateBase tile = (TileWoodenPressurePlateBase)worldIn.getTileEntity(pos);
        BlockState passedState = tile.getStateforRender(false);
        worldIn.playEvent(player, 2001, pos, getStateId(passedState));
        if (this.isIn(BlockTags.GUARDED_BY_PIGLINS)) {
            PiglinTasks.func_234478_a_(player, false);
        }
    }

    //Render correct particles when broken not directly
    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if(facing == Direction.DOWN && !stateIn.isValidPosition(worldIn, currentPos)){
            if(!(worldIn.isRemote())){
                TileWoodenPressurePlateBase tile = (TileWoodenPressurePlateBase)worldIn.getTileEntity(currentPos);
                BlockState passedState = tile.getStateforRender(false);
                worldIn.playEvent(null, 2001, currentPos, getStateId(passedState));
            }
            return Blocks.AIR.getDefaultState();
        }else
            return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    public ItemStack getPickBlock(BlockState state, RayTraceResult target, IBlockReader world, BlockPos pos, PlayerEntity player)
    {
        ItemStack itemBlock = this.getBlock().getItem(world, pos, state);
        if (itemBlock.getItem() instanceof IItemHolderItem){
            try{
                TileWoodenPressurePlateBase ttile = (TileWoodenPressurePlateBase)world.getTileEntity(pos);
                IItemHolderItem.setHolderItem(itemBlock, ttile.getPlateBlockStack());
            }catch (Error e){
                IItemHolderItem.setHolderItem(itemBlock, new ItemStack(Blocks.OAK_PRESSURE_PLATE));
            }
        }
        return itemBlock;
    }

    //Custom BlockState for passing plate color
    @Override
    public StateContainer<Block, BlockState> getStateContainer() {
        if(!invisible)
            return this.stateContainerCustom;
        return super.getStateContainer();
    }

    //Custom BlockState for passing plate color
    public static class PlateStateContainer extends BlockState{

        public PlateStateContainer(Block block, ImmutableMap<Property<?>, Comparable<?>> propertiesToValueMap, MapCodec<BlockState> codec){
            super(block, propertiesToValueMap, codec);
        }


        public MaterialColor getMaterialColor(IBlockReader worldIn, BlockPos pos){
            try{
                TileWoodenPressurePlateBase tile = (TileWoodenPressurePlateBase)worldIn.getTileEntity(pos);
                return tile.getStateforRender(false).getMaterialColor(worldIn,pos);
            }catch (Error ignored){

            }
            return MaterialColor.WOOD;
        }
    }
}
