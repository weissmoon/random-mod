package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.fluid.FluidState;
import net.minecraft.fluid.Fluids;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.pathfinding.PathType;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.fml.network.NetworkHooks;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.block.tile.TileHungryChest;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Weissmoon on 3/30/22.
 */
public class BlockHungryChest extends WeissBlock implements ITileEntityProvider, IWaterLoggable{

    protected static final VoxelShape SHAPE = Block.makeCuboidShape(1.0D, 0.0D, 1.0D, 15.0D, 14.0D, 15.0D);
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;

    public BlockHungryChest(){
        super(Strings.Blocks.HUNGRY_CHEST_NAME, Properties.create(Material.WOOD).harvestTool(ToolType.AXE).hardnessAndResistance(2.5F));
        this.setDefaultState(this.getDefaultState().with(FACING, Direction.NORTH).with(WATERLOGGED, false));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder){
        builder.add(FACING).add(WATERLOGGED);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite()).with(WATERLOGGED, context.getWorld().getFluidState(context.getPos()).getFluid() == Fluids.WATER);
    }

    @Override
    public boolean hasTileEntity(BlockState state){
        return true;
    }

    /**
     * Called by ItemBlocks after a block is set in the world, to allow post-place logic
     */
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack){
        if(stack.hasDisplayName()){
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if(tileentity instanceof TileHungryChest){
                ((TileHungryChest)tileentity).setCustomName(stack.getDisplayName());
            }
        }
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }
    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (worldIn.isRemote)
            return ActionResultType.SUCCESS;

        TileHungryChest tile = (TileHungryChest)worldIn.getTileEntity(pos);

        if(!player.getHeldItem(handIn).isEmpty() && player.getHeldItem(handIn).getItem() == Items.LEAD){
            if(!tile.isLocked()){
                tile.setLocked(true);
                if(!player.isCreative())
                    player.getHeldItem(handIn).shrink(1);
                return ActionResultType.SUCCESS;
            }
        }else if(player.getHeldItem(handIn).isEmpty())
            if(player.isSneaking())
                if(tile.isLocked()){
                    tile.setLocked(false);
                    if(!player.isCreative())
                        weissmoon.core.helper.InventoryHelper.givePlayerOrDropItemStack(new ItemStack(Items.LEAD), player);
                    return ActionResultType.SUCCESS;
                }


        TileHungryChest ilockablecontainer = (TileHungryChest)worldIn.getTileEntity(pos);

        if (ilockablecontainer != null) {
            if(isBlocked(worldIn, pos))
                return ActionResultType.FAIL;

            NetworkHooks.openGui((ServerPlayerEntity)player, ilockablecontainer, ilockablecontainer.getPos());
            return ActionResultType.SUCCESS;
        }

        return ActionResultType.PASS;
    }

    @Deprecated
    public boolean eventReceived(BlockState state, World worldIn, BlockPos pos, int id, int param) {
        TileEntity tileentity = worldIn.getTileEntity(pos);
        return tileentity.receiveClientEvent(id, param);
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean moving){
        TileEntity tileentity = worldIn.getTileEntity(pos);

        if (tileentity instanceof TileHungryChest){
            InventoryHelper.dropInventoryItems(worldIn, pos, (TileHungryChest)tileentity);
            worldIn.updateComparatorOutputLevel(pos, this);
        }

        super.onReplaced(state, worldIn, pos, newState, moving);
    }


    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if (stateIn.get(WATERLOGGED)) {
            worldIn.getPendingFluidTicks().scheduleTick(currentPos, Fluids.WATER, Fluids.WATER.getTickRate(worldIn));
        }

        return super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
    }

    @Override
    public FluidState getFluidState(BlockState state) {
        return state.get(WATERLOGGED) ? Fluids.WATER.getStillFluidState(false) : super.getFluidState(state);
    }

    @Override
    public void onEntityCollision(BlockState state, World worldIn, BlockPos pos, Entity entityIn){
        TileEntity tile = worldIn.getTileEntity(pos);
        if(tile instanceof TileHungryChest){
            TileHungryChest chest = (TileHungryChest)worldIn.getTileEntity(pos);
            chest.onEntityCollision(entityIn);
            if(entityIn instanceof ItemEntity){

            }
        }
//        entityIn.attackEntityFrom(DamageSource.CACTUS, 1.0F);
    }

    public boolean allowsMovement(BlockState state, IBlockReader worldIn, BlockPos pos, PathType type) {
        return false;
    }

    public static boolean isBlocked(IWorld world, BlockPos pos) {
        return isBelowSolidBlock(world, pos) || isCatSittingOn(world, pos);
    }

    private static boolean isBelowSolidBlock(IBlockReader reader, BlockPos worldIn) {
        BlockPos blockpos = worldIn.up();
        return reader.getBlockState(blockpos).isNormalCube(reader, blockpos);
    }

    private static boolean isCatSittingOn(IWorld world, BlockPos pos) {
        List<CatEntity> list = world.getEntitiesWithinAABB(CatEntity.class, new AxisAlignedBB(pos.getX(), (pos.getY() + 1), pos.getZ(), (pos.getX() + 1), (pos.getY() + 2), (pos.getZ() + 1)));
        if (!list.isEmpty()) {
            for(CatEntity catentity : list) {
                if (catentity.isEntitySleeping()) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn){
        return new TileHungryChest();
    }
}
