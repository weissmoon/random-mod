package weissmoon.random.block;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.core.item.WeissBlockItem;
import weissmoon.core.block.WeissBlockPressurePlate;
import weissmoon.random.Randommod;
import weissmoon.random.api.WeissRandomAPI;
import weissmoon.random.block.ore.BlockGoldSandOre;
import weissmoon.random.block.ore.BlockIronGravelOre;
import weissmoon.random.block.override.BlockBrewingStand;
import weissmoon.random.block.override.BlockMossBrick;
import weissmoon.random.block.override.BlockMossCobblestone;
import weissmoon.random.block.override.tile.TileBrewingStand;
import weissmoon.random.block.override.tile.TileJukebox;
import weissmoon.random.block.plates.BlockMossyPressurePlate;
import weissmoon.random.block.plates.BlockObsidianPressurePlate;
import weissmoon.random.block.plates.WoodenPressurePlate;
import weissmoon.random.block.tile.*;
import weissmoon.random.block.tile.woodenPlate.*;
import weissmoon.random.item.*;
import weissmoon.random.item.override.ItemBrewingStand;
import weissmoon.random.item.override.ItemMossBricks;
import weissmoon.random.item.override.ItemMossCobblestone;
import weissmoon.random.lib.Reference;
import weissmoon.random.lib.Strings;
import weissmoon.random.registry.ActivationArrowRegistry;

/**
 * Created by Weissmoon on 2/1/19.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModBlocks {

    public static final BlockIgniter igniterBlock = new BlockIgniter();
    public static final BlockCorn cornBlock = new BlockCorn();
    public static final BlockCornTop cornBlockTop = new BlockCornTop();
    public static final BlockDrawbrigde drawbridge = new BlockDrawbrigde();
    //public static final BlockSeaLantern lantern = new BlockSeaLantern();//TODO
    public static final BlockBrewingStand brewingStand = new BlockBrewingStand();
    public static final BlockProjectTable projectTable = new BlockProjectTable();
    public static final WeissBlockPressurePlate obsidianPressurePlate = new BlockObsidianPressurePlate(Strings.Blocks.OBSIDIAN_PRESSSURE_PLATE, false, false);
    public static final BlockCharcoal charcoalBlock = new BlockCharcoal();
    //public static final BlockRedstoneEngine redstoneEngine = new BlockRedstoneEngine();
    public static final WeissStemGrownBlock melonFire = new BlockMelonFire();
    public static final WeissStemGrownBlock melonEarth = new BlockMelonEarth();
    public static final WeissStemGrownBlock melonAir = new BlockMelonAir();
    public static final BlockModStem stemFire = new BlockModStem(melonFire, "stemfire");
    public static final BlockModStem stemEarth = new BlockModStem(melonEarth, "stemearth");
    public static final BlockModStem stemAir = new BlockModStem(melonAir, "stemair");
    public static final BlockMossCobblestone mossyCobblestone = new BlockMossCobblestone();
    public static final BlockMossBrick mossyStoneBrick = new BlockMossBrick();
    public static final Block ironGravelOre = new BlockIronGravelOre();
    public static final Block goldSandOre = new BlockGoldSandOre();
    public static final BlockJukebox jukebox = new BlockJukebox();
    public static final WeissBlockPressurePlate obsidianPressurePlateS = new BlockObsidianPressurePlate(Strings.Blocks.OBSIDIAN_PRESSSURE_SILENT_PLATE, true, false);
    public static final WeissBlockPressurePlate obsidianPressurePlateI = new BlockObsidianPressurePlate(Strings.Blocks.OBSIDIAN_PRESSSURE_INVISIBLE_PLATE, false, true);
    public static final WeissBlockPressurePlate obsidianPressurePlateSI = new BlockObsidianPressurePlate(Strings.Blocks.OBSIDIAN_PRESSSURE_SILENT_INVISIBLE_PLATE, true, true);
    public static final WeissBlockPressurePlate mossyPressurePlate = new BlockMossyPressurePlate(Strings.Blocks.MOSSY_PRESSSURE_PLATE, false, false);
    public static final WeissBlockPressurePlate mossyPressurePlateS = new BlockMossyPressurePlate(Strings.Blocks.MOSSY_PRESSSURE_SILENT_PLATE, true, false);
    public static final WeissBlockPressurePlate mossyPressurePlateI = new BlockMossyPressurePlate(Strings.Blocks.MOSSY_PRESSSURE_INVISIBLE_PLATE, false, true);
    public static final WeissBlockPressurePlate mossyPressurePlateSI = new BlockMossyPressurePlate(Strings.Blocks.MOSSY_PRESSSURE_SILENT_INVISIBLE_PLATE, true, true);
    public static final BlockBarrel barrelBlock = new BlockBarrel();
    public static final BlockTinyChest tinyChest = new BlockTinyChest();
    public static final WeissBlockPressurePlate woodenPressurePlateS = new WoodenPressurePlate(Strings.Blocks.WOODEN_PRESSURE_PLATE_SILENT, true, false);
    public static final WeissBlockPressurePlate woodenPressurePlateI = new WoodenPressurePlate(Strings.Blocks.WOODEN_PRESSURE_PLATE_INVISIBLE, false, true);
    public static final WeissBlockPressurePlate woodenPressurePlateSI = new WoodenPressurePlate(Strings.Blocks.WOODEN_PRESSURE_PLATE_SILENT_INVISIBLE, true, true);
    public static final BlockBambooCharcoal bambooCharcoalBlock = new BlockBambooCharcoal();
    public static final BlockHungryChest hungryChest = new BlockHungryChest();

    public static void init (){
        MinecraftForge.EVENT_BUS.register(new ModBlocks());
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> evt) {
        IForgeRegistry<Block> blockRegistry = evt.getRegistry();


        blockRegistry.register(igniterBlock);
        blockRegistry.register(cornBlock);
        blockRegistry.register(cornBlockTop);
        blockRegistry.register(drawbridge);
//        GameRegistry.registerTileEntity(TileDrawbrigde.class, "tiledrawbrigde");
        //blockRegistry.register(lantern);
        blockRegistry.register(brewingStand);
//        GameRegistry.registerTileEntity(TileBrewingStand.class, "brewing_stand");
        blockRegistry.register(projectTable);
//        GameRegistry.registerTileEntity(TileProjectTable.class, "tileprojecttable");
        blockRegistry.register(obsidianPressurePlate);
        blockRegistry.register(charcoalBlock);
        //blockRegistry.register(redstoneEngine);
        blockRegistry.register(melonFire);
        blockRegistry.register(melonEarth);
        blockRegistry.register(melonAir);
        blockRegistry.register(stemFire);
        blockRegistry.register(stemEarth);
        blockRegistry.register(stemAir);
        blockRegistry.register(mossyCobblestone);
        blockRegistry.register(mossyStoneBrick);
        blockRegistry.register(ironGravelOre);
        blockRegistry.register(goldSandOre);
        blockRegistry.register(jukebox);
        blockRegistry.register(obsidianPressurePlateS);
        blockRegistry.register(obsidianPressurePlateI);
        blockRegistry.register(obsidianPressurePlateSI);
        blockRegistry.register(mossyPressurePlate);
        blockRegistry.register(mossyPressurePlateS);
        blockRegistry.register(mossyPressurePlateI);
        blockRegistry.register(mossyPressurePlateSI);
        blockRegistry.register(barrelBlock);
        blockRegistry.register(tinyChest);
        blockRegistry.register(woodenPressurePlateS);
        blockRegistry.register(woodenPressurePlateI);
        blockRegistry.register(woodenPressurePlateSI);
        blockRegistry.register(bambooCharcoalBlock);
        blockRegistry.register(hungryChest);
        Randommod.proxy.registerTileEntities();
        ((ActivationArrowRegistry) WeissRandomAPI.renderRegistry).registerVanilla();

        TileEntityType.BREWING_STAND.factory = TileBrewingStand::new;
        TileEntityType.BREWING_STAND.validBlocks = ImmutableSet.of(brewingStand);
    }

    @SubscribeEvent
    public static void registerItemBlocks(RegistryEvent.Register<Item> evt) {
        IForgeRegistry<Item> itemRegistry = evt.getRegistry();


        itemRegistry.register(new WeissBlockItem(igniterBlock, new Item.Properties().group(ItemGroup.REDSTONE)));//.setRegistryName(igniterBlock.getWeissName()));
        itemRegistry.register(new WeissBlockItem(drawbridge, new Item.Properties().group(ItemGroup.REDSTONE)));
//        itemRegistry.register(new WeissItemBlock(lantern));
        itemRegistry.register(new WeissBlockItem(obsidianPressurePlate, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(melonFire, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        itemRegistry.register(new WeissBlockItem(melonEarth, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        itemRegistry.register(new WeissBlockItem(melonAir, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        itemRegistry.register(new ItemBlockProjectTable());
        itemRegistry.register(new ItemBlockCharcoal());
        itemRegistry.register(new ItemBrewingStand());
        itemRegistry.register(new ItemMossCobblestone());
        itemRegistry.register(new ItemMossBricks());
        itemRegistry.register(new WeissBlockItem(ironGravelOre, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        itemRegistry.register(new WeissBlockItem(goldSandOre, new Item.Properties().group(ItemGroup.BUILDING_BLOCKS)));
        itemRegistry.register(new ItemJukebox());
        itemRegistry.register(new WeissBlockItem(obsidianPressurePlateS, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(obsidianPressurePlateI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(obsidianPressurePlateSI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(mossyPressurePlate, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(mossyPressurePlateS, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(mossyPressurePlateI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(mossyPressurePlateSI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new WeissBlockItem(barrelBlock, new Item.Properties().group(ItemGroup.DECORATIONS)));
        itemRegistry.register(new ItemBlockTinyChest());
        itemRegistry.register(new ItemWoodenPressurePlate(woodenPressurePlateS, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new ItemWoodenPressurePlate(woodenPressurePlateI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new ItemWoodenPressurePlate(woodenPressurePlateSI, new Item.Properties().group(ItemGroup.REDSTONE)));
        itemRegistry.register(new ItemBlockBambooCharcoal());
        itemRegistry.register(new ItemBlockHungryChest());
    }

    @SubscribeEvent
    public static void registerTiles(RegistryEvent.Register<TileEntityType<?>> evt) {
        IForgeRegistry<TileEntityType<?>> tileEntityTypes = evt.getRegistry();

        tileEntityTypes.register(TileDrawbrigde.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.DRAWBRIGDE_NAME)));
        tileEntityTypes.register(TileProjectTable.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.PROJECT_TABLE)));
        tileEntityTypes.register(TileJukebox.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.JUKEBOX_NAME)));
        tileEntityTypes.register(TileBarrel.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.PICKLING_BARREL_NAME)));
        tileEntityTypes.register(TileTinyChest.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.TINY_CHEST)));
        tileEntityTypes.register(TileWoodenPressurePlateSilent.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.WOODEN_PRESSURE_PLATE_SILENT)));
        tileEntityTypes.register(TileWoodenPressurePlateInvisible.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.WOODEN_PRESSURE_PLATE_INVISIBLE)));
        tileEntityTypes.register(TileWoodenPressurePlateSilentInvisible.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.WOODEN_PRESSURE_PLATE_SILENT_INVISIBLE)));
        tileEntityTypes.register(TileHungryChest.TYPE.setRegistryName(new ResourceLocation(Reference.MOD_ID+":"+Strings.Blocks.HUNGRY_CHEST_NAME)));
    }
}
