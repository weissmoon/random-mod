package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.block.tile.TileBarrel;
import weissmoon.random.lib.Strings;

import static weissmoon.core.helper.BlockHelper.dropStackInWorld;

/**
 * Created by Weissmoon on 4/19/21.
 */
public class BlockBarrel extends WeissBlock implements ITileEntityProvider{

    //public static final IntegerProperty LID_POSITION = IntegerProperty.create("lid", 0, 2);
    protected static final VoxelShape SHAPE = VoxelShapes.or(Block.makeCuboidShape(2, 0, 2, 14, 14, 14));

    public BlockBarrel(){
        super(Strings.Blocks.PICKLING_BARREL_NAME, Properties.create(Material.WOOD, MaterialColor.BROWN));
        //this.setDefaultState(this.getDefaultState().with(LID_POSITION, 0));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        //builder.add(LID_POSITION);
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader var1){
        return new TileBarrel();
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        return SHAPE;
    }

    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        TileBarrel tile = (TileBarrel)worldIn.getTileEntity(pos);
        if(tile != null){
            return tile.handleRightClick(player, handIn);
        }
        return ActionResultType.PASS;
    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean isMoving) {
        if (state.getBlock() != newState.getBlock()) {
            this.dropRecorda(worldIn, pos);
            super.onReplaced(state, worldIn, pos, newState, isMoving);
        }
    }

    private void dropRecorda(World worldIn, BlockPos pos) {
        if (!worldIn.isRemote) {
            TileEntity tileentity = worldIn.getTileEntity(pos);
            if (tileentity instanceof TileBarrel) {
                TileBarrel jukeboxtileentity = (TileBarrel)tileentity;
                ItemStack itemstack = jukeboxtileentity.removeTopStack();
                while(!itemstack.isEmpty()){
                    dropStackInWorld(worldIn, pos, itemstack);
                    itemstack = jukeboxtileentity.removeTopStack();
                }
            }
        }
    }

}
