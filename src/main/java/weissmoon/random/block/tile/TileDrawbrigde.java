package weissmoon.random.block.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.IContainerProvider;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.BlockItem;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.DirectionalPlaceContext;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.*;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.util.INameable;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.FakePlayerFactory;
import net.minecraftforge.items.ItemStackHandler;
import weissmoon.random.block.BlockDrawbrigde;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.container.ContainerDrawbrigde;

import javax.annotation.Nonnull;

/**
 * Created by Weissmoon on 4/5/19.
 */
public class TileDrawbrigde extends TileEntity implements ITickableTileEntity, INameable, IInventory, IContainerProvider, INamedContainerProvider {

    private ItemStackHandler itemStackHandler = new Limithadler();
    private boolean stopped, locked = false;
    private ItemStack stack;
    private int length;
    //private WeakReference<FakePlayer> fakePlayer;
    private String STOPPED_TAG = "stopped";
    private String LOCKED_TAG = "locked";
    private String STACK_TAG = "placedStack";
    private String LENGTH_TAG = "length";
    public static final TileEntityType<TileDrawbrigde> TYPE = new TileEntityType<>(TileDrawbrigde::new,  ImmutableSet.of(ModBlocks.drawbridge), null);

    public TileDrawbrigde(){
        super(TYPE);
    }

    @Override
    public void tick() {
        if (!this.world.isRemote){
            ItemStack blockStack = itemStackHandler.getStackInSlot(0);
            BlockState tileBlock = this.world.getBlockState(this.pos);
            Direction facing = tileBlock.get(BlockDrawbrigde.FACING);
            boolean powered = false;
            for(Direction facing1:Direction.values()){
                if(facing != facing1 && world.isSidePowered(pos.offset(facing1), facing1)){
                    powered = true;
                    break;
                }
            }
            if(world.isSidePowered(pos, Direction.DOWN)) {
                powered = true;
            } else {
                BlockPos blockpos = pos.up();

                for(Direction facing1:Direction.values()){
                    if(facing1 != Direction.DOWN && world.isSidePowered(blockpos.offset(facing1), facing1)) {
                        powered = true;
                        break;
                    }
                }
            }
            if (powered){
                if (!blockStack.isEmpty()) {
                    if(!this.stopped) {
                        if(length == 17)
                            return;
                        if (world.isAirBlock(pos.offset(facing, length + 1))) {
                            if (!(blockStack.isEmpty()) && (blockStack.getItem() instanceof BlockItem)) {
                                if(world.setBlockState(pos.offset(facing, length + 1),
                                        ((BlockItem) blockStack.getItem()).getBlock().getStateForPlacement(
                                                new DirectionalPlaceContext(
                                                        this.world,
                                                        pos.offset(facing, length + 1),
                                                        Direction.DOWN,
                                                        blockStack,
                                                        Direction.DOWN
//                                                //blockStack.getMetadata(),
//                                                null
                                                )
                                        )))
                                {
                                    this.stack = new ItemStack(blockStack.getItem());
                                    blockStack.shrink(1);
                                    markDirty();
                                    this.length++;
                                    this.locked = true;
                                }else{
                                    this.stopped = true;
                                }
                            }
                        }
                    }
                }
            }else{
                if(this.length > 0){
                    BlockState blockE = world.getBlockState(this.pos.offset(facing, length));
                    BlockPos blockEPos = this.pos.offset(facing, length);
                    if(!world.isAirBlock(blockEPos)){
                        ItemStack blockEStack = blockE.getBlock().getPickBlock(blockE,
                                null,
                                this.world,
                                blockEPos,
                                FakePlayerFactory.getMinecraft((net.minecraft.world.server.ServerWorld)this.world));
                        if(blockEStack.isItemEqual(blockStack)){
                            blockStack.grow(1);
                            world.removeBlock(blockEPos, false);
                        }else if(blockEStack.isItemEqual(this.stack) && blockStack.isEmpty()){
                            itemStackHandler.setStackInSlot(0, blockEStack);
                            world.removeBlock(blockEPos, false);
                        }
                    }
                    markDirty();
                    this.length--;
                }else{
                    this.locked = false;
                }
            }
        }
    }

    @Override
    public CompoundNBT getUpdateTag() {
        // getUpdateTag() is called whenever the chunkdata is sent to the
        // client. In contrast getUpdatePacket() is called when the tile entity
        // itself wants to sync to the client. In many cases you want to send
        // over the same information in getUpdateTag() as in getUpdatePacket().
        return write(new CompoundNBT());
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        this.write(nbtTag);
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }


    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile entity
        this.read(getBlockState(), packet.getNbtCompound());
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if(compound.contains("items")) {
            itemStackHandler.deserializeNBT(compound.getCompound("items"));
        }
        if(compound.contains(STOPPED_TAG)){
            this.stopped = compound.getBoolean(STOPPED_TAG);
        }
        if(compound.contains(LOCKED_TAG)){
            this.locked = compound.getBoolean(LOCKED_TAG);
        }
        if(compound.contains(STACK_TAG)){
            this.stack = ItemStack.read(compound.getCompound(STACK_TAG));
        }
        if(compound.contains(LENGTH_TAG)){
            this.length = compound.getInt(LENGTH_TAG);
        }
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.put("items", itemStackHandler.serializeNBT());
        compound.putBoolean(STOPPED_TAG, stopped);
        compound.putBoolean(LOCKED_TAG, locked);
        CompoundNBT itemTag = new CompoundNBT();
        if (!getCamoflageBlock().isEmpty()) {
            getCamoflageBlock().write(itemTag);
            compound.put(STACK_TAG, itemTag);
        }
        compound.putInt(LENGTH_TAG, length);
        return compound;
    }

    public ItemStack getCamoflageBlock(){
        return this.itemStackHandler.getStackInSlot(1);
    }

    @Override
    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        //return null;
        return new ContainerDrawbrigde(id, playerInventory, this);
    }

    @Override
    public ITextComponent getDisplayName() {
        return new TranslationTextComponent("weissrandom:drawbridge");
    }

    @Override
    public ITextComponent getName(){
        return new TranslationTextComponent("container.drawbridge");
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    @Override
    public int getSizeInventory() {
        return 2;
    }

    @Override
    public boolean isEmpty() {
        return this.itemStackHandler.getStackInSlot(0).isEmpty();
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        if (index > 2) return ItemStack.EMPTY;
        return this.itemStackHandler.getStackInSlot(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count) {
        return this.itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        int count = this.itemStackHandler.getStackInSlot(index).getCount();
        return this.itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        this.itemStackHandler.setStackInSlot(index, stack);
    }

    @Override
    public int getInventoryStackLimit() {
        return 16 - length;
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        return true;
    }

    @Override
    public void openInventory(PlayerEntity player) {

    }

    @Override
    public void closeInventory(PlayerEntity player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        if(index == 0) {
            if (this.itemStackHandler.getStackInSlot(index).getCount() >= 16 - length)
                return false;
            if (stack.getItem() instanceof BlockItem)
                return true;
        }
        return false;
    }

//    @Override
//    public int getField(int id) {
//        return 0;
//    }
//
//    @Override
//    public void setField(int id, int value) {
//
//    }
//
//    @Override
//    public int getFieldCount() {
//        return 0;
//    }

    @Override
    public void clear() {

    }

    private class Limithadler extends ItemStackHandler{

        public Limithadler() {
            super(2);
        }

        @Override
        protected int getStackLimit(int slot, @Nonnull ItemStack stack){
            if(slot == 0)
                return Math.min(16 - length, stack.getMaxStackSize());
            return 1;
        }

        @Override
        public void setStackInSlot(int slot, @Nonnull ItemStack stack)
        {
            validateSlotIndex(slot);
            if(this.stacks.get(slot).getCount() <= 16 - length) {
                if (ItemStack.areItemStacksEqual(this.stacks.get(slot), stack))
                    return;
                this.stacks.set(slot, stack);
                onContentsChanged(slot);
            }
        }

        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate){
            if(locked) {
                return ItemStack.EMPTY;
            }else{
                return super.extractItem(slot, amount, simulate);
            }
        }

        @Override
        @Nonnull
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate){
            if(locked){
                return ItemStack.EMPTY;
            }else{
                return super.insertItem(slot, stack, simulate);
            }
        }

        @Override
        protected void onContentsChanged(int slot){
            if(world instanceof ServerWorld){
                SUpdateTileEntityPacket packet = getUpdatePacket();

                if(packet != null) {
                    ((ServerWorld) world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                        forEach(player -> player.connection.sendPacket(packet));
                }
            }
            markDirty();
        }
    }
}
