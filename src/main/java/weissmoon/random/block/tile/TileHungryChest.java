package weissmoon.random.block.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.ChestContainer;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.IContainerProvider;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.IChestLid;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.items.ItemStackHandler;
import weissmoon.random.block.BlockHungryChest;
import weissmoon.random.block.ModBlocks;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 3/30/22.
 */
public class TileHungryChest extends TileEntity implements IChestLid, ITickableTileEntity, IInventory, IContainerProvider, INamedContainerProvider{

    private ItemStackHandler itemStackHandler;
    public static final TileEntityType<TileHungryChest> TYPE = new TileEntityType<>(TileHungryChest::new, ImmutableSet.of(ModBlocks.hungryChest), null);
    private List<PlayerEntity> playersList = new ArrayList<>();
    private float lidAngle, prevLidAngle;
    private int playersLooking;
    private boolean locked = false;
    private boolean hasName = false;
    private ITextComponent name = new TranslationTextComponent("block.weissrandom.blockhungrychest");

    protected static final VoxelShape COLLISION_SHAPE = Block.makeCuboidShape(0, 0, 0, 16, 16, 16);

    public TileHungryChest(){
        super(TYPE);
        itemStackHandler = new ItemStackHandler(27);
    }

    @Override
    public int getSizeInventory(){
        return itemStackHandler.getSlots();
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if(compound.contains("items"))
            itemStackHandler.deserializeNBT(compound.getCompound("items"));
        if(compound.contains("name"))
            name = new StringTextComponent(compound.getString("name"));
        locked = compound.getBoolean("locked");
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.put("items", itemStackHandler.serializeNBT());
        if(hasName)
            compound.putString("name", name.getString());
        compound.putBoolean("locked", locked);
        return compound;
    }

    @Override
    public CompoundNBT getUpdateTag() {
        // getUpdateTag() is called whenever the chunkdata is sent to the
        // client. In contrast getUpdatePacket() is called when the tile entity
        // itself wants to sync to the client. In many cases you want to send
        // over the same information in getUpdateTag() as in getUpdatePacket().
        CompoundNBT compound = new CompoundNBT();
        super.write(compound);
        compound.putBoolean("locked", locked);
        return compound;
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        super.write(nbtTag);
        nbtTag.putBoolean("locked", locked);
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile entity
        CompoundNBT nbt = packet.getNbtCompound();
        locked = nbt.getBoolean("locked");
    }

    @Override
    public boolean isEmpty() {
        for(int i= 0;i<itemStackHandler.getSlots();i++)
            if (!itemStackHandler.getStackInSlot(i).isEmpty())
                return false;

        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index){
        return itemStackHandler.getStackInSlot(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count){
        return itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public ItemStack removeStackFromSlot(int index){
        return itemStackHandler.extractItem(index, this.itemStackHandler.getStackInSlot(index).getCount(), false);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack){
        itemStackHandler.setStackInSlot(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player){
        return true;
    }

    @Override
    public void clear(){

    }

    @Override
    public void openInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            if (playersLooking < 0) {
                playersLooking = 0;
            }

            playersList.add(player);
            playersLooking++;
            calculatePlayers();
        }
    }

    @Override
    public void closeInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            playersList.remove(player);
            playersLooking--;
            calculatePlayers();
        }
    }

    private void calculatePlayers(){
        Block block = getBlockState().getBlock();
        world.addBlockEvent(pos, block, 1, playersLooking);
    }

    public static int getPlayersUsing(IBlockReader reader, BlockPos posIn) {
        BlockState blockstate = reader.getBlockState(posIn);
        if (blockstate.hasTileEntity()) {
            TileEntity tileentity = reader.getTileEntity(posIn);
            if (tileentity instanceof TileHungryChest) {
                return ((TileHungryChest)tileentity).playersLooking;
            }
        }

        return 0;
    }

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if (id == 1){
            playersLooking = type;
            return true;
        }else if(id == 2){
            if(playersLooking == 0)
                lidAngle = 0.49F;
            return true;
        }else if(id == 3){
            if(type == 1)
                locked = true;
            else
                locked = false;
            return true;
        }else
            return false;
    }

    public void setCustomName(ITextComponent name){
        this.hasName = true;
        this.name = name;
    }

    @Override
    public ITextComponent getDisplayName(){
        return name;
    }

    @Nullable
    @Override
    public Container createMenu(int p_createMenu_1_, PlayerInventory p_createMenu_2_, PlayerEntity p_createMenu_3_){
        return ChestContainer.createGeneric9X3(p_createMenu_1_, p_createMenu_2_, this);
    }

    @Override
    public float getLidAngle(float partialTicks){
        return MathHelper.lerp(partialTicks, this.prevLidAngle, this.lidAngle);
    }

    @Override
    public void tick(){
        if(playersLooking > 0 && lidAngle == 0)
            world.playSound(null, pos, SoundEvents.BLOCK_CHEST_OPEN, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1.9F);

        prevLidAngle = lidAngle;

        if(playersLooking > 0)
            lidAngle += 0.2;
        else
            lidAngle -= 0.2;

        if(lidAngle < 0)
            lidAngle = 0;
        else if(lidAngle > 1)
            lidAngle = 1;

        if(lidAngle < 0.5 && prevLidAngle >= 0.5)
            world.playSound(null, pos, SoundEvents.BLOCK_CHEST_CLOSE, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1.6F);
    }

    public boolean setLocked(boolean locking){
        this.locked = locking;
        if(world instanceof ServerWorld){
            SUpdateTileEntityPacket packet = getUpdatePacket();

            if(packet != null)
                ((ServerWorld)world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                        forEach(player -> player.connection.sendPacket(packet));
        }
        return locked;
    }

    public boolean isLocked(){
        return locked;
    }

    public void onEntityCollision(Entity entity) {
        if (entity instanceof ItemEntity) {
            BlockPos blockpos = this.getPos();
            if (VoxelShapes.compare(VoxelShapes.create(entity.getBoundingBox().offset((-blockpos.getX()), (-blockpos.getY()), (-blockpos.getZ()))), COLLISION_SHAPE, IBooleanFunction.AND)) {
                if(BlockHungryChest.isBlocked(world, pos))
                    return;
                if(locked)
                    return;
                if(captureItem((ItemEntity)entity))
                    world.playSound(null, pos, SoundEvents.ENTITY_GENERIC_EAT, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1F);
                else
                    world.playSound(null, pos, SoundEvents.ENTITY_PLAYER_BURP, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1F);
                Block block = getBlockState().getBlock();
                world.addBlockEvent(pos, block, 2, playersLooking);
            }
        }

    }

    public boolean captureItem(ItemEntity itemEntity) {
        boolean flag = false;
        ItemStack itemstack = itemEntity.getItem().copy();
        ItemStack itemstack1 = tryInsertStack(itemstack);
        if (itemstack1.isEmpty()) {
            flag = true;
            itemEntity.remove();
        } else {
            itemEntity.setItem(itemstack1);
        }

        this.markDirty();
        return flag;
    }

    private ItemStack tryInsertStack(ItemStack stack){
        for(int k = 0; k < itemStackHandler.getSlots() && !stack.isEmpty(); ++k) {
            stack = itemStackHandler.insertItem(k, stack, false);
            if(stack.isEmpty())
                return stack;
        }
        return stack;
    }
}
