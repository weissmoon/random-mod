package weissmoon.random.block.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.block.Blocks;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.INameable;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.items.ItemStackHandler;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.container.ContainerProjectTable;

/**
 * Created by Weissmoon on 5/7/19.
 */
public class TileProjectTable extends TileEntity implements INamedContainerProvider, ISidedInventory, INameable {

    private ItemStackHandler itemStackHandler = new ItemStackHandler(27);
    private ItemStack woodStack = new ItemStack(Blocks.OAK_PLANKS);
    public static final TileEntityType<TileProjectTable> TYPE = new TileEntityType(TileProjectTable::new,  ImmutableSet.of(ModBlocks.projectTable), null);
    private static final ITextComponent name = new StringTextComponent("");

    public TileProjectTable() {
        super(TYPE);
    }

    @Override
    public int getSizeInventory() {
        return 27;
    }

    @Override
    public boolean isEmpty() {
        for(int i= 0;i<27;i++)
            if (!itemStackHandler.getStackInSlot(i).isEmpty())
                return false;

        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index) {
        if (index > 26) return ItemStack.EMPTY;
        return this.itemStackHandler.getStackInSlot(index);
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if(compound.contains("items")) {
            itemStackHandler.deserializeNBT( compound.getCompound("items"));
        }
        if(compound.contains("plank")){
            woodStack = ItemStack.read(compound.getCompound("plank"));
        }
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        //this.writeToNBT(nbtTag);
        super.write(nbtTag);
        nbtTag.put("plank", woodStack.serializeNBT());
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        // getUpdateTag() is called whenever the chunkdata is sent to the
        // client. In contrast getUpdatePacket() is called when the tile entity
        // itself wants to sync to the client. In many cases you want to send
        // over the same information in getUpdateTag() as in getUpdatePacket().
        CompoundNBT compound = new CompoundNBT();
        super.write(compound);
        compound.put("plank", woodStack.serializeNBT());
        return compound;
        //return writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile entity
        this.read(getBlockState(), packet.getNbtCompound());
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.put("items", itemStackHandler.serializeNBT());
        compound.put("plank", woodStack.serializeNBT());
        return compound;
    }


    @Override
    public ItemStack decrStackSize(int index, int count) {
        return this.itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public ItemStack removeStackFromSlot(int index) {
        return this.itemStackHandler.extractItem(index, this.itemStackHandler.getStackInSlot(index).getCount(), false);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack) {
        this.itemStackHandler.setStackInSlot(index, stack);
    }

    @Override
    public int getInventoryStackLimit() {
        return 64;
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player) {
        return true;
    }

    @Override
    public void openInventory(PlayerEntity player) {

    }

    @Override
    public void closeInventory(PlayerEntity player) {

    }

    @Override
    public boolean isItemValidForSlot(int index, ItemStack stack) {
        return true;
    }

//    @Override
//    public int getField(int id) {
//        return 0;
//    }
//
//    @Override
//    public void setField(int id, int value) {
//
//    }
//
//    @Override
//    public int getFieldCount() {
//        return 0;
//    }

    @Override
    public void clear() {

    }

    @Override
    public Container createMenu(int id, PlayerInventory playerInventory, PlayerEntity playerEntity) {
        return new ContainerProjectTable(id, playerInventory, this, playerEntity);
    }

    @Override
    public  ITextComponent getDisplayName() {
        return name;
    }

    @Override
    public ITextComponent getName(){
        return new TranslationTextComponent("container.projecttable");
    }

    @Override
    public boolean hasCustomName() {
        return false;
    }

    public ItemStack getWoodStack(){
        return this.woodStack;
    }

    public void setWoodStack(ItemStack stack){
        this.woodStack = stack;
    }

    @Override
    public int[] getSlotsForFace(Direction side){
        return new int[0];
    }

    @Override
    public boolean canInsertItem(int index, ItemStack itemStackIn, Direction direction){
        return false;
    }

    @Override
    public boolean canExtractItem(int index, ItemStack stack, Direction direction){
        return false;
    }
}
