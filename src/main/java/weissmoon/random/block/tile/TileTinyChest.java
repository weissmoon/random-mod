package weissmoon.random.block.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.IContainerProvider;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.*;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.items.ItemStackHandler;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.container.ContainerTinyChest;
import weissmoon.random.container.ContainerTinyTinyChest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Weissmoon on 6/27/21.
 */
@OnlyIn(
        value = Dist.CLIENT,
        _interface = IChestLid.class
)
public class TileTinyChest extends TileEntity implements IChestLid, ITickableTileEntity, IInventory, IContainerProvider, INamedContainerProvider{

    private TinyStackHandler itemStackHandler;
    public static final TileEntityType<TileTinyChest> TYPE = new TileEntityType<>(TileTinyChest::new, ImmutableSet.of(ModBlocks.tinyChest), null);
    private List<PlayerEntity> playersList = new ArrayList<>();
    private float lidAngle, prevLidAngle;
    private int playersLooking;
    private boolean tiny;
    private boolean hasName = false;
    private ITextComponent name = new TranslationTextComponent("block.weissrandom.blocktinychest");

    public TileTinyChest(){
        super(TYPE);
        itemStackHandler = new TinyStackHandler(1);
        tiny = true;
    }

    @Override
    public void validate() {
        this.removed = false;
    }


    @Override
    public void tick(){
        if(playersLooking > 0 && lidAngle == 0)
            world.playSound(null, pos, SoundEvents.BLOCK_CHEST_OPEN, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1.9F);

        prevLidAngle = lidAngle;

        if(playersLooking > 0)
            lidAngle += 0.2;
        else
            lidAngle -= 0.2;

        if(lidAngle < 0)
            lidAngle = 0;
        else if(lidAngle > 1)
            lidAngle = 1;

        if(lidAngle < 0.5 && prevLidAngle >= 0.5)
            world.playSound(null, pos, SoundEvents.BLOCK_CHEST_CLOSE, SoundCategory.BLOCKS, 0.5F, this.world.rand.nextFloat() * 0.1F + 1.6F);
    }

    private void calculatePlayers(){
        Block block = getBlockState().getBlock();
        world.addBlockEvent(pos, block, 1, playersLooking);
    }

    public static int getPlayersUsing(IBlockReader reader, BlockPos posIn) {
        BlockState blockstate = reader.getBlockState(posIn);
        if (blockstate.hasTileEntity()) {
            TileEntity tileentity = reader.getTileEntity(posIn);
            if (tileentity instanceof TileTinyChest) {
                return ((TileTinyChest)tileentity).playersLooking;
            }
        }

        return 0;
    }

    @Override
    public float getLidAngle(float partialTicks){
        return MathHelper.lerp(partialTicks, this.prevLidAngle, this.lidAngle);
    }

    @Override
    public boolean receiveClientEvent(int id, int type) {
        if (id == 1){
            playersLooking = type;
            return true;
        }else
            return false;
    }

    public boolean isTiny(){
        return tiny;
    }

    public void tiny(){
        tiny = false;

        for(PlayerEntity player : playersList){
            player.closeScreen();
        }

        TinyStackHandler newSize = new TinyStackHandler(108);
        newSize.setStackInSlot(0, itemStackHandler.getStackInSlot(0));
        itemStackHandler = newSize;
        if(world instanceof ServerWorld){
            SUpdateTileEntityPacket packet = getUpdatePacket();

            if(packet != null)
                ((ServerWorld)world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                        forEach(player -> player.connection.sendPacket(packet));
        }
    }

    @Override
    public int getSizeInventory(){
        return itemStackHandler.getSlots();
    }

    @Override
    public void read(BlockState state, CompoundNBT compound) {
        super.read(state, compound);
        if(compound.contains("tiny")){
            tiny = compound.getBoolean("tiny");
            if(!tiny)
                itemStackHandler = new TinyStackHandler(108);
        }
        if(compound.contains("items"))
            itemStackHandler.deserializeNBT(compound.getCompound("items"));
        if(compound.contains("name"))
            name = new StringTextComponent(compound.getString("name"));
    }

    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        compound.putBoolean("tiny", tiny);
        compound.put("items", itemStackHandler.serializeNBT());
        if(hasName)
            compound.putString("name", name.getString());
        return compound;
    }

    @Override
    public CompoundNBT getUpdateTag() {
        // getUpdateTag() is called whenever the chunkdata is sent to the
        // client. In contrast getUpdatePacket() is called when the tile entity
        // itself wants to sync to the client. In many cases you want to send
        // over the same information in getUpdateTag() as in getUpdatePacket().
        CompoundNBT compound = new CompoundNBT();
        super.write(compound);
        compound.putBoolean("tiny", tiny);
        if(tiny)
            compound.put("item", itemStackHandler.serializeNBT());
        return compound;
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        super.write(nbtTag);
        nbtTag.putBoolean("tiny", tiny);
        nbtTag.put("item", itemStackHandler.serializeNBT());
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile entity
        CompoundNBT nbt = packet.getNbtCompound();
//        playersLooking = nbt.getInt("count");
        tiny = nbt.getBoolean("tiny");
        if(!tiny)
            itemStackHandler = new TinyStackHandler(108);
    }

    @Override
    public boolean isEmpty() {
        for(int i= 0;i<itemStackHandler.getSlots();i++)
            if (!itemStackHandler.getStackInSlot(i).isEmpty())
                return false;

        return true;
    }

    @Override
    public ItemStack getStackInSlot(int index){
        return itemStackHandler.getStackInSlot(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count){
        return itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public ItemStack removeStackFromSlot(int index){
        return itemStackHandler.extractItem(index, this.itemStackHandler.getStackInSlot(index).getCount(), false);
    }

    @Override
    public void setInventorySlotContents(int index, ItemStack stack){
        itemStackHandler.setStackInSlot(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player){
        return true;
    }

    @Override
    public void clear(){

    }

    @Override
    public void openInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            if (playersLooking < 0) {
                playersLooking = 0;
            }

            playersList.add(player);
            playersLooking++;
            calculatePlayers();
        }
    }

    @Override
    public void closeInventory(PlayerEntity player) {
        if (!player.isSpectator()) {
            playersList.remove(player);
            playersLooking--;
            calculatePlayers();
        }
    }

    @Override
    public Container createMenu(int p_createMenu_1_, PlayerInventory p_createMenu_2_, PlayerEntity p_createMenu_3_){
        if(!tiny)
            return new ContainerTinyChest(p_createMenu_1_, p_createMenu_2_, this);
        return new ContainerTinyTinyChest(p_createMenu_1_, p_createMenu_2_, this);
    }

    public void setCustomName(ITextComponent name){
        this.hasName = true;
        this.name = name;
    }

    @Override
    public ITextComponent getDisplayName(){
        return name;
    }

    public ItemStack gettinyStack(){
        return itemStackHandler.getStackInSlot(0);
    }

    private class TinyStackHandler extends ItemStackHandler{

        private TinyStackHandler(int sixe){
            super(sixe);
        }

        @Override
        protected void onContentsChanged(int slot){
            if(slot == 0 && tiny)
                if(world instanceof ServerWorld){
                    SUpdateTileEntityPacket packet = getUpdatePacket();

                    if(packet != null)
                        ((ServerWorld)world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                                forEach(player -> player.connection.sendPacket(packet));
                }
            markDirty();
        }
    }
}
