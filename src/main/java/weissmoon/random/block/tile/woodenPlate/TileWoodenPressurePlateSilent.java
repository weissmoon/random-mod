package weissmoon.random.block.tile.woodenPlate;

import com.google.common.collect.ImmutableSet;
import net.minecraft.tileentity.TileEntityType;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 1/8/22.
 */
public class TileWoodenPressurePlateSilent extends TileWoodenPressurePlateBase{

    public static final TileEntityType<TileWoodenPressurePlateSilent> TYPE = new TileEntityType<>(TileWoodenPressurePlateSilent::new, ImmutableSet.of(ModBlocks.woodenPressurePlateS), null);

    public TileWoodenPressurePlateSilent(){
        super(TYPE);
    }

}
