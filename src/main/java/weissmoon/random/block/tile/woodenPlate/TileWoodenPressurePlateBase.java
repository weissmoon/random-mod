package weissmoon.random.block.tile.woodenPlate;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.server.ServerWorld;

import javax.annotation.Nonnull;
import java.util.HashMap;

/**
 * Created by Weissmoon on 1/3/22.
 */
public class TileWoodenPressurePlateBase extends TileEntity{


    private static final HashMap<Block, BlockState[]> blockState = new HashMap<>();
    private Block plateBase;
    private static final Block blockDefault = Blocks.OAK_PRESSURE_PLATE;

    static{
        BlockState unpowered = blockDefault.getDefaultState().with(BlockStateProperties.POWERED, false);
        BlockState powered = unpowered.with(BlockStateProperties.POWERED, true);

        BlockState[] blockStates = new BlockState[]{unpowered, powered};
        blockState.put(blockDefault, blockStates);
    }

    public TileWoodenPressurePlateBase(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    public void setBlockstate(Block block){
        try{
            if(blockState.get(block) == null){
                BlockState unpowered = block.getDefaultState().with(BlockStateProperties.POWERED, false);
                BlockState powered = unpowered.with(BlockStateProperties.POWERED, true);

                BlockState[] blockStates = new BlockState[]{unpowered, powered};
                blockState.put(block, blockStates);
            }
            this.plateBase = block;

        }catch (Error e){

            blockState.put(block, new BlockState[]{Blocks.OAK_PRESSURE_PLATE.getDefaultState(), Blocks.OAK_PRESSURE_PLATE.getDefaultState().with(BlockStateProperties.POWERED, true)});
        }
    }

    public BlockState getStateforRender(boolean powered){
        BlockState[] def = blockState.get(blockDefault);
        return powered ? blockState.getOrDefault(plateBase, def)[1] : blockState.getOrDefault(plateBase, def)[0];
    }



    @Override
    public CompoundNBT write(CompoundNBT compound) {
        super.write(compound);
        CompoundNBT compoundNBT = new CompoundNBT();
        new ItemStack(plateBase).write(compoundNBT);
        compound.put("plate", compoundNBT);
        return compound;
    }

    @Override
    public void read(BlockState state, CompoundNBT nbt) {
        super.read(state, nbt);
        if(nbt.contains("plate")){
            plateBase = ((BlockItem)ItemStack.read(nbt.getCompound("plate")).getItem()).getBlock();
            setBlockstate(plateBase);
        }
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        // Prepare a packet for syncing our TE to the client. Since we only have to sync the stack
        // and that's all we have we just write our entire NBT here. If you have a complex
        // tile entity that doesn't need to have all information on the clieent you can write
        // a more optimal NBT here.
        CompoundNBT nbtTag = new CompoundNBT();
        super.write(nbtTag);
        CompoundNBT compoundNBT = new CompoundNBT();
        new ItemStack(plateBase).write(compoundNBT);
        nbtTag.put("plate", compoundNBT);
        return new SUpdateTileEntityPacket(getPos(), 1, nbtTag);
    }

    @Override
    public CompoundNBT getUpdateTag() {
        return write(new CompoundNBT());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket packet) {
        // Here we get the packet from the server and read it into our client side tile entity
        CompoundNBT nbt = packet.getNbtCompound();
//        playersLooking = nbt.getInt("count");
        if(nbt.contains("plate")){
            this.plateBase = ((BlockItem)ItemStack.read(nbt.getCompound("plate")).getItem()).getBlock();
            setBlockstate(plateBase);
        }
    }

    @Nonnull
    public ItemStack getPlateBlockStack(){
        return new ItemStack(plateBase);
    }

    public void updatePlate(){
        if(world instanceof ServerWorld){
            SUpdateTileEntityPacket packet = getUpdatePacket();

            if(packet != null)
                ((ServerWorld)world).getChunkProvider().chunkManager.getTrackingPlayers(new ChunkPos(getPos()), false).
                        forEach(player -> player.connection.sendPacket(packet));
        }
        markDirty();
    }
}
