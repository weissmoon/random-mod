package weissmoon.random.block.tile.woodenPlate;

import com.google.common.collect.ImmutableSet;
import net.minecraft.tileentity.TileEntityType;
import weissmoon.random.block.ModBlocks;

/**
 * Created by Weissmoon on 1/8/22.
 */
public class TileWoodenPressurePlateInvisible extends TileWoodenPressurePlateBase{

    public static final TileEntityType<TileWoodenPressurePlateInvisible> TYPE = new TileEntityType<>(TileWoodenPressurePlateInvisible::new, ImmutableSet.of(ModBlocks.woodenPressurePlateSI), null);

    public TileWoodenPressurePlateInvisible(){
        super(TYPE);
    }
}
