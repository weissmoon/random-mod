package weissmoon.random.block.tile;

import com.google.common.collect.ImmutableSet;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.*;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.items.ItemStackHandler;
import weissmoon.core.helper.InventoryHelper;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.item.ModItems;
import weissmoon.random.recipe.RecipeBarrel;
import weissmoon.random.recipe.RecipeTypes;

import java.util.Optional;

import static net.minecraft.util.SoundEvents.ENTITY_BOAT_PADDLE_WATER;

/**
 * Created by Weissmoon on 4/19/21.
 */
public class TileBarrel extends TileEntity implements ITickableTileEntity, ISidedInventory{

    private ItemStackHandler itemStackHandler = new ItemStackHandler(3);
    //private ItemStack output = ItemStack.EMPTY;
    private RecipeBarrel recipe;
    private byte lidPosition = 0;
    private int craftingTimer = 0;

    public static final TileEntityType<TileBarrel> TYPE = new TileEntityType<>(TileBarrel::new, ImmutableSet.of(ModBlocks.barrelBlock), null);

    public TileBarrel(){
        super(TYPE);
    }

    @Override
    public void tick(){
        if(recipe != null){
            if(lidPosition == 2){
                if(craftingTimer > 0){
                    craftingTimer--;
                    markDirty();
                }
                if(craftingTimer <= 0)
                    craftRecipe();
            }
        }
    }

    public ItemStack getTopStack(){
        if(!itemStackHandler.getStackInSlot(2).isEmpty()){
            ItemStack returning = itemStackHandler.getStackInSlot(2).copy();
            itemStackHandler.setStackInSlot(2, ItemStack.EMPTY);
            return returning;
        }
        for(int i = 1;i>0;i--){
            ItemStack itemstack = this.itemStackHandler.getStackInSlot(i);

            if(!itemstack.isEmpty()){
                return itemstack;
            }
        }
        return ItemStack.EMPTY;
    }

    public ItemStack removeTopStack(){
        if(!itemStackHandler.getStackInSlot(2).isEmpty()){
            ItemStack returning = itemStackHandler.getStackInSlot(2).copy();
            itemStackHandler.setStackInSlot(2, ItemStack.EMPTY);
            return returning;
        }
        for(int i = 1;i>-1;i--){
            ItemStack itemstack = this.itemStackHandler.getStackInSlot(i);

            if(!itemstack.isEmpty()){
                return removeStackFromSlot(i);
            }
        }
        return ItemStack.EMPTY;
    }

    private ResourceLocation water = new ResourceLocation("minecraft", "water");

    public ActionResultType handleRightClick(PlayerEntity player, Hand hand){
        ItemStack item = player.getHeldItem(hand);
        //int lidPosition = world.getBlockState(pos).get(BlockBarrel.LID_POSITION);
        if(player.isSneaking()){
            if(lidPosition == 0){
                if(item.isEmpty()){
                    player.setHeldItem(hand, removeTopStack());
                    markDirty();
                    return ActionResultType.SUCCESS;
                }
            }else if(lidPosition == 1){
//                world.setBlockState(pos, world.getBlockState(pos).with(BlockBarrel.LID_POSITION, 2));
                lidPosition = 2;
                craftingTimer = recipe.time;
                markDirty();
                return ActionResultType.SUCCESS;
            }else{
                return ActionResultType.PASS;
            }
        }else{
            if(lidPosition == 0){
                if(!itemStackHandler.getStackInSlot(2).isEmpty()){
                    return ActionResultType.FAIL;
                }
                if (item.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).isPresent()){
                    IFluidHandlerItem fluidHandler = item.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).resolve().get();
                    for(int tankID = 0; fluidHandler.getTanks() > tankID; tankID++){
                        FluidStack fluid = fluidHandler.getFluidInTank(tankID);
                        if(!fluid.isEmpty() && fluid.getFluid().getRegistryName().equals(water)){
                            while(fluid.getAmount() >= 1000){
                                fluid.setAmount(fluid.getAmount() - 1000);
                                player.world.playSound(player, pos, ENTITY_BOAT_PADDLE_WATER, SoundCategory.BLOCKS, 0.3F, 1);
                                InventoryHelper.givePlayerOrDropItemStack(new ItemStack(ModItems.salt), player);
                            }
                            return ActionResultType.SUCCESS;
                        }
                    }
                }
                if(itemStackHandler.getStackInSlot(0).isEmpty()){
                    setInventorySlotContents(0, item);
                    player.setHeldItem(hand, ItemStack.EMPTY);
                    checkForRecipe();
                    markDirty();
                    return ActionResultType.SUCCESS;
                }
                if(itemStackHandler.getStackInSlot(1).isEmpty()){
                    setInventorySlotContents(1, item);
                    player.setHeldItem(hand, ItemStack.EMPTY);
                    checkForRecipe();
                    markDirty();
                    return ActionResultType.SUCCESS;
                }
                if(hasRecipe()){
//                    world.setBlockState(pos, world.getBlockState(pos).with(BlockBarrel.LID_POSITION, 1));
                    lidPosition = 1;
                    markDirty();
                    return ActionResultType.SUCCESS;
                }
            }else if(lidPosition == 1){
//                world.setBlockState(pos, world.getBlockState(pos).with(BlockBarrel.LID_POSITION, 0));
                lidPosition = 0;
                return ActionResultType.SUCCESS;
            }else if(lidPosition == 2){
                if(!itemStackHandler.getStackInSlot(2).isEmpty() || recipe == null){
//                    world.setBlockState(pos, world.getBlockState(pos).with(BlockBarrel.LID_POSITION, 0));
                    lidPosition = 0;
                    return ActionResultType.SUCCESS;
                }
                return ActionResultType.PASS;
            }
        }
        return ActionResultType.PASS;
    }

    private void checkForRecipe(){
        Optional<RecipeBarrel> maybeRecipe = world.getRecipeManager().getRecipe(RecipeTypes.BARREL_TYPE, this, this.world);
        maybeRecipe.ifPresent((recipeBarrel) -> {
            recipe = recipeBarrel;
        });
    }

    private void craftRecipe(){
        try{
            boolean reverse = recipe.ingredient1.isItemEqual(getStackInSlot(0));
            while (recipe.matches(this, world)){
                if(!reverse){
                    decrStackSize(1, recipe.ingredient1.getCount());
                    if(!recipe.isCatalyst)
                        decrStackSize(0, recipe.ingredient2.getCount());
                }else{
                    decrStackSize(0, recipe.ingredient1.getCount());
                    if(!recipe.isCatalyst)
                        decrStackSize(1, recipe.ingredient2.getCount());
                }
                if(itemStackHandler.getStackInSlot(2).isEmpty()){
                    itemStackHandler.setStackInSlot(2, recipe.output.copy());
                    continue;
                }
                itemStackHandler.getStackInSlot(2).grow(recipe.output.getCount());
                if(itemStackHandler.getStackInSlot(2).getMaxStackSize() < itemStackHandler.getStackInSlot(2).getCount() + recipe.output.getCount())
                    break;
            }
            recipe = null;
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private  boolean hasRecipe(){
        return recipe != null;
    }

    private boolean isEitherEmpty(){
        for(int i = 0;i<2;i++){
            ItemStack itemstack = this.itemStackHandler.getStackInSlot(i);
            if(itemstack.isEmpty()){
                return true;
            }
        }
        return false;
    }

    public int getTimer(){
        return craftingTimer;
    }

    @Override
    public boolean isEmpty(){
        for(int i = 0;i<2;i++){
            ItemStack itemstack = this.itemStackHandler.getStackInSlot(i);

            if(!itemstack.isEmpty()){
                return false;
            }
        }

        return true;
    }

    @Override
    public void read(BlockState state, CompoundNBT compound){
        super.read(state, compound);
        if(compound.contains("items")){
            itemStackHandler.deserializeNBT(compound.getCompound("items"));
        }
        if(compound.contains("output")){
            itemStackHandler.setStackInSlot(2, ItemStack.read(compound.getCompound("output")));
        }
        if(compound.contains("recipe")){
            Optional<RecipeBarrel> maybeRecipe = (Optional<RecipeBarrel>)this.world.getRecipeManager().getRecipe(new ResourceLocation(compound.getString("recipe")));
            maybeRecipe.ifPresent((recipeBarrel) -> recipe = recipeBarrel);
            if(recipe == null)
                return;
        }
        if(compound.contains("timer")){
            craftingTimer = compound.getInt("timer");
        }
        if(compound.contains("lid")){
            lidPosition = compound.getByte("lid");
        }
    }

    @Override
    public CompoundNBT write(CompoundNBT compound){
        super.write(compound);
        compound.put("items", itemStackHandler.serializeNBT());
        if(recipe != null){
            compound.putString("recipe", recipe.getId().toString());
        }
        compound.putInt("timer", craftingTimer);
        compound.putByte("lid", lidPosition);
        return compound;
    }


    public byte getLidPosition(){
        return lidPosition;
    }

    @Override
    public void onLoad(){
        super.onLoad();
//        if(recipe == null){
//            lidPosition = 0;
//        }
    }

    @Override
    public int getSizeInventory(){
        return 2;
    }

    @Override
    public ItemStack getStackInSlot(int index){
        if(index>1)
            return ItemStack.EMPTY;
        return this.itemStackHandler.getStackInSlot(index);
    }

    @Override
    public ItemStack decrStackSize(int index, int count){
        return this.itemStackHandler.extractItem(index, count, false);
    }

    @Override
    public ItemStack removeStackFromSlot(int index){
        return this.itemStackHandler.extractItem(index, this.itemStackHandler.getStackInSlot(index).getCount(), false);
    }


    @Override
    public void setInventorySlotContents(int index, ItemStack stack){
        this.itemStackHandler.setStackInSlot(index, stack);
    }

    @Override
    public boolean isUsableByPlayer(PlayerEntity player){
        return true;
    }

    @Override
    public void clear(){

    }

    private static final int[] SLOTS = new int[]{2, 1, 0};

    @Override
    public int[] getSlotsForFace(Direction direction){
        return SLOTS;
    }

    @Override
    public boolean canInsertItem(int i, ItemStack itemStack, Direction direction){
        return i != 3;
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemStack, Direction direction){
        if(lidPosition == 2)
            return false;
        return i == 2;
    }
}
