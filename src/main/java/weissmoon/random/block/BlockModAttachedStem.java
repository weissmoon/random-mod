package weissmoon.random.block;

import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissBlockRegistry;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 11/12/19.
 */
public class BlockModAttachedStem extends AttachedStemBlock implements IBlockWeiss{

    private final String ModId;
    protected final String RegName;
    private Item seedItem;

    public BlockModAttachedStem(StemGrownBlock crop, String name) {
        super(crop, Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.STEM));
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    public void setSeedItem(Item item){
        this.seedItem = item;
    }

    @Nullable
    protected Item getSeedItem()
    {
        return this.seedItem;
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }
}
