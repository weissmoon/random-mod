package weissmoon.random.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.block.BlockRenderType;
import net.minecraft.util.NonNullList;
import net.minecraft.world.IBlockReader;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.block.WeissBlock;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.random.Randommod;

import java.util.Collection;

/**
 * Created by Weissmoon on 4/11/19.
 */
public class BlockSeaLantern extends WeissBlock implements IBlockWeiss, ITileEntityProvider {


    public BlockSeaLantern(String name, MaterialColor color) {
        super(name, Properties.create(Material.GLASS, color));
//        this.ModId = Loader.instance().activeModContainer().getModId();
//        this.RegName = Strings.Blocks.COLOURED_SEA_LANTERN;
//        this.setUnlocalizedName(this.ModId.toLowerCase() + ":" + this.RegName);
//        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
//        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
//        setHardness(0.3F);
//        setSoundType(SoundType.GLASS);
//        setLightLevel(1.0F);
        //this.setDefaultState(this.getDefaultState().with(COLOR, EnumDyeColor.WHITE));
        //this.setCreativeTab(CreativeTabs.DECORATIONS);
    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return Randommod.proxy.getLanternTile(worldIn);
    }

//    @Override
//    public void onBlockAdded(World worldIn, BlockPos pos, IBlockState state) {
//        if(worldIn.isRemote)
//            worldIn.addTileEntity(Randommod.proxy.getLanternTile(worldIn, state.getValue(COLOR).getMetadata()));
//    }


//    @Override
//    public int getLightValue(BlockState state)
//    {
//        return this.lightValue;
//    }

    @Override
    public BlockRenderType getRenderType(BlockState state)
    {
            return BlockRenderType.ENTITYBLOCK_ANIMATED;
    }

    public void getSubBlocks(ItemGroup itemIn, NonNullList<ItemStack> items) {
        //items.add(new ItemStack(this));
    }

    @Override
    public Collection<ItemGroup> getCreativeTabs()
    {
        return null;
    }
}
