package weissmoon.random.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import weissmoon.random.lib.Strings;


/**
 * Created by Weissmoon on 11/10/19.
 */
public class BlockMelonAir extends WeissStemGrownBlock {
    public BlockMelonAir() {
        super(Strings.Blocks.MELON_AIR_NAME, Properties.create(Material.GOURD, MaterialColor.CYAN).hardnessAndResistance(1.0F).sound(SoundType.WOOD));
    }

}
