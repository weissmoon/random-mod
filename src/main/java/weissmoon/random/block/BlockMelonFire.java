package weissmoon.random.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 11/10/19.
 */
public class BlockMelonFire extends WeissStemGrownBlock{
    public BlockMelonFire() {
        super(Strings.Blocks.MELON_FIRE_NAME, Properties.create(Material.GOURD, MaterialColor.ADOBE).hardnessAndResistance(1.0F).sound(SoundType.WOOD));
    }
}
