package weissmoon.random.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 5/25/19.
 */
public class BlockCharcoal extends WeissBlock {

    public BlockCharcoal() {
        super(Strings.Blocks.CHARCOAL_BLOCK_NAME, Properties.create(Material.ROCK, MaterialColor.BROWN).hardnessAndResistance(5.0F, 6.0F).sound(SoundType.WOOD));
        //setCreativeTab(CreativeTabs.BUILDING_BLOCKS);
    }
}
