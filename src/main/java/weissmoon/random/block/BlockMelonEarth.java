package weissmoon.random.block;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 11/10/19.
 */
public class BlockMelonEarth extends WeissStemGrownBlock {
    public BlockMelonEarth() {
        super(Strings.Blocks.MELON_EARTH_NAME, Properties.create(Material.GOURD, MaterialColor.BROWN).hardnessAndResistance(1.0F).sound(SoundType.WOOD));
    }
}
