package weissmoon.random.block.rotation;

/**
 * Created by Weissmoon on 6/3/19.
 */
public interface IRotationProducer{

    float getSpeed();

    float getTorque();
}
