package weissmoon.random.block.rotation;

import net.minecraft.block.Block;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.block.material.Material;
import net.minecraft.state.DirectionProperty;
import net.minecraft.block.BlockState;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.block.WeissBlockContainer;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 6/3/19.
 */
public class BlockRedstoneEngine extends WeissBlockContainer {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public BlockRedstoneEngine() {
        super(Strings.Blocks.REDSTONE_ENGINE_NAME, Properties.create(Material.IRON));
        this.setDefaultState(getDefaultState().with(FACING, Direction.NORTH));
        //this.setHardness(5.0F);
        //this.setCreativeTab(CreativeTabs.REDSTONE);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        Direction direction = context.getPlacementHorizontalFacing();
        return this.getDefaultState().with(FACING, direction);
    }

//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer(){
//        return BlockRenderLayer.CUTOUT;
//    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
//        return new TileRedstoneEngine();
        return null;
    }
}
