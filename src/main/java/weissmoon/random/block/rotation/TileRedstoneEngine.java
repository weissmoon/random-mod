package weissmoon.random.block.rotation;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;

/**
 * Created by Weissmoon on 6/3/19.
 */
public class TileRedstoneEngine extends TileEntity implements IRotationProducer{


    public TileRedstoneEngine(TileEntityType<?> tileEntityTypeIn) {
        super(tileEntityTypeIn);
    }

    @Override
    public float getSpeed() {
        return 0;
    }

    @Override
    public float getTorque() {
        return 0;
    }
}
