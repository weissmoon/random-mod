package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.block.material.Material;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.BlockItemUseContext;
import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;
import weissmoon.core.block.WeissBlockContainer;
import weissmoon.random.Randommod;
import weissmoon.random.block.tile.TileDrawbrigde;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 4/5/19.
 */
public class BlockDrawbrigde extends WeissBlockContainer {

    public static final DirectionProperty FACING = BlockStateProperties.FACING;

    public BlockDrawbrigde() {
        super(Strings.Blocks.DRAWBRIGDE_NAME, Properties.create(Material.IRON).hardnessAndResistance(5.0F, 6.0F).sound(SoundType.METAL).notSolid());
        this.setDefaultState(this.getDefaultState().with(FACING, Direction.NORTH));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(FACING);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getNearestLookingDirection().getOpposite());
    }

//    @Override
//    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack){
//        worldIn.setBlockState(pos, state.withProperty(FACING, EnumFacing.getDirectionFromEntityLiving(pos, placer)), 2);
//    }

    @Override
    public void onReplaced(BlockState state, World worldIn, BlockPos pos, BlockState newState, boolean moving)
    {
            TileEntity tileentity = worldIn.getTileEntity(pos);

            if (tileentity instanceof TileDrawbrigde)
            {
                InventoryHelper.dropInventoryItems(worldIn, pos, (TileDrawbrigde)tileentity);
                worldIn.updateComparatorOutputLevel(pos, this);
            }

        super.onReplaced(state, worldIn, pos, newState, moving);
    }


    @Override
    public ActionResultType onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity playerIn, Hand hand, BlockRayTraceResult result) {
        if (worldIn.isRemote) {
            return ActionResultType.SUCCESS;
        } else {
            TileDrawbrigde ilockablecontainer = (TileDrawbrigde)worldIn.getTileEntity(pos);

            if (ilockablecontainer != null) {
                playerIn.openContainer(ilockablecontainer);
                BlockPos pos1 = ilockablecontainer.getPos();
                ServerPlayerEntity playerIn1 = (ServerPlayerEntity) playerIn;
                NetworkHooks.openGui((ServerPlayerEntity) playerIn, ilockablecontainer, ilockablecontainer.getPos());
            }

            return ActionResultType.SUCCESS;
        }
    }


//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer(){
//        return BlockRenderLayer.CUTOUT;
//    }

    @Override
    public TileEntity createNewTileEntity(IBlockReader worldIn) {
        return new TileDrawbrigde();
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }
}
