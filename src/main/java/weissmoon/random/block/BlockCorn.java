package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.IGrowable;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.block.Blocks;
import net.minecraft.item.ItemStack;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.PlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.block.WeissBlock;
import weissmoon.core.helper.RNGHelper;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;
import java.util.Random;

/**
 * Created by Weissmoon on 2/25/19.
 */
public class BlockCorn extends WeissBlock implements IGrowable, IPlantable{

    //public static final BooleanProperty IS_TOP = BooleanProperty.create("top");

    //copy of BlockCrops
    public static final IntegerProperty AGE = IntegerProperty.create("age", 0, 4);
    private static final VoxelShape[] CROPS_AABB = new VoxelShape[] {
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 0.125D, 1.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 0.375D, 1.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 0.5D, 1.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 0.75D, 1.0D),
            Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D)
    };

    public BlockCorn() {
        super(Strings.Blocks.CORN_NAME, Properties.create(Material.PLANTS, MaterialColor.FOLIAGE).tickRandomly());
        this.setDefaultState(this.getDefaultState().with(AGE, 0));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(AGE);
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean moving){
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos, moving);
        if (!worldIn.isRemote) {
            if (!this.canBlockStay(worldIn, pos, state))
            {
                Block.spawnDrops(state, worldIn, pos);
                worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
            }
        }
    }

    //Copy of BlockBush
    public boolean canBlockStay(World worldIn, BlockPos pos, BlockState state)
    {
        if (state.getBlock() == this) //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
        {
            BlockState soil = worldIn.getBlockState(pos.down());
            return soil.getBlock().canSustainPlant(soil, worldIn, pos.down(), net.minecraft.util.Direction.UP, this);
        }
        BlockState stateb = worldIn.getBlockState(pos.down());
        return stateb.getBlock() == Blocks.GRASS || stateb.getBlock() == Blocks.DIRT || stateb.getBlock() == Blocks.FARMLAND;
    }

    public boolean canSustainPlant(BlockState state, IBlockReader world, BlockPos pos, Direction facing, net.minecraftforge.common.IPlantable plantable) {
       return (plantable instanceof BlockCornTop) && 4 == state.get(AGE);
    }

    @Nullable
    @Override
    public VoxelShape getCollisionShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return VoxelShapes.empty();
    }

    @Override
    public VoxelShape getRenderShape(BlockState state, IBlockReader worldIn, BlockPos pos)
    {
        return CROPS_AABB[state.get(AGE)];
    }

//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer()
//    {
//        return BlockRenderLayer.CUTOUT;
//    }

    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
    {
        super.tick(state, worldIn, pos, rand);


        int i = state.get(AGE);

        if (i < 4)
        {
            float f = getGrowthChance(this, worldIn, pos);

            if(net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, rand.nextInt((int)(25.0F / f) + 1) == 0))
            {
                worldIn.setBlockState(pos, this.getDefaultState().with(AGE, ++i), 2);
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
            }
        }else{

            float f = getGrowthChance(this, worldIn, pos);

            if(net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, rand.nextInt((int)(25.0F / f) + 1) == 0))
            {
//                worldIn.setBlockState(pos.add(0, 1, 0), this.getDefaultState().with(IS_TOP, true), 2);
                worldIn.setBlockState(pos.add(0, 1, 0), ModBlocks.cornBlockTop.getDefaultState(), 2);
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
            }
        }
    }

    @Override
    public boolean canGrow(IBlockReader worldIn, BlockPos pos, BlockState state, boolean isClient) {
        return worldIn.getBlockState(pos.add(0, 1, 0)).isAir(worldIn, pos.add(0, 1, 0));
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, BlockState state) {
        return worldIn.getBlockState(pos.add(0, 1, 0)).isAir(worldIn, pos.add(0, 1, 0));
    }

    @Override
    public void onBlockHarvested(World worldIn, BlockPos pos, BlockState state, PlayerEntity player)
    {
//        if (state.getValue(IS_TOP)){
//        }
    }

//    public static List<ItemStack> getDrops(BlockState state, ServerWorld worldIn, BlockPos pos, @Nullable TileEntity tileEntityIn, Entity entityIn, ItemStack stack)
//    {
//        Random rand = worldIn != null ? worldIn.rand : new Random();
//        int k = EnchantmentHelper.getEnchantmentLevel(Enchantments.FORTUNE, stack);
//        List<ItemStack> drops = Collections.emptyList();
//        if(state.get(IS_TOP)){
//
//            //int k = 3 + fortune;
//
//            for (int i = 0; i < 2 + k; ++i)
//            {
//                if (rand.nextInt(2 * 7) <= 4)
//                {
//                    drops.add(new ItemStack(ModItems.corn, 1));
//                }
//            }
//            drops.add(new ItemStack(ModItems.corn, 1));
//            return drops;
//        }
//
//        int age = state.get(AGE);
//
//        if (age >= 4)
//        {
//            //int k = 3 + fortune;
//
//            for (int i = 0; i < 2 + k; ++i)
//            {
//                if (rand.nextInt(2 * 4) <= age)
//                {
//                    drops.add(new ItemStack(ModItems.cornSeed, 1));
//                }
//            }
//        }
//        drops.add(new ItemStack(ModItems.cornSeed));
//        return drops;
//    }


    @Override
    public void grow(ServerWorld worldIn, Random rand, BlockPos pos, BlockState state) {
        if (state.get(AGE) == 4){
//            worldIn.setBlockState(pos.add(0, 1, 0), this.getDefaultState().with(IS_TOP, true), 2);
            worldIn.setBlockState(pos.add(0, 1, 0), ModBlocks.cornBlockTop.getDefaultState(), 2);
        }
        int i = state.get(AGE) + RNGHelper.getIntClamp(1,4);
        int j = 4;

        if (i > j)
        {
            i = j;
        }

        worldIn.setBlockState(pos, this.getDefaultState().with(AGE, i), 2);
    }

    //copy of BlockCrops
    public static float getGrowthChance(Block blockIn, World worldIn, BlockPos pos)
    {
        float f = 1.0F;
        BlockPos blockpos = pos.down();

        for (int i = -1; i <= 1; ++i)
        {
            for (int j = -1; j <= 1; ++j)
            {
                float f1 = 0.0F;
                BlockState iblockstate = worldIn.getBlockState(blockpos.add(i, 0, j));

                if (iblockstate.getBlock().canSustainPlant(iblockstate, worldIn, blockpos.add(i, 0, j), net.minecraft.util.Direction.UP, (net.minecraftforge.common.IPlantable)blockIn))
                {
                    f1 = 1.0F;

                    if (iblockstate.getBlock().isFertile(iblockstate, worldIn, blockpos.add(i, 0, j)))
                    {
                        f1 = 3.0F;
                    }
                }

                if (i != 0 || j != 0)
                {
                    f1 /= 4.0F;
                }

                f += f1;
            }
        }

        BlockPos blockpos1 = pos.north();
        BlockPos blockpos2 = pos.south();
        BlockPos blockpos3 = pos.west();
        BlockPos blockpos4 = pos.east();
        boolean flag = blockIn == worldIn.getBlockState(blockpos3).getBlock() || blockIn == worldIn.getBlockState(blockpos4).getBlock();
        boolean flag1 = blockIn == worldIn.getBlockState(blockpos1).getBlock() || blockIn == worldIn.getBlockState(blockpos2).getBlock();

        if (flag && flag1)
        {
            f /= 2.0F;
        }
        else
        {
            boolean flag2 = blockIn == worldIn.getBlockState(blockpos3.north()).getBlock() || blockIn == worldIn.getBlockState(blockpos4.north()).getBlock() || blockIn == worldIn.getBlockState(blockpos4.south()).getBlock() || blockIn == worldIn.getBlockState(blockpos3.south()).getBlock();

            if (flag2)
            {
                f /= 2.0F;
            }
        }

        return f;
    }

    @Override
    public PlantType getPlantType(IBlockReader world, BlockPos pos) {
        return PlantType.CROP;
    }

    @Override
    public BlockState getPlant(IBlockReader world, BlockPos pos) {
        return getDefaultState();
    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        return new ItemStack(ModItems.cornSeed);
    }
}
