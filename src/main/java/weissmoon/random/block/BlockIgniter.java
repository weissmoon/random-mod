package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.FireBlock;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.block.NetherPortalBlock;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.block.material.Material;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItemUseContext;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.Direction;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.TickPriority;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.lib.Strings;

import java.util.Random;

/**
 * Created by Weissmoon on 2/1/19.
 */
public class BlockIgniter extends WeissBlock {

    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;
    public static final BooleanProperty ISON = BooleanProperty.create("active");


    public BlockIgniter() {
        super(Strings.Blocks.IGNITER_NAME, Properties.create(Material.ROCK, MaterialColor.OBSIDIAN).hardnessAndResistance(5.0F, 600F));
        this.setDefaultState(this.getDefaultState().with(FACING, Direction.NORTH).with(ISON, false));
        //this.setHardness(5.0F);
        //this.setCreativeTab(CreativeTabs.REDSTONE);
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder)
    {
        builder.add(FACING).add(ISON);
    }

    @Override
    public BlockState getStateForPlacement(BlockItemUseContext context) {
        return this.getDefaultState().with(FACING, context.getPlacementHorizontalFacing().getOpposite());
    }

//    @Override
//    public void onBlockPlacedBy(World worldIn, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack){
//        worldIn.setBlockState(pos, state.withProperty(FACING, EnumFacing.getHorizontal(EnumFacing.getDirectionFromEntityLiving(pos, placer).getHorizontalIndex())), 2);
//    }

    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random rand)
    {
        super.tick(state, worldIn, pos, rand);
        if (worldIn.isAirBlock(pos.offset(state.get(FACING)))) {
            worldIn.setBlockState(pos.offset(state.get(FACING)), Blocks.FIRE.getDefaultState(), 11);
        }
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean moving){
        if (!worldIn.isRemote) {
            boolean flag = worldIn.isBlockPowered(pos) || worldIn.isBlockPowered(pos.up());
            boolean flag1 = state.get(ISON);

            if(flag){
                if(worldIn.getBlockState(pos.offset(state.get(FACING))).getMaterial().isReplaceable()){
//                    worldIn.setBlockState(pos.offset(state.getValue(FACING)), Blocks.FIRE.getDefaultState(), 11);
                    worldIn.getPendingBlockTicks().scheduleTick(pos, this, 1, TickPriority.HIGH);
                }
                worldIn.setBlockState(pos, state.with(ISON, true));
            }else if(flag1){
                if(worldIn.getBlockState(pos.offset(state.get(FACING))).getBlock() instanceof NetherPortalBlock){
                    worldIn.removeBlock(pos.offset(state.get(FACING)), false);
                }
                if(worldIn.getBlockState(pos.offset(state.get(FACING))).getBlock() instanceof FireBlock){
                    worldIn.removeBlock(pos.offset(state.get(FACING)), false);
                }
                worldIn.setBlockState(pos, state.with(ISON, false));
            }
        }
    }

    @Override
    public BlockState rotate(BlockState state, Rotation rot) {
        return state.with(FACING, rot.rotate(state.get(FACING)));
    }


//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer()
//    {
//        return BlockRenderLayer.CUTOUT;
//    }
}
