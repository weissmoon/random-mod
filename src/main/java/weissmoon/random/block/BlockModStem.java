package weissmoon.random.block;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import net.minecraft.block.*;
import net.minecraft.block.material.Material;
import net.minecraft.state.DirectionProperty;
import net.minecraft.state.IntegerProperty;
import net.minecraft.state.Property;
import net.minecraft.item.Item;
import net.minecraft.state.StateContainer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.PlantType;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissBlockRegistry;

import javax.annotation.Nullable;
import java.util.Map;
import java.util.Random;

/**
 * Created by Weissmoon on 11/12/19.
 */
public class BlockModStem extends BushBlock implements IBlockWeiss, IGrowable{

    private final String ModId;
    protected final String RegName;

    private Item seedItem;
    public static final IntegerProperty AGE = BlockStateProperties.AGE_0_7;
    public static final DirectionProperty FACING = BlockStateProperties.FACING_EXCEPT_UP;
    protected static final VoxelShape[] SHAPES = new VoxelShape[]{Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 2.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 4.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 6.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 8.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 10.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 12.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 14.0D, 9.0D), Block.makeCuboidShape(7.0D, 0.0D, 7.0D, 9.0D, 16.0D, 9.0D)};
    private static final Map<Direction, VoxelShape> ATTACHED_SHAPES = Maps.newEnumMap(ImmutableMap.of(Direction.SOUTH, Block.makeCuboidShape(6.0D, 0.0D, 6.0D, 10.0D, 10.0D, 16.0D), Direction.WEST, Block.makeCuboidShape(0.0D, 0.0D, 6.0D, 10.0D, 10.0D, 10.0D), Direction.NORTH, Block.makeCuboidShape(6.0D, 0.0D, 0.0D, 10.0D, 10.0D, 10.0D), Direction.EAST, Block.makeCuboidShape(6.0D, 0.0D, 6.0D, 16.0D, 10.0D, 10.0D)));
    private final WeissStemGrownBlock crop;

    public BlockModStem(WeissStemGrownBlock crop, String name) {
        super(Block.Properties.create(Material.PLANTS).doesNotBlockMovement().tickRandomly().hardnessAndResistance(0.0F).sound(SoundType.STEM));
        this.crop = crop;
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        this.setDefaultState(this.getDefaultState().with(AGE, 0).with(FACING, Direction.DOWN));
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        VoxelShape shape = (state.get(FACING) == Direction.DOWN) ? SHAPES[state.get(AGE)]: ATTACHED_SHAPES.get(state.get(FACING));
        return shape != null ? shape : SHAPES[1];
    }

    @Override
    protected boolean isValidGround(BlockState state, IBlockReader worldIn, BlockPos pos) {
        return state.getBlock() == Blocks.FARMLAND;
    }

    @Override
    public BlockState updatePostPlacement(BlockState stateIn, Direction facing, BlockState facingState, IWorld worldIn, BlockPos currentPos, BlockPos facingPos) {
        if(isValidGround(worldIn.getBlockState(currentPos.down()), worldIn, currentPos.down())){
            if(facing == Direction.DOWN)
                return stateIn;
            return facingState.getBlock() != this.crop && facing == stateIn.get(FACING) ? this.getDefaultState().with(StemBlock.AGE, 7):super.updatePostPlacement(stateIn, facing, facingState, worldIn, currentPos, facingPos);
        }else
            return Blocks.AIR.getDefaultState();
    }

    @Override
    public void tick(BlockState state, ServerWorld worldIn, BlockPos pos, Random random) {
        super.tick(state, worldIn, pos, random);
        if (!worldIn.isAreaLoaded(pos, 1)) return; // Forge: prevent loading unloaded chunks when checking neighbor's light
        if (worldIn.getLightSubtracted(pos, 0) >= 9) {
            //float f = CropsBlock.getGrowthChance(this, worldIn, pos);
            float f = 1;
            if (net.minecraftforge.common.ForgeHooks.onCropsGrowPre(worldIn, pos, state, random.nextInt((int)(25.0F / f) + 1) == 0)) {
                int i = state.get(AGE);
                if (i < 7) {
                    worldIn.setBlockState(pos, state.with(AGE, i + 1), 2);
                } else {
                    Direction direction = Direction.Plane.HORIZONTAL.random(random);
                    BlockPos blockpos = pos.offset(direction);
                    BlockState soil = worldIn.getBlockState(blockpos.down());
                    Block block = soil.getBlock();
                    if (worldIn.getBlockState(blockpos).isAir(worldIn, blockpos) && (soil.canSustainPlant(worldIn, blockpos.down(), Direction.DOWN, this) || block == Blocks.FARMLAND || block == Blocks.DIRT || block == Blocks.COARSE_DIRT || block == Blocks.PODZOL || block == Blocks.GRASS_BLOCK)) {
                        worldIn.setBlockState(blockpos, this.crop.getDefaultState());
                        worldIn.setBlockState(pos, state.with(FACING, direction));
                    }
                }
                net.minecraftforge.common.ForgeHooks.onCropsGrowPost(worldIn, pos, state);
            }

        }
    }

    public void setSeedItem(Item item){
        this.seedItem = item;
    }

    @Nullable
    protected Item getSeedItem()
    {
        return this.seedItem;
    }

    @Override
    public boolean canGrow(IBlockReader worldIn, BlockPos pos, BlockState state, boolean isClient) {
        return state.get(AGE) != 7;
    }

    @Override
    public boolean canUseBonemeal(World worldIn, Random rand, BlockPos pos, BlockState state) {
        return true;
    }

    @Override
    public void grow(ServerWorld worldIn, Random rand, BlockPos pos, BlockState state) {
        int i = Math.min(7, state.get(AGE) + MathHelper.nextInt(worldIn.rand, 2, 5));
        BlockState blockstate = state.with(AGE, i);
        worldIn.setBlockState(pos, blockstate, 2);
        if (i == 7) {
            blockstate.tick(worldIn, pos, worldIn.rand);
        }

    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(AGE).add(FACING);
    }

    public WeissStemGrownBlock getCrop() {
        return this.crop;
    }

    @Override
    public final String getModID() {
        return this.ModId;
    }

    @Override
    public final String getWeissName(){
        return this.RegName;
    }

    //FORGE START
    @Override
    public net.minecraftforge.common.PlantType getPlantType(IBlockReader world, BlockPos pos) {
        return PlantType.CROP;
    }
}
