package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.client.render.IIconRegister;
import weissmoon.core.helper.WeissBlockRegistry;

/**
 * Created by Weissmoon on 12/7/19.
 */
public class WeissStemGrownBlock extends Block implements IBlockWeiss {

    private final String ModId;
    protected final String RegName;
    private BlockModStem blockModStem;

    public WeissStemGrownBlock(String name, Block.Properties properties) {
        super(properties);
        this.ModId = ModLoadingContext.get().getActiveContainer().getModId();
        this.RegName = name;
        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    public BlockModStem getStem() {
        return this.blockModStem;
    }


    public BlockModStem setStem(BlockModStem block) {
        return this.blockModStem = block;
    }

    @Override
    public String getModID() {
        return ModId;
    }

    @Override
    public String getWeissName() {
        return RegName;
    }
}
