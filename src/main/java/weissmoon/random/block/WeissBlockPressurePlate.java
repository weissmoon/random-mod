//package weissmoon.random.block;
//
//import net.minecraft.block.BlockBasePressurePlate;
//import net.minecraft.block.material.MapColor;
//import net.minecraft.block.material.Material;
//import net.minecraft.block.properties.IProperty;
//import net.minecraft.block.properties.PropertyBool;
//import net.minecraft.block.state.BlockStateContainer;
//import net.minecraft.block.state.IBlockState;
//import net.minecraft.client.renderer.block.statemap.IStateMapper;
//import net.minecraft.creativetab.CreativeTabs;
//import net.minecraft.entity.Entity;
//import net.minecraft.util.math.AxisAlignedBB;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.world.World;
//import net.minecraftforge.fml.common.Loader;
//import weissmoon.core.block.IBlockWeiss;
//import weissmoon.core.client.render.IIconRegister;
//import weissmoon.core.helper.WeissBlockRegistry;
//
//import java.util.List;
//
///**
// * Created by Weissmoon on 5/24/19.
// */
//@Deprecated
//public abstract class WeissBlockPressurePlate extends BlockBasePressurePlate implements IBlockWeiss {
//
//    private final String ModId;
//    protected final String RegName;
//
//    public static final PropertyBool POWERED = PropertyBool.create("powered");
//
//    public WeissBlockPressurePlate(String name, Material blockMaterialIn, MapColor blockMapColorIn) {
//        super(blockMaterialIn, blockMapColorIn);
//        this.ModId = Loader.instance().activeModContainer().getModId();
//        this.RegName = name;
//        this.setUnlocalizedName(this.ModId.toLowerCase() + ":" + this.RegName);
//        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
//        this.setDefaultState(this.blockState.getBaseState().withProperty(POWERED, false));
//        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
//    }
//
//    public WeissBlockPressurePlate(String name, Material materialIn) {
//        super(materialIn);
//        this.ModId = Loader.instance().activeModContainer().getModId();
//        this.RegName = name;
//        this.setUnlocalizedName(this.ModId.toLowerCase() + ":" + this.RegName);
//        this.setRegistryName(this.ModId.toLowerCase() + ":" + this.RegName);
//        this.setCreativeTab(CreativeTabs.DECORATIONS);
//        this.setDefaultState(this.blockState.getBaseState().withProperty(POWERED, false));
//        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
//    }
//
//    @Override
//    public int getMetaFromState(IBlockState state){
//        return 0;
//    }
//
//    @Override
//    public IBlockState getStateFromMeta(int e){
//
//        return this.getDefaultState().withProperty(POWERED, e > 0);
//    }
//
//    @Override
//    protected int computeRedstoneStrength(World worldIn, BlockPos pos) {
//        //AxisAlignedBB axisalignedbb = PRESSURE_AABB.offset(pos);
//
//        List<? extends Entity> list;
//
//        list = this.getEntitylist(worldIn, pos);
//
//        if(list.size() <= 0)
//            return 0;
//
//        if (!list.isEmpty())
//        {
//            for (Entity entity : list)
//            {
//                if (!entity.doesEntityNotTriggerPressurePlate())
//                {
//                    return 15;
//                }
//            }
//        }
//
//        return 0;
//    }
//
//    @Override
//    protected BlockStateContainer createBlockState()
//    {
//        return new BlockStateContainer(this, new IProperty[]{POWERED});
//    }
//
//    @Override
//    protected int getRedstoneStrength(IBlockState state) {
//        return state.getValue(POWERED) ? 15 : 0;
//    }
//
//    @Override
//    protected IBlockState setRedstoneStrength(IBlockState state, int strength) {
//        return state.withProperty(POWERED, strength > 0);
//    }
//
//    @Override
//    public String getModID() {
//        return this.ModId;
//    }
//
//    @Override
//    public String getWeissName() {
//        return this.RegName;
//    }
//
//    @Override
//    public IProperty[] getIgnoredProperties() {
//        return new IProperty[0];
//    }
//
//    @Override
//    public IStateMapper getStateMapper() {
//        return null;
//    }
//
//    @Override
//    public boolean hasItemBlock() {
//        return false;
//    }
//
//    @Override
//    public void registerBlockIcons(IIconRegister iIconRegister) {
//        iIconRegister.registerIcon(this);
//    }
//
//
//    protected abstract List<? extends Entity> getEntitylist(World world, BlockPos pos);
//}
