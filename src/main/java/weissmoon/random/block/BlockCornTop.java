package weissmoon.random.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraft.item.ItemStack;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.common.IPlantable;
import weissmoon.core.block.WeissBlock;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Strings;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 3/22/19.
 */
public class BlockCornTop extends WeissBlock implements IPlantable {

    VoxelShape shape = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 1.0D);

    public BlockCornTop() {
        super(Strings.Blocks.CORN_TOP_NAME, Properties.create(Material.PLANTS, MaterialColor.FOLIAGE));
        this.setDefaultState(this.getDefaultState());
    }

    @Override
    public VoxelShape getRenderShape(BlockState state, IBlockReader worldIn, BlockPos pos)
    {
        return shape;
    }

    @Override
    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean moving){
        super.neighborChanged(state, worldIn, pos, blockIn, fromPos, moving);
        if (!worldIn.isRemote) {
            if (!this.canBlockStay(worldIn, pos, state))
            {
                Block.spawnDrops(state, worldIn, pos);
                worldIn.setBlockState(pos, Blocks.AIR.getDefaultState(), 3);
            }
        }
    }

    public boolean canBlockStay(World worldIn, BlockPos pos, BlockState state)
    {
        if (state.getBlock() == this) //Forge: This function is called during world gen and placement, before this block is set, so if we are not 'here' then assume it's the pre-check.
        {
            BlockState soil = worldIn.getBlockState(pos.down());
            return soil.getBlock() instanceof BlockCorn;
        }
        return false;
    }

    @Nullable
    @Override
    public VoxelShape getCollisionShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context)
    {
        return VoxelShapes.empty();
    }


//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer()
//    {
//        return BlockRenderLayer.CUTOUT;
//    }


    @Override
    public BlockState getPlant(IBlockReader world, BlockPos pos) {
        return getDefaultState();
    }

    @Override
    public ItemStack getItem(IBlockReader worldIn, BlockPos pos, BlockState state) {
        return new ItemStack(ModItems.corn);
    }
}
