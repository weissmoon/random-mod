package weissmoon.random.block.ore;

import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/16/20.
 */
public class BlockIronGravelOre extends BlockFallingOre {
    public BlockIronGravelOre() {
        super(Strings.Blocks.IRON_GRAVEL_ORE);
    }
}
