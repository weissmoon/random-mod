package weissmoon.random.block.ore;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
//import net.minecraft.util.BlockRenderLayer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import weissmoon.random.lib.Strings;

/**
 * Created by Weissmoon on 1/16/20.
 */
public class BlockGoldSandOre extends BlockFallingOre {
    public BlockGoldSandOre() {
        super(Strings.Blocks.GOLD_SAND_ORE, Properties.create(Material.SAND).hardnessAndResistance(0.5F).sound(SoundType.SAND));
    }

//    @OnlyIn(Dist.CLIENT)
//    @Override
//    public BlockRenderLayer getRenderLayer()
//    {
//        return BlockRenderLayer.CUTOUT_MIPPED;
//    }
}
