package weissmoon.random.block.ore;

import net.minecraft.block.FallingBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;
import net.minecraftforge.fml.ModLoadingContext;
import weissmoon.core.block.IBlockWeiss;
import weissmoon.core.helper.WeissBlockRegistry;

/**
 * Created by Weissmoon on 1/16/20.
 */
public class BlockFallingOre extends FallingBlock implements IBlockWeiss {

    private final String ModId;
    protected final String RegName;

    public BlockFallingOre(String name){
        this(name, Properties.create(Material.SAND, MaterialColor.STONE).hardnessAndResistance(0.6F).notSolid().sound(SoundType.GROUND));
    }

    public BlockFallingOre(String name, Properties properties){
        super(properties);
        ModId = ModLoadingContext.get().getActiveContainer().getModId();
        RegName = name;
        setRegistryName(ModId.toLowerCase() + ":" + RegName);
        WeissBlockRegistry.weissBlockRegistry.regBlock(this);
    }

    @Override
    public final String getModID() {
        return ModId;
    }

    @Override
    public final String getWeissName(){
        return RegName;
    }
}
