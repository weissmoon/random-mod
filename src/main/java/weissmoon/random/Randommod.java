package weissmoon.random;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.item.crafting.IRecipeSerializer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.fml.loading.FMLEnvironment;
import weissmoon.random.advancements.EnvyTrigger;
import weissmoon.random.client.ClientHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
//import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.common.*;
import weissmoon.core.WeissCore;
import weissmoon.random.api.WeissRandomAPI;
import weissmoon.random.compat.CuriosCompat;
import weissmoon.random.container.ModContainers;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.effect.ModEffects;
import weissmoon.random.entity.Entities;
import weissmoon.random.events.*;
import weissmoon.random.item.equipment.QuiverEvents;
import weissmoon.random.item.*;
import weissmoon.random.item.model.ModelItems;
import weissmoon.random.lib.*;
import weissmoon.random.proxy.ClientProxy;
import weissmoon.random.proxy.CommonProxy;
import weissmoon.random.proxy.ServerProxy;
import weissmoon.random.recipe.RecipeTypes;
import weissmoon.random.recipe.WeissSpecialRecipe;
import weissmoon.random.registry.ActivationArrowRegistry;
import weissmoon.random.world.ModWorldGen;

@Mod(Reference.MOD_ID)//, name = Reference.MOD_NAME, version = Reference.VERSION, dependencies = "required-after:weisscore@[0.1.1,);after:botania;after:thaumcraft", guiFactory = Reference.GUI_FACTORY_CLASS)
public class Randommod{
    //@Instance(Reference.MOD_ID)
    public static Randommod instance;
    //@SidedProxy(clientSide = Reference.CLIENT_PROXY, serverSide = Reference.SERVER_PROXY)
    public static CommonProxy proxy;
    public static boolean baublesLoaded;
    public static boolean botaniaLoaded;
    public static boolean thaumcraftLoaded;
    public static boolean enderIOLoaded;
    public static boolean embersLoaded;
    public static boolean aquacultureLoaded;
    public static boolean curiosLoaded;
    public static boolean atumLoaded;

    public Randommod(){
        instance = this;
        baublesLoaded = ModList.get().isLoaded("baubles");
        botaniaLoaded = ModList.get().isLoaded("botania");
        thaumcraftLoaded = ModList.get().isLoaded("thaumcraft");
        enderIOLoaded = ModList.get().isLoaded("enderio");
        embersLoaded = ModList.get().isLoaded("embers");
        aquacultureLoaded = ModList.get().isLoaded("aquaculture");
        curiosLoaded = ModList.get().isLoaded("curios");
        atumLoaded = ModList.get().isLoaded("atum");
        proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setupClient);
        if(curiosLoaded)
            FMLJavaModLoadingContext.get().getModEventBus().addListener(CuriosCompat::enqueueIMCCurio);
        MinecraftForge.EVENT_BUS.register(new ModContainers());
        ModBlocks.init();
        ModItems.init();
        ModelItems.init();
        ModEffects.init();
        ModSoundEvents.init();
        WeissRandomAPI.renderRegistry = new ActivationArrowRegistry();
        MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, ModWorldGen::addWorldgen);
        FMLJavaModLoadingContext.get().getModEventBus().addGenericListener(IRecipeSerializer.class, RecipeTypes::register);
        CriteriaTriggers.register(EnvyTrigger.INSTANCE);
    }

    public void setup (FMLCommonSetupEvent event){
        WeissCore.instance.setDummyItem();
        MinecraftForge.EVENT_BUS.register(this);
        //ConfigurationHandler.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + ReferenceCore.AUTHOR + File.separator + Reference.MOD_ID + ".cfg"));
        MinecraftForge.EVENT_BUS.register(proxy);
        boolean server = FMLEnvironment.dist == Dist.DEDICATED_SERVER;
        if(server){
            MinecraftForge.EVENT_BUS.register(new SonicArrowEvent());
            MinecraftForge.EVENT_BUS.register(new FoodSeedEvent());
        }
        MinecraftForge.EVENT_BUS.register(new QuiverEvents());
        if(server)
            MinecraftForge.EVENT_BUS.register(new ArrowMeleeEvent());
        MinecraftForge.EVENT_BUS.register(new BreakSpawnerEvent());
        MinecraftForge.EVENT_BUS.register(new RepairRecipeEvents());
        MinecraftForge.EVENT_BUS.register(new RegisterSittingOnChestGoal());
        //NetworkRegistry.instances.registerGuiHandler(instance, new GuiHandler());
        ModItems.initRecipies();
        String clname = WeissRandomAPI.renderRegistry.getClass().getName();
        String expect = "weissmoon.random.registry.ActivationArrowRegistry";
        if(!clname.equals(expect)) {
            new IllegalAccessError("The WeissRandomAPI public ActivationArrow registry has been overriden. "
                    + "This will cause crashes, which is why it's labeled "
                    + " \"Do not Override\". Go back to primary school and learn to read. "
                    + "(Expected classname: " + expect + ", Actual classname: " + clname + ")").printStackTrace();
            System.exit(1);
        }
//        ModWorldGen.addWorldgen();
        Entities.init();
        proxy.initRender();
//        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
//        for (ValidRailAmmo e : list){
//            String pname = e.getDictionaryName();
//            List<ItemStack> orestacks = OreDictionary.getOres(pname);
//            outerloop:
//            for (ItemStack workingStack : orestacks){
//                if (workingStack != null && workingStack != ItemStack.EMPTY){
//                    for (ItemStack active : e.getNugetlist()){
//                        if (active == workingStack){
//                            continue outerloop;
//                        }
//                    }
//                    e.addTolist(workingStack);
//                    e.setActive();
//                }
//            }
//        }
    }

    private void setupClient(FMLClientSetupEvent event){
        ClientHandler.setupClient();
    }

    public void complete(FMLLoadCompleteEvent event){
        WeissSpecialRecipe.reloadIngredients();
    }

//    public void preInit (FMLPreInitializationEvent event){
//        baublesLoaded = Loader.isModLoaded("baubles");
//        botaniaLoaded = Loader.isModLoaded("botania");
//        thaumcraftLoaded = Loader.isModLoaded("thaumcraft");
//        enderIOLoaded = Loader.isModLoaded("enderio");
//        embersLoaded = Loader.isModLoaded("embers");
//        aquacultureLoaded = Loader.isModLoaded("aquaculture");
//        ConfigurationHandler.init(new File(event.getModConfigurationDirectory().getAbsolutePath() + File.separator + ReferenceCore.AUTHOR + File.separator + Reference.MOD_ID + ".cfg"));
//        ModBlocks.init();
//        ModItems.init();
//        if(Side.CLIENT == FMLCommonHandler.instance().getSide())
//            MinecraftForge.EVENT_BUS.register(proxy);
//    }

//    @EventHandler
//    public void init (FMLInitializationEvent event) {
//        MinecraftForge.EVENT_BUS.register(new SonicArrowEvent());
//        MinecraftForge.EVENT_BUS.register(new FoodSeedEvent());
//        MinecraftForge.EVENT_BUS.register(new QuiverEvents());
//        MinecraftForge.EVENT_BUS.register(new ArrowMeleeEvent());
//        proxy.initRender();
//        NetworkRegistry.INSTANCE.registerGuiHandler(instance, new GuiHandler());
//        ModItems.initRecipies();
//    }


//    @EventHandler
//    public void postInit (FMLPostInitializationEvent event){
//        String clname = WeissRandomAPI.renderRegistry.getClass().getName();
//        String expect = "weissmoon.random.registry.ActivationArrowRegistry";
//        if(!clname.equals(expect)) {
//            new IllegalAccessError("The WeissRandomAPI public ActivationArrow registry has been overriden. "
//                    + "This will cause crashes, which is why it's labeled "
//                    + " \"Do not Override\". Go back to primary school and learn to read. "
//                    + "(Expected classname: " + expect + ", Actual classname: " + clname + ")").printStackTrace();
//            FMLCommonHandler.instance().exitJava(1, true);
//        }
//        Entities.init();
//    }

//    @EventHandler
//    public void loadedEvent (FMLLoadCompleteEvent event){
//        List<ValidRailAmmo> list = Arrays.asList(ValidRailAmmo.values());
//        for (ValidRailAmmo e : list){
//            String pname = e.getDictionaryName();
//            List<ItemStack> orestacks = OreDictionary.getOres(pname);
//            outerloop:
//            for (ItemStack workingStack : orestacks){
//                if (workingStack != null && workingStack != ItemStack.EMPTY){
//                    for (ItemStack active : e.getNugetlist()){
//                        if (active == workingStack){
//                            continue outerloop;
//                        }
//                    }
//                    e.addTolist(workingStack);
//                    e.setActive();
//                }
//            }
//        }
//    }

    /*TODO
     * Dubstep Guns
    */
}
