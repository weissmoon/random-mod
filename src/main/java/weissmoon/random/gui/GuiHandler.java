package weissmoon.random.gui;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
//import net.minecraftforge.fml.common.network.IGuiHandler;
import weissmoon.random.client.gui.Quiver1Gui;
import weissmoon.random.container.ContainerDrawbrigde;
import weissmoon.random.container.ContainerProjectTable;
import weissmoon.random.block.tile.TileDrawbrigde;
import weissmoon.random.block.tile.TileProjectTable;
import weissmoon.random.client.gui.DrawbrigdeGui;
import weissmoon.random.client.gui.ProjectTableGui;
import weissmoon.random.container.ContainerQuiver1;
import weissmoon.random.item.equipment.ItemQuiverBase;
import weissmoon.random.item.ModItems;

import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 4/10/19.
 */
@Deprecated
public class GuiHandler{

    @Nullable
//    @Override
    public Object getServerGuiElement(int ID, PlayerEntity player, World world, int x, int y, int z) {
        if(ID == 1000){
            if (player.getHeldItemMainhand() != ItemStack.EMPTY && player.getHeldItemMainhand().getItem() == ModItems.quiver1){
                //return new ContainerQuiver1(ID, player.inventory, ((ItemQuiverBase)player.getHeldItemMainhand().getItem()).getQuiverInventory(player.getHeldItemMainhand()));
            }
            if (player.getHeldItemOffhand() != ItemStack.EMPTY && player.getHeldItemOffhand().getItem() == ModItems.quiver1){
                //return new ContainerQuiver1(ID, player.inventory, ((ItemQuiverBase)player.getHeldItemOffhand().getItem()).getQuiverInventory(player.getHeldItemOffhand()));
            }
        }
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity tile = world.getTileEntity(pos);
        if(tile instanceof TileDrawbrigde){
            //return new ContainerDrawbrigde(0, player.inventory, (IInventory)tile);
        }
        if(tile instanceof TileProjectTable){
            //return new ContainerProjectTable(0, player.inventory, (IInventory)tile, player);
        }
        return null;
    }

    @Nullable
//    @Override
    public Object getClientGuiElement(int ID, PlayerEntity player, World world, int x, int y, int z) {
        if(ID == 1000){
            if (player.getHeldItemMainhand() != ItemStack.EMPTY && player.getHeldItemMainhand().getItem() == ModItems.quiver1){
                //return new Quiver1Gui(new ContainerQuiver1(ID, player.inventory, ((ItemQuiverBase)player.getHeldItemMainhand().getItem()).getQuiverInventory(player.getHeldItemMainhand())));
            }
            if (player.getHeldItemOffhand() != ItemStack.EMPTY && player.getHeldItemOffhand().getItem() == ModItems.quiver1){
                //return new Quiver1Gui(new ContainerQuiver1(ID, player.inventory, ((ItemQuiverBase)player.getHeldItemOffhand().getItem()).getQuiverInventory(player.getHeldItemOffhand())));
            }
        }
        BlockPos pos = new BlockPos(x, y, z);
        TileEntity tile = world.getTileEntity(pos);
        if(tile instanceof TileDrawbrigde){
            //return new DrawbrigdeGui(new ContainerDrawbrigde(0, player.inventory, (IInventory)tile));
        }
        if(tile instanceof TileProjectTable){
            //return new ProjectTableGui(new ContainerProjectTable(0, player.inventory, (IInventory)tile, player));
        }
        return null;
    }
}
