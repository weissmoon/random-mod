package weissmoon.random.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.network.IPacket;
import net.minecraft.potion.Effects;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.EffectInstance;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.random.effect.ModEffects;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Reference;

import java.util.List;

/**
 * Created by Weissmoon on 2/13/19.
 */
public class SonicArrowEntity extends AbstractArrowEntity {


    @ObjectHolder(Reference.MOD_ID + ":sonicarrowprojectile")
    public static EntityType<SonicArrowEntity> TYPE;

    public SonicArrowEntity(EntityType<SonicArrowEntity> type, World world) {
        super(TYPE, world);
    }

    public SonicArrowEntity(World worldIn, double x, double y, double z) {
        super(TYPE, x, y, z, worldIn);
    }

    public SonicArrowEntity(World worldIn, LivingEntity shooter) {
        super(TYPE, shooter, worldIn);
        this.setPosition(shooter.getPosX(), shooter.getPosY() + (double)shooter.getEyeHeight() - (double)0.1F, shooter.getPosZ());
    }

    @Override
    protected void registerData(){
        super.registerData();
    }

    @Override
    public void tick(){
        super.tick();

        List<Entity> entityList = this.world.getEntitiesWithinAABBExcludingEntity(this, new AxisAlignedBB(this.getPosX()-15, this.getPosY()-15, this.getPosZ()-15, this.getPosX()+15,this.getPosY()+15, this.getPosZ()+15));
        for(Entity entity : entityList)
            if(entity instanceof LivingEntity){
                LivingEntity entityl = (LivingEntity) entity;
                EffectInstance potioneffect = new EffectInstance(Effects.GLOWING, 10, 0);
                entityl.addPotionEffect(potioneffect);
            }
    }

    @Override
    protected void onImpact(RayTraceResult raytraceResultIn) {
        super.onImpact(raytraceResultIn);

        if (raytraceResultIn instanceof EntityRayTraceResult) {
            Entity entity = ((EntityRayTraceResult) raytraceResultIn).getEntity();
            if (entity instanceof LivingEntity) {
                EffectInstance potioneffect = new EffectInstance(ModEffects.RESONANCE, 600, 1);
                ((LivingEntity) entity).addPotionEffect(potioneffect);
            }
        }
    }

    @Override
    protected ItemStack getArrowStack(){
        return new ItemStack(ModItems.sonicArrow);
    }

    @Override
    public IPacket<?> createSpawnPacket(){
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    public static SonicArrowEntity fromNetwork(FMLPlayMessages.SpawnEntity spawnEntity, World world){
        SonicArrowEntity entity = new SonicArrowEntity(world, spawnEntity.getPosX(), spawnEntity.getPosY(), spawnEntity.getPosZ());
        entity.setVelocity(spawnEntity.getVelX(), spawnEntity.getVelY(), spawnEntity.getVelZ());
        return entity;
    }
}
