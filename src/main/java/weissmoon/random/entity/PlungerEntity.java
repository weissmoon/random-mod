//package weissmoon.random.entity;
//
//import net.minecraft.block.material.Material;
//import net.minecraft.entity.EntityLivingBase;
//import net.minecraft.entity.LivingEntity;
//import net.minecraft.entity.player.EntityPlayer;
//import net.minecraft.item.ItemStack;
//import net.minecraft.util.math.RayTraceResult;
//import net.minecraft.world.World;
//import weissmoon.core.entity.EntityBulletCore;
//import weissmoon.core.helper.InventoryHelper;
//import weissmoon.random.item.ModItems;
//import weissmoon.random.lib.Strings;
//
//import java.util.ArrayList;
//
///**
// * Created by Weissmoon on 5/1/17.
// */
//public class PlungerEntity extends EntityBulletCore {
//
//    public PlungerEntity(World p_i1582_1_) {
//        super(p_i1582_1_);
//    }
//
//    public PlungerEntity(World world, LivingEntity entity) {
//        super(world, entity, false);
//        this.gravity = true;
//        this.gravityFloat = 0.08F;
//        this.drag = true;
//        this.motionFactor = 0.95F;
//    }
//
//    @Override
//    public float getSpeed(EntityBulletCore entity){
//        return 5;
//    }
//
//    @Override
//    protected void setupPhaseList(){
//        this.phaseList = new ArrayList<Material>();
//    }
//
//    @Override
//    protected void michaelisColision(RayTraceResult p_70184_1_) {
//        ItemStack plunger = ModItems.dummy.newDummyItemStack(Strings.Items.PLUNGER_NAME, 1);
//        InventoryHelper.givePlayerOrDropItemStack(plunger, ((EntityPlayer)p_70184_1_.entityHit));
//        this.setDead();
//    }
//}
