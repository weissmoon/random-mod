package weissmoon.random.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import weissmoon.random.Randommod;
import weissmoon.random.lib.Reference;

@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class Entities{
    public static void init (){
//        registerModEntity(EntityRailBullet.class, "railBullet", 1, Randommod.instance, 80, 3, true);
        //registerModEntity(PlungerEntity.class, "plungerProjectile", 2, Randommod.instance, 80, 3, true);
        registerModEntity(SonicArrowEntity.class, "sonicArrowProjectile", 3, Randommod.instance, 80, 3, true);
        registerModEntity(ActivationArrowEntity.class, "activationArrowProjectile", 4, Randommod.instance, 80, 3, true);
    }

    private static void registerModEntity(Class<? extends Entity>  entity, String name, int id, Object mod, int trackingRange, int updateFrequency, boolean sendsVelocityUpdates){
        String domain = Reference.MOD_ID;
        //GameRegistry.findRegistry(EntityType.class).register(new ResourceLocation(domain, name), entity, name, id, mod, trackingRange, updateFrequency, sendsVelocityUpdates);
    }

    @SubscribeEvent
    public static void registerEntities(RegistryEvent.Register<EntityType<?>> event){
//        event.getRegistry().register(EntityType.Builder.<EntityRailBullet>create(
//                EntityRailBullet::new, EntityClassification.MISC)
//                .size(0.25F, 0.25F)
//                .setUpdateInterval(10)
//                .setTrackingRange(64)
//                .setShouldReceiveVelocityUpdates(true)
//                .setCustomClientFactory(EntityRailBullet::fromNetwork)
//                .build("")
//                .setRegistryName(new ResourceLocation(Reference.MOD_ID, "railbullet")));
        event.getRegistry().register(EntityType.Builder.<SonicArrowEntity>create(
                SonicArrowEntity::new, EntityClassification.MISC)
                .size(0.25F, 0.25F)
                .setUpdateInterval(10)
                .setTrackingRange(64)
                .setShouldReceiveVelocityUpdates(true)
                .setCustomClientFactory(SonicArrowEntity::fromNetwork)
                .build("")
                .setRegistryName(new ResourceLocation(Reference.MOD_ID, "sonicarrowprojectile")));
        event.getRegistry().register(EntityType.Builder.<ActivationArrowEntity>create(
                ActivationArrowEntity::new, EntityClassification.MISC)
                .size(0.25F, 0.25F)
                .setUpdateInterval(10)
                .setTrackingRange(64)
                .setShouldReceiveVelocityUpdates(true)
                .setCustomClientFactory(ActivationArrowEntity::fromNetwork)
                .build("")
                .setRegistryName(new ResourceLocation(Reference.MOD_ID, "activationarrowprojectile")));
    }
}
