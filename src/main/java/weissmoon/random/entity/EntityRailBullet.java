//package weissmoon.random.entity;
//
//import io.netty.buffer.ByteBuf;
//import net.minecraft.entity.EntityClassification;
//import net.minecraft.entity.IRendersAsItem;
//import net.minecraft.entity.LivingEntity;
//import net.minecraft.entity.EntityType;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.item.ItemStack;
//import net.minecraft.nbt.CompoundNBT;
//import net.minecraft.network.IPacket;
//import net.minecraft.network.PacketBuffer;
//import net.minecraft.potion.*;
//import net.minecraft.util.*;
//import net.minecraft.util.math.EntityRayTraceResult;
//import net.minecraft.util.math.RayTraceResult;
//import net.minecraft.world.World;
//import net.minecraftforge.fml.common.network.ByteBufUtils;
//import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
//import net.minecraftforge.fml.network.FMLPlayMessages;
//import net.minecraftforge.fml.network.NetworkHooks;
//import net.minecraftforge.registries.ObjectHolder;
//import weissmoon.core.entity.EntityLinearProjectile;
//import weissmoon.core.helper.InventoryHelper;
//import weissmoon.random.lib.Reference;
//import weissmoon.random.lib.ValidRailAmmo;
//
//public class EntityRailBullet extends EntityLinearProjectile implements IEntityAdditionalSpawnData, IRendersAsItem {
//
//    public ValidRailAmmo ammo;
//    private byte ammoByte;
//    public ItemStack bulletStack;
//    private String BULLET_TAG = "bulletItem";
//    @ObjectHolder(Reference.MOD_ID + ":railbullet")
//    public static EntityType<EntityRailBullet> TYPE;
//
//    public EntityRailBullet(EntityType<EntityRailBullet> type, World world) {
//        super(TYPE, world);
//    }
//
//    public EntityRailBullet (World world){
//        super(EntityType.Builder.create(EntityClassification.MISC).build(""),world);
//        //setSize(.60F, .60F);
//        this.michaelis = true;
//        this.phase = true;
//    }
//
//    public EntityRailBullet(World worldIn, double x, double y, double z) {
//        super(TYPE, worldIn);
//        setPosition(x, y, z);
//    }
//
//
//    public EntityRailBullet (World world, LivingEntity player, ValidRailAmmo validRailAmmo, ItemStack bulletStack){
//        super(EntityType.Builder.create(EntityClassification.MISC).build(""), world, player, false);
//        //setSize(.60F, .60F);
//        if ((ValidRailAmmo.UNKNOWN == validRailAmmo) || (!validRailAmmo.isActive())){
//            remove();
//        }
//        this.michaelis = true;
//        this.phase = true;
//        this.posY -= 0.2D;
//        this.ammo = validRailAmmo;
//        this.ammoByte = ((byte)validRailAmmo.ordinal());
//        this.bulletStack = bulletStack;
//        //setHeading(this.motionX, this.motionY, this.motionZ, getSpeed(this), getBulletAccuracy());
//    }
//
//    //    @Override
//    public float getBulletAccuracy (){
//        if (this.ammo == null){
//            return 0;
//        }
//        return 0.005F * this.ammo.getAccuracy();
//    }
//
//    @Override
//    public float getMotionFactor (){
//        return this.ammo.getSpeed() * 1.2F;
//    }
//
//    @Override
//    public void onImpact (RayTraceResult movingObjectPosition){
//        if (!this.world.isRemote){
//            if (movingObjectPosition instanceof EntityRayTraceResult){
//                if ((((EntityRayTraceResult) movingObjectPosition).getEntity() instanceof LivingEntity)){
//                    if (this.ice)
//                        ((LivingEntity)((EntityRayTraceResult) movingObjectPosition).getEntity()).addPotionEffect(new EffectInstance(Effects.SLOWNESS, 10, 0, false, false));
//                    ((EntityRayTraceResult) movingObjectPosition).getEntity().attackEntityFrom(DamageSource.causeMobDamage(this.thrower), this.ammo.getBias());
//                }
//            }
//        }
//        remove();
//    }
//
//    @Override
//    public void remove(){
//        this.remove(false);
//    }
//
//    @Override
//    public void writeSpawnData (PacketBuffer buffer){
//        //super.writeSpawnData(buffer);
//        buffer.writeByte(this.ammoByte);
//        buffer.writeItemStack(this.bulletStack);
//    }
//
//    @Override
//    public void readSpawnData (PacketBuffer additionalData){
//        //super.readSpawnData(additionalData);
//        this.ammoByte = additionalData.readByte();
//        this.ammo = ValidRailAmmo.getFromOrdinal(this.ammoByte);
//        this.bulletStack = additionalData.readItemStack();
//    }
//
//    @Override
//    public void writeAdditional(CompoundNBT compound){
//        super.writeAdditional(compound);
//        compound.putByte("bullet", this.ammoByte);
//        CompoundNBT cmpnd = new CompoundNBT();
//        compound.put(BULLET_TAG, this.bulletStack.serializeNBT());
//    }
//
//    @Override
//    public void readAdditional (CompoundNBT compound){
//        super.readAdditional(compound);
//        this.ammoByte = compound.getByte("bullet");
//        if (!ValidRailAmmo.getFromOrdinal(this.ammoByte).isActive()){
//            remove();
//        }else{
//            this.ammo = ValidRailAmmo.getFromOrdinal(this.ammoByte);
//        }
//        if (((this.bulletStack = ItemStack.read((CompoundNBT) compound.getCompound(BULLET_TAG)))).isEmpty()){
//            this.remove();
//        }
//    }
//
//    @Override
//    protected boolean michaelisColision(EntityRayTraceResult p_70184_1_) {
//        InventoryHelper.givePlayerOrDropItemStack(this.bulletStack, (PlayerEntity) p_70184_1_.getEntity());
//        remove();
//        return true;
//    }
//
//    @Override
//    public ItemStack getItem() {
//        return this.bulletStack;
//    }
//
//    @Override
//    public IPacket<?> createSpawnPacket() {
//        return NetworkHooks.getEntitySpawningPacket(this);
//    }
//
//    public static EntityRailBullet fromNetwork(FMLPlayMessages.SpawnEntity spawnEntity, World world) {
//        EntityRailBullet entity = new EntityRailBullet(world, spawnEntity.getPosX(), spawnEntity.getPosY(), spawnEntity.getPosZ());
//        entity.setVelocity(spawnEntity.getVelX(), spawnEntity.getVelY(), spawnEntity.getVelZ());
//        return entity;
//    }
//}
