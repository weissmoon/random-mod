package weissmoon.random.entity;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.projectile.AbstractArrowEntity;
import net.minecraft.item.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.math.*;
import net.minecraft.world.World;
import net.minecraftforge.fml.network.FMLPlayMessages;
import net.minecraftforge.fml.network.NetworkHooks;
import net.minecraftforge.registries.ObjectHolder;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Reference;
import weissmoon.random.registry.ActivationArrowRegistry;

/**
 * Created by Weissmoon on 7/10/19.
 */
public class ActivationArrowEntity extends AbstractArrowEntity {

    private static final DataParameter<Boolean> DUST = EntityDataManager.createKey(AbstractArrowEntity.class, DataSerializers.BOOLEAN);
    @ObjectHolder(Reference.MOD_ID + ":activationarrowprojectile")
    public static EntityType<ActivationArrowEntity> TYPE;

    public ActivationArrowEntity(EntityType<ActivationArrowEntity> type, World worldIn) {
        super(TYPE, worldIn);
    }

    public ActivationArrowEntity(World worldIn, double x, double y, double z) {
        super(TYPE, x, y, z, worldIn);
    }

    public ActivationArrowEntity(World worldIn, LivingEntity shooter) {
        super(TYPE, shooter, worldIn);
        this.setPosition(shooter.getPosX(), shooter.getPosY() + (double)shooter.getEyeHeight() - (double)0.1F, shooter.getPosZ());
    }

    @Override
    protected void registerData(){
        super.registerData();
        this.dataManager.register(DUST, true);
    }

    @Override
    protected void onImpact(RayTraceResult raytraceResultIn){
        super.onImpact(raytraceResultIn);
        if(raytraceResultIn instanceof EntityRayTraceResult){
            Entity entity = ((EntityRayTraceResult) raytraceResultIn).getEntity();
            if(this.isDust())
                if(ActivationArrowRegistry.ENTITY_BEHAVIOR_REGISTRY.getOrDefault(entity.getClass()).onHit(this, world, (EntityRayTraceResult)raytraceResultIn))
                    removeDust();
        }else{
            BlockState iblockstate = this.world.getBlockState(((BlockRayTraceResult)raytraceResultIn).getPos());
            if(this.isDust())
                if (ActivationArrowRegistry.BLOCK_BEHAVIOR_REGISTRY.getOrDefault(iblockstate.getBlock()).onHit(this, world, (BlockRayTraceResult)raytraceResultIn))
                    removeDust();
        }
    }

    @Override
    public void writeAdditional(CompoundNBT compound){
        super.writeAdditional(compound);
        compound.putBoolean("dust", this.dataManager.get(DUST));
    }

    @Override
    public void readAdditional(CompoundNBT compound){
        super.readAdditional(compound);
        this.dataManager.set(DUST, compound.getBoolean("dust"));
    }

    private void removeDust(){
        this.dataManager.set(DUST, false);
    }

    public boolean isDust(){
        return this.dataManager.get(DUST);
    }

    @Override
    protected ItemStack getArrowStack(){
        if(isDust())
            return new ItemStack(ModItems.activationArrow);
        return new ItemStack(Items.ARROW);
    }

    @Override
    public IPacket<?> createSpawnPacket(){
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    public static ActivationArrowEntity fromNetwork(FMLPlayMessages.SpawnEntity spawnEntity, World world){
        ActivationArrowEntity entity = new ActivationArrowEntity(world, spawnEntity.getPosX(), spawnEntity.getPosY(), spawnEntity.getPosZ());
        entity.setVelocity(spawnEntity.getVelX(), spawnEntity.getVelY(), spawnEntity.getVelZ());
        return entity;
    }
}
