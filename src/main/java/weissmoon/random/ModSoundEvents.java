package weissmoon.random;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import weissmoon.random.lib.Reference;

/**
 * Created by Weissmoon on 4/22/22.
 */
@Mod.EventBusSubscriber(modid = Reference.MOD_ID, bus=Mod.EventBusSubscriber.Bus.MOD)
public class ModSoundEvents{

    public static WeissSoundEvent chestEat = new WeissSoundEvent(new ResourceLocation("weissrandom:hungrychesteat"));
    public static WeissSoundEvent chestFull = new WeissSoundEvent(new ResourceLocation("weissrandom:hungrychestfull"));

    public static void init (){
        MinecraftForge.EVENT_BUS.register(ModSoundEvents.class);
    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<SoundEvent> evt){
        IForgeRegistry<SoundEvent> GameRegistry = evt.getRegistry();


        GameRegistry.register(chestEat);
        GameRegistry.register(chestFull);
    }

    private static class WeissSoundEvent extends SoundEvent{

        public WeissSoundEvent(ResourceLocation name){
            super(name);
            setRegistryName(name);
        }
    }
}
