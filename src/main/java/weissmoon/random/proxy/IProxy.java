package weissmoon.random.proxy;

import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;

public abstract interface IProxy{
    public abstract void initArmRender ();

    public abstract void initSound ();

    public abstract void initRender ();

    public abstract void registerTileEntities ();

    public abstract void registerKeyBind ();

    public abstract TileEntity getLanternTile(IBlockReader world);
}
