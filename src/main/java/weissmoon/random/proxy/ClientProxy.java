package weissmoon.random.proxy;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.world.IBlockReader;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import weissmoon.core.client.render.renderOverride.CustomRenderRegistry;
import weissmoon.random.Randommod;
import weissmoon.random.block.ModBlocks;
import weissmoon.random.block.override.tile.TileJukebox;
import weissmoon.random.block.tile.*;
import weissmoon.random.block.tile.woodenPlate.TileWoodenPressurePlateSilent;
import weissmoon.random.client.hud.IHUDItems;
import weissmoon.random.client.render.BlockColorHandler;
import weissmoon.random.client.render.entity.*;
import weissmoon.random.client.render.item.RenderItemRailgun;
import weissmoon.random.client.render.tile.*;
import weissmoon.random.compat.aquaculture.AquacultureCompatClient;
import weissmoon.random.compat.atum.AtumCompat;
import weissmoon.random.entity.ActivationArrowEntity;
//import weissmoon.random.entity.EntityRailBullet;
import weissmoon.random.entity.SonicArrowEntity;
import weissmoon.random.item.ModItems;
import weissmoon.random.lib.Reference;

@Mod.EventBusSubscriber(value = Dist.CLIENT, modid = Reference.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class ClientProxy extends CommonProxy{
    public void initArmRender (){
    }

    public void registerTileEntities (){
//        GameRegistry.registerTileEntity(TileBrewingStand.class, "brewing_stand");
    }

    public void initSound (){
    }

    public void initRender (){
        IHUDItems.register();
        if(Randommod.aquacultureLoaded)
            AquacultureCompatClient.registerIHUDItems();
        if(Randommod.atumLoaded)
            AtumCompat.registerIHUDItems();
        RenderTypeLookup.setRenderLayer(ModBlocks.igniterBlock, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.projectTable, RenderType.getTranslucent());
        RenderTypeLookup.setRenderLayer(ModBlocks.drawbridge, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.ironGravelOre, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.goldSandOre, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.cornBlock, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.cornBlockTop, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.stemAir, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.stemEarth, RenderType.getCutout());
        RenderTypeLookup.setRenderLayer(ModBlocks.stemFire, RenderType.getCutout());
    }

    @SubscribeEvent
    public static void modelRegistryEvent(ModelRegistryEvent event){
        //MinecraftForgeClient.registerItemRenderer(ModItems.railgun, new RenderItemRailgun());
        CustomRenderRegistry.registerItemRenderer(ModItems.railgun, new RenderItemRailgun());
//        MinecraftForge.EVENT_BUS.register(new SetItemUsage());
        //MinecraftForge.EVENT_BUS.register(new RadialEventHandler());

        //RenderingRegistry.registerEntityRenderingHandler(EntityRailBullet.class, manager -> new RailBulletRender(Minecraft.getInstance().getRenderManager()));
//        RenderingRegistry.registerEntityRenderingHandler(EntityRailBullet.TYPE, manager -> new SpriteRenderer<>(manager, Minecraft.getInstance().getItemRenderer()));
        RenderingRegistry.registerEntityRenderingHandler(SonicArrowEntity.TYPE, manager -> new SonicArrowRender(Minecraft.getInstance().getRenderManager()));
//        RenderingRegistry.registerEntityRenderingHandler(ActivationArrowEntity.class, manager -> new ActivationArrowRender(Minecraft.getInstance().getRenderManager()));
        RenderingRegistry.registerEntityRenderingHandler(ActivationArrowEntity.TYPE, ActivationArrowRender::new);
        ClientRegistry.bindTileEntityRenderer(TileDrawbrigde.TYPE, DrawbridgeTESR::new);
//        ClientRegistry.bindTileEntityRenderer(TileLantern.TYPE, LanternTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileEntityType.BREWING_STAND, BrewingStandTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileProjectTable.TYPE, ProjectTableTESR::new);
        //ClientRegistry.bindTileEntityRenderer(TileRedstoneEngine.class, RedstoneEngineTESR());
        ClientRegistry.bindTileEntityRenderer(TileJukebox.TYPE, JukeboxTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileBarrel.TYPE, CraftingBarrelTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileTinyChest.TYPE, TinyChestTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileWoodenPressurePlateSilent.TYPE, WoodenPressurePlateTESR::new);
        ClientRegistry.bindTileEntityRenderer(TileHungryChest.TYPE, HungryChestTESR::new);
    }

    public void registerKeyBind (){
    }

    @Override
    public TileEntity getLanternTile(IBlockReader world) {
        return new TileLantern(world);
    }

    @SubscribeEvent
    public static void onBlockColours(ColorHandlerEvent.Block event){
        event.getBlockColors().register(new BlockColorHandler.FireMelon(), ModBlocks.stemFire);
        event.getBlockColors().register(new BlockColorHandler.EarthMelon(), ModBlocks.stemEarth);
        event.getBlockColors().register(new BlockColorHandler.AirMelon(), ModBlocks.stemAir);
    }
}
