package weissmoon.random.config;

import net.minecraftforge.common.ForgeConfigSpec;
//import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;

public class ConfigurationHandler{
    public static final String NEW_LINE = System.getProperty("line.separator");

    public static final Common COMMON;
    public static final ForgeConfigSpec COMMON_SPEC;
    static {
        final Pair<Common, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(Common::new);
        COMMON_SPEC = specPair.getRight();
        COMMON = specPair.getLeft();
    }
//    public static Configuration configuration;
//    public static final String CATEGORY_AUDIO = "audio";
//    public static final String CATEGORY_ITEM = "item";
    public static boolean lore;
    public static boolean michaelis;

    public static class Common {
        public static ForgeConfigSpec.Builder configuration;
//    public static void init (File configFile){
//        if (configuration == null){
//            configuration = new Configuration(configFile);
//            loadCofiguration();
//        }
//    }

        public Common(ForgeConfigSpec.Builder builder) {
            configuration = builder;
        }
        private static void loadCofiguration() {
            lore = configuration.comment("Display lore in item tooltip.").define("lore", false).get();
            michaelis = configuration.comment(
                    "Certain bullets appear in a shot player inventory if PVP is disabled" +
                            NEW_LINE +
                            "Otherwise bullets phase through").define("michaelis", false).get();

//            if (configuration.hasChanged()) {
//                configuration.save();
//            }
        }

//        @SubscribeEvent
//        public void onConfigurationChangedEvent(ConfigChangedEvent.OnConfigChangedEvent event) {
//            if (event.getModID().equalsIgnoreCase("randani")) {
//                loadCofiguration();
//            }
//        }
    }
}
