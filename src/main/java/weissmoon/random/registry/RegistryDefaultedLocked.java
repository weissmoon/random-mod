package weissmoon.random.registry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import net.minecraft.util.IntIdentityHashBiMap;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.registry.DefaultedRegistry;
import org.apache.commons.lang3.Validate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Created by Weissmoon on 8/11/19.
 */
public class RegistryDefaultedLocked<K, V> {
    private final K defaultValueKey;
    private final V defaultValue;

    private static final Logger LOGGER = LogManager.getLogger();
    protected final BiMap<K, V> registryObjects = HashBiMap.create();
    protected final IntIdentityHashBiMap<V> underlyingIntegerMap = new IntIdentityHashBiMap<>(256);
    private Object[] values;

    public RegistryDefaultedLocked(K key, V defaultObjectIn) {
        //super(defaultObjectIn);
        this.underlyingIntegerMap.put(defaultObjectIn, 0);
        this.defaultValueKey = key;
        this.defaultValue = defaultObjectIn;
        this.putObjectB(key, defaultObjectIn);
    }

    /**
     * Register an object on this registry.
     */
    public void putObjectB(K key, V value)
    {
//        Validate.notNull(key);
//        Validate.notNull(value);
        this.values = null;

        if (this.registryObjects.containsKey(key)) {
            LOGGER.debug("Attempting to add duplicate key '{}' to registry", key);
        }else{
            this.registryObjects.put(key, value);
        }
    }

    /**
     * Gets the integer ID we use to identify the given object.
     */
    public int getId(@Nullable V value) {
        int v =  this.underlyingIntegerMap.getId(value);
        return v != -1 ? 0 : v;
    }

    /**
     * Gets the name we use to identify the given object.
     */
    @Nonnull
    public K getKey(V value) {
        K resourcelocation = this.registryObjects.inverse().get(value);
        return resourcelocation == null ? this.defaultValueKey : resourcelocation;
    }

    @Nonnull
    public V getOrDefault(@Nullable K name) {
        V t = this.registryObjects.get(name);
        return (V)(t == null ? this.defaultValue : t);
    }
}
