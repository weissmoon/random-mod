package weissmoon.random.registry;

import net.minecraft.block.*;
//import net.minecraft.block.state.PistonBlockStructureHelper;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.LightningBoltEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.item.minecart.TNTMinecartEntity;
import net.minecraft.entity.item.TNTEntity;
import net.minecraft.entity.monster.CreeperEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.tileentity.PistonTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.EntityRayTraceResult;
import net.minecraft.world.server.ServerWorld;
import org.apache.logging.log4j.LogManager;
import weissmoon.core.helper.RNGHelper;
import weissmoon.random.api.activationarrow.IActivationArrowRegistry;
import weissmoon.random.api.activationarrow.IHitWithArrowBehaviour;

import java.util.UUID;

import static net.minecraft.block.DispenserBlock.TRIGGERED;
import static net.minecraft.block.PistonBlock.EXTENDED;
import static net.minecraft.block.TNTBlock.UNSTABLE;

/**
 * Created by Weissmoon on 8/11/19.
 */
public class ActivationArrowRegistry implements IActivationArrowRegistry {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger();

    private static IHitWithArrowBehaviour<BlockRayTraceResult> blockDefault = (IHitWithArrowBehaviour.IBlockHit) (arrow, world, rayTraceResult) -> false;

    private static IHitWithArrowBehaviour<EntityRayTraceResult> entityDefault = (IHitWithArrowBehaviour.IEntityHit) (arrow, world, rayTraceResult) -> false;

    public static final RegistryDefaultedLocked<Block, IHitWithArrowBehaviour<BlockRayTraceResult>> BLOCK_BEHAVIOR_REGISTRY = new RegistryDefaultedLocked<>(Blocks.BEDROCK, blockDefault);
    public static final RegistryDefaultedLocked<Object, IHitWithArrowBehaviour<EntityRayTraceResult>> ENTITY_BEHAVIOR_REGISTRY = new RegistryDefaultedLocked<>(null, entityDefault);

    @Override
    public final void registerBlockBehaviour(Block block, IHitWithArrowBehaviour.IBlockHit behaviour) {
        BLOCK_BEHAVIOR_REGISTRY.putObjectB(block, behaviour);
    }

    @Override
    public final void registerEntityBehaviour(Class entity, IHitWithArrowBehaviour.IEntityHit behaviour) {
        ENTITY_BEHAVIOR_REGISTRY.putObjectB(entity, behaviour);
    }

    public final void registerVanilla(){
        registerBlockBehaviour(Blocks.TNT, (arrow, world, raytraceResult) -> {
            BlockPos blockpos = raytraceResult.getPos();
            BlockState iblockstate = world.getBlockState(blockpos);
            Block inTile = iblockstate.getBlock();
            if (!world.isRemote) {
                CompoundNBT tag = new CompoundNBT();
                arrow.writeAdditional(tag);
                UUID playerUUID = tag.getUniqueId("Owner");

                Entity shooter = ((ServerWorld)world).getEntityByUuid(playerUUID);
                LivingEntity entityIn = null;
                if (shooter instanceof LivingEntity)
                    entityIn = (LivingEntity) shooter;
                TNTEntity tntentity = new TNTEntity(world, ((float)blockpos.getX() + 0.5F), blockpos.getY(), ((float)blockpos.getZ() + 0.5F), entityIn);
                world.addEntity(tntentity);
                world.playSound(null, tntentity.getPosX(), tntentity.getPosY(), tntentity.getPosZ(), SoundEvents.ENTITY_TNT_PRIMED, SoundCategory.BLOCKS, 1.0F, 1.0F);
            }
            world.removeBlock(blockpos, false);
            return true;
        });
        registerBlockBehaviour(Blocks.DISPENSER, (arrow, world, raytraceResult) -> {
            if (!world.isRemote) {
                BlockPos blockpos = raytraceResult.getPos();
                BlockState iblockstate = world.getBlockState(blockpos);
                if(!iblockstate.get(TRIGGERED)) {
                    world.setBlockState(blockpos, iblockstate.with(TRIGGERED, true), 4);
                    world.getPendingBlockTicks().scheduleTick(blockpos, Blocks.DISPENSER, 4);
                    world.setBlockState(blockpos, iblockstate.with(TRIGGERED, false), 4);
                    world.getPendingBlockTicks().scheduleTick(blockpos, Blocks.DISPENSER, 8);
                    return true;
                }
            }
            return false;
        });
        registerBlockBehaviour(Blocks.NOTE_BLOCK, (arrow, world, raytraceResult) -> {
            if (!world.isRemote) {
                BlockPos blockpos = raytraceResult.getPos();
                TileEntity tileentity = world.getTileEntity(blockpos);

                if (world.isAirBlock(blockpos.up())) {
                    world.addBlockEvent(blockpos, Blocks.NOTE_BLOCK, 0, 0);
                }
            }
            return true;
        });
        registerBlockBehaviour(Blocks.PISTON, (arrow, world, raytraceResult) -> {
            BlockPos blockpos = raytraceResult.getPos();
            BlockState iblockstate = world.getBlockState(blockpos);
            Block inTile = iblockstate.getBlock();
            Direction enumfacing = iblockstate.get(DirectionalBlock.FACING);

            if (!iblockstate.get(EXTENDED)){

                if ((new PistonBlockStructureHelper(world, blockpos, enumfacing, true)).canMove())
                {
                    if(net.minecraftforge.event.ForgeEventFactory.onPistonMovePre(world, blockpos, enumfacing, true)) return false;
                    ((PistonBlock)Blocks.PISTON).doMove(world, blockpos, enumfacing, true);//return false;

                    world.setBlockState(blockpos, iblockstate.with(EXTENDED, true), 67);
                    world.playSound(null, blockpos, SoundEvents.BLOCK_PISTON_EXTEND, SoundCategory.BLOCKS, 0.5F, world.rand.nextFloat() * 0.25F + 0.6F);
                    net.minecraftforge.event.ForgeEventFactory.onPistonMovePost(world, blockpos, enumfacing, true);
                    return true;
                }
                return false;
            }
            return false;
        });
        registerBlockBehaviour(Blocks.STICKY_PISTON, (arrow, world, raytraceResult) -> {
            BlockPos blockpos = raytraceResult.getPos();
            BlockState iblockstate = world.getBlockState(blockpos);
            Block inTile = iblockstate.getBlock();
            Direction enumfacing = iblockstate.get(DirectionalBlock.FACING);

            if (!iblockstate.get(EXTENDED)){

                if ((new PistonBlockStructureHelper(world, blockpos, enumfacing, true)).canMove())
                {
                    if(net.minecraftforge.event.ForgeEventFactory.onPistonMovePre(world, blockpos, enumfacing, true)) return false;
                    ((PistonBlock)Blocks.STICKY_PISTON).doMove(world, blockpos, enumfacing, true);//return false;

                    world.setBlockState(blockpos, iblockstate.with(EXTENDED, true), 67);
                    world.playSound(null, blockpos, SoundEvents.BLOCK_PISTON_EXTEND, SoundCategory.BLOCKS, 0.5F, world.rand.nextFloat() * 0.25F + 0.6F);
                    net.minecraftforge.event.ForgeEventFactory.onPistonMovePost(world, blockpos, enumfacing, true);
                    return true;
                }
                return false;
            }
            return false;
        });
        registerBlockBehaviour(Blocks.DROPPER, (arrow, world, raytraceResult) -> {
            if (!world.isRemote) {
                BlockPos blockpos = raytraceResult.getPos();
                BlockState iblockstate = world.getBlockState(blockpos);
                if(!iblockstate.get(TRIGGERED)) {
                    world.setBlockState(blockpos, iblockstate.with(TRIGGERED, true), 4);
                    world.getPendingBlockTicks().scheduleTick(blockpos, Blocks.DROPPER, 4);
                    world.setBlockState(blockpos, iblockstate.with(TRIGGERED, false), 4);
                    world.getPendingBlockTicks().scheduleTick(blockpos, Blocks.DROPPER, 8);
                    return true;
                }
            }
            return false;
        });


//        registerEntityBehaviour(EntityMinecartEmpty.class, (arrow, world, raytraceResult) -> {
//            if(raytraceResult.entityHit.isBeingRidden()){
//                if (arrow.shootingEntity instanceof EntityLivingBase){
//                    EntityLivingBase entityLiving = (EntityLivingBase) arrow.shootingEntity;
//                    PotionEffect slowness = new PotionEffect(MobEffects.SLOWNESS, 60, 1);
//
//                    PotionEffect current = entityLiving.getActivePotionEffect(slowness.getPotion());
//                    if (current != null){
//                        int amp = current.getAmplifier();
//                        if (amp <= slowness.getAmplifier()) {
//                            int total = current.getDuration() + slowness.getDuration();
//                            boolean particles = current.doesShowParticles() || slowness.doesShowParticles();
//                            entityLiving.addPotionEffect(new PotionEffect(slowness.getPotion(), total, amp, false, particles));
//                        }
//                    }else {
//                        entityLiving.addPotionEffect(slowness);
//                    }
//
//                }
//                raytraceResult.entityHit.removePassengers();
//                return true;
//            }
//            return false;
//        });
//        registerEntityBehaviour(EntityMinecartTNT.class, (arrow, world, raytraceResult) -> {
//            if (((EntityMinecartTNT)raytraceResult.entityHit).isIgnited()){
//                ((EntityMinecartTNT)raytraceResult.entityHit).ignite();
//                return true;
//            }
//            return false;
//        });
        registerEntityBehaviour(CreeperEntity.class, (arrow, world, raytraceResult) -> {
            if(!world.isRemote) {
                float rng = RNGHelper.getFloat();
                if (0.15 > rng) {
                    CreeperEntity creeper = (CreeperEntity) raytraceResult.getEntity();
                    LightningBoltEntity lightning = EntityType.LIGHTNING_BOLT.create(world);
                    lightning.moveForced(creeper.getPosX(), creeper.getPosY(), creeper.getPosZ());
                    lightning.setEffectOnly(true);
                    world.addEntity(lightning);
                    if (0.05 > rng) {
                        boolean burning = creeper.isBurning();
                        creeper.func_241841_a((ServerWorld)world, lightning);
                        if(burning)
                            creeper.extinguish();
                    }
                }
            }
            return true;
        });
    }
}
